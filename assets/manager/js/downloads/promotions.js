$(document).ready(function () {
  $("#btnUploadPromotion").on("click", function (e) {
    e.preventDefault();
    $("#imagen").click();
  });

  $("#imagen").on("change", function () {
    uploadImagePromotion($(this));
  });

  $(document).on("click", "#btnDeletePromotion", function (e) {
    e.preventDefault();
    deleteImage(e.target.dataset.id);
  });
});

async function deleteImage(id) {
  var post = baseUrl + "manager/Promotions/deleteImage";
  var data = new URLSearchParams({ id: id });
  const response = await fetch(post, { method: "POST", body: data });
  const result = await response.json();

  if (result.status === 200) {
    alert(result.message);
    //refresh page
    location.reload();
  } else {
    alert(result.message);
  }
}

async function uploadImagePromotion(e) {
  var post = baseUrl + "manager/Promotions/uploadImage";
  var file = $("#imagen")[0];

  var data = new FormData();
  data.append("file", file.files[0]);

  const response = await fetch(post, {
    method: "POST",
    body: data,
  });

  const result = await response.json();

  if (result.status === 200) {
    fill_images(result.upload.publicfullpath);
    successToast(result.title, result.message);
  } else {
    dangerToast(result.title, result.message);
  }
}

function fill_images(path) {
  var thumbs = `<li class="d-flex flex-column align-items-center justify-content-center">
                        <img 
                            src="${path}" 
                            style="width: 300px;">
                        <button class="btn btn-danger rounded-pill mt-4">Eliminar</button>
                    </li>`;
  $("#thumbs").append(thumbs);
}
