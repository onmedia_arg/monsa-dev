// Setup module
// ------------------------------
var InputsBasic = (function () {
  // Uniform
  var _componentUniform = function () {
    if (!$().uniform) {
      console.warn("Warning - uniform.min.js is not loaded.");
      return;
    }
    // Custom select
    $(".input-xls-file").uniform({
      fileButtonClass: "action btn bg-blue",
      fileButtonHtml: "Buscar",
      // selectClass: 'uniform-select bg-pink-400 border-pink-400'
    });
  };

  return {
    init: function () {
      _componentUniform();
    },
  };
})();

var SetSelect2 = (function () {
  var _setSelect2 = function () {
    $("#type").select2({
      minimumResultsForSearch: -1,
      placeholder: "Seleccione el tipo de Archivo",
    });
  };

  return {
    init: function () {
      _setSelect2();
    },
  };
})();

var ImportFunction = (function () {
  var _formNewFile = function () {
    $("#btn-new-file").click(function () {
      $("#btn-new-file").attr("disabled", true);
      uploadNewFile();
    });
  };

  return {
    init: function () {
      _formNewFile();
    },
  };
})();

var MsjFunctions = (function () {
  var _msjDownload = function () {
    $("#btn_save_msj").click(function () {
      $("#btn-new-file").attr("disabled", true);
      save_msj();
    });
  };

  return {
    init: function () {
      _msjDownload();
    },
  };
})();

// Initialize module
// ------------------------------

document.addEventListener("DOMContentLoaded", function () {
  $("#msj-download").summernote();
  InputsBasic.init();
  SetSelect2.init();
  getFiles();
  ImportFunction.init();
  MsjFunctions.init();

  $("#dwnl-btn-up-xls").on("click", function () {
    $("#file-pdf").val("");
    $("#file-xls").click();
  });
  $("#file-xls").on("change", function () {
    var upload = uploadFile($(this), "XLS");
    // $('#dwnl-btn-xls').attr('href', fullpath)
  });

  $("#dwnl-btn-up-pdf").on("click", function () {
    $("#file-pdf").click();
  });

  $("#file-pdf").on("change", function () {
    uploadFile($(this), "PDF");
    // uploadPdf($(this));
    $("#file-pdf").val("");
  });

  $("#dwnl-btn-up-csv").on("click", function () {
    $("#file-csv").click();
  });
  $("#file-csv").on("change", function () {
    uploadFile($(this), "CSV");
  });
});

function getFiles() {
  var post = baseUrl + "manager/Downloads/get_all_files";

  fetch(post, {
    method: "POST",
  })
    .then((data) => data.json())
    .then((data) => {
      if (data.status === 200) {
        $("#msj-download").summernote("code", data.msj.opt_value);
        const xls = data.files.find(({ type }) => type === "XLS");

        // $('#dwnl-icon-xls').attr('href',xls.publicfullpath )
        // $('#dwnl-btn-xls').attr('href',xls.publicfullpath )
        // $('#dwnl-date-xls').html(xls.updated_at)

        const pdf = data.files.find(({ type }) => type === "PDF");
        $("#dwnl-icon-pdf").attr("href", pdf.publicfullpath);
        $("#dwnl-btn-pdf").attr("href", pdf.publicfullpath);
        // $('#dwnl-date-pdf').html(pdf-updated_at)

        const csv = data.files.find(({ type }) => type === "CSV");
        $("#dwnl-icon-csv").attr("href", csv.publicfullpath);
        $("#dwnl-btn-csv").attr("href", csv.publicfullpath);
        // $('#dwnl-date-csv').html(csv.updated_at)

        setPromotionFiles(data.promotions);
        // renderFileList(data.files)
      } else {
        /*Armamos el mensaje toast*/
        dangerToast(data.title, data.message);
      }
    })
    .catch((error) => {
      /*Armamos el mensaje toast*/
      dangerToast(error.title, error.message);
    });
}

function setPromotionFiles(promotions) {
  var thumbs = `<ul class="list-unstyled" 
                      style="display: grid; 
                             grid-template-columns: repeat(3, 1fr); 
                             gap: 50px; 
                             decoration: none;
                             align-items: center;
                             justify-items: center;">`;

  promotions.forEach((promotion) => {
    thumbs += `<li class="d-flex flex-column align-items-center justify-content-center">
                        <img 
                            src="${promotion.publicfullpath}" 
                            alt="${promotion.name}" 
                            style="width: 300px;">
                        <button id="btnDeletePromotion" data-id="${promotion.id}" class="btn btn-danger rounded-pill mt-4">Eliminar</button>
                    </li>`;
  });
  thumbs += `</ul>`;
  $("#thumbs").empty().append(thumbs);
}

function renderFileList(files) {
  var cards = "";
  files.forEach((file) => {
    cards += ` `;
  });

  // $('#files-list').empty().append(cards)
}

async function uploadFile(e, type) {
  $("#loading-promotion-pdf").removeClass("d-none");
  const post = `${baseUrl}manager/Downloads/upload_file`;
  const file = e[0];
  let newName; // Declare newName outside the try block
  let cleanFileName; // Declare cleanFileName outside the try block
  try {
    const fileNameParts = file.files[0].name.split(".");
    const extension = fileNameParts.pop();
    // Limpia el nombre de archivo
    cleanFileName = fileNameParts.join(".")
      .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, "")
      .replace(/ /g, "-")
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "")
      .toLowerCase();
    newName = `${cleanFileName}.${extension}`;
  } catch (error) {
    console.error("Error uploading file:", error);
    alert("Se encontro un error en el nombre del archivo");
    $("#file-pdf").val("");
    $("#loading-promotion-pdf").addClass("d-none");
    return; // Exit the function if there's an error
  }
  try {
    const data = new FormData();
    data.append("fileName", newName);
    data.append("file", file.files[0]);
    data.append("name", cleanFileName);
    data.append("description", $("#description").val());
    data.append("type", type);

    const response = await fetch(post, {
      method: "POST",
      body: data,
    });
    const responseData = await response.json();

    if (responseData.status === 200) {
      alert(responseData.message);
      $("#loading-promotion-pdf").addClass("d-none");
      window.location.reload();
      // successToast(responseData.title, responseData.message);
      // return responseData.upload;
    } else {
      alert(responseData.message);
      $("#loading-promotion-pdf").addClass("d-none");
      $("#file-pdf").val("");
      // dangerToast(responseData.title, responseData.message);
    }
  } catch (error) {
    console.error("Error uploading file:", error);
    alert("There was an error uploading the file.");
    $("#file-pdf").val("");
    $("#loading-promotion-pdf").addClass("d-none");
  }
}


function uploadNewFile() {
  var post = baseUrl + "manager/Downloads/new_file";
  var file = $("#newfile")[0];
  var fileName = file.files[0]["name"].split(".");
  var extension = fileName.pop();

  // Limpia el nombre de archivo
  fileName = fileName[0]
    .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, "")
    .replace(/ /g, "-")
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .toLowerCase();
  var newName = fileName + "." + extension;

  var data = new FormData();
  data.append("fileName", newName);
  data.append("file", file.files[0]);
  data.append("name", $("#name").val());
  data.append("description", $("#description").val());
  data.append("type", $("#type").val());

  fetch(post, {
    method: "POST",
    body: data,
  })
    .then((data) => data.json())
    .then((data) => {
      if (data.status === 200) {
        successToast(data.title, data.message);
      } else {
        /*Armamos el mensaje toast*/
        dangerToast(data.title, data.message);
      }
    })
    .catch((error) => {
      /*Armamos el mensaje toast*/
      dangerToast(error.title, error.message);
    });
}

function save_msj() {
  var post = baseUrl + "manager/Downloads/save_msj";

  var attrs = new URLSearchParams({
    msj: $("#msj-download").val(),
  });

  fetch(post, {
    method: "POST",
    body: attrs,
  })
    .then((data) => data.json())
    .then((data) => {
      if (data.status == 200) {
        successToast("Bien Hecho", data.message);
        // alert(data.message)
      }
    });
}

function getPromotionFiles() {}
