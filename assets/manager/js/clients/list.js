var baseUrl = $("body").data('base_url');
var table = null;

window.addEventListener('load', reviewInit())

function reviewInit() {
    loadClientsDatatable()
    set_select2_filter()

    $('#set-filtro').click(function () {
        table.ajax.reload()
    })

    $('#btn-clear-flt').click(function () {
        clearFilter()
    })

    $('#updateStock').click(function () {
        updateStock()
    })

}

function loadClientsDatatable() {
    table = $('#clients-list').DataTable({
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Buscar:</span> _INPUT_',
            searchPlaceholder: 'Buscar...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;',
                'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'
            },
            info: 'Páginas _PAGE_ de _PAGES_'
        },
        "processing": true,
        "serverSide": true,
        "order": [[0, 'asc']],
        ajax: {
            url: baseUrl + "manager/clients/get_list_dt", // json datasource
            type: "post",
            data: function (data) {
                return JSON.stringify({
                    draw: data.draw,
                    start: data.start,
                    length: data.length,
                    order: data.order,
                    search: data.search/* ,
                        idFamilia: $('#filter_familia').val(),
                        idMarca:   $('#filter_marca').val(),
                        show:      $('#filter_show').val() */
                })
            },
            error: function (error) {
                console.log(error)
            }
        },
        "columnDefs": [
            //{ className: "text-right", "targets": [ 4 ] },
            { className: "text-center", "targets": [0, 1, 2, 3, 5, 6, 7, 8, 9] }
        ]
    })
}

function loadFilterData() {

}

function clearFilter() {
    $('.filter-select').val(null).trigger('change')
    set_select2_filter()
    table.ajax.reload();
}

function set_select2_filter() {

    $('#filter_familia').select2({
        minimumResultsForSearch: -1,
        placeholder: "Seleccione una Familia"
    })

    $('#filter_marca').select2({
        minimumResultsForSearch: -1,
        placeholder: "Seleccione una Marca"
    })

    $('#filter_show').select2({
        minimumResultsForSearch: -1,
        placeholder: "Seleccione un Valor"
    })

}


$(document).on('click', '.delete-cliente', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var razonsocial = $(this).data('razonsocial');
    Swal.fire({
        title: '¿Desea Eliminar el Cliente?',
        text: "Cliente: " + razonsocial,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            var url = baseUrl + 'manager/clients/remove/' + id
            fetch(url, {
                method: 'GET'
            })
                .then(data => data.json())
                .then(data => {
                    if (data.status == 200) {
                        successToast(data.title, data.message)
                        setTimeout(() => {
                            window.location.href = baseUrl + "manager/clients";
                        }, 5000);
                    } else {
                        dangerToast('Usuario', data.message)
                    }
                })
                .catch(error => {
                    dangerToast('Usuario', error.message)
                })
        }
    })
});

$(document).ready(function () {
    //$('.is_activo').bootstrapSwitch();
})

$(document).on('click', '.is_activo', function (e) {
    e.preventDefault();
    if ($(this).is(':checked')) {
        var active = 1;
    } else {
        var active = 0;
    }
    
    var url = baseUrl + 'manager/clients/update_client_status/' + $(this).val() + '/' + active
    fetch(url, {
        method: 'GET'
    })
        .then(data => data.json())
        .then(data => {
            if (data.code == 1) {
                successToast('Cliente', data.message)
                if (active == 0) {
                    $(this).prop('checked', false);
                } else {
                    $(this).prop('checked', true);
                }
            } else {
                dangerToast('Cliente', data.message)
            }
        })
        .catch(error => {
            dangerToast('Cliente', "No se ha podido procesar")
        })
});
