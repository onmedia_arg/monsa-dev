var unblockConfig = {
    message: '<p><i class="fa fa-cog fa-spin fa-2x"></i> <br><b>Obteniendo detalle de Cuenta Corriente</b></p>',
      css:{
          backgroundColor: 'transparent',
          border: 'none', 
      },
      overlayCSS: {
          backgroundColor: '#FFF',
      }
};

var form_new_member = $('#formClientData').validate({
	ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
	errorClass: 'validation-invalid-label',
	successClass: 'validation-valid-label',
	validClass: 'validation-valid-label',
	highlight: function (element, errorClass) {
		$(element).removeClass(errorClass);
	},
	unhighlight: function (element, errorClass) {
		$(element).removeClass(errorClass);
	},
	success: function (label) {
		label.addClass('validation-valid-label').text('OK'); // remove to hide Success message
	},
	// Different components require proper error label placement
	errorPlacement: function (error, element) {
		// Unstyled checkboxes, radios
		if (element.parents().hasClass('form-check')) {
			error.appendTo(element.parents('.form-check').parent());
		}
		// Input with icons and Select2
		else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
			error.appendTo(element.parent());
		}
		// Input group, styled file input
		else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
			error.appendTo(element.parent().parent());
		}
		// Other elements
		else {
			error.insertAfter(element);
		}
	},
	rules: {
		razon_social: { required: true },
		//tipo_cliente: {required: true},
		cuit: { required: true },
		mail: { required: true },
	},
	messages: {
		razon_social: { required: 'Este campo es requerido' },
		//tipo_cliente: {required: 'Este campo es requerido'},
		cuit: { required: 'Este campo es requerido' },
		mail: { required: 'Este campo es requerido' },

	}
})

// var img = []
$(document).on('click', '#btnUpdateCliente', function (e) {
	e.preventDefault();
	if ($("#formClientData").valid()) {
		updateClient()
	} else {
		dangerToast('Error', 'Faltan campos por completar!')
	}
});

$(document).on('click', '#btnViewMovimientos', function (e) {
	e.preventDefault();
	$.blockUI(unblockConfig);
	$('#details_cuenta_corriente').addClass('d-none')
	$('#cliente_not_found').addClass('d-none')
	var post = baseUrl + 'manager/CuentaCorriente/get_cuentaCorriente/'+$('#id_cliente').val()
	
	fetch(post, {
		method: 'POST'
	})
		.then(data => data.json())
		.then(data => {
			if (data.code == 200) {
				$('#table-details').addClass('table')
				if(parseFloat(data.data.saldo.saldo.replaceAll(".",""),10) > 0){
					$data_saldo = '<span class="text-success"> $ '+data.data.saldo.saldo+'</span>';
					
				}else{
					$data_saldo = '<span class="text-danger"> $ '+data.data.saldo.saldo+'</span>';
				}

				$data_movimiento = '';
				data.data.movimientos.detalle.forEach(element => {
					$data_movimiento += `'<tr>'
						'<td class="text-center">${element.fecha}</td>'
						'<td class="text-center">${element.tipo}</td>'
						'<td class="text-center">${element.nro_documento}</td>'
						'<td class="text-center">${element.estado}</td>'
						'<td class="text-center">${element.referencia}</td>'
						'<td class="text-right">$ ${element.importe}</td>'
					'</tr>'`
				});
				$('#details_movimientos').html($data_movimiento)
				$('#saldo').html($data_saldo)
				$('#details_cuenta_corriente').removeClass('d-none')
				
			} else if(data.code == 203){
				$('#cliente_not_found').removeClass('d-none')
			}else {
				dangerToast(null, data.message)
			}
			$.unblockUI();
		})
		.catch(error => {
			$.unblockUI();
			dangerToast(null, error.message)
		})
		
});

$(document).ready(function () {
	$('.tab-pane .table').DataTable({
		dom: '<"datatable-header"fl><"datatable-scroll"t><"datatabl	e-footer"ip>',
		language: {
			search: '<span>Buscar:</span> _INPUT_',
			searchPlaceholder: 'Buscar...',
			lengthMenu: '<span>Ver:</span> _MENU_',
			paginate: {
				"first": "Inicio",
				"last": "Último",
				"next": "Siguiente",
				"previous": "Anterior"
			},
			info: 'Páginas _PAGE_ de _PAGES_'
		},
		"columnDefs": [{
			"targets": -1,
			"orderable": false
		}],
		"pageLength": 10,

	});
})

async function updateClient() {

	var post = baseUrl + 'manager/Clients/updateUser'
	var data = new URLSearchParams({
		razon_social: $('#razon_social').val(),
		cuit: $('#cuit').val(),
		tipo_cliente: $('#tipo_cliente').val(),
		telefono: $('#telefono').val(),
		mail: $('#mail').val(),
		direccion: $('#direccion').val(),
		id_cliente_interno: $('#id_cliente_interno').val(),
		id_cliente: $('#id_cliente').val(),
		email_vendedor: $('#email_vendedor').val()
	})
	console.log(data)
	const response = await fetch(post, {
		method: 'POST',
		body: data
	})
	const result = await response.json()
	
	if (result.status == 200) {
		successToast(result.title, result.message)
	} else {
		dangerToast(null, result.message)
	}
}

$(document).on('click', '[data-target="#order-details"]', function () {

	var id_order = $(this).data('id')
	$('#order-details-table').block(unblockConfig);
	$('#order-details-table tbody').empty()

	$.getJSON(baseUrl + 'manager/Orders/get_order_json/' + id_order, function (data) {

		var html = '';
		data['details'].forEach(function (item, index) {

			console.log(item.producto)

			html += '<tr>'
			html += '<td>' + item.posnr + '</td>'
			html += '<td>' + item.producto.nombre + '</td>'
			html += '<td>' + item.producto.precio + '$</td>'
			html += '<td>' + item.qty + '</td>'
			html += '<td>' + item.total + '$</td>'
			html += '</tr>'

		})

		$('#order-details-table').unblock();
		$('#order-details-table tbody').html(html)
		$('#order-date').text(data['order'].created)
		$('#order-id').text(data['order'].idOrderHdr)
		$('#order-total').text(data['order'].total + '$')
		$('#order-email').text(data['client'].mail)
		$('#order-address').text(data['client'].direccion)
		$('#order-cuit').text(data['client'].cuit)
		$('#order-rs').text(data['client'].razon_social)
		$('#order-tel').text(data['client'].telefono)
	})
})

$(document).on('click', '#deleteUser', function (e) {
	e.preventDefault();
	var id = $(this).data('id');
	var username = $(this).data('username');
	Swal.fire({
		title: '¿Desea Eliminar el Usuario?',
		text: "Usuario: " + username,
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Eliminar',
		cancelButtonText: 'Cancelar'
	}).then((result) => {
		if (result.isConfirmed) {
			var post = baseUrl + 'manager/user/remove/' + id
			fetch(post, {
				method: 'GET'
			})
				.then(data => data.json())
				.then(data => {
					if (data.status == 200) {
						successToast(data.title, data.message)
						setTimeout(() => {
							window.location.href = baseUrl + "manager/clients";
						}, 5000);
					} else {
						dangerToast('Usuario', data.message)
					}
				})
				.catch(error => {
					dangerToast('Usuario', error.message)
				})
		}
	})
});

$(document).on('click', '#updatePassword', function (e) {
	e.preventDefault();
	var id = $(this).data('id');
	var username = $(this).data('username');
	Swal.fire({
		title: '¿Desea actualizar la contraseña?',
		text: "Cliente: " + username,
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Actualizar',
		cancelButtonText: 'Cancelar'
	}).then((result) => {
		if (result.isConfirmed) {
			var post = baseUrl + 'manager/user/update_password/' + id
			fetch(post, {
				method: 'GET'
			})
				.then(data => data.json())
				.then(data => {
					if (data.status == 200) {
						successToast(data.title, data.message)
						Swal.fire({
							title: 'Usuario: ' + data.user,
							  text: 'Contraseña: ' +data.password,
							confirmButtonText: 'Aceptar',
							})
					} else {
						dangerToast('Usuario', data.message)
					}
				})
				.catch(error => {
					dangerToast('Usuario', error.message)
				})
		}
	})
});

$(document).on('click', '.is_active', function(){ 

	
	if( $(this).is(':checked') ){
		var active = 1;
	}else{
		var active = 0;
	}

	$.getJSON( baseUrl + 'user/update_user_status/' + $(this).val() + '/' + active, function(data){

		if ( data.code == 1 ) {

			iziToast.success({
				title: 'OK!',
				message: data.message,
			});

		}else{

			iziToast.error({
				title: 'Error!',
				message: data.message,
			});

		}

	})
})

