var form_new_member = $('#formClientData').validate({
	ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
	errorClass: 'validation-invalid-label',
	successClass: 'validation-valid-label',
	validClass: 'validation-valid-label',
	highlight: function(element, errorClass) {
		$(element).removeClass(errorClass);
	},
	unhighlight: function(element, errorClass) {
		$(element).removeClass(errorClass);
	},
	success: function(label) {
		label.addClass('validation-valid-label').text('OK'); // remove to hide Success message
	},
	// Different components require proper error label placement
	errorPlacement: function(error, element) {
			// Unstyled checkboxes, radios
			if (element.parents().hasClass('form-check')) {
				error.appendTo( element.parents('.form-check').parent() );
			}
			// Input with icons and Select2
			else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
				error.appendTo( element.parent() );
			}
			// Input group, styled file input
			else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
				error.appendTo( element.parent().parent() );
			}
			// Other elements
			else {
				error.insertAfter(element);
			}
	},
	rules:{
            razon_social: {required: true},
            tipo_cliente: {required: true},
            cuit: {required: true},
			mail: {required: true},
            usuario: {required: true},
            nombre_usuario: {required: true},
            //password_usuario: {required: true}
	},
	messages:{
			razon_social: {required: 'Este campo es requerido'},
			tipo_cliente: {required: 'Este campo es requerido'},
			cuit:    {required: 'Este campo es requerido'},
			mail:    {required: 'Este campo es requerido'},
			usuario:   {required: 'Este campo es requerido'},
            nombre_usuario:    {required: 'Este campo es requerido'},
			//password_usuario:   {required: 'Este campo es requerido'}
	}
})

// var img = []

$(document).on('click', '#btnAddCliente', function (e) {
	$('#btnAddCliente').prop('disabled', true)
    e.preventDefault();
    if ( $("#formClientData").valid() ){
        insertClient()	
    }else{
        dangerToast('Error', 'Faltan campos por completar!')
    }
	$('#btnAddCliente').prop('disabled', false)
});

function insertClient(){

	var post = baseUrl + 'manager/Clients/insertUser' 
	var data = new URLSearchParams({
        razon_social:   $('#razon_social').val(), 
        cuit :          $('#cuit').val(),
        tipo_cliente :  $('#tipo_cliente').val(),
        telefono :      $('#telefono').val(),
        mail :          $('#mail').val(),
        direccion :     $('#direccion').val(),
        usuario :       $('#usuario').val(),
        nombre_usuario :$('#nombre_usuario').val(),
        //password :      $('#password_usuario').val(),
        id_cliente_interno :      $('#id_cliente_interno').val()
	})
	
	fetch(post, {
		method: 'POST',
		body: data
	})
	.then(data=>data.json())
	.then(data=>{
		if( data.status == 200 ){
            successToast(data.title, data.message)
			Swal.fire({
				title: data.user,
  				text: 'Contraseña: ' +data.password,
				confirmButtonText: 'Aceptar',
				})
		}else{ 
			dangerToast( 'Cliente', data.message )
		}

	})
	.catch(error=>{
		dangerToast( 'Cliente', error.message )
	})
}

$(document).ready(function () {
	
})



