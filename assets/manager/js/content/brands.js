$(document).ready(function () {
    getBrands();
    
    $("#btnUploadBrands").on("click", function (e) {
      e.preventDefault();
      $("#imagenBrands").click();
    });

    $("#imagenBrands").on("change", function () {
      uploadImageBrands($(this));
    });
  
    $(document).on("click", "#btnDeleteBrands", function (e) {
      e.preventDefault();
      deleteImageBrands(e.target.dataset.id);
    });
  });



async function getBrands(){

    var post       = baseUrl + 'manager/Content/get_brands'

    const response = await fetch(post, {
        method: 'GET'
    })

    const data = await response.json()
    renderBrands(data);
    
}


async function uploadImageBrands(){
    var post = baseUrl + "manager/Content/uploadImageBrands";
    var file = $("#imagenBrands")[0];
  
    var data = new FormData();
    data.append("file", file.files[0]);
  
    const response = await fetch(post, {
      method: "POST",
      body: data,
    });
  
    const result = await response.json();
  
    if (result.status === 200) {
      fill_images(result.upload.publicfullpath, result.upload.id);
    //   successToast(result.title, result.message);
    } else {
    //   dangerToast(result.title, result.message);
    }
}

function renderBrands(data){
    console.log(data);

    $("#thumbsBrands")
    .html(data.map(item => `<li class="d-flex flex-column align-items-center mb-4" style="border-bottom: 1px solid #ccc; padding: 10px;">
                                <img src="${item.publicfullpath}" alt="Slider" width=200px class="img-fluid">
                                <button class="btn btn-danger rounded-pill mt-4" id="btnDeleteBrands" data-id="${item.id}">Eliminar</button>
                            </li>`).join(''));
}

function fill_images(path, id) {
 
    var thumbs = `<li class="d-flex flex-column align-items-center mb-4" style="border-bottom: 1px solid #ccc; padding: 10px;">
                                <img src="${path}" alt="Slider" width=200px class="img-fluid">
                                <button class="btn btn-danger rounded-pill mt-4" id="btnDeleteBrands" data-id="${id}">Eliminar</button>
                                </li>`;
    
    $("#thumbsBrands").append(thumbs);
}

async function deleteImageBrands(id){
    
    var post = baseUrl + "manager/Content/deleteImageBrands";

    var data = new URLSearchParams({ id: id });
    const response = await fetch(post, { method: "POST", body: data });
    const result = await response.json();

    console.log(result.status);
    if(result.status == 200){
        // successToast(result.title, result.message);
        location.reload();
    }else{
        // dangerToast(result.title, result.message);
    }
}