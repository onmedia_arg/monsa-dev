$(document).ready(function () {
    getSlider();
    
    $("#btnUploadSlider").on("click", function (e) {
      e.preventDefault();
      $("#location").val("D");
      $("#imagenSlider").click();
    });

    $("#btnUploadSliderMobile").on("click", function (e) {
      e.preventDefault();
      $("#location").val("M");
      $("#imagenSlider").click();
    });
  
    $("#imagenSlider").on("change", function () {
      uploadImageSlider($(this));
    });
  
    $(document).on("click", "#btnDeleteSlider", function (e) {
      e.preventDefault();
      deleteImageSlider(e.target.dataset.id);
    });
  });



async function getSlider(){

    var post       = baseUrl + 'manager/Content/get_slider'

    const response = await fetch(post, {
        method: 'GET'
    })

    const data = await response.json()
    renderSlider(data);
    
}


async function uploadImageSlider(){
    var post = baseUrl + "manager/Content/uploadImageSlider";
    var file = $("#imagenSlider")[0];
  
    var data = new FormData();
    data.append("file", file.files[0]);
    data.append("location", $("#location").val());
  
    const response = await fetch(post, {
      method: "POST",
      body: data,
    });
  
    const result = await response.json();
  
    if (result.status === 200) {
      fill_images_slider(result.upload.publicfullpath, result.upload.id);
      successToast(result.title, result.message);
    } else {
      dangerToast(result.title, result.message);
    }
}

function renderSlider(data){
    console.log(data);

    $("#thumbsSlider")
    .html(data.filter(item => item.location == "D")
              .map(item => `<li class="d-flex flex-column align-items-center mb-4" style="border-bottom: 1px solid #ccc; padding: 10px;">
                                <img src="${item.publicfullpath}" alt="Slider" width=450px class="img-fluid">
                                <button class="btn btn-danger rounded-pill mt-4" id="btnDeleteSlider" data-id="${item.id}">Eliminar</button>
                            </li>`).join(''));
    $("#thumbsSliderMobile")
    .html(data.filter(item => item.location == "M")
              .map(item => `<li class="d-flex flex-column align-items-center mb-4" style="border-bottom: 1px solid #ccc; padding: 10px;">
                                <img src="${item.publicfullpath}" alt="Slider" width=150px class="img-fluid">
                                <button class="btn btn-danger rounded-pill mt-4" id="btnDeleteSlider" data-id="${item.id}">Eliminar</button>
                                </li>`).join(''));
}

function fill_images_slider(path, id) {
    if($("#location").val() == "D"){
    var thumbs = `<li class="d-flex flex-column align-items-center mb-4" style="border-bottom: 1px solid #ccc; padding: 10px;">
                                <img src="${path}" alt="Slider" width=450px class="img-fluid">
                                <button class="btn btn-danger rounded-pill mt-4" id="btnDeleteSlider" data-id="${id}">Eliminar</button>
                                </li>`;
    

        $("#thumbsSlider").append(thumbs);
    }else{
        var thumbs = `<li class="d-flex flex-column align-items-center mb-4" style="border-bottom: 1px solid #ccc; padding: 10px;">
        <img src="${path}" alt="Slider" width=150px class="img-fluid">
        <button class="btn btn-danger rounded-pill mt-4" id="btnDeleteSlider" data-id="${id}">Eliminar</button>
        </li>`;        
        $("#thumbsSliderMobile").append(thumbs);
    }
}

async function deleteImageSlider(id){
    var post = baseUrl + "manager/Content/deleteImageSlider";
    var data = new URLSearchParams({ id: id });
    const response = await fetch(post, { method: "POST", body: data });
    const result = await response.json();

    console.log(result.status);
    if(result.status == 200){
        // successToast(result.title, result.message);
        location.reload();
    }else{
        // dangerToast(result.title, result.message);
    }
}