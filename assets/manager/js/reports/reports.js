
var baseUrl = $("body").data("base_url");

var buildProductSelect = function(){

    var post = baseUrl + "/manager/Reports/fetch_all_products/";

    fetch(post,{
        method: 'POST'
    })
    .then(data => data.json())
    .then(data => {
        var options = ""
        data.map(product => {
            options += `<option value="${product.idProduct}">${product.title}</option>`;
        });

        $("#product").append(options)

    })
}

var EchartsLines = function() {

    // Line charts
    var _lineChartExamples = function() {
        if (typeof echarts == 'undefined') {
            console.warn('Warning - echarts.min.js is not loaded.');
            return;
        }

        // Define elements
        var line_basic_element = document.getElementById('line_basic');
        var product = document.getElementById('product').value;

        //
        // Charts configuration
        //
        var post = baseUrl + "/manager/Reports/get_data/" + product;
        var fecha = ""
        var iniciado = ""
        var procesando = ""
        var pagado = ""
        var error = ""

        fetch(post,{
            method: 'POST'
        })
        .then(data => data.json())
        .then(data => {
            if(data.status == 200){
                fecha = data.fecha
                iniciado = data.iniciado
                procesando = data.procesando
                pagado = data.pagado
                error = data.erroremision
            
                var line_basic = echarts.init(line_basic_element);

                //
                // Chart config
                //
    
                // Options
                line_basic.setOption({
    
                    // Define colors
                    color: ['#EF5350', '#66BB6A'],
    
                    // Global text styles
                    textStyle: {
                        fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                        fontSize: 13
                    },
    
                    // Chart animation duration
                    animationDuration: 750,
    
                    // Setup grid
                    grid: {
                        left: 0,
                        right: 40,
                        top: 35,
                        bottom: 60,
                        containLabel: true
                    },
    
                    // Add legend
                    legend: {
                        data: ['Iniciado', 'Procesando Pago', 'Pagado', 'Error Emisión' ],
                        itemHeight: 8,
                        itemGap: 20
                    },
    
                    // Add tooltip
                    tooltip: {
                        trigger: 'axis',
                        backgroundColor: 'rgba(0,0,0,0.75)',
                        padding: [10, 15],
                        textStyle: {
                            fontSize: 13,
                            fontFamily: 'Roboto, sans-serif'
                        }
                    },
    
                    // Horizontal axis
                    xAxis: [{
                        type: 'category',
                        boundaryGap: false,
                        data:fecha,
                        // data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                        axisLabel: {
                            color: '#333'
                        },
                        axisLine: {
                            lineStyle: {
                                color: '#999'
                            }
                        },
                        splitLine: {
                            lineStyle: {
                                color: ['#eee']
                            }
                        }
                    }],
    
                    // Vertical axis
                    yAxis: [{
                        type: 'value',
                        axisLabel: {
                            formatter: '{value}',
                            color: '#333'
                        },
                        axisLine: {
                            lineStyle: {
                                color: '#999'
                            }
                        },
                        splitLine: {
                            lineStyle: {
                                color: ['#eee']
                            }
                        },
                        splitArea: {
                            show: true,
                            areaStyle: {
                                color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                            }
                        }
                    }],

                    // Zoom control
                    dataZoom: [
                        {
                            type: 'inside',
                            start: 30,
                            end: 70
                        },
                        {
                            show: true,
                            type: 'slider',
                            start: 30,
                            end: 70,
                            height: 40,
                            bottom: 0,
                            borderColor: '#ccc',
                            fillerColor: 'rgba(0,0,0,0.05)',
                            handleStyle: {
                                color: '#585f63'
                            }
                        }
                    ],
                    // Add series
                    series: [
                        {
                            name: 'Iniciado',
                            type: 'line',
                            // data: [11, 11, 15, 13, 12, 13, 10],
                            data: iniciado,
                            smooth: true,
                            symbolSize: 7,
                            itemStyle: {
                                normal: {
                                    borderWidth: 2,
                                    color:['#00bcd4']
                                }
                            }
                        },
                        {
                            name: 'Procesando Pago',
                            type: 'line',
                            // data: [1, -2, 2, 5, 3, 2, 0],
                            data:procesando,
                            smooth: true,
                            symbolSize: 7,
                            itemStyle: {
                                normal: {
                                    borderWidth: 2,
                                    color:['#2196f3']
                                }
                            }
                        },
                        {
                            name: 'Pagado',
                            type: 'line',
                            // data: [1, -2, 2, 5, 3, 2, 0],
                            data:pagado,
                            smooth: true,
                            symbolSize: 7,
                            itemStyle: {
                                normal: {
                                    borderWidth: 2,
                                    color:['#4caf50']
                                }
                            }
                        },
                        {
                            name: 'Error Emisión',
                            type: 'line',
                            // data: [1, -2, 2, 5, 3, 2, 0],
                            data:error,
                            smooth: true,
                            symbolSize: 7,
                            itemStyle: {
                                normal: {
                                    borderWidth: 2,
                                    color:['#f44336']
                                }
                            }
                        },

                    ]
                });            
            }else{
                line_basic.clear()
            }
        })


        // Basic line chart
        if (line_basic_element) {

            // Initialize chart

        }

        //
        // Resize charts
        //

        // Resize function
        var triggerChartResize = function() {
            line_basic_element && line_basic.resize();
        };

        // On sidebar width change
        $(document).on('click', '.sidebar-control', function() {
            setTimeout(function () {
                triggerChartResize();
            }, 0);
        });

        // On window resize
        var resizeCharts;
        window.onresize = function () {
            clearTimeout(resizeCharts);
            resizeCharts = setTimeout(function () {
                triggerChartResize();
            }, 200);
        };
    };

    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _lineChartExamples();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    
    buildProductSelect();
    EchartsLines.init();

});



