var baseUrl = $("body").data('base_url'); 
var table = null;


window.addEventListener('load', reviewInit())

function reviewInit() {
    fill_widget();
    loadOrdersDatatable();
    loadUsersDatatable();
    loadOrdersEvents(); 
    loadProcessState();   
}

function loadOrdersDatatable(){
    table =  $('#orders-list').DataTable({
            dom: '<"datatable-scroll"t>',
            "processing": true,
            "serverSide": true,
            "order": [[ 0, 'desc' ]],
            ajax:{
                url : baseUrl + "manager/Orders/get_list_home_dt", 
                type: "post",
                error: function(error) {
                    console.log(error)
                }
            },
            columnDefs: [
                {className: "text-center", targets: [0, 4]},
                {width    : "70px", targets: 0},
            ]
    })
}

function loadUsersDatatable(){

    table =  $('#clients-list').DataTable({
        dom: '<"datatable-scroll"t>',
        "processing": true,
        "serverSide": true,
        "order": [[ 0, 'desc' ]],
        ajax:{
            url : baseUrl + "manager/Clients/get_list_home_dt", 
            type: "post",
            error: function(error) {
                console.log(error)
            }
        },
    })
}

function loadProcessState(){
   
	    var post = baseUrl + 'manager/Sys_Config/getProcessState';

		fetch(post, {
			method: 'POST', 
		})
		.then(data=>data.json())
		.then(data=>{ 

			if(data.status == 'false'){
                html = '<li class="media">'
                            +'<div class="mr-3">'
                                +'<a href="#" class="btn bg-transparent border-success text-success rounded-round border-2 btn-icon"><i class="icon-checkmark3"></i></a>'
                            +'</div>'
                            +'<div class="media-body">'
                                +'Actualización de stock finalizado'
                            +'<div class="text-muted">Productos Actualizados '+ data.processed+' de '+ data.total +'</div>'
                            +'<div class="text-muted">Finalizado: '+data.finish+'</div>'
                            +'</div>'
                        +'</li>'
            }else{
                html = '<li class="media">'

                        +'<div class="mr-3">'
                        +    '<a href="#" class="btn bg-transparent border-warning text-warning rounded-round border-2 btn-icon"><i class="icon-bubble-notification "></i></a>'
                        +'</div>'


                        +'<div class="media-body">'
                        +    'Actualización de stock en proceso ('+ data.processed +' de '+ data.total +')'
                        +    '<div class="progress">'
                        +        '<div class="progress-bar" role="progressbar" style="width: '+ (data.processed/data.total)*100 +'%;" aria-valuenow="'+ (data.processed/data.total)*100 +'" aria-valuemin="0" aria-valuemax="100">'+ ((data.processed/data.total)*100).toFixed(2) +'%</div>'
                        +   '</div>'
                        +'<div class="text-muted">Proceso iniciado: '+data.start+'</div>'
                        +'</div>'
					+'</li>'	
            }

            $('#process-state').html('')
            $('#process-state').html(html)
 
		})


   /*  $.ajax({
        type: "POST",
        url:  baseUrl + "manager/Sys_Config/getProcessState",
        success: function(data) {
            console.log(data.status)
            if(data['status'] != 'true'){
                html = '<li class="media">'
                            +'<div class="mr-3">'
                                +'<a href="#" class="btn bg-transparent border-success text-success rounded-round border-2 btn-icon"><i class="icon-checkmark3"></i></a>'
                            +'</div>'
                            +'<div class="media-body">'
                                +'Actualización de Stock'
                            +'<div class="text-muted">Abr 17, 08:00</div>'
                            +'</div>'
                        +'</li>'
                    }
                    $('#process-state').html('')
                    $('#process-state').html(html)
                }            
    }); */
}

function fill_widget(){
    
    var post = baseUrl + "manager/Orders/get_info_widget";
    
    fetch(post, {
        method: 'POST',
    })
    .then(data=>data.json())
    .then(data=>{
        $("#widget-pedidos").html(data.cant)
        $("#widget-ventas").html("$ " + data.total)
        
        var mes = moment();
        mes.locale('es');

       $(".c_month").html( mes.format('MMMM YYYY') );
       
    //    var avg = data.total / data.cant
       $("#avg-ventas").html( "Prom: $ " + data.prom );
       
       $("#clie-actv").html(40)

    })

}

function loadOrdersEvents(){

    $(document).on('click', '.btnChangeStatus', function (e) {
        e.preventDefault();
        changeStatus($(this))
    });

}

async function changeStatus(order){

    var post =  baseUrl + "manager/Orders/changeOrderStatus/"
    
    var data = new URLSearchParams({        
        id:     $(order).data('id'),
        status: 3
    });

    const response = await fetch(post, {method: 'POST',body:data })
    const res      = await response.json()

    if(res.status == 200 ){
		  console.log(res.message)
		  successToast('Bien Hecho!', res.message)
          $('#orders-list').DataTable().ajax.reload();
    }else{
        iziToast.error({
            title: 'Error!',
            message: res.message,
        });   
    }

}

