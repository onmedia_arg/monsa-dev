
document.addEventListener('DOMContentLoaded', function() {
    var valuesList = ""
    var previousValue = ""
    var id_atributo_loaded = ""

    loadFamiliesAsync()
    loadMarcasAsync()

    $(document).on('click', '.btn-edit-family', function () {
        let idFamilia = $(this).data("idatributo")
        let name = $(this).data("value")

        $('#updateFamily').val(name)
        $('#idFamily').val(idFamilia)
        if($('#formUpdateFamily').hasClass('d-none')){
            $('#formUpdateFamily').removeClass('d-none');
            $('#formCreateFamily').addClass('d-none');
        }

    })

    $(document).on('click', '#btnUpdateFamily', function () {

        let idFamilia = $('#idFamily').val()
        let name = $('#updateFamily').val()
        // fetch to update array in database
        updateFamily(idFamilia, name)

        $('#idFamily').val('')
        $('#updateFamily').val('')
        $('#formUpdateFamily').toggleClass('d-none');
        $('#formCreateFamily').toggleClass('d-none');
        
    })


    $(document).on('click', '.btn-delete-family', function () {
            
            let idFamilia = $(this).data("idatributo")
            let name = $(this).data("value")
            deleteFamily(idFamilia, name)
    
            //if atributes-card is visible, hide it
            if(!$('#atributes-card').hasClass('d-none')){
                $('#atributes-card').addClass('d-none');
            }

    })
    
    $(document).on('click','.btn-show-atributes',function(){
        var idFamilia = $(this).data("idfamilia")
        const btnAddAtribute = document.getElementById("btnCreateAtribute");
        btnAddAtribute.setAttribute("data-idfamilia", idFamilia);
        
        //agregamos el nombre de la familia al titulo del card
        $('#family-name').text($(this).data("nombre"))
        //en el span family-name setear el data-idfamilia 
        $('#family-name').attr('data-idfamilia', idFamilia)

        //limpiamos el input de crear atributo
        $('#createAtribute').val('')
        //si formUpdateAtribute esta visible, lo ocultamos
        if(!$('#formUpdateAtribute').hasClass('d-none')){
            $('#formUpdateAtribute').addClass('d-none');
            $('#formCreateAtribute').removeClass('d-none');
        }

        loadAtributesAsync(idFamilia)        
    } )

    $(document).on('click','.btn-show-values',function(){
        var idAtributo = $(this).data("idatributo")
        var values = $(this).data("values")
        id_atributo_loaded = idAtributo
        const btnAddValue = document.getElementById("btnCreateValue");
        btnAddValue.setAttribute("data-idatributo", idAtributo);
        $('#updateValue').val('')
        //si formUpdateValue esta visible, lo ocultamos
        if(!$('#formUpdateValue').hasClass('d-none')){
            $('#formUpdateValue').addClass('d-none');
            $('#formCreateValue').removeClass('d-none');
        }

        loadValues(idAtributo)        
    } )

    $(document).on('click','#btnCreateValue',function(){
        let idAtributo = this.getAttribute("data-idatributo");
        let newValue = $('#createValue').val()
        addValue(idAtributo, newValue)        
    })

    $(document).on('click','#btnCreateFamily',function(){
            let name = $('#createFamily').val()
            addFamily(name)
    })

    $(document).on('click','#btnCreateAtribute',function(){

        let idFamilia = this.getAttribute("data-idfamilia");
        let name = $('#createAtribute').val()
        addAtribute(idFamilia, name)       
    })

    //Boton de editar en el listado de atributos, completa el form de editar atributo
    $(document).on('click', '.btn-edit-attr', function () {
        let idAtributo = $(this).data("idatributo")
        let name = $(this).data("value")

        $('#updateAtribute').val(name)
        $('#idAtribute').val(idAtributo)

        if($('#formUpdateAtribute').hasClass('d-none')){
            $('#formUpdateAtribute').removeClass('d-none');
            $('#formCreateAtribute').addClass('d-none');
        }

    })

    //Boton en el form de editar atributo
    $(document).on('click', '#btnUpdateAtribute', function () {

        let idAtributo = $('#idAtribute').val()
        let name = $('#updateAtribute').val()
        // fetch to update array in database
        updateAtribute(idAtributo, name)

        $('#idAtribute').val('')
        $('#updateAtribute').val('')
        $('#formUpdateAtribute').toggleClass('d-none');
        $('#formCreateAtribute').toggleClass('d-none');

    })
    
    $(document).on('click', '.btn-delete-attr', function () {

        let idAtributo = $(this).data("idatributo")
        let name = $(this).data("value")
        deleteAtribute(idAtributo, name)

        //if values-card is visible, hide it
        if(!$('#values-card').hasClass('d-none')){
            $('#values-card').addClass('d-none');
        }

    })

    $(document).on('click', '.btn-edit-value', function () {
        // Get the value to edit from the data-value attribute of the button
        previousValue = $(this).data("value");
        
        // Set the value in the input field for editing
        $('#updateValue').val(previousValue);
        if($('#formUpdateValue').hasClass('d-none')){
            $('#formUpdateValue').removeClass('d-none');
            $('#formCreateValue').addClass('d-none');
        }

    });

    $(document).on('click', '#btnUpdateValue', function () {
        
        let updatedValue = $('#updateValue').val()
        let oldValue = previousValue
        // fetch to update array in database
        updateValue(id_atributo_loaded, updatedValue, oldValue)

        $('#updateValue').val('')
        $('#formUpdateValue').toggleClass('d-none');
        $('#formCreateValue').toggleClass('d-none');
   
    })

    $(document).on('click', '.btn-delete-value', function () {

        deleteValue(id_atributo_loaded, $(this).data("value"))

    })

    $(document).on('click', '#btn-show-families', function () {
        $('.family-card').removeClass('d-none'); // Card principal de familyy
        $('.family').addClass('d-none');
        $('.marcas-card').addClass('d-none');

    })

    // Funcionalidad de Marcas
    $(document).on('click', '#btn-show-marcas', function () {
        $('.family-card').addClass('d-none'); // Card principal de familyy
        $('.family').addClass('d-none');
        $('.marcas-card').removeClass('d-none');

    })

    $(document).on('click', '.btn-edit-marca', function () {
        let idMarca = $(this).data("idmarca")
        let name = $(this).data("value")

        $('#updateMarca').val(name)
        $('#idMarca').val(idMarca)
        if($('#formUpdateMarca').hasClass('d-none')){
            $('#formUpdateMarca').removeClass('d-none');
            $('#formCreateMarca').addClass('d-none');
        }

    })

    $(document).on('click', '#btnUpdateMarca', function () {

        let idMarca = $('#idMarca').val()
        let name = $('#updateMarca').val()
        // fetch to update array in database
        updateMarca(idMarca, name)

        $('#idMarca').val('')
        $('#updateMarca').val('')
        $('#formUpdateMarca').toggleClass('d-none');
        $('#formCreateMarca').toggleClass('d-none');
        
    })


    $(document).on('click', '.btn-delete-marca', function () {
            
            let idMarca = $(this).data("idmarca")
            let name = $(this).data("value")
            deleteMarca(idMarca, name)
    
    })

    $(document).on('click','#btnCreateMarca',function(){
        let name = $('#createMarca').val()
        addMarca(name)
})

    // Funcion para manejar el Drag AND Drop del listado de familia. 
    new Sortable(familyList,{
        animation: 150,
        handle: '.handle',
        ghostClass: 'bg-secondary',
        store: {
            set: function (sorteable) {
                const sorts = sorteable.toArray();
                updateOrdenFamily(sorts);
            }
        }
    });

    // Funcion para manejar el Drag AND Drop del listado de familia. 
    new Sortable(marcasList,{
        animation: 150,
        handle: '.handle',
        ghostClass: 'bg-secondary',
        store: {
            set: function (sorteable) {
                const sorts = sorteable.toArray();
                updateOrdenMarca(sorts);
            }
        }
    });

})


async function updateOrdenFamily(sorts){
    let route = baseUrl + 'manager/Customizing/updateOrdenFamily/'
    let formData = new FormData();

    formData.append('sorts', sorts)
    const response = await fetch(route, {
                                method: 'POST',
                                body: formData
                            })
    
    let data = await response.json()
    
    if(response.status == 200){
        successToast('Familia:', data.data)
        loadFamiliesAsync()
    }else{
        dangerToast('Error', 'No se pudo actualizar el orden de las familias')
    }
}

async function updateFamily(idFamilia, name){

    let route = baseUrl + 'manager/Customizing/updateFamily/'
    let formData = new FormData();

    formData.append('idFamilia', idFamilia)
    formData.append('familyName', name)

    const response = await fetch(route, {
                                    method: 'POST',
                                    body: formData
                                })
    
    let data = await response.json()
    //show in console the response data
    console.log(data.data)
    
    if(response.status == 200){
        successToast('Familia modificada', data.data)
        loadFamiliesAsync()
    }else{
        
    }

}


async function deleteFamily(idFamilia){
        
        let route = baseUrl + 'manager/Customizing/deleteFamily/'
        let formData = new FormData();
    
        formData.append('idFamilia', idFamilia)
    
        const response = await fetch(route, {
                                        method: 'POST',
                                        body: formData
                                    })
        
        let data = await response.json()
        //show in console the response data
        console.log(data.data)
        
        if(response.status == 200){
            successToast('Familia eliminada', data.data)
            loadFamiliesAsync()
        }else{
            
        }
    

}

async function deleteAtribute(idAtributo){
    
    let route = baseUrl + 'manager/Customizing/deleteAtribute/'
    let formData = new FormData();

    formData.append('idAtributo', idAtributo)

    const response = await fetch(route, {
                                    method: 'POST',
                                    body: formData
                                })
    
    let data = await response.json()
    //show in console the response data
    console.log(data.data)
    
    if(response.status == 200){
        successToast('Atributo eliminado', data.data)
        getAtributes($('#family-name').attr('data-idfamilia'))
    }else{
        
    }
}


async function updateAtribute(idAtributo, name){
    
    let route = baseUrl + 'manager/Customizing/updateAtribute/'
    let formData = new FormData();

    formData.append('idAtributo', idAtributo)
    formData.append('atribute', name)

    const response = await fetch(route, {
                                    method: 'POST',
                                    body: formData
                                })
    
    let data = await response.json()
    //show in console the response data
    console.log(data.data)
    
    if(response.status == 200){
        successToast('Atributo modificado', data.data)
        getAtributes($('#family-name').attr('data-idfamilia'))
    }else{
        
    }
}

async function deleteValue(id_atributo_loaded, value){

    let route = baseUrl + 'manager/Customizing/deleteValue/'
    let formData = new FormData();

    formData.append('value', value)
    formData.append('idAtributo', id_atributo_loaded)

    const response = await fetch(route, {
                                    method: 'POST',
                                    body: formData
                                })
    
    let data = await response.json()
    //show in console the response data
    console.log(data.data)
    
    if(response.status == 200){
        successToast('Valor eliminado', data.data)
        loadValues(id_atributo_loaded)
    }else{
        
    }
}

async function updateValue(id_atributo_loaded, updatedValue, oldValue){

    let route = baseUrl + 'manager/Customizing/updateValues/'
    let formData = new FormData();

    formData.append('newValue', updatedValue)
    formData.append('oldValue', oldValue)
    formData.append('idAtributo', id_atributo_loaded)

    const response = await fetch(route, {
                                    method: 'POST',
                                    body: formData
                                })
    
    let data = await response.json()
    console.log(data.data)
    
    if(response.status == 200){
        successToast('Valor modificado', data.data)
        loadValues(id_atributo_loaded)
    }else{
        
    }

}

async function addAtribute(idFamilia, name){
        
        let route = baseUrl + 'manager/Customizing/addAtribute/'

        let formData = new FormData()
        formData.append('idFamilia', idFamilia)
        formData.append('atribute', name)

        const response = await fetch(route, {
                                        method: 'POST', 
                                        body: formData
                                    })
    
        if(response.status == 200){
            successToast('Atributo agregado', 'Se ha agregado el atributo correctamente')
            getAtributes(idFamilia)
            $('#createAtribute').val('')
        }else{
            errorToast('Error', 'No se pudo agregar el atributo')
        }
}

async function addFamily(name){
    
    let route = baseUrl + 'manager/Customizing/addFamily/'

    let formData = new FormData()
    formData.append('familyName', name)

    const response = await fetch(route, {method: 'POST', body: formData})

    if(response.status == 200){
        successToast('Familia agregada', 'Se ha agregado la familia correctamente')
        loadFamiliesAsync()
        $('#createFamily').val('')
    }else{
        errorToast('Error', 'No se pudo agregar la familia')
    }

}

async function loadFamiliesAsync(){

    var post       = baseUrl + 'manager/Customizing/getFamilies'

    const response  = await fetch(post, {method: 'POST'})
    const registros = await response.json()

    var list = ""
    registros.families.forEach( data => {
        if(data.idFamilia == 0){ return }

        list += `<tr data-id="${data.idFamilia}">
                    <td><i class="handle fas fa-grip-vertical"></i></td>
                    <td>${data.idFamilia}</td>
                    <td>${data.nombre}</td>
                    <td class="px-0">
                        <button type="button" data-idatributo="${data.idFamilia}" data-value="${data.nombre}" class="btn-edit-family btn btn-outline bg-blue-400 text-blue-800 btn-icon rounded-round"><i class="icon-pencil"></i></button>
                        <button type="button" data-idatributo="${data.idFamilia}" data-value="${data.nombre}" class="btn-delete-family btn btn-outline bg-pink-400 text-pink-800 btn-icon rounded-round"><i class="icon-trash"></i></button>
                        <button type="button" class="btn-show-atributes btn btn-outline bg-success-400 text-success-800 rounded-round" data-nombre="${data.nombre}" data-idfamilia="${data.idFamilia}"><i class="icon-list-unordered mr-2"></i>Atributos</button>                                    
                    </td>
                </tr>`
    })
    
    $('#familyList').empty().append(list)
} 

async function loadMarcasAsync(){

    var post       = baseUrl + 'manager/Customizing/getMarcas'

    const response  = await fetch(post, {method: 'POST'})
    const registros = await response.json()

    var list = ""
    registros.marcas.forEach( data => {
        //if(data.idMarca == 0){ return }

        list += `<tr data-id="${data.idMarca}">
                    <td><i class="handle fas fa-grip-vertical"></i></td>
                    <td>${data.idMarca}</td>
                    <td>${data.nombre}</td>
                    <td class="px-0">
                        <button type="button" data-idmarca="${data.idMarca}" data-value="${data.nombre}" class="btn-edit-marca btn btn-outline bg-blue-400 text-blue-800 btn-icon rounded-round"><i class="icon-pencil"></i></button>
                        <button type="button" data-idmarca="${data.idMarca}" data-value="${data.nombre}" class="btn-delete-marca btn btn-outline bg-pink-400 text-pink-800 btn-icon rounded-round"><i class="icon-trash"></i></button>                                  
                    </td>
                </tr>`
    })
    
    $('#marcasList').empty().append(list)
}


async function loadAtributesAsync(idFamilia){
    $('#atributes-card').addClass('d-none')
    $('#values-card').addClass('d-none')
	await getAtributes(idFamilia)

}

async function getAtributes(idFamilia){

    var post       = baseUrl + 'manager/Customizing/getAtributes/' + idFamilia

    const response = await fetch(post, {method: 'POST'})
    const registros = await response.json()
    
    var list = ""
    registros.atributos.forEach( data =>{
        list += `<tr>
                    <td>${data.idAtributo}</td>
                    <td>${data.nombre}</td>
                    <td class="px-0">
                        <button type="button" data-idatributo="${data.idAtributo}" data-value="${data.nombre}" class="btn-edit-attr btn btn-outline bg-blue-400 text-blue-800 btn-icon rounded-round"><i class="icon-pencil"></i></button>
                        <button type="button" data-idatributo="${data.idAtributo}" data-value="${data.nombre}" class="btn-delete-attr btn btn-outline bg-pink-400 text-pink-800 btn-icon rounded-round"><i class="icon-trash"></i></button>
                        <button type="button" class="btn-show-values btn btn-outline bg-success-400 text-success-800 rounded-round" data-idatributo="${data.idAtributo}" data-values='${data.valor.split(",")}' ><i class="icon-list-unordered mr-2"></i>Valores</button>                                    
                    </td>
                </tr>`
    })
       
    $('#atributes-list').empty().append(list)
    $('#atributes-card').removeClass('d-none')
} 

async function loadValues(idAtributo, valuesList){

    var list = ""

    const response = await fetch(baseUrl + 'manager/Customizing/getAtributesValues/' + idAtributo, {method: 'GET'})
    const result = await response.json()

    result.values.forEach( (val) => {
        list += `<tr>
                    <td>${val}</td>
                    <td class="px-0">
                        <button data-value="${val}" type="button" class="btn-edit-value btn btn-outline bg-blue-400 text-blue-800 btn-icon rounded-round"><i class="icon-pencil"></i></button>
                        <button data-value="${val}" type="button" class="btn-delete-value btn btn-outline bg-pink-400 text-pink-800 btn-icon rounded-round"><i class="icon-trash"></i></button>                                    
                    </td>
                </tr>`
    })
    
    valuesList = result.values
    $('#values-list').empty().append(list)
	$('#values-card').removeClass('d-none')

}
 
async function addValue(idAtributo, newValue){

    let route = baseUrl + 'manager/Customizing/addValue/' + idAtributo + '/' + newValue

    const response = await fetch(route, {method: 'POST'})
    // const result = await response.json()

    if(response.status == 200){
        successToast('Valor agregado', 'Se ha agregado el valor correctamente')
        loadValues(idAtributo)
        $('#createValue').val('')
    }else{
        errorToast('Error', 'No se pudo agregar el valor')
    }

}

async function addMarca(name){
    
    let route = baseUrl + 'manager/Customizing/addMarca/'

    let formData = new FormData()
    formData.append('marcaName', name)

    const response = await fetch(route, {method: 'POST', body: formData})
    if(response.status == 200){
        successToast('Marca agregada', 'Se ha agregado la marca correctamente')
        loadMarcasAsync()
        $('#createMarca').val('')
    }else{
        dangerToast('Error', 'No se pudo agregar la marca')
    }

}

async function updateMarca(idMarca, name){

    let route = baseUrl + 'manager/Customizing/updateMarca/'
    let formData = new FormData();

    formData.append('idMarca', idMarca)
    formData.append('marcaName', name)

    const response = await fetch(route, {
                                    method: 'POST',
                                    body: formData
                                })
    
    let data = await response.json()
    //show in console the response data
    console.log(data.data)
    
    if(response.status == 200){
        successToast('Marca modificada', data.data)
        loadMarcasAsync()
    }else{
        dangerToast('Error', 'No se pudo actualizar la marca')
    }

}

async function updateOrdenMarca(sorts){
    let route = baseUrl + 'manager/Customizing/updateOrdenMarca/'
    let formData = new FormData();

    formData.append('sorts', sorts)
    const response = await fetch(route, {
                                method: 'POST',
                                body: formData
                            })
    
    let data = await response.json()
    
    if(response.status == 200){
        successToast('Marca:', data.data)
        loadMarcasAsync()
    }else{
        dangerToast('Error', 'No se pudo actualizar el orden de las marcas')
    }
}


async function deleteMarca(idMarca){
        
        let route = baseUrl + 'manager/Customizing/deleteMarca/'
        let formData = new FormData();
    
        formData.append('idMarca', idMarca)
    
        const response = await fetch(route, {
                                        method: 'POST',
                                        body: formData
                                    })
        
        let data = await response.json()
        
        if(data.status == 200){
            successToast('Marca eliminada', data.data)
            loadMarcasAsync()
        }else{
            dangerToast('Error', 'No se pudo eliminar la marca')
        }
}

