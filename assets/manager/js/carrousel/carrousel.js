document.addEventListener('DOMContentLoaded', function() {
    $('.form-check-input-styled').uniform();
    loadCarrouselAsync()

    $(document).on('click','#btnCreateCarrousel',function(){
        let title = $('#carrouselTitle').val()  
        addCarrousel(title)
    });

    $(document).on('change','.is_carrousel_active',function(){
        let idCarrousel = $(this).data('id')
        let value = $(this).is(':checked') ? 1 : 0
        updateCarrousel(idCarrousel, value)
    });



    async function updateCarrousel(id, value){
        let route = baseUrl + 'manager/Carrousel/update/'
        let formData = new FormData()
        formData.append('id', id)
        formData.append('activo', value)
        const response = await fetch(route, {method: 'POST', body: formData})

        if(response.status == 200){
            successToast('Carrusel actualizado', 'Se ha actualizado el carrusel correctamente')
        }else{
            dangerToast('Error', 'No se pudo actualizar el carrusel')
        }
    }

    async function addCarrousel(title){
        if (title === '') return;
        
        let route = baseUrl + 'manager/Carrousel/store/'
    
        let formData = new FormData()
        formData.append('titulo', title)
        formData.append('activo', 1)
        formData.append('label', title)
    
        const response = await fetch(route, {method: 'POST', body: formData})
        if(response.status == 200){
            successToast('Carrusel agregado', 'Se ha agregado el carrusel correctamente')
            loadCarrouselAsync()
            $('#carrouselTitle').val('')
        }else{
            dangerToast('Error', 'No se pudo agregar el carrusel')
        }
    }

    async function loadCarrouselAsync(){

        var post       = baseUrl + 'manager/Carrousel/getData'
    
        const response  = await fetch(post, {method: 'POST'})
        const registros = await response.json()
    
        var list = ""
        registros.forEach( data => {
            if(data.id == 0){ return }
    
            list += `<tr data-id="${data.id}">
                        <td>${data.id}</td>
                        <td>${data.titulo}</td>
                        <td class="input-cell">
                            <input id="is_carrousel_active_${data.id}" 
                                   data-id="${data.id}"
                                   class="is_carrousel_active"
                                   name="is_carrousel_active_${data.id}" 
                                   type="checkbox" 
                                   ${data.activo == 1 ? 'checked' : ''}
                                   >                            
                        </td>
                        <td class="px-0">
                            <button type="button" data-idatributo="${data.id}" data-value="${data.titulo}" class="btn-edit-carrousel btn btn-outline bg-blue-400 text-blue-800 btn-icon rounded-round"><i class="icon-pencil"></i></button>
                            <button type="button" data-idatributo="${data.id}" data-value="${data.titulo}" class="btn-delete-carrousel btn btn-outline bg-pink-400 text-pink-800 btn-icon rounded-round"><i class="icon-trash"></i></button>
                        </td>
                    </tr>`
        })
        
        $('#carrouselList').empty().append(list)
    }     
    
});