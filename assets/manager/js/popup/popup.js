
var img = ""

document.addEventListener('DOMContentLoaded', function() {
    
    $('.form-check-input-styled').uniform();

    fill_data()

    $('#img-place').on('click', function(){
        $('#img-popup').click()
    }) 

    $('#btn-up-popup').on('click', function(){
            $('#img-popup').click()
    })

    $('#img-popup').on('change', function(){
        var upload = uploadImage($(this))
    })

    $('#btn-save-popup').on('click', function(){
        update_popup()
    })

});


function fill_data(){
    var post = baseUrl + 'manager/Popup/getAll' 

    fetch(post, {method:'POST'})
    .then(data=>data.json())
    .then(data=>{
        if(data.status == 200){
            $('#url').val(data.data.url)
            
            if(data.data.active == '1'){
                $('#active').prop('checked', true).uniform(); 
            }else{
                $('#active').prop('checked', false).uniform(); 
            }

            if(data.data.image != ""){
                $('#image-preview').empty()
                let preview = `<img class="img-thumb h-20 w-20" src="${baseUrl}${data.data.image}"></img>`
                $('#image-preview').append(preview)
            }

            // successToast(data.title, data.message)
        }
    })

}

function update_popup(){
    var post = baseUrl + 'manager/Popup/update' 
    
    var active   = $('#active').parent().hasClass('checked') ? '1' : '0'
    var data = new URLSearchParams({
                    active: active,
                    url: $('#url').val()
                })

    fetch(post, {method:'POST', body:data})
    .then(data=>data.json())
    .then(data=>{
        if(data.status == 200){
            successToast(data.title, data.message)
        }
    })

}

function uploadImage(e){

    var post       = baseUrl + 'manager/Popup/uploadImage'
    var file       = e[0]
    var fileName   = file.files[0]['name'].split('.')
    var extension  = fileName.pop()

    // Limpia el nombre de archivo
    fileName = fileName[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '').replace(/ /g, "-").normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase()
    var newName = fileName + '.' + extension
    
    var fullpath = `uploads/popup/${newName}`

    var data = new FormData()
        data.append('fileName', newName)
        data.append('file', file.files[0])
        data.append('name', 'popup')

        fetch(post, {
            method: 'POST',
            body: data
        })
        .then(data=>data.json())
        .then(data=>{
            if(data.status===200){
                $('#image-preview').empty()
                let preview = `<img class="img-thumb h-20 w-20" src="${baseUrl}${fullpath}"></img>`
                $('#image-preview').append(preview)
                successToast(data.title, data.message) 
            }else{
                /*Armamos el mensaje toast*/
                dangerToast(data.title, data.message) 
            }
        })
        .catch(error=>{
            /*Armamos el mensaje toast*/	
            dangerToast(error.title, error.message) 
        })    

}




