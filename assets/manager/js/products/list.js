var baseUrl = $("body").data('base_url');
var table = null;

var unblockConfig = {
    message: '<p><i class="fa fa-cog fa-spin fa-2x"></i> <br><b>Actualizando Stock</b></p>',
      css:{
          backgroundColor: 'transparent',
          border: 'none', 
      },
      overlayCSS: {
          backgroundColor: '#FFF',
      }
};

window.addEventListener('load', reviewInit())

function reviewInit() {

    $('#filter_promo_active').uniform()

    loadProductsDatatable()
    set_select2_filter()
    
    $('#set-filtro').click(function(){
        table.ajax.reload(null, false)
        table.page('first').draw('page')
    })

	$('#btn-clear-flt').click(function(){
        clearFilter()
    })    

    $('#updateStock').click(function(){
        updateStock()
    })
    $('#updatePrecioBase').click(function(){
        updatePrecioBase()
    })
    $('#updatePrecioPublico').click(function(){
        updatePrecioPublico()
    })

}

function loadProductsDatatable(){
    table =  $('#products-list').DataTable({
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: 	'<span>Buscar:</span> _INPUT_',
                searchPlaceholder: 'Buscar...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 
                            'last':  'Last', 
                            'next':  $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 
                            'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' },
                info: 		'Páginas _PAGE_ de _PAGES_'
            },
            "processing": true,
            "serverSide": true,
            "order": [[ 0, 'desc' ]],
            ajax:{
                url : baseUrl + "manager/Products/get_list_dt", // json datasource
                type: "post", 
                data: function(data){
                    return JSON.stringify({
                        draw: 	   data.draw,
                        start: 	   data.start,
                        length:    data.length,
                        order: 	   data.order,
                        search:    data.search,
                        idFamilia: $('#filter_familia').val(),
                        idMarca:   $('#filter_marca').val(),
                        show:      $('#filter_show').val(),
                        promo:     $('#filter_promo_active').is(':checked') ? '1' : '0'
                    })
                },	
                error: function(error) {
                    console.log(error)
                }
            },
            "columnDefs": [
                    //{ className: "text-right", "targets": [ 4 ] },
                    { className: "text-center", "targets": [ 0,1,2,3,5 ] }
                ]
    })
}

function loadFilterData(){
    
}

function clearFilter(){
    $('.filter-select').val(null).trigger('change')
    $('#filter_promo_active').parent().removeClass('checked')
    set_select2_filter()
    table.ajax.reload();    
}

function set_select2_filter(){

	$('#filter_familia').select2({
        minimumResultsForSearch: -1,
        placeholder: "Seleccione una Familia"
    })

	$('#filter_marca').select2({
        minimumResultsForSearch: -1,
        placeholder: "Seleccione una Marca"
    })

	$('#filter_show').select2({
        minimumResultsForSearch: -1,
        placeholder: "Seleccione un Valor"
    })

}

/* async function updateStock(){

    // Modificar por FETCH.. Modo Asincronico

    let post = baseUrl + 'manager/Products/update_stock'

    const a = await fetch(post,{method: 'POST'})
    const b = await a.json()

    if(b.code == 200 ){
		console.log(b)
  	}    

} */

// async function updateStock(){

//     $.blockUI(unblockConfig); 
//     var post = baseUrl + "manager/Products/update_stock";
    
//     fetch(post, {
//         method: 'POST', 
//     })
//     .then(data=>data.json())
//     .then(data=>{ 
//         $.unblockUI();
//         if(data.code == 409){
//             warningToast( null, data.message )
//         }
//         if(data.code == 200){
//             successToast( 'Stock: ', data.message )
//         }
//     })
//     .catch(error=>{
//         $.unblockUI(); 
//         dangerToast( null, 'No se ha podido realizar el proceso de actualización.' )
//     })
// }

// async function updatePrecioBase(){

//     $.blockUI(unblockConfig); 
//     var post = baseUrl + "manager/Products/update_base_price";
    
//     fetch(post, {
//         method: 'POST', 
//     })
//     .then(data=>data.json())
//     .then(data=>{ 
//         $.unblockUI();
//         if(data.code == 409){
//             warningToast( null, data.message )
//         }
//         if(data.code == 200){
//             successToast( 'Stock: ', data.message )
//         }
//     })
//     .catch(error=>{
//         $.unblockUI(); 
//         dangerToast( null, 'No se ha podido realizar el proceso de actualización.' )
//     })
// }

// async function updatePrecioPublico(){

//     $.blockUI(unblockConfig); 
//     var post = baseUrl + "manager/Products/update_public_price";
    
//     fetch(post, {
//         method: 'POST', 
//     })
//     .then(data=>data.json())
//     .then(data=>{ 
//         $.unblockUI();
//         if(data.code == 409){
//             warningToast( null, data.message )
//         }
//         if(data.code == 200){
//             successToast( 'Stock: ', data.message )
//         }
//     })
//     .catch(error=>{
//         $.unblockUI(); 
//         dangerToast( null, 'No se ha podido realizar el proceso de actualización.' )
//     })
// }

// Funciones de actualización específicas

function updateStock() {
    startUpdate('stock');
}

function updatePrecioBase() {
    startUpdate('base_price');
}

function updatePrecioPublico() {
    startUpdate('public_price');
}

async function updateOneStock(sku){
    $.blockUI(unblockConfig); 
    var post = baseUrl + "manager/Product/update_stock";
    var data = new URLSearchParams({        
        sku:     sku
    });

    fetch(post, {
        method: 'POST', 
        body:data
    })
    .then(data=>data.json())
    .then(data=>{ 
        $.unblockUI();
        if(data.code == 200){
            successToast( 'Stock: ', data.message )
        }else{
            dangerToast( null, "No se ha podido realizar el proceso de actualización." )
        }
    })
}
$(document).on('click', '.is_promo_active', function (e) {
    
    e.preventDefault();

    if ($(this).is(':checked')) {
        var active = "1";
    } else {
        var active = "0";
    }

    updatePromo(this, $(this).val(), active)

    return;

})

async function updatePromo(e, idProducto, is_promo_active){
    var url = baseUrl + 'manager/product/update_promo/'+ idProducto + '/' + is_promo_active
    await fetch(url, {
        method: 'GET'
    })
    .then(data => data.json())
    .then(data => { 
        if(data.code == 1){
            successToast('Producto', data.message)
            if(is_promo_active == 1){
                $(e).prop('checked', true)
            }else{
                $(e).prop('checked', false)
            }

            return true;
        }else{
            dangerToast('Producto', data.message)
            
            return false;
        }
    })
}


$(document).on('click', '.visibilidad', function (e) {
    e.preventDefault();
    if ($(this).is(':checked')) {
        var active = "publicar";
    } else {
        var active = "oculto";
    }
    $(this).prop('checked', false)

    var url = baseUrl + 'manager/product/update_visibilidad/' + $(this).val() + '/' + active
    fetch(url, {
        method: 'GET'
    })
        .then(data => data.json())
        .then(data => {
            if (data.code == 1) {
                successToast('Producto', data.message)
                if (active !== 'publicar') {
                    $(this).prop('checked', false);
                } else {
                    $(this).prop('checked', true);
                }
            } else {
                dangerToast('Producto', data.message)
            }
        })
        .catch(error => {
            dangerToast('Producto', error.message)
        }) 
});

async function startUpdate(updateType) {
    try {
        const response = await fetch(`${baseUrl}manager/Products/start_update/${updateType}`, {
            method: 'POST'
        });
        const data = await response.json();

        if (response.status === 202) {
            successToast(updateType, data.message);
            pollUpdateStatus(updateType);

        } else {
            warningToast(null, data.message);
        }
    } catch (error) {
        dangerToast(null, 'No se ha podido iniciar el proceso de actualización.');
    }
}


function pollUpdateStatus(updateType) {
    $.blockUI({
        message: '<p><i class="fa fa-cog fa-spin fa-2x"></i> <br><b>Actualizando...</b></p><div id="update-progress"></div>',
        css: {
            backgroundColor: 'transparent',
            border: 'none',
        },
        overlayCSS: {
            backgroundColor: '#FFF',
        }
    });

    const intervalId = setInterval(async () => {
        try {
            const response = await fetch(`${baseUrl}manager/Products/check_update_status/${updateType}`);
            const data = await response.json();

            if (data[`${updateType}_update_status`] === 'false') {
                clearInterval(intervalId);
                $.unblockUI();
                successToast(updateType, 'Actualización completada');
            } else {
                const processed = parseInt(data[`${updateType}_update_processed`]);
                const total = parseInt(data[`${updateType}_update_total`]);
                updateProgressUI(processed, total);
            }
        } catch (error) {
            clearInterval(intervalId);
            $.unblockUI();
            dangerToast(null, 'Error al consultar el estado de la actualización');
        }
    }, 5000); // Consulta cada 5 segundos
}

function updateProgressUI(processed, total) {
    // const percentage = Math.min((processed / total) * 100, 100).toFixed(2);
    // $('#update-progress').html(`Progreso: ${percentage}% (${processed}/${total})`);
}