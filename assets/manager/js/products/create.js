// var form_new_member = $("#formEditProduct").validate({
//     ignore: "input[type=hidden], .select2-search__field", // ignore hidden fields
//     errorClass: "validation-invalid-label",
//     successClass: "validation-valid-label",
//     validClass: "validation-valid-label",
//     highlight: function (element, errorClass) {
//       $(element).removeClass(errorClass);
//     },
//     unhighlight: function (element, errorClass) {
//       $(element).removeClass(errorClass);
//     },
//     success: function (label) {
//       label.addClass("validation-valid-label").text("OK"); // remove to hide Success message
//     },
//     // Different components require proper error label placement
//     errorPlacement: function (error, element) {
//       // Unstyled checkboxes, radios
//       if (element.parents().hasClass("form-check")) {
//         error.appendTo(element.parents(".form-check").parent());
//       }
//       // Input with icons and Select2
//       else if (
//         element.parents().hasClass("form-group-feedback") ||
//         element.hasClass("select2-hidden-accessible")
//       ) {
//         error.appendTo(element.parent());
//       }
//       // Input group, styled file input
//       else if (
//         element.parent().is(".uniform-uploader, .uniform-select") ||
//         element.parents().hasClass("input-group")
//       ) {
//         error.appendTo(element.parent().parent());
//       }
//       // Other elements
//       else {
//         error.insertAfter(element);
//       }
//     },
//     rules: {
//       modelo: { required: true },
//       precio: { required: true },
//       sku: { required: true },
//       slug: { required: true },
//       cantidad_minima: { required: true, mayorACero: true },
//       rango: { required: true, mayorACero: true },
//     },
//     messages: {
//       modelo: { required: "Este campo es requerido" },
//       precio: { required: "Este campo es requerido" },
//       sku: { required: "Este campo es requerido" },
//       slug: { required: "Este campo es requerido" },
//       cantidad_minima: {
//         required: "Este campo es requerido",
//         mayorACero: "La cantidad mínima debe ser mayor a cero.",
//       },
//       rango: {
//         required: "Este campo es requerido",
//         mayorACero: "El rango debe ser mayor a cero.",
//       },
//     },
//   });
  
  // var img = []
  
  $(document).ready(function () {
  
    $('.form-check-input-styled').uniform();
  
    // Método de validación personalizada para verificar que el valor sea mayor a cero
    // $.validator.addMethod(
    //   "mayorACero",
    //   function (value, element) {
    //     return this.optional(element) || parseInt(value) > 0;
    //   },
    //   "El valor debe ser mayor a cero."
    // );
  
    $("#idFamilia").change(function () {
      getAtributos();
    });
  
    set_select2();
    stock();
    fill_product_data();
    fill_images();
  
    // Boton Guardar
    $(document).on("click", "#btnUpdateProduct", function (e) {
      e.preventDefault();
  
      if ($("#formEditProduct").valid()) {
        updateProduct();
      } else {
        dangerToast("Error", "Faltan campos por completar!");
      }
    });
  
    $("#btnUploadImage").on("click", function (e) {
      e.preventDefault();
      $("#imagen").click();
    });
  
    $("#imagen").on("change", function () {
      uploadImage($(this));
    });
  
    $(document).on("click", "#btnDeleteImage", function (e) {
      e.preventDefault();
      var index = img.indexOf($(this).data("img"));
      img.splice(index, 1);
  
      //TODO: Llamar al controlador para borrar la imagen fisica, actualizar el campo imagen
      deleteImage($(this).data("img"));
      fill_images();
    });
  
    $("#clear-show").on("click", function (e) {
      e.preventDefault();
      $("#show").val("").trigger("change");
    });
  
    $("#update_stock").on("click", function (e) {
      e.preventDefault();
      sku = $("#sku").val();
      updateStock(sku);
    });
  
    $("#add-etiqueta").on("click", function (e) {
      e.preventDefault();
      $("#etiqueta").addClass("d-none");
      $("#etiqueta-input").removeClass("d-none");
    });
  
  });
  
  function set_select2() {
    $("#idFamilia").select2({
      minimumResultsForSearch: -1,
      placeholder: "Seleccione la Familia",
    });
  
    $("#idMarca").select2({
      minimumResultsForSearch: -1,
      placeholder: "Seleccione la Marca",
    });
  
    $("#idTipo").select2({
      minimumResultsForSearch: -1,
      placeholder: "Seleccione el tipo de Producto",
    });
  
    $("#stock").select2({
      minimumResultsForSearch: -1,
      placeholder: "Seleccione el Stock",
    });
  
    $("#show").select2({
      minimumResultsForSearch: -1,
      placeholder: "Seleccione una opción",
    });
  
    $("#etiqueta").select2({
      tags: true,
      minimumResultsForSearch: -1,
      placeholder: "Seleccione una Etiqueta",
      maximumSelectionLength: 1,
    });
  }
  
  function fill_product_data() {
    // createBadge(producto.search);
    // $("#titulo").empty().html(`ID: ${producto.idProducto} - ${producto.nombre}`);
    // $("#idProducto").val(producto.idProducto);
    // $("#nombre").val(producto.nombre);
    // $("#idFamilia").val(producto.idFamilia).trigger("change");
    // $("#idMarca").val(producto.idMarca).trigger("change");
    // $("#modelo").val(producto.modelo);
    // $("#precio").val(producto.precio);
    // $("#price_public").val(producto.price_public);
    // $("#idTipo").val(producto.idTipo).trigger("change");
    // $("#visibilidad").val(producto.visibilidad).trigger("change");
    // $("#sku").val(producto.sku);
    // $("#slug").val(producto.slug);
    // //$('#search').val(producto.search)
    // $("#stock").val(producto.stock).trigger("change");
    // $("#descripcion").val(producto.descripcion);
    // $("#show").val(producto.show).trigger("change");
  
    // $("#stock_type").val(producto.stock_type).trigger("change");
    // $("#stock_fijo").val(producto.stock_fijo).trigger("change");
    // $("#stock_quantity").val(producto.stock_quantity).trigger("change");
    // $("#stock_limit1").val(producto.stock_limit1).trigger("change");
    // $("#stock_limit2").val(producto.stock_limit2).trigger("change");
  
    // $("#cantidad_minima").val(producto.cantidad_minima);
    // $("#rango").val(producto.rango);
  
    // $("#peso").val(producto.peso);
    // $("#alto").val(producto.alto);
    // $("#ancho").val(producto.ancho);
    // $("#largo").val(producto.largo);
  
    // if(producto.idTag != null){
    //   $("#etiqueta").val(producto.tag_name).trigger("change");
    // }
  
    // // $("#is_promo_active").val(producto.is_promo_active);
    // $("#price_promo").val(producto.price_promo);
  
    // if(producto.is_promo_active == '1'){
    //   $('#is_promo_active').prop('checked', true).uniform(); 
    // }else{
    //   $('#is_promo_active').prop('checked', false).uniform(); 
    // }
  
  
    // setLabelStock();
  }
  
  function fill_images() {
    // $("#thumbs").empty();
    // // if(typeof(img) != 'undefined'){
    // if (Array.isArray(img)) {
    //   img.forEach((imagen) => {
    //     var thumb = `<div class="img-thumb-box">
    //                           <div class="button-container">
    //                               <button id="btnDeleteImage" data-img="${imagen}" class="btn btn-danger rounded-round"><i class='icon-trash mr-2'></i>Eliminar</button>
    //                           </div>
    //                           <img class="img-thumb" src="${baseUrl}${imagen}">
    //                       </div>`;
    //     $("#thumbs").append(thumb);
    //   });
    // }
  }
  
  async function updateProduct() {
    var post = baseUrl + "manager/Product/updateProduct";
  
    var nombre = `${$("#idFamilia option:selected").text()} ${$(
      "#idMarca option:selected"
    ).text()} ${$("#modelo").val()}`;
  
    var atributos = [];
  
    $("#attr-list")
      .find("select")
      .each(function (i, elemento) {
        var atributo = {
          idProdAtrib: $(this).attr("data-idprodatrib"),
          idProducto: $("#idProducto").val(),
          idAtributo: $(this).data("idatributo"),
          valores: $(this).val(),
        };
  
        atributos.push(atributo);
      });
  
    var data = new URLSearchParams({
      idProducto: $("#idProducto").val(),
      nombre: nombre, //$('#nombre').val(),
      idFamilia: $("#idFamilia").val(),
      idMarca: $("#idMarca").val(),
      modelo: $("#modelo").val(),
      precio: $("#precio").val(),
      price_public: $("#price_public").val(),
      idTipo: $("#idTipo").val(),
      sku: $("#sku").val(),
      slug: $("#slug").val(),
      search: obtainSearch(), //$('#search').val(),
      stock: $("#stock").val(),
      descripcion: $("#descripcion").val(),
      atributos: JSON.stringify(atributos),
      stock_type: $("#stock_type").val(),
      stock_fijo: $("#stock_fijo").val(),
      stock_quantity: $("#stock_quantity").val(),
      stock_limit1: $("#stock_limit1").val(),
      stock_limit2: $("#stock_limit2").val(),
      cantidad_minima: $("#cantidad_minima").val(),
      rango: $("#rango").val(),
      show: $("#show").val(),
      visibilidad: $("#visibilidad").val(),
      peso: $("#peso").val(),
      alto: $("#alto").val(),
      ancho: $("#ancho").val(),
      largo: $("#largo").val(),
      tags: $("#etiqueta").val(),
  
      is_promo_active: $('#is_promo_active').parent().hasClass('checked') ? '1' : '0',
      price_promo: $("#price_promo").val(),
  
    });
  
    const response = await fetch(post, {
      method: "POST",
      body: data,
    })
      .then((data) => data.json())
      .then((data) => {
        if (data.status == 200) {
          successToast(data.title, data.message);
        } else {
          /*Armamos el mensaje toast*/
          dangerToast(null, data.message);
        }
      })
      .catch((error) => {
        /*Armamos el mensaje toast*/
        dangerToast(null, error.message);
      });
  
    return response;
  }
  
  function uploadImage(e) {
    var post = baseUrl + "manager/Product/uploadImage";
    var file = $("#imagen")[0];
    var fileName = file.files[0]["name"].split(".");
    var extension = fileName.pop();
  
    // Limpia el nombre de archivo
    fileName = fileName[0]
      .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, "")
      .replace(/ /g, "-")
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "")
      .toLowerCase();
    var formattedDate = getFormattedDate();
    var newName =
      $("#idProducto").val() +
      "_" +
      formattedDate +
      "_" +
      fileName +
      "." +
      extension;
  
    var familia = $("#idFamilia option:selected").text().toLowerCase();
    var fullpath = `/resources/img/uploads/${familia}/${newName}`;
    img.push(fullpath);
  
    var data = new FormData();
    data.append("fileName", newName);
    data.append("file", file.files[0]);
    data.append("imagen", JSON.stringify(img));
    data.append("familia", familia);
    data.append("idProducto", $("#idProducto").val());
  
    fetch(post, {
      method: "POST",
      body: data,
    })
      .then((data) => data.json())
      .then((data) => {
        if (data.status === 200) {
          fill_images();
          successToast(data.title, data.message);
        } else {
          /*Armamos el mensaje toast*/
          dangerToast(data.title, data.message);
        }
      })
      .catch((error) => {
        /*Armamos el mensaje toast*/
        dangerToast(error.title, error.message);
      });
  }
  
  function deleteImage(remove) {
    var post = baseUrl + "manager/Product/deleteImage";
  
    var data = new URLSearchParams({
      idProducto: $("#idProducto").val(),
      imagen: JSON.stringify(img),
      remove: remove,
    });
  
    fetch(post, {
      method: "POST",
      body: data,
    })
      .then((data) => data.json())
      .then((data) => {
        if (data.status == 200) {
          successToast(data.title, data.message);
        } else {
          /*Armamos el mensaje toast*/
          dangerToast(null, data.message);
        }
      })
      .catch((error) => {
        /*Armamos el mensaje toast*/
        dangerToast(null, error.message);
      });
  }
  
  async function getAtributos() {
    var post = baseUrl + "manager/Product/getAtributos";
  
    var data = new URLSearchParams({
      idProducto: $("#idProducto").val(),
      idFamilia: $("#idFamilia").val(),
    });
  
    await fetch(post, {
      method: "POST",
      body: data,
    })
      .then((data) => data.json())
      .then((data) => {
        if (data.status == 200) {
          if ($("#idTipo").val() != 1) {
            render_atributos_simple(data);
          } else {
            render_atributos_multiple(data);
          }
        } else {
          /*Armamos el mensaje toast*/
          dangerToast(null, data.message);
        }
      })
      .catch((error) => {
        /*Armamos el mensaje toast*/
        dangerToast(null, error.message);
      });
  }
  
  function render_atributos_simple(data) {
    var list_attr = "";
  
    //Se recorre el array data.atributos que tiene los atributos por Familia
    if (!Array.isArray(data.atributos)) {
      return;
    }
  
    data.atributos.forEach((attr) => {
      // Se arman las options posibles de cada atributo
      var valores = JSON.parse(attr.valor);
      var options = "<option></option>";
  
      if (Array.isArray(valores)) {
        valores.forEach((valor) => {
          options += `<option value="${valor}">${valor}</option>`;
        });
      }
  
      // Se renderiza el select de cada atributo
      list_attr += `<div class="col-md-3">
                          <label for="${attr.nombre}">${attr.nombre}</label>
                          <select id="attr_${attr.idAtributo}" 
                                  name="${attr.nombre}" 
                                  data-idatributo="${attr.idAtributo}" 
                                  data-idprodatrib="${attr.idProdAtrib}"
                                  class="form-control select2-attrs"">
                          ${options}
                          </select>
                          </div>`;
    });
  
    $("#attr-list").empty().append(list_attr);
  
    $(".select2-attrs").select2({
      placeholder: "Seleccione un valor",
      minimumResultsForSearch: -1,
    });
  
    // Se setean los valores del producto en cada select de atributo
    if (Array.isArray(data.valores)) {
      data.valores.forEach((valor) => {
        var element = `attr_${valor.idAtributo}`;
        $("#" + element)
          .val(valor.valores)
          .trigger("change");
        $("#" + element).attr("data-idprodatrib", valor.idProdAtrib);
      });
    }
  }
  
  function render_atributos_multiple(data) {
    var list_attr = "";
  
    //Se recorre el array data.atributos que tiene los atributos por Familia
    if (!Array.isArray(data.atributos)) {
      return;
    }
  
    data.atributos.forEach((attr) => {
      // Se arman las options posibles de cada atributo
      var valores = JSON.parse(attr.valor);
      var options = "<option></option>";
      if (Array.isArray(valores)) {
        valores.forEach((valor) => {
          options += `<option value="${valor}">${valor}</option>`;
        });
      }
  
      // Se renderiza el select de cada atributo
      list_attr += `<div class="col-md-3">
                          <label for="${attr.nombre}">${attr.nombre}</label>
                          <select id="attr_${attr.idAtributo}" 
                                  name="${attr.nombre}" 
                                  data-idatributo="${attr.idAtributo}" 
                                  data-idprodatrib="${attr.idProdAtrib}"
                                  class="form-control select2-attrs"
                                  multiple="multiple">
                          ${options}
                          </select>
                          </div>`;
    });
  
    $("#attr-list").empty().append(list_attr);
  
    $(".select2-attrs").select2({
      tags: true,
      tokenSeparators: [","],
      placeholder: "Seleccione un valor",
      minimumResultsForSearch: -1,
    });
  
    if (Array.isArray(data.valores)) {
      data.valores.forEach((valor) => {
        var element = `attr_${valor.idAtributo}`;
        $("#" + element)
          .val(JSON.parse(valor.valores))
          .trigger("change");
        $("#" + element).attr("data-idprodatrib", valor.idProdAtrib);
      });
    }
  }
  
  function stock() {
    setLabelStock();
  
    $("#group-stock-fijo").toggleClass("d-none");
    $("#group-stock-formula").toggleClass("d-none");
  
    $("#stock_type").on("change", function () {
      $("#label-stock").empty();
      if ($("#stock_type").val() == "fijo") {
        $("#group-stock-fijo").removeClass("d-none");
        $("#group-stock-formula").addClass("d-none");
      } else {
        $("#group-stock-formula").removeClass("d-none");
        $("#group-stock-fijo").addClass("d-none");
      }
    });
  
    $("#stock_fijo").on("change", function () {
      setLabelStock();
    });
  
    $("#stock_quantity").on("change", function () {
      calculateStock();
    });
    $("#stock_limit1").on("change", function () {
      calculateStock();
    });
    $("#stock_limit2").on("change", function () {
      calculateStock();
    });
  }
  
  function setLabelStock() {
    if ($("#stock_type").val() == "fijo") {
      setColorStock($("#stock_fijo").val());
    } else if ($("#stock_type").val() == "formula") {
      calculateStock();
    }
  }
  
  function calculateStock() {
    c = parseInt($("#stock_quantity").val());
    l1 = parseInt($("#stock_limit1").val());
    l2 = parseInt($("#stock_limit2").val());
  
    if (c >= 0 && c <= l1) {
      setColorStock("bajo");
    } else if (c > l1 && c <= l2) {
      setColorStock("medio");
    } else if (c > l2) {
      setColorStock("alto");
    }
  }
  
  function setColorStock(value) {
    if (value == "bajo") {
      $("#label-stock")
        .empty()
        .append('<span class="badge bg-danger font-size-sm">STOCK: BAJO</span>');
    } else if (value == "medio") {
      $("#label-stock")
        .empty()
        .append('<span class="badge bg-yellow font-size-sm">STOCK: MEDIO</span>');
    } else if (value == "alto") {
      $("#label-stock")
        .empty()
        .append('<span class="badge bg-success font-size-sm">STOCK: ALTO</span>');
    }
  }
  
  async function updateStock(sku) {
    $.blockUI(unblockConfig);
    var post = baseUrl + "manager/Product/update_stock";
    var data = new URLSearchParams({
      sku: sku,
    });
  
    fetch(post, {
      method: "POST",
      body: data,
    })
      .then((data) => data.json())
      .then((data) => {
        $.unblockUI();
        if (data.code == 200) {
          successToast("Stock: ", data.message);
          $("#stock_quantity").val(data.data);
        } else {
          dangerToast(
            null,
            "No se ha podido realizar el proceso de actualización."
          );
        }
      });
  }
  
  $(document).on("click", "#btnDeleteProduct", function (e) {
    e.preventDefault();
    var id = $("#idProducto").val();
    var nombreProducto = `${$("#idFamilia option:selected").text()} ${$(
      "#idMarca option:selected"
    ).text()} ${$("#modelo").val()}`;
    Swal.fire({
      title: "¿Desea Eliminar el Producto?",
      text: "Producto: " + nombreProducto,
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Eliminar",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.isConfirmed) {
        var url = baseUrl + "manager/product/remove/" + id;
        fetch(url, {
          method: "GET",
        })
          .then((data) => data.json())
          .then((data) => {
            if (data.status == 200) {
              successToast(data.title, data.message);
              setTimeout(() => {
                window.location.href = baseUrl + "manager/Products";
              }, 2000);
            } else {
              dangerToast("Producto", data.message);
            }
          })
          .catch((error) => {
            dangerToast("Producto", error.message);
          });
      }
    });
  });
  
  function getFormattedDate() {
    const now = new Date();
    const year = now.getFullYear();
    const month = String(now.getMonth() + 1).padStart(2, "0"); // Meses de 0 a 11
    const day = String(now.getDate()).padStart(2, "0");
    const hours = String(now.getHours()).padStart(2, "0");
    const minutes = String(now.getMinutes()).padStart(2, "0");
    const seconds = String(now.getSeconds()).padStart(2, "0");
  
    return `${year}${month}${day}_${hours}${minutes}${seconds}`; // Formato: AAAAMMDD_HHMMSS
  }
  
  function addSearch() {
    const value = $("#search").val().trim();
    const badges = document.querySelectorAll("#badges-container .badge");
  
    if (value) {
      // Verificar si es repetido;
      const exists = Array.from(badges).some(
        (badge) => badge.textContent.toUpperCase() === value.toUpperCase()
      );
      if (exists) {
        dangerToast("Párametro", "El parametro ya fue ingresado previamente.");
      } else {
        const container = document.getElementById("badges-container");
  
        // Crear badge
        const badge = document.createElement("div");
        badge.className = "badge";
        badge.textContent = value;
  
        // Crear icono de trash
        const trashIcon = document.createElement("i");
        trashIcon.className = "fas fa-trash icon";
        trashIcon.onclick = () => {
          container.removeChild(badge); // Eliminar el badge del DOM
        };
  
        // Añadir el icono al badge
        badge.appendChild(trashIcon);
        container.appendChild(badge);
        successToast("Párametro", "Se ha ingresado correctamente el párametro.");
      }
  
      // Limpiar el input
      $("#search").val("");
    }
  }
  
  function createBadge(items) {
    // Separar el string en un array
    const searchItems = items.split(";");
  
    // Eliminar duplicados usando Set
    const uniqueItems = Array.from(new Set(searchItems));
  
    // Si quieres convertirlo de nuevo a un string separado por ';'
    const uniqueItemsString = uniqueItems.join(";");
  
    const container = document.getElementById("badges-container");
  
    const badges = uniqueItemsString.split(";");
    // Crear badges
    badges.forEach((item) => {
      const badge = document.createElement("div");
      badge.className = "badge";
      badge.textContent = item;
  
      // Crear icono de trash
      const trashIcon = document.createElement("i");
      trashIcon.className = "fas fa-trash icon";
      trashIcon.onclick = () => {
        container.removeChild(badge); // Eliminar el badge al hacer clic en el icono
      };
  
      // Añadir el icono al badge
      badge.appendChild(trashIcon);
      container.appendChild(badge);
    });
  }
  
  function obtainSearch() {
    const badges = document.querySelectorAll("#badges-container .badge");
    const badgeStrings = Array.from(badges).map((badge) => badge.textContent); // Obtener el texto de cada badge
  
    // Unir los textos separados por ';'
    const badgeString = badgeStrings.join(";");
    // Eliminar duplicados
    const uniqueBadges = Array.from(new Set(badgeString.split(";")));
  
    // Volver a unir los textos en un string
    const uniqueBadgeString = uniqueBadges.join(";");
  
    return uniqueBadgeString;
  }
      