// Setup module
// ------------------------------
var InputsBasic = function () {
    // Uniform
    var _componentUniform = function() {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }
        // Custom select
        $('.input-xls-file').uniform({
            fileButtonClass: 'action btn bg-blue',
            fileButtonHtml: 'Buscar'
            // selectClass: 'uniform-select bg-pink-400 border-pink-400'
        });
    };

    var _getTemplates = function(){

        var post = baseUrl + 'manager/Products/get_import_templates'

        fetch(post, {method:'POST'})
        .then(data => data.json())
        .then(data =>{
            var options = "<option></option>"
            if(data.status===200){
                data.families.forEach(family=>{
                    if(family.idFamilia > 0){
                        //options += `<option value="${baseUrl}${family.import_file}">${family.nombre}</option>`
                        options += `<option value="${baseUrl}manager/Products/templateFamilia/${family.idFamilia}">${family.nombre}</option>`
                    }
                    
                })
                $("#templates-list").append(options)
                
                $("#templates-list").select2({
                    placeholder: 'Seleccione una familia',
                    minimumResultsForSearch: -1,}) 
                
                    $('#templates-list').change(function(){

                    if($('#recupera_datos').val() === '1'){
                        $('#recupera_datos').val(0)
                        $('#marca-id').val('')
                        $('#marcas-list').addClass('d-none')
                    }

                    let url = $('#templates-list').val()
                    $('#template').attr('href',url)
                })                     
            }
        })

    }

    var _eventDownloadTemplate = function(){
        // $('#templates-list').change(function(){
        //     let url = $('#templates-list').val()
        //     $('#template').attr('href',url)
        // })
    }

    return {
        init: function() {
            _componentUniform();
            _getTemplates()
            _eventDownloadTemplate()
        }
    }
}();

var ImportFunction = function () {

    var _modalInsert = function() {
        
        $('#btn_modal_import').click(function(){
            import_file();
            $('#modal_import').modal('hide')
        });

        $('#btn_modal_import_simple').click(function(){
            import_file_simple();
            $('#modal_import_simple').modal('hide')
        });
    };

    return {
        init: function() {
            _modalInsert();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    InputsBasic.init();
    ImportFunction.init();
});

function import_file(){

    var post       = baseUrl + 'manager/Products/import'
    var file       = $("#modalInputFile")[0]
    var fileName   = file.files[0]['name'].split('.')
    var extension  = fileName.pop()
    
    // Limpia el nombre de archivo
    fileName = fileName[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '').replace(/ /g, "-").normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase()
    var newName = fileName + '.' + extension
    
    var data = new FormData()
    data.append('fileName', newName)
    data.append('file', file.files[0])
    
    fetch(post, {
        method: 'POST',
        body: data
    })
    .then(data=>data.json())
    .then(data=>{
        if(data.status===200){
            /*Armamos el mensaje toast*/
            successToast(data.title, data.message) 
        }else{
            /*Armamos el mensaje toast*/
            dangerToast(data.title, data.message) 
        }
    })
    .catch(error=>{
        /*Armamos el mensaje toast*/	
        dangerToast(error.title, error.message) 
    })
}

function import_file_simple(){

    var post       = baseUrl + 'manager/Products/importsimple'
    var file       = $("#modalInputFileSimple")[0]
    var fileName   = file.files[0]['name'].split('.')
    var extension  = fileName.pop()
    
    // Limpia el nombre de archivo
    fileName = fileName[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '').replace(/ /g, "-").normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase()
    var newName = fileName + '.' + extension
    
    var data = new FormData()
    data.append('fileName', newName)
    data.append('file', file.files[0])
    
    fetch(post, {
        method: 'POST',
        body: data
    })
    .then(data=>data.json())
    .then(data=>{
        if(data.status===200){
            /*Armamos el mensaje toast*/
            successToast(data.title, data.message) 
        }else{
            /*Armamos el mensaje toast*/
            dangerToast(data.title, data.message) 
        }
    })
    .catch(error=>{
        /*Armamos el mensaje toast*/	
        dangerToast(error.title, error.message) 
    })
}

//template_simple
$(document).on('click', '#template_simple', function (e) {
    e.preventDefault();
    window.location.href = baseUrl + "/uploads/templateUpload/templateImportarSimple.xlsx";
});


//$('#recupera_datos').change(function(){
$(document).on('change', '#recupera_datos', function (e) {
    $view_marca = $('#recupera_datos').val();

    if($view_marca == 1){
        $('#marca-id').val('')
        $('#marcas-list').removeClass('d-none')
    }else{
        $('#marcas-list').addClass('d-none')
        let url = $('#templates-list').val()
        $('#template').attr('href',url)
    }
});

$(document).on('change', '#marca-id', function (e) {
    let url = $('#templates-list').val()
    if(url != ''){
        let marca_id = $('#marca-id').val()
        $('#template').attr('href',url+'/'+marca_id)
    }else{
        $('#marca-id').val('')
        dangerToast('Error', 'Debe seleccionar una Familia') 
    }
})
//template_simple
/* $(document).on('click', '#template', function (e) {
    e.preventDefault();
    //window.location.href = baseUrl + "/uploads/templateUpload/templateImportarSimple.xlsx";
    let id = $('#templates-list').val()
    alert(id);
}); */

