document.addEventListener("load", detailInit());

function detailInit() {
	
	loadOptions();
	loadOrder();
	
	$("#chgst-to-prep").on('click', function(){
		$('#chgst-to-prep').addClass('d-none')
		changeStatus($('#order_idOrderHdr').html(), 3)
	})

	$("#chgst-to-fin").on('click', function(){
		$('#chgst-to-fin').addClass('d-none')
		changeStatus($('#order_idOrderHdr').html(), 4)
	})
}

function loadOptions(){

	$('#chgst-to-prep').addClass('d-none')
	$('#chgst-to-fin').addClass('d-none')
}

function loadOrder(){

	var post = baseUrl + "/manager/Orders/getOrderDetail/" + idOrderHdr;

	fetch(post, {
		method: "POST",
	})
	.then(data => data.json())
	.then(data => {
		if (data.status === 200) {
			/*Armamos el mensaje modal*/
			renderHeader(data.header)
			renderTableDetail(data.items)
		} else {
			/*Armamos el mensaje modal*/
			dangerToast(data.title, data.message);
		}
	})
	.catch(error => console.log("## Load error: " + error));
}


function renderHeader(data){

	$('#order_idOrderHdr').html(data.idOrderHdr)
	$('#order_cliente').html(data.razon_social)	
	$('#order_createdat').html(data.created)
	
	var status = `<span class="badge ${data.color} ">${data.statusText}</span>`

	$('#order_status').html(status)	

	if(data.idOrderStatus == 2 ){ //Nuevo
		$('#chgst-to-prep').removeClass('d-none')
	}else if(data.idOrderStatus == 3 ){ //En Preparacion
		$('#chgst-to-fin').removeClass('d-none')
		$('#chgst-to-prep').addClass('d-none')
	}

	// $('#order_total').html(data.total)
	// $('#order_cantidad').html(data.)

	$('#order_nota').html(data.nota)	
	

}

function renderTableDetail(data){

	var output = ""
	data.forEach(item => {
							output += `<tr>
											<td>${item.posnr}</td>
											<td>${item.sku}</td>
											<td>${item.fNombre}</td>
											<td>${item.mNombre}</td>
											<td>${item.pNombre}</td>
											<td>${item.qty}</td>
											<td>$ ${formatMoney2(item.precio)}</td>
											<td>$ ${formatMoney2(item.total)}</td>
										</tr>`
						})

	$('#detail-body').append(output)
}

async function changeStatus(id, status){

	var post = baseUrl + "/manager/Orders/changeOrderStatus/";
    
    var data = new URLSearchParams({        
		id: id,
		status: status
    });

    const response = await fetch(post, {method: 'POST',body:data })
    const res      = await response.json()

    if(res.status == 200 ){
		  console.log(res.message)
		  successToast('Bien Hecho!', res.message)

		  var tag_status = `<span class="badge ${res.header.color} ">${res.header.statusText}</span>`
		  $('#order_status').html(tag_status)	

		 if(status == 3){
			$('#chgst-to-fin').removeClass('d-none')
		 } 

	}else{
        iziToast.error({
            title: 'Error!',
            message: res.message,
        });   
    }

}


