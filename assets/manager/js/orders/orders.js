var baseUrl = $("body").data('base_url'); 
var table = null;


window.addEventListener('load', reviewInit())
	
function reviewInit() {
	
	set_select2_filter()
	getStatusList()
	getClientList()
	loadOrdersDatatable();

    $('#set-filtro').click(function(){
        table.ajax.reload()
    })

	$('#btn-clear-flt').click(function(){
        clearFilter()
    })

    $(document).on('click', '.btnChangeStatus', function (e) {
        e.preventDefault();
		const order = $(this)
		const title = `Cambiar estado del Pedido Nro: ${order.data('id')} ` 

        iziToast.question({
            timeout: 200000,
            close: false,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 9999,
            backgroundColor: '#FFB200',
            title: title,
            message: 'El Pedido se pasará a En Preparación',
            position: 'center',
            buttons: [
                    ['<button class="izi-btn-confirm">Continuar</button>', 
						function (instance, toast) {
							instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
							changeStatus(order)
						}, true],
						
                    ['<button class="izi-btn-cancel">Cancelar</button>', function (instance, toast) {
                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            }],
            ],
        });



    });

	$(document).on('click', '.btnChangeFinished', function (e) {
        e.preventDefault();
		const order = $(this)
		const title = `Cambiar estado del Pedido Nro: ${order.data('id')} ` 

        iziToast.question({
            timeout: 200000,
            close: false,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 9999,
            backgroundColor: '#FFB200',
            title: title,
            message: 'El Pedido se pasará a Finalizado',
            position: 'center',
            buttons: [
                    ['<button class="izi-btn-confirm">Continuar</button>', 
						function (instance, toast) {
							instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
							changeStatusFinished(order)
						}, true],
						
                    ['<button class="izi-btn-cancel">Cancelar</button>', function (instance, toast) {
                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            }],
            ],
        });
    });
	
}

function loadOrdersDatatable(){
	table =  $('#orders-list').DataTable({
			dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
			language: {
				search: 	'<span>Buscar:</span> _INPUT_',
				searchPlaceholder: 'Buscar...',
                lengthMenu: '<span>Show:</span> _MENU_',
				paginate: { 'first': 'First', 
							'last':  'Last', 
							'next':  $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 
							'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' },
				info: 		'Páginas _PAGE_ de _PAGES_'
			},
			"processing": true,
			"serverSide": true,
			"order": [[ 0, 'desc' ]],
			ajax:{
				url : baseUrl + "manager/Orders/get_list_dt", // json datasource
				type: "post", 
				data: function(data){
					return JSON.stringify({
						draw: 	data.draw,
						start: 	data.start,
						length: data.length,
						order: 	data.order,
						search: data.search,
						idcliente: $('#idcliente').val(),
						idorderstatus: $('#filter_status').val()						
					})
				},	
				error: function(error) {
					console.log(error)
				}
			},
			  "columnDefs": [
					{ className: "text-right", "targets": [ 4 ] },
					{ className: "text-center", "targets": [ 0,1,2,3,5 ] }
				]
	})
}

async function getStatusList(){

	var post = baseUrl + "/manager/Orders/getStatusList/";

    const response = await fetch(post, {method: 'POST'})
    const res      = await response.json()	

    if(res.code == 200 ){
		console.log(res.message)
		var options = "<option></option>"
		res.status.forEach(row=>{
			options += `<option value="${row.idOrderStatus}">${row.statusText}</option>`
		})			
		$('#filter_status').empty().append(options).attr("disabled", false)
  	}


}

async function getClientList(){

	var post = baseUrl + "/manager/Clients/getAll/";

    const response = await fetch(post, {method: 'POST'})
    const res      = await response.json()	

    if(res.status == 200 ){
		var options = "<option></option>"
		res.clientes.forEach(row=>{
			options += `<option value="${row.idCliente}">${row.razon_social}</option>`
		})			
		$('#idcliente').empty().append(options).attr("disabled", false)
  	}

}

function clearFilter(){
    $('.filter-select').val(null).trigger('change')
    set_select2_filter()
    table.ajax.reload();    
}

function set_select2_filter(){

	$('#idcliente').select2({
        placeholder: "Seleccione un Cliente"
    })

	$('#filter_status').select2({
        minimumResultsForSearch: -1,
        placeholder: "Seleccione un Estado"
    })

}

async function changeStatus(order){

    var post =  baseUrl + "manager/Orders/changeOrderStatus/"
    
    var data = new URLSearchParams({        
        id:     order.data('id'),
        status: 3
    });

    const response = await fetch(post, {method: 'POST',body:data })
    const res      = await response.json()

    if(res.status == 200 ){
		  console.log(res.message)
		  successToast('Bien Hecho!', res.message)
          $('#orders-list').DataTable().ajax.reload();
    }else{
        iziToast.error({
            title: 'Error!',
            message: res.message,
        });   
    }

}

async function changeStatusFinished(order){

    var post =  baseUrl + "manager/Orders/changeOrderStatus/"
    
    var data = new URLSearchParams({        
        id:     order.data('id'),
        status: 4
    });

    const response = await fetch(post, {method: 'POST',body:data })
    const res      = await response.json()

    if(res.status == 200 ){
		  console.log(res.message)
		  successToast('Bien Hecho!', res.message)
          $('#orders-list').DataTable().ajax.reload();
    }else{
        iziToast.error({
            title: 'Error!',
            message: res.message,
        });   
    }

}