function successToast(title, text){

    var title = ( title !== '' ) ? title : 'OK!';

    iziToast.success({
        title: title,
        message: text
        // class: 'alert bg-success border-success text-white alert-styled-left alert-dismissible',
    });

}

function warningToast(title, text){

    var title = ( title !== '' ) ? 'Advertencia!': title;

    iziToast.warning({
        title: title,
        message: text
        // class: 'alert bg-warning border-warning text-white alert-styled-left alert-dismissible',
    });

}

function dangerToast(title, text){

    var title = ( title !== '' ) ? 'Error!': title;

    iziToast.error({
        title: 'Error!',
        message: text
        // class: 'alert bg-danger border-danger text-white alert-styled-left alert-dismissible',
    });

}

function questionToast( text, buttons ){

    iziToast.question({
        timeout: 20000, 
        overlay: true,
        displayMode: 'once', 
        message: text,
        position: 'center',
        class: 'alert bg-warning border-warning text-white alert-styled-left alert-dismissible',
        buttons: buttons 
    });

}

function formatMoney2(a, c, d, t){
    var n = a, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
       return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
     };