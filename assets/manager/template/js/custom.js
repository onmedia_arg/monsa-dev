/* ------------------------------------------------------------------------------
 *
 *  # Custom JS code
 *
 *  Place here all your custom js. Make sure it's loaded after app.js
 *
 * ---------------------------------------------------------------------------- */

	// Modal template
	var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
	    '  <div class="modal-content">\n' +
	    '    <div class="modal-header align-items-center">\n' +
	    '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
	    '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
	    '    </div>\n' +
	    '    <div class="modal-body">\n' +
	    '      <div class="floating-buttons btn-group"></div>\n' +
	    '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
	    '    </div>\n' +
	    '  </div>\n' +
	    '</div>\n';

	// Buttons inside zoom modal
	var previewZoomButtonClasses = {
	    toggleheader: 'btn btn-light btn-icon btn-header-toggle btn-sm',
	    fullscreen: 'btn btn-light btn-icon btn-sm',
	    borderless: 'btn btn-light btn-icon btn-sm',
	    close: 'btn btn-light btn-icon btn-sm'
	};

	// Icons inside zoom modal classes
	var previewZoomButtonIcons = {
	    prev: '<i class="icon-arrow-left32"></i>',
	    next: '<i class="icon-arrow-right32"></i>',
	    toggleheader: '<i class="icon-menu-open"></i>',
	    fullscreen: '<i class="icon-screen-full"></i>',
	    borderless: '<i class="icon-alignment-unalign"></i>',
	    close: '<i class="icon-cross2 font-size-base"></i>'
	};

	// File actions
	var fileActionSettings = {
	    zoomClass: '',
	    zoomIcon: '<i class="icon-zoomin3"></i>',
	    dragClass: 'p-2',
	    dragIcon: '<i class="icon-three-bars"></i>',
	    removeClass: '',
	    removeErrorClass: 'text-danger',
	    removeIcon: '<i class="icon-bin"></i>',
	    indicatorNew: '<i class="icon-file-plus text-success"></i>',
	    indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
	    indicatorError: '<i class="icon-cross2 text-danger"></i>',
	    indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
	};

	// iziToast - settings
	iziToast.settings({
	  	timeout: 3000, // default timeout
	  	resetOnHover: true,
	  // icon: '', // icon class
	  	transitionIn: 'fadeInUp',
	  	transitionOut: 'fadeOut',
	  	messageColor: '#fff',
	  	iconColor: '#fff',
	  	titleColor: '#fff',
		icon: null
	});

	$(document).on('show.bs.modal', '.modal', function (e) { 
		$(this).find('select').select2()
		$(this).find('#company').select2({
        	minimumResultsForSearch: -1,
        	placeholder: "Seleccione una Agencia"
	    }) 
	})

    // Get Token
	$(window).on('load', function(){

        getToken();	        

    });

	function successToast(title, text){

		var title = ( title !== '' ) ? 'OK!': title;

        iziToast.error({
            title: title,
            message: text,
            class: 'alert bg-success border-success text-white alert-styled-left alert-dismissible',
        });

	}

	function warningToast(title, text){

		var title = ( title !== '' ) ? 'Advertencia!': title;

        iziToast.warning({
            title: title,
            message: text,
            class: 'alert bg-warning border-warning text-white alert-styled-left alert-dismissible',
        });

	}

	function dangerToast(title, text){

		var title = ( title !== '' ) ? 'Error!': title;

        iziToast.error({
            title: 'Error!',
            message: text,
            class: 'alert bg-danger border-danger text-white alert-styled-left alert-dismissible',
        });

	}

	function questionToast( text, buttons ){

		iziToast.question({
		    timeout: 20000, 
		    overlay: true,
		    displayMode: 'once', 
		    message: text,
		    position: 'center',
		    class: 'alert bg-warning border-warning text-white alert-styled-left alert-dismissible',
		    buttons: buttons 
		});

	}

    var swalInit = swal.mixin({
        buttonsStyling: false,
        confirmButtonClass: 'btn btn-primary',
        cancelButtonClass: 'btn btn-light'
    });

        
	function setModalMessage(type, title, status, message){

	 	$('#modalHeader').addClass('bg-'+type)
		$('#modalStatus').html('Status '+status)
		$('#modalTitle').html(title)
		$('#modalMessage').html(message)

	} 

    function getToken(){

        if( $('html').hasClass('no-token') ){

            $.blockUI( unblockConfig )

            $.getJSON( baseUrl + 'manager/Admin/get_token', function( response ){ 

                    if ( response.code == 1 ) {  

                    	// nothing

                    }else{

                        dangerToast( '', response.message ); 

                    }

                    $.unblockUI()

            }) 
        
        }

    }

/* ------------------------------------------------------------------------------
 *
 *  # Block config
 *
 * ---------------------------------------------------------------------------- */

// 	Unblock Config
	var unblockConfig = {
	      message: '<i class="fa fa-cog fa-spin fa-2x"></i>',
	        css:{
	            backgroundColor: 'transparent',
	            border: 'none', 
	        },
	        overlayCSS: {
	            backgroundColor: '#FFF',
	        }
	  };

/* ------------------------------------------------------------------------------
 *
 *  # Buttons and button dropdowns
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var Buttons = function() {
    //
    // Setup module components
    //

    // Progress buttons
    var _componentLadda = function() {
        if (typeof Ladda == 'undefined') {
            console.warn('Warning - ladda.min.js is not loaded.');
            return;
        }

        // Button with spinner
        Ladda.bind('.btn-ladda-spinner', {
            dataSpinnerSize: 16,
            timeout: 2000
        });

        // Button with progress
        Ladda.bind('.btn-ladda-progress', {
            callback: function(instance) {
                var progress = 0;
                var interval = setInterval(function() {
                    progress = Math.min(progress + Math.random() * 0.1, 1);
                    instance.setProgress(progress);

                    if( progress === 1 ) {
                        instance.stop();
                        clearInterval(interval);
                    }
                }, 200);
            }
        });
    };

    // Loading button
    var _componentLoadingButton = function() {
        $('.btn-loading').on('click', function () {
            var btn = $(this),
                initialText = btn.data('initial-text'),
                loadingText = btn.data('loading-text');
            btn.html(loadingText).addClass('disabled');
            setTimeout(function () {
                btn.html(initialText).removeClass('disabled');
            }, 3000)
        });
    };

    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentLadda();
            _componentLoadingButton();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    Buttons.init();
});

/* ------------------------------------------------------------------------------
 *
 *  # Formating
 *
 * ---------------------------------------------------------------------------- */

function formatMoney(number, decPlaces, decSep, thouSep) {
    
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
       decSep = typeof decSep === "undefined" ? "." : decSep;
      thouSep = typeof thouSep === "undefined" ? "," : thouSep;
    
    var sign = number < 0 ? "-" : "";
    var    i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
    var    j = (j = i.length) > 3 ? j % 3 : 0;

    return sign +
        (j ? i.substr(0, j) + thouSep : "") +
        i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
        (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}

function formatMoney2(a, c, d, t){
		var n = a, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
		   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
		 };


/* ------------------------------------------------------------------------------
 *
 *  # Cookies
 *
 * ---------------------------------------------------------------------------- */

/*!
 * JavaScript Cookie v2.2.1
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */

(function (factory) {
	var registeredInModuleLoader;
	if (typeof define === 'function' && define.amd) {
		define(factory);
		registeredInModuleLoader = true;
	}
	if (typeof exports === 'object') {
		module.exports = factory();
		registeredInModuleLoader = true;
	}
	if (!registeredInModuleLoader) {
		var OldCookies = window.Cookies;
		var api = window.Cookies = factory();
		api.noConflict = function () {
			window.Cookies = OldCookies;
			return api;
		};
	}
}(function () {
	function extend () {
		var i = 0;
		var result = {};
		for (; i < arguments.length; i++) {
			var attributes = arguments[ i ];
			for (var key in attributes) {
				result[key] = attributes[key];
			}
		}
		return result;
	}

	function decode (s) {
		return s.replace(/(%[0-9A-Z]{2})+/g, decodeURIComponent);
	}

	function init (converter) {
		function api() {}

		function set (key, value, attributes) {
			if (typeof document === 'undefined') {
				return;
			}

			attributes = extend({
				path: '/'
			}, api.defaults, attributes);

			if (typeof attributes.expires === 'number') {
				attributes.expires = new Date(new Date() * 1 + attributes.expires * 864e+5);
			}

			// We're using "expires" because "max-age" is not supported by IE
			attributes.expires = attributes.expires ? attributes.expires.toUTCString() : '';

			try {
				var result = JSON.stringify(value);
				if (/^[\{\[]/.test(result)) {
					value = result;
				}
			} catch (e) {}

			value = converter.write ?
				converter.write(value, key) :
				encodeURIComponent(String(value))
					.replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);

			key = encodeURIComponent(String(key))
				.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent)
				.replace(/[\(\)]/g, escape);

			var stringifiedAttributes = '';
			for (var attributeName in attributes) {
				if (!attributes[attributeName]) {
					continue;
				}
				stringifiedAttributes += '; ' + attributeName;
				if (attributes[attributeName] === true) {
					continue;
				}

				// Considers RFC 6265 section 5.2:
				// ...
				// 3.  If the remaining unparsed-attributes contains a %x3B (";")
				//     character:
				// Consume the characters of the unparsed-attributes up to,
				// not including, the first %x3B (";") character.
				// ...
				stringifiedAttributes += '=' + attributes[attributeName].split(';')[0];
			}

			return (document.cookie = key + '=' + value + stringifiedAttributes);
		}

		function get (key, json) {
			if (typeof document === 'undefined') {
				return;
			}

			var jar = {};
			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all.
			var cookies = document.cookie ? document.cookie.split('; ') : [];
			var i = 0;

			for (; i < cookies.length; i++) {
				var parts = cookies[i].split('=');
				var cookie = parts.slice(1).join('=');

				if (!json && cookie.charAt(0) === '"') {
					cookie = cookie.slice(1, -1);
				}

				try {
					var name = decode(parts[0]);
					cookie = (converter.read || converter)(cookie, name) ||
						decode(cookie);

					if (json) {
						try {
							cookie = JSON.parse(cookie);
						} catch (e) {}
					}

					jar[name] = cookie;

					if (key === name) {
						break;
					}
				} catch (e) {}
			}

			return key ? jar[key] : jar;
		}

		api.set = set;
		api.get = function (key) {
			return get(key, false /* read as raw */);
		};
		api.getJSON = function (key) {
			return get(key, true /* read as json */);
		};
		api.remove = function (key, attributes) {
			set(key, '', extend(attributes, {
				expires: -1
			}));
		};

		api.defaults = {};

		api.withConverter = init;

		return api;
	}

	return init(function () {});
}));


		 