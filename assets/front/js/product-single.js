function increaseQuantity(productId) {
    const quantityInput = document.getElementById(productId);
    const step = parseInt(quantityInput.getAttribute("step")) || 1;
    quantityInput.value = parseInt(quantityInput.value) + step;
}

function decreaseQuantity(productId) {
    const quantityInput = document.getElementById(productId);
    const min = parseInt(quantityInput.getAttribute("min")) || 1;
    const step = parseInt(quantityInput.getAttribute("step")) || 1;
    
    if (quantityInput.value > min) {
        quantityInput.value = parseInt(quantityInput.value) - step;
    }
}