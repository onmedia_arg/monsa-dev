$(document).ready(function(){

    viewCuentaCorriente();

})

function viewCuentaCorriente(){
    var post = window.base + 'manager/CuentaCorriente/get_cuentaCorriente'
	var data = new URLSearchParams({
	})
	fetch(post, {
		method: 'POST'
	})
		.then(data => data.json())
		.then(data => {
			if (data.code == 200) {
                    if(parseFloat(data.data.saldo.saldo.replaceAll(".",""),10) > 0){
                        $data_saldo = '<span class=""> $ '+data.data.saldo.saldo+'</span>';
                        
                    }else{
                        $data_saldo = '<span class=""> $ '+data.data.saldo.saldo+'</span>';
                    }

                    $data_movimiento = '';
                    data.data.movimientos.detalle.forEach(element => {
                        $data_movimiento += `'<tr>'
                            '<td class="text-center">${element.fecha}</td>'
                            '<td class="text-center">${element.tipo}</td>'
                            '<td class="text-center">${element.nro_documento}</td>'
                            '<td class="text-center">${element.estado}</td>'
                            '<td class="text-center">${element.referencia}</td>'
                            '<td class="text-right">$ ${element.importe}</td>'`
                        '</tr>'
                    });
                    $('#detail_cuenta').html($data_movimiento)
                    $('#saldo').html($data_saldo)
                    $('#spinner').addClass('d-none')
                    $('.detail-cuenta-corriente').removeClass('d-none')

			} else if(data.code == 203){
                    $('#spinner').addClass('d-none')
                    $('#cliente_not_found').removeClass('d-none')
                    $('#saldo').html('')
                    $('#detail_cuenta').html('')
			}else{
                $('#spinner').addClass('d-none')
                $('#saldo').html('')
                $('#detail_cuenta').html('')
				iziToast.warning({
                    title: 'Ups',
                    message: 'No hubo resultados para su búsqueda, intente nuevamente.',
                });
            }
		})
		.catch(error => {
            $('#spinner').addClass('d-none')
            $('#saldo').html('')
            $('#detail_cuenta').html('')
			iziToast.warning({
                title: 'Ups',
                message: 'Se ha producido un error al momento de procesar',
            });
		})
}
