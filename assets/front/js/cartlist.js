$(document).ready(function(){

    set_cart_list();
    
    // $('.guardar_borrador').toggleClass('d-none')    

    if(parseInt( $('#count-items').html() ) < 1 ){
        $('#btn-new-order').toggleClass('d-none')
        $('.enviar_pedido').toggleClass('d-none')
        $('#cart-notes').toggleClass('d-none')
    } 

    $('#btn-new-order').on('click', function(){

        iziToast.question({
            timeout: 200000,
            close: false,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 9999,
            backgroundColor: '#FFB200',
            title: 'Vaciar Pedido',
            message: 'Se borraran todos los items del pedidos',
            position: 'center',
            buttons: [
                    ['<button class="izi-btn-confirm">Continuar</button>', function (instance, toast) {
                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                                clear_cart() 
                            }, true],
                    ['<button class="izi-btn-cancel">Cancelar</button>', function (instance, toast) {
                                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            }],
            ],
        });
    })


    $('.enviar_pedido').on('click', function(e){
        e.preventDefault();
        $('.form-cart').css('display','none');
        $('.spinner').css('display','block');
        sendOrderAsync($(this))
        // $('.spinner').css('display','none');
        // $('.form-cart').css('display','block');
    })

    $('.guardar_borrador').on('click', function(e){
            e.preventDefault();
            save_order($(this))
    }) 
})

function set_cart_list(){
    build_cart_table();
    $('.spinner').css('display','none');
    render_cart();
    $('.form-cart').css('display','block');
}

function render_cart(){
    $('.cart-list-table').empty();
    $('.cart-list-table').append(cart_list);    
}

async function sendOrder(){

    var post = window.base + "cart/process_order"
    
    var data = new URLSearchParams({        
        nota: $('#nota-cart').val(),
    });

    
    const response = await fetch(post, {method: 'POST',body:data })
    const res      = await response.json()

    if(res.code == 1 || res.code == '1' ){
    //    console.log(res.message)
        window.location.replace( res.url );
    }else{
        $('.spinner').css('display','none');
        $('.form-cart').css('display','block');

        iziToast.error({
            title: 'Error!',
            message: res.message,
        });   
    }
}

async function sendOrderAsync(e){
    e.removeClass('enviar_pedido')
    await sendOrder()
}

function save_order(e){

    e.removeClass('guardar_borrador')
        
    var nota = $('#nota-cart').val();

    $.ajax({
            url    : window.base + "cart/save_cart_draft",
            method : 'POST',
            data   : {
                        nota:       nota
                    },
            success: function(data){
                    var response = $.parseJSON(data)
                    iziToast.success({
                        title: 'OK',
                        message: 'Pedido Guardado Exitosamente'})
            } 
    });

}

function clear_cart(){

    var url = window.base + 'Cart/clear_cart';

    $.ajax({
        async  : false,
        url    : url,
        method : 'POST',
        success : function(r){
                result = JSON.parse(r);

                render_side_cart();
                render_cart();
                $('#btn-new-order').toggleClass('d-none')
                $('.enviar_pedido').toggleClass('d-none')
                $('#cart-notes').toggleClass('d-none')
                
                $('#title').html('Nuevo Pedido')

                iziToast.success({
                    title: 'OK',
                    message: result.message,
                })

        }
    })


}


function increaseQuantity(productId) {
    const quantityInput = document.getElementById(productId);
    const step = parseInt(quantityInput.getAttribute("rango")) || 1;
    quantityInput.value = parseInt(quantityInput.value) + step;
}

function decreaseQuantity(productId) {
    const quantityInput = document.getElementById(productId);
    const min = parseInt(quantityInput.getAttribute("cantidad_minima")) || 1;
    const step = parseInt(quantityInput.getAttribute("rango")) || 1;
    
    if (quantityInput.value > min) {
        quantityInput.value = parseInt(quantityInput.value) - step;
    }
}