var shop_layout = readCookie("shop_layout");
if (shop_layout == null) {
		// do cookie doesn't exist stuff;
		document.cookie = "shop_layout=default";
}

document.addEventListener("load", shopInit());

function shopInit(){
// $(document).ready(function(){


    filter_data(1); 

    filter_init();

    flash_message();

    $(document).on('click', '.pagination li a', function(event){
            event.preventDefault();
            var page = $(this).data('ci-pagination-page');
            filter_data(page);
    });

    $(document).on('change', '.common_selector', function(){ 

        // Si es familia actualiza los filtros
        if ( $(this).attr('name') == 'familia' ) {
                update_filter( $(this).attr('data-familia') ); 
        }

        if ( $(this).is(':checked') ) {
                // var filter =  '<li class="single-filter float-left">';
                //     filter += $(this).attr('data-name')
                //     filter += '<button remove-filter data-name="' + $(this).attr('name') + '" data-id="' + $(this).val() + '"><i class="fa fa-times"></i></button>'
                //     filter += '</li>'  

                        var filter =`<li>
                        <button remove-filter data-id="${$(this).val()}" data-name="${$(this).attr('name')}" class="cust-btn gray_btn search-label">${$(this).attr('data-name')}<i class="fa fa-times ml-2 pr-0"></i></button>
                        </li>`
                        

        }else{
            $('[remove-filter][data-id="' + $(this).val() + '"]').closest('.single-filter').remove()
        }

        $('#active-filters').append( filter )

        // Actualizar URL
        var url = new URL(window.location.href);

        var brandFilters = $('.common_selector[name=marca]').serializeArray(),
                    familyFilters = $('.common_selector[name=familia]').serializeArray(),
                    attributeFilters = $('.common_selector[name=atributo]').serializeArray(),
                    atributoArray = Array() 

        // Limpiar parametros
        url.searchParams.delete( 'marca' )
        url.searchParams.delete( 'familia' ) 
        url.searchParams.delete( 'atributo' ) 

        $.each(brandFilters, function(i, field){ 
            updateParamURL( url, field.name, field.value )  
        });

        $.each(familyFilters, function(i, field){ 
            updateParamURL( url, field.name, field.value )  
        }); 

        $.each(attributeFilters, function(i, field){ 
                const idFamilia = url.searchParams.get("familia");
                var idAttribute = $(`.common_selector[name="atributo"][value="${field.value}"][data-idfamilia="${idFamilia}"]`).attr('data-idattr');
                atributoArray.push( { 'idAttribute' : idAttribute, 'value': field.value } ) // ATRIBUTO, VALOR DEL ATRIBUTO
                // Actualiza URL
                updateArrayURL( url, 'atributo', atributoArray )
        });
        // Actualiza url
        window.history.pushState( null, null, url.href);
        filter_data(1);
    });


    $("#btn-search").on('click',function(){

        var search = $('#search').val()
        
        if(search.length > 0){
            searchForm()
        }
        

    })
    

    $("#btn-grid-layout").on('click',function(){
        document.cookie = "shop_layout=default";
        $('#catalogo').empty();
        $('#pagination_link').empty();
        filter_data(1);
    })
    
    $("#btn-list-layout").on('click',function(){
        document.cookie = "shop_layout=list";
        $('#catalogo').empty();
        $('#pagination_link').empty();      
        filter_data(1);

    })

    $(document).on('mouseenter', '.category-list .row.single-product-list', function(){
        $(this).find('.quantity').focus()
    });


    $(document).on('change', '#item-number', function(){
        filter_data(1); 
    })

}


function filter_data(page){
    var action  = 'fetch_data';
    var marca   = get_filter('marca');
    var familia = get_filter('familia');

    // var search  = $('#search').val();
    var search  = $('.search-label').data("term");

    var url = new URL(window.location.href);
    var atributo = decodeURIComponent( url.searchParams.get( 'atributo' ) ); // enviamos como json string

    // console.log( url.searchParams.get( 'atributo' ) )

    var cantidad = $('#item-number option:selected').val();

    var shop_layout = readCookie("shop_layout");   

    // Ajax de grilla
    $.ajax({
            url: window.base + 'Shop/fetch_data/' +page,
            method:"POST",
            dataType:"JSON",
            cache: false,
            data:{
                action  : action, 
                familia : familia, 
                marca   : marca,
                atributo: atributo,
                cantidad: cantidad,
                shop_layout: shop_layout,
                search:   search,
            },
            beforeSend: function(){
                    $('#catalogo').fadeOut()
                    // setTimeout(function(){ 
                    //     $('.spinner').fadeIn()
                    // },200);
            },
            success:function(data)
            {
                var output = "";

                if ( data.product_list.length !== 0 ) {

                        for( var i = 0; i < data.product_list.length; i++ ) {
                             if (shop_layout == 'default'){
                                    output = render_shop_default(output, data.product_list[i]);
                             }
                             else
                             {
                                    output = render_shop_list(output, data.product_list[i]);
                             }
                        }   

                } else {
                        iziToast.warning({
                                title: 'Ups',
                                message: 'No hubo resultados para su búsqueda, intente nuevamente.',
                        });
                }

                $('#search').val("")
                $('.filter_data').html(output);
                $('#pagination_link').html(data.pagination_link);

                // $('.spinner').hide()
                setTimeout(function(){
                    $('#catalogo').fadeIn()
                }, 200);
                scrollPageTop();     
            }
    })    

}

function render_shop_default(output, data) {

    atributos = list_attr( data['idProducto'] );

    attrString = "";
    separador = "";
    for( var i = 0; i < atributos.length ; i ++){
        separador = " - ";
        if ((i+1) == atributos.length) {
            separador = "";
        }
        attrString += atributos[i]['valores'] + separador;

    }
        

    if (data['idTipo'] == 0){
        output += '<div id="product-' + data['idProducto'] +'" class="col-lg-4 col-md-6">';
        output +=   '<div class="single-product">';
        
        //output +=     '<a href="' + window.base + 'frontController/detalle/' + data['slug'] +'">';
        //output +=       '<div class="img-prod" style="background:url(' + data['imagen'] + ')"></div>';
        //output +=     '</a>';

        output +=     '<div class="product-img">'
        if( data['is_promo_active'] == 1 && data['tag'] != ''){
            output += '<span class="discount-label">' + data['tag'] + '</span>'
        }
        output += '<a data-fancybox="gallery" href=" ' + data['imagen'] + '"><img src="' + data['imagen'] + '"></a></div>'

        output +=     '<div class="product-details">'
        output +=       '<div class="product-list-row-top">'
        output +=      '<span>SKU:' + data['sku'] + '</span>'
        if(data['stock_type'] == 'fijo'){
            output += setColorStock(data['stock_fijo'])
        }else if(data['stock_type'] == 'formula'){
            output += calculateStock(data['stock_quantity'],data['stock_limit1'],data['stock_limit2'], )
        }
        output +=       '</div>'

        output +=       '<h6><a href="' + window.base + 'frontController/detalle/' + data['slug'] + '">' + data['nombre'] + '</a></h6>';
        output += 		'<span>' + attrString + '</span>'
        output +=       '<div class="row-price price">';
        
        console.log('is_promo_active')
        console.log(data['is_promo_active'])
        if(data['is_promo_active'] == null || data['is_promo_active'] == 0){
            output +=         '<div class="p-price"><span style="font-size:18px">$</span> ' + formatCurrency(data['precio']) + '</div>';
        }else{
            output +=         '<div class="p-price-promo" stye="text-muted"><span style="font-size:18px;">$ </span>' + formatCurrency(data['precio']) + '</div>';
            output +=         '<div class="p-price"><span style="font-size:18px">$</span> ' + formatCurrency(data['price_promo']) + '</div>';
        }

        
        
        output +=       '</div>';
        output +=       '<div style="display:block;font-size:0.9em">No incluye IVA</div>';
        
 
        
        output +=       '<div class="add-bag quantity-visible">';
        output +=       '<div class="input-group mb-3">';
        output +=       '<div class="input-group-prepend">';
        output +=       '<button class="btn btn-add-cart" type="button"';
        output +=       'onclick="decreaseQuantity(' + data["idProducto"] + ')"';
        output +=       '><i class="fas fa-minus"></i></button>';
        output +=       '</div>';  

        output +=         '<input type="number" name="quantity" class="form-control quantity" value="' + data["cantidad_minima"] + '" min="' + data["cantidad_minima"] + '" id="' + data["idProducto"] + '" step="' + data["rango"] + '" onkeydown="return false;"/>';
        output +=       '<div class="input-group-append">';
        output +=       '<button class="btn btn-add-cart" type="button"';
        output +=       'onclick="increaseQuantity(' + data["idProducto"] + ')"';
        output +=       '><i class="fas fa-plus"></i></button>';
        output +=       '</div>';
        output +=       '</div>';          
        
        
        
        output +=         '<button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart "'; 
        output +=                   '  data-productname = "' + data["nombre"]; 
        output +=                   '" data-price       = "' + data["precio"];
        output +=                   '" data-sku         = "' + data["sku"];
        output +=                   '" data-productid   = "' + data["idProducto"];
        output +=                   '" data-cantidadminima   = "' + data["cantidad_minima"];
        output +=                   '" data-attributes  = "' + attrString;
        output +=                   '">AGREGAR</button>'			
        output +=       '</div>';
        
        
        
        output +=     '</div>';
        output +=   '</div>';    
        output += '</div>';  

    }else{

        output += '<div id="product-' + data['idProducto'] +'" class="col-lg-4 col-md-6">';
        output +=   '<div class="single-product">';

        // output +=     '<a href="' + window.base + 'frontController/detalle/' + data['slug'] +'">';
        // output +=       '<div class="img-prod" style="background:url(' + data['imagen'] + ')"></div>';
        // output +=     '</a>';
        
        output +=     '<div class="product-img"><a data-fancybox="gallery" href=" ' + data['imagen'] + '"><img src="' + data['imagen'] + '"></a></div>'
        output +=     '<div class="product-details">';
        output +=       '<span>SKU:' +data['sku'] + '</span>' 
        output +=       '<h6><a href="' + window.base + 'frontController/detalle/' + data['slug'] + '">' + data['nombre'] + '</a></h6>';
        // output +=         '<div class="price">';
        // output +=           '<p class="p-price">$ ' + data['precio'] + '</p>';
        // output +=         '</div>';
        output +=       '<div class="add-bag no-quantity-visible">';
        output +=         '<input type="number" name="quantity" class="form-control quantity" value="' + data["cantidad_minima"] + '" min="' + data["cantidad_minima"] + '" id="' + data["idProducto"] + '" step="' + data["rango"] + '" onkeydown="return false;"/>';
        output +=                   '<a class="cust-btn primary-btn variation-btn" href="' + window.base + 'frontController/detalle/' + data['slug'] + '"><i class="fas fa-search"></i>Ver Variaciones</a>'
        output +=       '</div>';
        output +=     '</div>';
        output +=   '</div>';    
        output += '</div>';   

    }
    return output;

}

// Funcion para renderizar la vista en modo LISTA
function render_shop_list(output, data){
    var atributos = list_attr( data['idProducto'] );

    switch( data['idTipo'] ){

        // Simple
        case 0:
        case '0':
        // Variacion
        case 2:
        case '2':

            // Atributos

            if ( ( data['idTipo'] == 2 || data['idTipo'] == '2' ) || ( data['idTipo'] == 0 || data['idTipo'] == '0' ) ) { 
                    
                    var attr = '';

                    atributos.forEach(function( item, index ){   

                            if ( isJson( item.valores ) ){

                                    var multiple = $.parseJSON( item.valores )  
                                            
                                    // Si son varios atributos
                                    if ( multiple !== '' && multiple !== null && multiple !== undefined && Array.isArray( multiple ) ) {

                                            multiple.forEach(function( item, index ){

                                                    attr += `${item} - `;

                                            }) 
                                    // Si es un solo atributo
                                    } else {
                                        attr += `${item.valores} - `;
                                    }

                            } else {
                                    attr += item.valores + ' - ';
                            }

                    })

                    // Quitar el ultimo guion
                    attr = attr.substring(0, attr.length-3); 

            }

            output += '<div id="product-' + data['idProducto'] +'" class="col-lg-12 col-md-12">';
            output +=  '<div class="row single-product-list">';
            output +=    '<div class="col-md-9">';
            output +=       '<div class="row">';        
            output +=         '<div class="col-md-2">';
            output +=     '<div class="product-img-list"><a data-fancybox="gallery" href=" ' + data['imagen'] + '"><img src="' + data['imagen'] + '"></a></div>'
            output +=         '</div>';
            output +=         '<div class="col-md-7">';
            output +=           '<div class="product-details">';
            output +=             '<div class="product-list-row-top"><h6 style="margin:0px" >SKU:' +data['sku'] + '</h6>';
            
            if(data['stock_type'] == 'fijo'){
                output += setColorStock(data['stock_fijo'])
            }else if(data['stock_type'] == 'formula'){
                output += calculateStock(data['stock_quantity'],data['stock_limit1'],data['stock_limit2'], )
            }
            
            output +=             '</div>';
            output +=             '<h6><a href="' + window.base + 'frontController/detalle/' + data['slug'] + '">' + data['nombre'] + '</a></h6>';
            output +=             '</div>';
            output +=         '</div>';
            output +=         '<div class="col-md-3 quan-box">';
            output +=           '<h6 for="quantity">Cantidad</h6>'             
            output +=           '<input type="number" name="quantity" class="form-control quantity" placeholder="0" id="' + data["idProducto"] + '">';
            output +=         '</div>';
            output +=         `<div class="col-12 attr-box"><small> ${attr} </small></div>`;
            output +=       '</div>';
            output +=   '</div>';

            output +=    '<div class="col-md-3">';
            output +=      '<div class="add-bag align-items-center">';
            output +=           '<div class="price_list"><p class="p-price">$ ' + formatCurrency(data['precio']) + '<small> s/iva</small></p></div>';        
            output +=           `<button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart_list" data-attributes="${attr}" data-productname="${data['nombre']}" data-sku="${data['sku']}" data-price="${data['precio']}" data-productid="${data['idProducto']}">Agregar</button>`;
            output +=      '</div>';
            output +=    '</div>';


            output +=   '</div>';
            output += '</div>';

        break;

        // Variable
        case 1:

            // No mostrar

        break; 

    } 

    return output;

}

function list_attr( idProducto ){

    var url = window.base + 'shop/list_attr/' + idProducto
            result = '';
    
    $.ajax({
        async  : false,
        url    : url,
        method : 'POST', 
        success : function(r){
                result = JSON.parse(r); 
                // console.log( result )
        }
    })
    
    return result;

}

function searchForm(){
    $('.search-label').remove()
    
    var value = $('#search').val()
    var filter =`<li>
                    <button remove-filter data-name="search" data-term="${value}" class="cust-btn gray_btn search-label">${value} <i class="fa fa-times ml-2 pr-0"></i></button>
                </li>`
    $('#active-filters').append( filter )
    filter_data(1);
}

function get_filter(class_name){
    var filter = [];
    $('.'+class_name+':checked').each(function(){
            filter.push($(this).val());
    });
    return filter;
}

function readCookie(name) {

    var nameEQ = name + "="; 
    var ca = document.cookie.split(';');

    for(var i=0;i < ca.length;i++) {

        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) {
            return decodeURIComponent( c.substring(nameEQ.length,c.length) );
        }

    }

    return null;

}

function getCookie(name) {
        var dc = document.cookie;
        var prefix = name + "=";
        var begin = dc.indexOf("; " + prefix);
        if (begin == -1) {
                begin = dc.indexOf(prefix);
                if (begin != 0) return null;
        }
        else
        {
                begin += 2;
                var end = document.cookie.indexOf(";", begin);
                if (end == -1) {
                end = dc.length;
                }
        }
        // because unescape has been deprecated, replaced with decodeURI
        //return unescape(dc.substring(begin + prefix.length, end));
        return decodeURI(dc.substring(begin + prefix.length, end));
} 

function filter_init(){ 

    var filter = '';

    $('.common_selector:checked').each(function(i, field){

        // filter += '<li class="single-filter float-left">';
        // filter += $(this).attr('data-name')
        // filter += '<button remove-filter data-name="' + $(this).attr('name') + '" data-id="' + $(this).val() + '"><i class="fa fa-times"></i></button>'
        // filter += '</li>' 

            filter +=`<li>
                                                <button remove-filter data-name="${$(this).attr('name')}" data-id="${$(this).val()}" class="cust-btn gray_btn">${$(this).attr('data-name')} <i class="fa fa-times ml-2 pr-0"></i></button>
                                            </li>`

    })

    $('#active-filters').append( filter )

    // Actualiza si hay familia seleccionada
    $('.common_selector[name="familia"]:checked').each(function(i, field){

            update_filter( $(this).attr('data-familia') )

    })

}

function update_filter( idFamilia ){ 

    var id = idFamilia

    if( $('.common_selector[name="familia"]:checked').length >= 1 ){ 

            $( '.parent-' + id ).collapse('show')

            $('[data-familia="' + id + '"]').closest('label').addClass('active')

            // Ocultar otros filtros
            $('.common_selector[name="familia"]:not(:checked)').each(function(){
                    $(this).closest('.list-group-item').fadeOut()
            })

            $('#brands-wrap').fadeOut();

    }else{ 

            $( '.parent-' + id ).collapse('hide')

            // Mostrar filtros
            $('[data-familia="' + id + '"]').closest('label').removeClass('active')
            $( '.parent-' + id ).find('input[name="atributo"]:checked').click();

            var url = new URL(window.location.href);
            url.searchParams.delete( 'atributo' ) 

            $('.common_selector[name="familia"]').each(function(){
                    $(this).closest('.list-group-item').fadeIn()
            })

            $('#brands-wrap').fadeIn(); 

    } 

}

function scrollPageTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

	// Detect JSON
function isJson(str) {
    try {
            JSON.parse(str);
    } catch (e) {
            return false;
    }
    return true;
}

function updateArrayURL( url, name, array ){  

    var value = encodeURIComponent( JSON.stringify( array ) );

    url.searchParams.delete( 'atributo' ) 

    url.searchParams.append( name , value );  

}

function updateParamURL( url, name, value ){

var currentParam = url.searchParams.get( name )

//Si el parametro existe
if ( currentParam ) {

    //Picar
    var check = currentParam.split(',')

    //Revisa si hay mas de un valor en el parametro
    if ( Array.isArray(check) == true ){
        //Revisa si el valor del loop existe en el array
        if( $.inArray( value, check ) !== -1){
            // No hacer nada si existe
        }else{
            var m = currentParam + ',' + value
            url.searchParams.set( name , m );
            console.log(m)
        }
    }

}else{
    url.searchParams.append( name , value );
}

}

function getAllUrlParams(url) {

// get query string from url (optional) or window
var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

// we'll store the parameters here
var obj = {};

// if query string exists
if (queryString) {

    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];

    // split our query string into its component parts
    var arr = queryString.split('&');

    for (var i = 0; i < arr.length; i++) {
        // separate the keys and the values
        var a = arr[i].split('=');

        // set parameter name and value (use 'true' if empty)
        var paramName = a[0];
        var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

        // (optional) keep case consistent
        paramName = paramName.toLowerCase();
        if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

        // if the paramName ends with square brackets, e.g. colors[] or colors[2]
        if (paramName.match(/\[(\d+)?\]$/)) {

            // create key if it doesn't exist
            var key = paramName.replace(/\[(\d+)?\]/, '');
            if (!obj[key]) obj[key] = [];

            // if it's an indexed array e.g. colors[2]
            if (paramName.match(/\[\d+\]$/)) {
                // get the index value and add the entry at the appropriate position
                var index = /\[(\d+)\]/.exec(paramName)[1];
                obj[key][index] = paramValue;
            } else {
                // otherwise add the value to the end of the array
                obj[key].push(paramValue);
            }
        } else {
            // we're dealing with a string
            if (!obj[paramName]) {
                // if it doesn't exist, create property
                obj[paramName] = paramValue;
            } else if (obj[paramName] && typeof obj[paramName] === 'string'){
                // if property does exist and it's a string, convert it to an array
                obj[paramName] = [obj[paramName]];
                obj[paramName].push(paramValue);
            } else {
                // otherwise add the property
                obj[paramName].push(paramValue);
            }
        }
    }
}

return obj;
}

function flash_message(){

    $('#home-msj-close').click(function(){
        $('.home-msj').slideUp()
    })


}

function calculateStock(cant, limit1, limit2){
    
    c  = parseInt(cant)
    l1 = parseInt(limit1)
    l2 = parseInt(limit2)
    
    if( c == 0){
        return setColorStock("sinstock")
    }else if(l1 == 0 && l2  == 0){
        return setColorStock("bajo")
    }else if(c >= 0 && c <= l1 ){
        return setColorStock("bajo")
    }else if(c > l1 && c <= l2 ){
        return setColorStock("medio")
    }else if(c > l2 ){
        return setColorStock("alto")
    }    
}

function setColorStock(value){

    var a = ""

    if(value == "bajo"){
        a = '<span class="font-size-sm">STOCK: BAJO</span>'
    }else if(value == "medio"){
        a = '<span class="font-size-sm">STOCK: MEDIO</span>'
    }else if(value == "alto"){
        a = '<span class="font-size-sm">STOCK: ALTO</span>'
    }else if(value == "sinstock"){
        a = '<span class="font-size-sm">SIN STOCK</span>'
    }   

    return a

}

function formatCurrency(amount) {
    // Formatea el número a una cadena con el formato "$ XX.XXX,XX"
    return parseFloat(amount).toLocaleString('es-ES', {
        style: 'currency',
        currency: 'ARS',
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
        useGrouping: true
    }).replace('ARS', ''); 
}


function increaseQuantity(productId) {
    const quantityInput = document.getElementById(productId);
    const step = parseInt(quantityInput.getAttribute("step")) || 1;
    quantityInput.value = parseInt(quantityInput.value) + step;
}

function decreaseQuantity(productId) {
    const quantityInput = document.getElementById(productId);
    const min = parseInt(quantityInput.getAttribute("min")) || 1;
    const step = parseInt(quantityInput.getAttribute("step")) || 1;
    
    if (quantityInput.value > min) {
        quantityInput.value = parseInt(quantityInput.value) - step;
    }
}