$(document).ready(function(){

//  Fn Agregar productos al para Carrito
	$(document).on('click','#add_cart' ,function(){
        add_cart($(this))
    }); 

//  Fn Borrar productos de Carrito
    $(document).on('click', '.remove_item', function(){
        remove_item($(this))
    });

//  Fn Update cantidad en item de Carrito
    $(document).on('change', '.qty', function(e){

        const min = parseInt(this.min);
        const step = parseInt(this.step);
        let value = parseInt(this.value);

        // Si el valor no es un múltiplo de `step` respecto al mínimo, ajusta el valor
        if ((value - min) % step !== 0 || value < min) {
            // Ajusta al múltiplo de `step` más cercano al mínimo
            value = Math.round((value - min) / step) * step + min;
            this.value = value > min ? value : min; // Mantén el valor mínimo
        }

        update_cart($(this))
    });   


})


function add_cart(e){

    var product_id    = e.data("productid");
    var cantidad_minima    = e.data("cantidadminima");
    var product_name  = e.data("productname").replace(/[^\w\s]/gi, '').substring(0,40);

    var product_price = e.data("price");
    var quantity      = $('#' + product_id).val();
    const sku = e.data("sku");
    const attributes = e.data("attributes");

    var is_promo_active = e.data("is_promo_active");
    var price_promo = e.data("price_promo");

    if (quantity != '' && quantity > 0){
            $.ajax({
                url:  window.base + 'cart/add_to_cart',
                method:"POST",
                data:{
                        product_id   :product_id, 
                        product_name :product_name, 
                        product_price:product_price, 
                        quantity:quantity,
                        sku: sku,
                        attributes: attributes,
                        is_promo_active: is_promo_active,
                        price_promo: price_promo
                    },
                success:function(data)
                {
                    var response = $.parseJSON(data)

                    if ( response.code == 1 || response.code == '1' ) {

                        iziToast.success({
                                title: 'OK',
                                message: response.message,
                        });
                        render_side_cart();
                        // $('#count-items').html(response.count)
                        // alert("Producto Agregado al Pedido"); 
                        $('#' + product_id).val(cantidad_minima);

                    }else{
                        iziToast.error({
                                title: 'Error!',
                                message: response.message,
                        }); 
                        console.log(response.console)
                    }
                }
            });

    }
}

function update_cart(e){

    var row_id = e.attr("id");
    var qty    = e.val();
    
    $.ajax({
            url    : window.base + "cart/update_cart",
            method : "POST",
            data   :{ row_id : row_id,
                      qty    : qty 
                    },
            success:function(data)
            {       
                render_side_cart();
                if (typeof render_cart == 'function') { 
                    render_cart()
                }                
            }
        });


}

function remove_item(e){

    var row_id = e.attr("id");

    iziToast.question({
            timeout: 20000,
            close: false,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 9999,
            backgroundColor: '#FFB200',
            title: '¿Estás Seguro?',
            message: 'Quieres eliminar el producto?',
            position: 'center',
            buttons: [
                    ['<button><b>YES</b></button>', function (instance, toast) {
     
                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
     
                            $.ajax({
                                url    : window.base + "cart/remove_from_cart",
                                method : "POST",
                                data   : {row_id:row_id},
                                success:function(data)
                                {

                                        var response = $.parseJSON(data)

                                        if ( response.code == 1 || response.code == '1' ) {

                                            iziToast.success({
                                                    title: 'OK',
                                                    message: response.message,
                                            });

                                            // $('#' + product_id).val(1);
                                            // $('#count-items').html(response.count)
                                            render_side_cart();
                                            if (typeof render_cart == 'function') { 
                                                render_cart()
                                            } 

                                        }else{

                                            iziToast.error({
                                                    title: 'Error!',
                                                    message: response.message,
                                            }); 

                                            console.log(response.console)

                                        }

  
                                }
                            });

                    }, true],
                    ['<button>NO</button>', function (instance, toast) {
     
                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
     
                    }],
            ],
    });



}

function render_side_cart(){
    build_cart_table();
    $('#side-cart-content table').empty();
    $('#side-cart-content table').append(cart_list);    
}

function build_cart_table(){
    $.ajax({
        async  : false,
        url    : window.base + 'cart/get_cart_content',
        method : 'POST',
        dataType: "html",          
        success: function(response){
            data = JSON.parse(response)

            updateCartList(data.items)
            $('#count-items').html(data.cantidad)

            // if(parseInt(data.cantidad) < 1 ){
            //     $('#btn-new-order').toggleClass('d-none')
            //     $('.enviar_pedido').toggleClass('d-none')
            //     $('.guardar_borrador').toggleClass('d-none')
            // } 
        }
    })  
}

function updateCartList(data){
    cart_list = data; 
}

