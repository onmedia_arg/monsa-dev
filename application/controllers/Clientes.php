<?php
include_once(FCPATH."/application/controllers/BaseController.php");
 
class Clientes extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();

        // Load form validation library
        $this->load->library('form_validation');

        if ($this->is_monsa_login()) {
            $this->load->model('Clientes_model');
            $this->load->model('TipoCliente_model');
            $this->load->model('Base_model');
            $this->load->model('RelClienteUsuario_model');
            $this->load->model('Order_hdr_model');

            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of cliente
     */
    function index()
    {
        $data['cliente'] = $this->Clientes_model->get_all_clientes(); 
        $data['user'] = $this->user;
        $data['_view'] = 'clientes/index';
        $this->load->view('layouts/main', $data);
    }
    /*
    * Functions Package
    */
    use FamilyFuntions;
}

trait FamilyFuntions {
    /*
     * Adding a new clientes
     */
    function add()
    {   

        $data['tipos_usuarios'] = $this->TipoCliente_model->get_all_tc();

        // Si envia formulario
        if ( isset( $_POST ) && ! empty( $_POST ) ) {

            // Check validation for user input in SignUp form
            $this->form_validation->set_rules('razon_social', 'Nombre / Razón Social', 'required')
                                    ->set_rules('cuit', 'CUIT', 'required')
                                    ->set_rules('telefono', 'Telefono', 'required')
                                    ->set_rules('mail', 'Mail', 'required|valid_email')
                                    ->set_rules('direccion', 'Dirección', 'required')
                                    ->set_rules('usuario', 'Usuario', 'required')
                                    ->set_rules('nombre_usuario', 'Nombre y Apellido del Usuario', 'required');

            if ($this->form_validation->run() !== FALSE) { 

                $params['username'] = $this->input->post('usuario');
                $params['email'] = $this->input->post('mail');
                $params['passwd'] = $this->input->post('password_usuario');
                $params['passwd'] = sha1($params['passwd']); 
                $params['auth_level'] = 1;

                $verify = $this->Base_model->verifyUserAndEmail( $params['username'] , $params['email'] );

                if ($verify != false) {

                    $this->session->set_flashdata( 'alert_message', "Usuario y/o email ya esta registrado, intenta recuperar contraseña o crea un nuevo usuario." );   
                    redirect( base_url('Start/showRegisterForm'), 'refresh' );

                }else{

                    // Usuario
                    $user_id = $this->user_model->add_user($params);

                    // Cliente
                    $client_data = array(
                                        'razon_social' => $this->input->post('razon_social'),
                                        'idTipoCliente' => $this->input->post('tipo_cliente'),
                                        'cuit' => $this->input->post('cuit'),
                                        'direccion' => $this->input->post('direccion'),
                                        'telefono' => $this->input->post('telefono'),
                                        'mail' => $params['email'],
                                        'is_activo' => 0, 
                                        'id_cliente_interno' => $this->input->post('id_cliente_interno'),
                                    ); 

                    $client_id = $this->Clientes_model->add_cliente( $client_data );

                    // Relacion Cliente Usuario
                    $rel_data = array(
                                    'idCliente' => $client_id,
                                    'idUsuario' => $user_id,
                                );

                    $rel = $this->RelClienteUsuario_model->add_rel( $rel_data );
                    
                    if ( $user_id && $client_id && $rel ) {

                        // Enviar Correo
                        ob_start(); 
                            $this->load->view( 'layouts/email/email-header' );
                                $this->load->view( 'layouts/email/new-user' );
                            $this->load->view( 'layouts/email/email-footer');
                        $body = ob_get_clean(); 

                        // Correo usuario
                        $send = $this->Base_model->send_email_template( 'Gracias por registrarte!', $params['email'], $body );

                        // Enviar Correo
                        ob_start(); 
                            $this->load->view( 'layouts/email/email-header' );
                                $this->load->view( 'layouts/email/new-user-admin' );
                            $this->load->view( 'layouts/email/email-footer');
                        $bodyAdmin = ob_get_clean(); 

                        // Correo usuario
                        $sendAdmin = $this->Base_model->send_email_template( 'Nuevo usuario registrado', $this->Base_model->get_sysopt_value('admin_mail'), $bodyAdmin );

                    }

                }

                $this->session->set_flashdata( 'success_message', 'Cliente agregado correctamente!' );    

                redirect( 'clientes/index' );

            }else{

                $this->session->set_flashdata( 'alert_message', validation_errors() );   

                $data['_view'] = 'clientes/add';
                $data['user'] = $this->user;
                $this->load->view('layouts/main',$data);

            }

        }else{

                $data['_view'] = 'clientes/add';
                $data['user'] = $this->user;
                $this->load->view('layouts/main',$data);

        }
    }  

    /*
     * Editing a clientes
     */
    function edit( $idCliente )
    {   
        // check if the clientes exists before trying to edit it
        $data['cliente'] = $this->Clientes_model->get_cliente($idCliente); 
        $data['usuarios'] = $this->Clientes_model->get_client_users( $idCliente );
        $data['ordenes'] = $this->Order_hdr_model->get_orders_by_client( $idCliente );
        $data['tipos_usuarios'] = $this->TipoCliente_model->get_all_tc();        
        
        if(isset($data['cliente']['idCliente']))
        {

            // Si envia formulario
            if ( isset( $_POST ) && ! empty( $_POST ) ) {


                // Check validation for user input in SignUp form
                $this->form_validation->set_rules('razon_social', 'Nombre / Razón Social', 'required')
                                        ->set_rules('cuit', 'CUIT', 'required')
                                        ->set_rules('telefono', 'Telefono', 'required')
                                        ->set_rules('mail', 'Mail', 'required|valid_email')
                                        ->set_rules('direccion', 'Dirección', 'required');

                // Si todo esta bien
                if ( $this->form_validation->run() !== FALSE ) {

                    // Cliente
                    $client_data = array(
                                        'razon_social' => $this->input->post('razon_social'),
                                        'idTipoCliente' => $this->input->post('tipo_cliente'),
                                        'cuit' => $this->input->post('cuit'),
                                        'direccion' => $this->input->post('direccion'),
                                        'telefono' => $this->input->post('telefono'),
                                        'mail' => $this->input->post('mail'),
                                        'email_vendedor' => $this->input->post('email_vendedor'),
                                        'is_activo' => 0,  
                                    ); 
                    var_dump($client_data);die;
                    
                    $this->Clientes_model->update_cliente( $idCliente, $client_data );   
                    $this->session->set_flashdata( 'success_message', 'Cliente actualizado correctamente!' );    

                    redirect('clientes/index', 'refresh');

                }else{

                    $this->session->set_flashdata( 'alert_message', validation_errors() );   

                    $data['_view'] = 'clientes/edit';
                    $data['user'] = $this->user;
                    $this->load->view('layouts/main',$data);

                }

            }else{

                $data['_view'] = 'clientes/edit';
                $data['user'] = $this->user;
                $this->load->view( 'layouts/main', $data );

            }  

        }else{
            $this->session->set_flashdata( 'alert_message', 'El cliente seleccionado no existe.' );   
            redirect('clientes/index', 'refresh');
        }

    } 

    /*
     * Deleting clientes
     */
    function remove($idCliente)
    {

        $clientes = $this->Clientes_model->get_cliente($idCliente);

        // check if the clientes exists before trying to delete it
        if(isset($clientes['idCliente']))
        {
            $this->Clientes_model->delete_cliente($idCliente);
            $this->session->set_flashdata( 'success_message', 'El cliente ha sido eliminado.' );   

            redirect('clientes/index');
        }else{
            $this->session->set_flashdata( 'alert_message', 'El cliente seleccionado no existe.' );   
            redirect('clientes/index');
        }

    } 

    public function update_client_status( $idCliente = null, $status = null ){

        if ( $idCliente !== null && $status !== null ) {

            $update = $this->Clientes_model->update_cliente( $idCliente, ['idCliente' => $idCliente, 'is_activo' => $status ] );

            if ( $this->db->affected_rows() >= 1 ) {
        
                    $return = json_encode(['code' => 1, 'message' => 'Cliente actualizado correctamente.']);

            }else{

                    $return = json_encode(['code' => 0, 'message' => 'Hubo un error y no se pudo actualizar.']);

            }

        }else{

            $return = json_encode(['code' => 0, 'message' => 'No hay datos.']);

        }

        echo $return;

    }

    public function get_client_users_json( $idCliente ){

        $users = $this->Clientes_model->get_client_users( $idCliente );

        echo json_encode( $users );

    }

}
