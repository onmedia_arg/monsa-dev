<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class User extends BaseController{
    private $user;
    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('User_model');
            $this->load->model('Clientes_model');
            $this->load->model('Order_hdr_model');
            $this->load->model('RelClienteUsuario_model');
            // Load form validation library
            $this->load->library('form_validation');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of users
     */
    function index()
    {
        $data['users'] = $this->User_model->get_all_users();
        
        $data['_view'] = 'user/index';
        $data['user'] = $this->user;
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new user
     */
    function add()
    {   
        
        $data['clientes'] = $this->Clientes_model->get_all_clientes();

        // Si envia formulario
        if ( isset( $_POST ) && ! empty( $_POST ) ) {

            // Check validation for user input in SignUp form
            $this->form_validation->set_rules('username', 'Nombre de Usuario', 'required')
                                    ->set_rules('email', 'Correo', 'required')
                                    // ->set_rules('auth_level', 'Nivel de Usuario', 'required')
                                    ->set_rules('passwd', 'Contraseña', 'required');

            // Si todo esta bien
            if ( $this->form_validation->run() !== FALSE ) {

                $params = array(
                    // 'user_id' => mt_rand(1000000000, 9999999999),
                    'username' => $this->input->post('username'),
                    'email' => $this->input->post('email'),
                    'auth_level' => '1',
                    'passwd' => sha1($this->input->post('passwd')),
                );
                $existe_email = $this->User_model->verify_user_email($this->input->post('email'));
                $existe_username = $this->User_model->verify_user_nick($this->input->post('username'));
                if($existe_email || $existe_username){
                    //print_r("TRUE");exit();
                    $this->session->set_flashdata( 'alert_message', 'El usuario se encuentra registrado.' ); 
                    $data['_view'] = 'user/add';
                    $data['user'] = $this->user;
                    $this->load->view('layouts/main',$data);
                    

                }else{
                    //print_r("FALSE");exit();
                    $user_id = $this->User_model->add_user($params);

                    // Si asigna cliente asociado
                    if ( isset( $_POST['cliente'] ) && $_POST['cliente'] !== 0 ) {

                        // Relacion Cliente Usuario
                        $rel_data = array(
                                        'idCliente' => $_POST['cliente'],
                                        'idUsuario' => $user_id,
                                    );

                        $rel = $this->RelClienteUsuario_model->add_rel( $rel_data );

                    }

                    $this->session->set_flashdata( 'success_message', 'Usuario agregado correctamente!' );   
                    redirect('user/index');
                }
                

            }else{

                $this->session->set_flashdata( 'alert_message', validation_errors() );   

                $data['_view'] = 'user/add';
                $data['user'] = $this->user;
                $this->load->view('layouts/main',$data);

            }

        }else{
 
            $data['_view'] = 'user/add';
            $data['user'] = $this->user;
            $this->load->view('layouts/main',$data);

        }
 
    }  

    /*
     * Editing a user
     */
    function edit($user_id)
    {   
        // check if the user exists before trying to edit it
        $data['user'] = $this->User_model->get_user($user_id);
        $data['clientes'] = $this->Clientes_model->get_all_clientes();
        
        if(isset($data['user']['user_id']))
        {

            // Si envia formulario
            if ( isset( $_POST ) && ! empty( $_POST ) ) {

                // Check validation for user input in SignUp form
                $this->form_validation->set_rules('username', 'Nombre de Usuario', 'required')
                                        ->set_rules('email', 'Correo', 'required')
                                        ->set_rules('auth_level', 'Nivel de Usuario', 'required')
                                        ->set_rules('banned', 'Banned', 'required|numeric');

                // Si todo esta bien
                if ( $this->form_validation->run() !== FALSE ) {

                    $fecha = new DateTime();
                    $modified_at = $fecha->format('Y-m-d H:i:s');
                    $params = array(
    					'username' => $this->input->post('username'),
    					'email' => $this->input->post('email'),
    					'auth_level' => $this->input->post('auth_level'),
    					'banned' => $this->input->post('banned'),
    					'modified_at' => $modified_at,
                    );

                    $this->User_model->update_user($user_id,$params);   
                    $this->session->set_flashdata( 'success_message', 'Usuario actualizado correctamente!' );    
                    redirect('user/index');

                }
                else
                {
                    $this->session->set_flashdata( 'alert_message', validation_errors() );   
                    $data['_view'] = 'user/edit';
                    $this->load->view('layouts/main',$data);
                }

            }else{

                $data['_view'] = 'user/edit';
                $this->load->view('layouts/main',$data);

            }  

        }else{
            $this->session->set_flashdata( 'alert_message', 'El usuario seleccionado no existe.' );   
            redirect('user/index', 'refresh');
        }

    } 

    /* 
     * Deleting user
     */
    function remove($user_id)
    {
        $user = $this->User_model->get_user($user_id);

        // check if the user exists before trying to delete it
        if(isset($user['user_id']))
        {
            if(count($this->Order_hdr_model->get_orders_by_user_pending($user_id)) == 0){
                $this->User_model->delete_user($user_id);
                echo returnJson( ['status' => 200, 'title' => 'Usuario', 'message' => 'Usuario eliminado correctamente!' ] );
            }else{
                echo returnJson( ['status' => 500, 'title' => 'Usuario', 'message' => 'El usuario posee ordenes pendientes!' ] );
            }

        }else{
            echo returnJson( ['status' => 500, 'title' => 'Usuario', 'message' => 'Usuario no se ha podido eliminar!' ] );
        }
    }

    /**
     * Log out
     */
    public function logout()
    {
        $this->authentication->logout();

        // Set redirect protocol
        $redirect_protocol = USE_SSL ? 'https' : NULL;

        redirect( site_url( LOGIN_PAGE . '?' . AUTH_LOGOUT_PARAM . '=1', $redirect_protocol ) );
    }    
    
    public function update_user_status( $user_id = null, $status = null ){

        if ( $user_id !== null && $status !== null ) {

            $update = $this->user_model->update_user( $user_id, ['user_id' => $user_id, 'is_active' => $status ] );

            if ( $this->db->affected_rows() >= 1 ) {
        
                    $return = json_encode(['code' => 1, 'message' => 'Usuario actualizado correctamente.']);

            }else{

                    $return = json_encode(['code' => 0, 'message' => 'Hubo un error y no se pudo actualizar.']);

            }

        }else{

            $return = json_encode(['code' => 0, 'message' => 'No hay datos.']);

        }

        echo $return;

    }

}
