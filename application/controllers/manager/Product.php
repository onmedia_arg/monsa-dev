<?php
include_once(FCPATH."/application/controllers/BaseController.php");

class Product extends BaseController{
    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('manager/Productosingle_model', 'Productsingle', true);
            $this->load->model('manager/Marca_model', 'Marcas', true);
            $this->load->model('manager/Familia_model', 'Familias', true);
            $this->load->model('manager/Atributo_model', 'Atributos', true);
            $this->load->model('manager/ProductoAtributo_model', 'ProductoAtributos', true);   
            $this->load->model('manager/Sysoptions_model', 'Sysoptions', true);  
            $this->load->model('manager/Apilog_model', 'Apilog', true); 
            $this->load->model('manager/Products_model', 'Products_model', true);     
            $this->load->model('manager/Tag_model', 'Tags', true);
            $this->load->model('manager/Carrousel_model', 'Carrousel', true);
            $this->load->model('manager/ProductCarrousel_model', 'ProductCarrousel', true);
            $this->user = $this->dataUser();
        
        }else{
        
            redirect('/', 'refresh');
        }
    } 

    function index($idProducto){

        $script = array(
            base_url('assets/manager/plugins/forms/validation/validate.min.js'),            
            // base_url('assets/manager/js/products/product.js?v='.time()),
            base_url('assets/manager/js/products/product.js'),
        );
                
        $csss = array(
        );

        $familias = $this->Familias->get_familias();
        $marcas   = $this->Marcas->get_marcas();
        $producto = $this->Productsingle->get_detail($idProducto);
        $productoCarrousel = $this->ProductCarrousel->getCarrouselByProduct($idProducto);

        $tags     = $this->Tags->get_tags();
        $carruseles = $this->Carrousel->getData();
        


        $data = array(
            "scripts"	=>	$script,
            "csss"		=>	$csss,
            "title"		=>	'Producto',  
            "buttons"	=>	[
                                array(
                                    'title' => 'Guardar',
                                    'url'   => '#', //base_url('manager/Products/import'),
                                    'icon'  => 'icon-floppy-disk',
                                    'attr'  => 'id="btnUpdateProduct"',
                                ),
                                array(
                                    'title' => 'Eliminar',
                                    'url'   => '#', //base_url('manager/Products/import'),
                                    'icon'  => 'icon-trash',
                                    'attr'  => 'id="btnDeleteProduct"',
                                ),
                            ],
            "breadcumbs" =>  array(
                                'Inicio' => base_url(),
                                'Productos' => null,
                                'Detalle' => null,
                            ),
            "idProducto" => $idProducto,
            "familias"  => $familias,
            "marcas"    => $marcas,
            "producto"  => $producto,
            "tags"      => $tags,
            "carruseles" => $carruseles,
            "productoCarrousel" => $productoCarrousel
            //"imagenes"  => $producto["imagen"],
           //"modals" =>  'manager/products/modal-import',
        );

        // enviamos dentro de $data array con js y css espeíficos de la hoja
        //Cargamos la vista correspondiente
        $this->load->view('manager/products/single/index', $data);         

    } 
    function create(){

        $script = array(
            base_url('assets/manager/plugins/forms/validation/validate.min.js'),            
            base_url('assets/manager/js/products/create.js?v='.time()),
        );
                
        $csss = array(
        );

        $familias = $this->Familias->get_familias();
        $marcas   = $this->Marcas->get_marcas();

        
        // $producto = $this->Productsingle->get_detail($idProducto);
        $tags     = $this->Tags->get_tags();

        $data = array(
            "scripts"	=>	$script,
            "csss"		=>	$csss,
            "title"		=>	'Producto',  
            "buttons"	=>	[
                                array(
                                    'title' => 'Guardar',
                                    'url'   => '#',
                                    'icon'  => 'icon-floppy-disk',
                                    'attr'  => 'id="btnSaveProduct"',
                                ),
                                array(
                                    'title' => 'Cancelar',
                                    'url'   => '#',
                                    'icon'  => 'icon-cross',
                                    'attr'  => 'id="btnCancelProduct"',
                                ),
                            ],
            "breadcumbs" =>  array(
                                'Inicio' => base_url(),
                                'Productos' => null,
                                'Nuevo' => null,
                            ),
            // "idProducto" => $idProducto,
            "familias"  => $familias,
            "marcas"    => $marcas,
            // "producto"  => $producto,
            // "tags"      => $tags,
            //"imagenes"  => $producto["imagen"],
           //"modals" =>  'manager/products/modal-import',
        );

        // enviamos dentro de $data array con js y css espeíficos de la hoja
        //Cargamos la vista correspondiente
        $this->load->view('manager/products/create/index', $data);         

    } 


    function updateProduct(){
        
        $idProducto = $this->security->xss_clean( $this->input->post('idProducto') );
		$details_data = [
            'nombre'      => $this->security->xss_clean( $this->input->post('nombre') ),
            'idFamilia'   => $this->security->xss_clean( $this->input->post('idFamilia') ),
            'idMarca'     => $this->security->xss_clean( $this->input->post('idMarca') ),
            'modelo'      => $this->security->xss_clean( $this->input->post('modelo') ),
            'precio'      => $this->security->xss_clean( $this->input->post('precio') ),
            'price_public'      => $this->security->xss_clean( $this->input->post('price_public') ),
            'idTipo'      => $this->security->xss_clean( $this->input->post('idTipo') ),
            'sku'         => $this->security->xss_clean( $this->input->post('sku') ),
            'slug'        => $this->security->xss_clean( $this->input->post('slug') ),
            'search'      => $this->security->xss_clean( $this->input->post('search') ),
            'stock'       => $this->security->xss_clean( $this->input->post('stock') ),
            'descripcion' => $this->security->xss_clean( $this->input->post('descripcion') ),
            'stock_type'  => $this->input->post('stock_type'),
            'stock_fijo'  => $this->input->post('stock_fijo'),
            'stock_quantity' => $this->input->post('stock_quantity'),
            'stock_limit1' => $this->input->post('stock_limit1'),
            'stock_limit2' => $this->input->post('stock_limit2'),
            'cantidad_minima' => $this->input->post('cantidad_minima'),
            'rango' => $this->input->post('rango'),
            'visibilidad' => $this->input->post('visibilidad'),
            'show'        => $this->security->xss_clean( $this->input->post('show') ),
            'peso'        => $this->security->xss_clean( $this->input->post('peso') ),  
            'alto'        => $this->security->xss_clean( $this->input->post('alto') ),  
            'ancho'       => $this->security->xss_clean( $this->input->post('ancho') ),  
            'largo'       => $this->security->xss_clean( $this->input->post('largo') ),

            'is_promo_active' => $this->security->xss_clean( $this->input->post('is_promo_active') ),
            'price_promo' => $this->security->xss_clean( $this->input->post('price_promo') ),


        ];       
        
        $atributos = json_decode($this->input->post('atributos'), true);
        $updateAttr = $this->ProductoAtributos->updateProductoAtributo($atributos);


        $tag = $this->security->xss_clean( $this->input->post('tags') );

        if(!empty($tag)){
            $tag_id = $this->Tags->get_tag_id($tag);
        }else{
            $tag_id = null;
        }

        $details_data['idTag'] = $tag_id;

        if(!empty($this->input->post('carrousel'))){
            $carrousel = $this->security->xss_clean( $this->input->post('carrousel') );
            $carrousel = explode(';', $carrousel);
            
            $this->ProductCarrousel->syncCarrousel($idProducto, $carrousel);
        }else{
            $this->ProductCarrousel->deleteCarrousel($idProducto);
        }

        $update = $this->Productsingle->updateProduct($idProducto, $details_data); 

        if ( $update ) {
             echo returnJson( ['status' => 200, 'title' => 'Bien Hecho!', 'message' => 'Producto modificado exitosamente' ] );
		}else{
			echo returnJson( ['status' => 0, 'message' => 'Error al guardar los cambios' ] );  
		}
    }

    function uploadImage(){

        $fileName = $this->input->post('fileName');
        $familia  = $this->input->post('familia');
        $folder   = 'resources/img/uploads/' . $familia . '/';

        $file					 = 'file';
        $config['upload_path']   = $folder;
        $config['file_name']     = $fileName;
        $config['allowed_types'] = 'jpg|png|jpeg|bmp';
        $config['max_size']      = 10000;
        $config['max_width']     = 2048;
        $config['max_height']    = 2048;
    
        $this->load->library('upload', $config);

        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
            chmod($folder, 0777);
        }

        $data=$this->upload->do_upload($file);
    
        if(!$data){ //Error

            $data = $this->upload->display_errors();
            $response=[
                'status'	=>400,
                'type'		=>'danger',
                'title'     => 'Ups!',
                'message'	=>'Error en el archivo: '.$data
            ];
            
        }else{ //Success

            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            
            // $file_name = $upload_data['file_name'];
            
            $idProducto = $this->security->xss_clean( $this->input->post('idProducto') );

            $details_data = [
                'imagen'  => $this->security->xss_clean( $this->input->post('imagen') ),
            ];

            $update = $this->Productsingle->updateProduct($idProducto, $details_data);
            
            $response=[
                'status'	      => 200,
                'type'		      => 'success',
                'title'           => 'Bien Hecho!',
                'message'	      => 'Archivo subido correctamente',
            ];
    
            }
    
            //convertimos la respuesta a json
            echo json_encode($response);
    }

    function deleteImage(){

        $idProducto = $this->security->xss_clean( $this->input->post('idProducto') );
        $details_data = [
            'imagen'  => $this->security->xss_clean( $this->input->post('imagen') ),
        ];
        $image_to_remove = $this->security->xss_clean( $this->input->post('remove') );
        
        //TODO: Probar en servidor el borrado fisico de la imagen
        //delete_files( base_url($image_to_remove) );
        // Verificar si el archivo existe
        if (file_exists('.'.$image_to_remove)) {
            // Borrar el archivo
            if (!unlink('.'.$image_to_remove)) {
                echo returnJson( ['status' => 0, 'title' => 'Ups!', 'message' => 'Error al eliminar el archivo' ] );  
            }
        } else {
            echo returnJson( ['status' => 0, 'title' => 'Ups!', 'message' => 'Error no se encontro el archivo en el servidor' ] );  
        }
        $update = $this->Productsingle->updateProduct($idProducto, $details_data); 

        if ( $update ) {
			echo returnJson( ['status' => 200, 'title' => 'Bien Hecho!', 'message' => 'Imagen borrada exitosamente' ] );
		}else{
			echo returnJson( ['status' => 0, 'title' => 'Ups!', 'message' => 'Error al guardar los cambios' ] );  
        }
        
    }

    function getAtributos(){

        $idProducto = $this->security->xss_clean($this->input->post('idProducto'));
        $idFamilia = $this->security->xss_clean($this->input->post('idFamilia'));

        $atributosByFamilia = $this->Atributos->getAtributosByFamilia($idFamilia);
        $atributosByProduct = $this->ProductoAtributos->getAtributosByProduct($idProducto);

        $response = [
            'status'    => 200,
            'atributos' => $atributosByFamilia,
            'valores'   => $atributosByProduct

        ];

        //convertimos la respuesta a json
        echo json_encode($response);
   
   
    }

    public function update_stock(){
        $client = new \GuzzleHttp\Client();
        $sku = $this->input->post('sku');
        $url_endpoint = '';
        $process_config = $this->Sysoptions->getProcessState();
        
        foreach ($process_config as $r) {
            switch ($r['opt_name']) {
                case 'stock_update_endpoint':
                    $url_endpoint = $r['opt_value'];
                    break;
                }
        }
        
        //Formateo el Body
        $data = '[{"Clave" : "'.str_pad($sku, 6, 0, STR_PAD_LEFT).'"} ]';
        $header = array('Content-Type' => 'application/json');
        $url_endpoint;
                
        try {
            $response = $client->request('GET', $url_endpoint,
                                            ['headers' => $header, 
                                            'body'    => $data]);
            $r = $response->getBody()->getContents();
            $list = json_decode($r);
                
            $api_data['url'] = strval($url_endpoint);
            $api_data['payload'] = strval($data);
            $api_data['response'] = strval($r);
            $api_data['status'] = strval($response->getStatusCode());
            $this->Apilog->insertApiLog($api_data);
            //Actualiza    
            if($list[0]->Nombre != 'Codigo Inexistente'){
                $sku = ltrim($list[0]->Clave, "0"); 
                $value['stock_quantity'] = $list[0]->Existencias;
                $this->Products_model->updateProductSku($sku, $value);
            }
            echo json_encode( [ 'code'   => 200, 
                                'message' => 'El proceso de actualización finalizo satisfactoriamente',
                                'data' => $list[0]->Existencias ] );
        } catch (\Throwable $th) {

            $api_data['url'] = strval($url_endpoint);
            $api_data['payload'] = strval($data);
            $api_data['response'] = strval($th->getMessage());
            $api_data['status'] = strval($th->getCode());
            $this->Apilog->insertApiLog($api_data);
            echo json_encode( [ 'code'   => 500, 
                                'message' => 'El proceso no se pudo ejecutar correctamente']);
        }
		
    }

    function remove($idProducto)
    {
        try {
            $producto = $this->Productsingle->delete_producto($idProducto);
            if($producto){
                echo returnJson(['status' => 200, 'title' => 'Producto', 'message' => 'Producto eliminado correctamente!']);
            }else{  
                echo returnJson(['status' => 200, 'title' => 'Producto', 'message' => 'No se pudo realizar la eliminacion correctamente!']);
            }
        } catch (\Throwable $th) {
            echo json_encode( [ 'code'   => 500, 
                                'message' => 'El proceso no se pudo ejecutar correctamente']);
        }
    }

    public function update_visibilidad($idProducto = null, $visibilidad = null)
    {

        if ($idProducto !== null && $visibilidad !== null) {

            $update = $this->Productsingle->updateProduct($idProducto, ['visibilidad' => $visibilidad]);

            if ($this->db->affected_rows() >= 1) {

                $return = json_encode(['code' => 1, 'message' => 'Producto actualizado correctamente.']);
            } else {

                $return = json_encode(['code' => 0, 'message' => 'Hubo un error y no se pudo actualizar.']);
            }
        } else {

            $return = json_encode(['code' => 0, 'message' => 'No hay datos.']);
        }

        echo $return;
    }

    public function update_promo($idProducto = null, $is_promo_active = null)
    {

        if ($idProducto !== null && $is_promo_active !== null) {

            $update = $this->Productsingle->updateProduct($idProducto, ['is_promo_active' => $is_promo_active]);

            if ($this->db->affected_rows() >= 1) {

                $return = json_encode(['code' => 1, 'message' => 'Producto actualizado correctamente.']);
            } else {

                $return = json_encode(['code' => 0, 'message' => 'Hubo un error y no se pudo actualizar.']);
            }
        } else {

            $return = json_encode(['code' => 0, 'message' => 'No hay datos.']);
        }

        echo $return;
    }
}
