<?php
include_once(FCPATH . "/application/controllers/BaseController.php");
class Downloads extends BaseController
{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('manager/Downloads_model', 'Downloads');
            $this->load->model('manager/Sysoptions_model', 'Sysoptions', true);
            $this->load->model('manager/Promotions_model', 'Promotions', true);
            $this->user = $this->dataUser();
        } else {
            redirect('/', 'refresh');
        }
    }


    function index()
    {

        $script = array(
            base_url('assets/manager/js/downloads/files.js?v=' . time()),
            base_url('assets/manager/js/downloads/promotions.js?v=' . time()),
            'https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js'
        );

        $csss = array(
            'https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css'
        );


        $data = array(
            "scripts" => $script,
            "csss" => $csss,
            "title" => 'Descargas',
            "breadcumbs" => array(
                'Inicio' => base_url(),
                'Descargas' => null,
            ),
            "view" => 'manager/downloads/files'
        );

        //Enviamos dentro de $data array con js y css espeíficos de la hoja
        $this->load->view('manager/downloads/index', $data);
    }

    public function get_all_files()
    {

        $files = $this->Downloads->getAll();
        $msj = $this->db->get_where('system_options', array('opt_name' => 'home_dwnl_msj'))->row();
        $promotions = $this->Promotions->getAll();

        echo json_encode([
            'status' => 200,
            'files' => $files,
            'msj' => $msj,
            'promotions' => $promotions
        ]);

    }

    public function get_home_file()
    {

        $files = $this->Downloads->getAll();
        $msj = $this->db->get_where('system_options', array('opt_name' => 'home_dwnl_msj'))->row();

        echo json_encode(['status' => 200, 'files' => $files, "msj" => $msj]);

    }

    public function upload_file()
    {

        if (empty($_FILES)) {
            log_message('error', 'No files were uploaded.');
        } else {
            log_message('info', 'Files uploaded: ' . print_r($_FILES, true));
        }
        $folder = 'uploads/descargas/';

        // Check if folder exists, create if not
        if (!is_dir($folder)) {
            mkdir($folder, 0777, true);
        }

        $file = 'file';
        $fileName = $_FILES[$file]['name'];

        $config = [
            'upload_path' => $folder,
            'file_name' => $fileName,
            'allowed_types' => 'xls|xlsx|pdf',
            'max_size' => 40000,
            'max_width' => 2048,
            'max_height' => 2048
        ];

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($file)) {
            $error = $this->upload->display_errors('', '');
            $response = [
                'status' => 400,
                'type' => 'danger',
                'message' => 'Error en el archivo: ' . $error
            ];
        } else {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];
            $type = $this->input->post('type');

            // Delete existing file of the same type
            $clear = $this->Downloads->deleteFile($type);
            if ($clear !== NULL) {
                delete_files($clear->publicfullpath);
            }

            $data = [
                'name' => "Lista Actualizada",
                'description' => $this->input->post('description'),
                'icon' => "",
                'type' => $type,
                'section' => 'HOME',
                'folder' => $folder,
                'fullpath' => $folder . $file_name,
                'publicfullpath' => base_url($folder . $file_name),
                'is_active' => 1,
                'created_by' => $_SESSION['user_id']
            ];

            $this->Downloads->insert($data);
            $upload = $this->Downloads->getFileByType($type);

            $response = [
                'status' => 200,
                'type' => 'success',
                'message' => 'Archivo subido correctamente',
                'upload' => $upload
            ];
        }

        // Convertimos la respuesta a JSON
        echo json_encode($response);
    }
    // public function upload_file(){

    //     $folder   = 'uploads/descargas/';

    //     //check if folder exists
    //     if (!file_exists($folder)) {
    //         mkdir($folder, 0777, true);
    //     }

    // 	$file					 = 'file';

    //     $fileName = $_FILES[$file]['name'] ;

    // 	$config['upload_path']   = $folder;
    // 	$config['file_name']     = $fileName;
    //     $config['allowed_types'] = 'xls|xlsx|pdf';
    //     $config['max_size']      = 40000;
    //     $config['max_width']     = 2048;
    //     $config['max_height']    = 2048;

    //     $this->load->library('upload', $config);

    //     $data=$this->upload->do_upload($file);

    // 	if (!$data) {

    // 		$data = $this->upload->display_errors();

    //         $response=[
    // 			'status'	=>400,
    // 			'type'		=>'danger',
    // 			'message'	=>'Error en el archivo: '.$data
    //         ];

    //     }else{

    // 		$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.

    //         $file_name = $upload_data['file_name'];
    //         $type = $this->input->post('type');

    //         $clear = $this->Downloads->deleteFile($type);
    //         // var_dump($clear->publicfullpath);die;
    //         if($clear != NULL){
    //             delete_files($clear->publicfullpath);
    //         } 


    //         // $this->security->xss_clean(trim($this->input->post('title')))
    //         $data = Array(
    //             'name'           => "Lista Actualizada",
    //             'description'    => $this->input->post('description'),
    //             'icon'           => "",
    //             'type'           => $type,
    //             'section'        => 'HOME',
    //             'folder'         => $folder,
    //             'fullpath'       => $folder . $file_name,
    //             'publicfullpath' => base_url($folder . $file_name),
    //             'is_active'      => 1,
    //             'created_by'     => $_SESSION['user_id']
    //         );

    //         $r = $this->Downloads->insert($data);
    //         $upload = $this->Downloads->getFileByType($type);

    //     	$response=[
    // 			'status'	=> 200,
    // 			'type'		=> 'success',
    //             'message'	=> 'Archivo subido correctamente',
    //             'upload'    => $upload
    // 		];

    //     }

    //     //convertimos la respuesta a json
    //     echo json_encode($response);
    // }

    public function new_file()
    {

        $fileName = $this->input->post('fileName');
        $folder = 'uploads/descargas/';

        $file = 'file';
        $config['upload_path'] = $folder;
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|pdf';
        $config['max_size'] = 10000;
        $config['max_width'] = 2048;
        $config['max_height'] = 2048;

        $this->load->library('upload', $config);

        $data = $this->upload->do_upload($file);

        // var_dump($config);die();

        if (!$data) {

            $data = $this->upload->display_errors();

            $response = [
                'status' => 400,
                'type' => 'danger',
                'message' => 'Error en el archivo: ' . $data
            ];

        } else {

            $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.

            $file_name = $upload_data['file_name'];
            $type = $this->input->post('type');

            if ($type == 'XLS') {
                $icon = '/resources/img/xls.png';
            } else {
                $icon = '/resources/img/pdf.png';
            }


            // $this->security->xss_clean(trim($this->input->post('title')))
            $data = array(
                'name' => $this->input->post('name'),
                'description' => $this->input->post('description'),
                'icon' => $icon,
                'type' => $type,
                'section' => 'HOME',
                'folder' => $folder,
                'fullpath' => $folder . $file_name,
                'publicfullpath' => base_url($folder . $file_name),
                'is_active' => 1,
                'created_by' => $_SESSION['user_id']
            );

            $r = $this->Downloads->insert($data);

            $response = [
                'status' => 200,
                'type' => 'success',
                'message' => 'Archivo subido correctamente',
            ];

        }

        //convertimos la respuesta a json
        echo json_encode($response);
    }

    public function save_msj()
    {
        //Validamos la data enviada por el formulario
        $datos = array(
            'opt_value' => $this->input->post('msj')
        );

        $this->db->where('opt_name', 'home_dwnl_msj');
        $this->db->update('system_options', $datos);

        $json_data = array(
            "status" => 200,
            "message" => 'EL MENSAJE SE ACTUALIZO CORRECTAMENTE'
        );

        echo json_encode($json_data);
        //enviamos los datos al modelo para su insercion
        // $result_field = $this->Sysoptions->update('', $datos);


    }
}