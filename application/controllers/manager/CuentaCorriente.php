<?php
include_once(FCPATH . "/application/controllers/BaseController.php");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);

class CuentaCorriente extends BaseController
{
    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('manager/Clients_model', 'Clientes', true);
            $this->load->model('manager/Sysoptions_model', 'Sysoptions', true);
            $this->user = $this->dataUser();
        } else {
            redirect('/', 'refresh');
        }

        header("Access-Control-Allow-Origin: *");
    }

    function get_cuentaCorriente($id = null)
    {
        $data_saldo = '';
        $data_movimientos = '';
        $cant_dias = 30;
        $fecha = date("Ymd",strtotime(date("d-m-Y")."- ".$cant_dias." days"));

        //CLIENTE GUZZLE
        $client = new \GuzzleHttp\Client();
        $header = array('Content-Type' => 'application/json');

        if($id){
            // Busqueda por Cliente ID
            $id_cliente = $id;
            $resultado = $this->Clientes->get_id_interno_cliente_by_client($id_cliente);
        }else{
            //Busqueda por User_id | Usuario logueado
            $id_user = $this->session->userdata('user_id');
            $resultado = $this->Clientes->get_id_interno_cliente_by_user($id_user);
        }
        
        //print_r("CLIENTE: ".$resultado[0]['id_cliente_interno']);exit();

        if(!$resultado){
            echo json_encode( [ 'code'   => 203, 
                                    'message' => 'Cliente no identificado, por favor comunicarse con su Oficial de Cuenta.']);
                                    exit();
        }
        $id_cliente = $resultado[0]['id_cliente_interno'];

        // Obtener los ENDPOINT
        $url_endpoint_saldo = '';
        $url_endpoint_movimientos = '';
        
        $process_config = $this->Sysoptions->getProcessState();
        foreach ($process_config as $r) {
            switch ($r['opt_name']) {
                case 'webapi_client_getSaldo_endpoint':
                    $url_endpoint_saldo = $r['opt_value'];
                    break;
                case 'webapi_client_getMovimientos_endpoint':
                    $url_endpoint_movimientos = $r['opt_value'];
                    break;
                }
        }

        // OBTENER DATA DE SALDO..
        try {
            $body_saldo = '{"Clave" : "'.str_pad($id_cliente, 5, 0, STR_PAD_LEFT).'"}';
            $response_saldo = $client->request('GET', $url_endpoint_saldo,
                ['headers' => $header, 
                'body'    => $body_saldo]);
            $r_saldo = $response_saldo->getBody()->getContents();
            $data_saldo = json_decode($r_saldo);

            // CONTROLO EXISTENCIA DE ID CLIENTE
            if($data_saldo->Nombre === 'Codigo Inexistente'){
                echo json_encode( [ 'code'   => 203, 
                                'message' => 'Cliente no identificado, por favor comunicarse con su Oficial de Cuenta.']);
            }else{
                $body_movimientos = '{"Clave" : "'.str_pad($id_cliente, 5, 0, STR_PAD_LEFT).'" , "FechaDesde": "'.$fecha.'"}';
                $response_movimientos = $client->request('GET', $url_endpoint_movimientos,
                    ['headers' => $header, 
                    'body'    => $body_movimientos]);

                $r_movimientos = $response_movimientos->getBody()->getContents();
                $data_movimientos = json_decode($r_movimientos);
                
                // FORMATEAR LOS DATOS DE MOVIMIENTOS
                $data['saldo'] = array(
                    "saldo"     => number_format(($data_saldo->Saldo), 2, ',', '.'),
                    "fecha"     => substr($data_saldo->Fecha, 6, 2).'/'.substr($data_saldo->Fecha, 4, 2).'/'.substr($data_saldo->Fecha, 0, 4),
                    "nombre"    => $data_saldo->Nombre
                );
                
                $data['movimientos']['saldoInicio'] = number_format(($data_movimientos->ClienteSaldoInicial->Saldo), 2, ',', '.');
                $data['movimientos']['fechaSaldoInicio'] = substr($data_movimientos->ClienteSaldoInicial->Fecha, 6, 2).'/'.substr($data_movimientos->ClienteSaldoInicial->Fecha, 4, 2).'/'.substr($data_movimientos->ClienteSaldoInicial->Fecha, 0, 4);
                $data['movimientos']['saldoFinal'] = number_format(($data_movimientos->ClienteSaldoFinal->Saldo), 2, ',', '.');
                $data['movimientos']['fechaSaldoFinal'] = substr($data_movimientos->ClienteSaldoFinal->Fecha, 6, 2).'/'.substr($data_movimientos->ClienteSaldoFinal->Fecha, 4, 2).'/'.substr($data_movimientos->ClienteSaldoFinal->Fecha, 0, 4);
                
                foreach ($data_movimientos->ClienteMovimientoSet as $movimiento) {
                    $datos[] = array(
                        "fecha" => substr($movimiento->Fecha, 6, 2).'/'.substr($movimiento->Fecha, 4, 2).'/'.substr($movimiento->Fecha, 0, 4),
                        "tipo" => $movimiento->Tipo,
                        "nro_documento" => $movimiento->Documento,
                        "estado" => $movimiento->Importe > 0 ? 'DEBE' : 'HABER',
                        "referencia" => $movimiento->Referencia,
                        "importe" => number_format(($movimiento->Importe * -1), 2, ',', '.')
                    );
                }
                $data['movimientos']['detalle'] = $datos;

                echo json_encode( [ 'code'   => 200, 
                                'message' => 'Consulta de movimientos se ha realizado correctamente',
                                'title' => 'Consulta de movimiento',
                                'data' => $data] );
            }
            
            
        } catch (\Throwable $th) {
            //print_r($th);exit();
            echo json_encode( [ 'code'   => 500, 
                                'message' => 'El proceso no se pudo ejecutar correctamente']);
        }
    }

    /* function viewMovimientos()
    {
        //CLIENTE GUZZLE
        $client = new \GuzzleHttp\Client();
        $id_cliente = $this->session->userdata('user_id');
        $id_cliente = '03288';
        $cant_dias = 10;

        $url_endpoint_saldo = '';
        $url_endpoint_movimientos = '';
        
        $process_config = $this->Sysoptions->getProcessState();
        foreach ($process_config as $r) {
            switch ($r['opt_name']) {
                case 'webapi_client_getSaldo_endpoint':
                    $url_endpoint_saldo = $r['opt_value'];
                    break;
                case 'webapi_client_getMovimientos_endpoint':
                    $url_endpoint_movimientos = $r['opt_value'];
                    break;
                }
        }
            
        //Formateo el Body
        $fecha = date("Ymd",strtotime(date("d-m-Y")."- ".$cant_dias." days"));
        
        $data_saldo = '{"Clave" : "'.str_pad($id_cliente, 5, 0, STR_PAD_LEFT).'"}';
        $data_movimientos = '{"Clave" : "'.str_pad($id_cliente, 5, 0, STR_PAD_LEFT).'" , "FechaDesde": "'.$fecha.'"}';
        //print_r($data_movimientos);exit();
        $header = array('Content-Type' => 'application/json');

        try {
            $response_saldo = $client->request('GET', $url_endpoint_saldo,
            ['headers' => $header, 
            'body'    => $data_saldo]);
            $r_saldo = $response_saldo->getBody()->getContents();
            $saldo = json_decode($r_saldo);

            $response_movimientos = $client->request('GET', $url_endpoint_movimientos,
            ['headers' => $header, 
            'body'    => $data_movimientos]);
            $r_movimientos = $response_movimientos->getBody()->getContents();
            $movimientos = json_decode($r_movimientos);

            echo json_encode( [ 'code'   => 200, 
                                'message' => 'Consulta de movimientos se ha realizado correctamente',
                                'title' => 'Consulta de movimiento',
                                'saldo' => $saldo,
                                'movimientos' => $movimientos ] );
        } catch (\Throwable $th) {
            print_r($th);exit();
            echo json_encode( [ 'code'   => 500, 
                                'message' => 'El proceso no se pudo ejecutar correctamente']);
        }
        
        //$clientes = $this->Clientes->getAll();
        //echo json_encode(['status' => 200, 'clientes' => $clientes]);
    } */


}
