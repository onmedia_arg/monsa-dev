<?php
include_once(FCPATH."/application/controllers/BaseController.php");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);

class Products extends BaseController{
    private $user;
    private $productImport;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('manager/Products_model', 'Products_model', true);
            $this->load->model('manager/ProductoAtributo_model', 'ProductoAtributo', true);
            $this->load->model('manager/Familia_model', 'Familia_model', true);
            $this->load->model('manager/Marca_model', 'Marcas', true);
            $this->load->model('BulkUpdateLogs_model');
            $this->load->model('manager/Sysoptions_model', 'Sysoptions', true);  
            $this->load->model('manager/Apilog_model', 'Apilog', true);
            $this->load->model('manager/Atributo_model', 'Atributo_model', true);
            $this->load->model('manager/Tag_model', 'Tag_model', true);

            $this->load->helper("logs");
            $this->load->library('ImportProducts');

            $this->user = $this->dataUser();
            $this->productImport = new ImportProducts($this->Products_model, $this->Sysoptions, $this->Apilog);

        }else{
            redirect('/', 'refresh');
        }

		header("Access-Control-Allow-Origin: *");
    } 

    function index(){
        
        $familias = $this->Familia_model->get_familias();
        $marcas   = $this->Marcas->get_marcas();

        $script = array(
            base_url('assets/manager/plugins/datatables/datatables.min.js'),
            base_url('assets/manager/js/products/list.js?v='.time()),
            base_url('assets/manager/js/products/products.js?v='.time()),
        );
                
        $csss = array(
            base_url('assets/manager/plugins/datatables/datatables.min.css'),
            base_url('assets/manager/plugins/datatables/dataTables.bootstrap4.min.css'),
        );

        $data = array(
            "scripts"	=>	$script,
            "csss"		=>	$csss,
            "title"		=>	'Productos',  
            "buttons"	=>	[
                                array(
                                    'title' => 'Actualizar Pr.Base',
                                    'url'   => '#',
                                    'icon'  => 'icon-loop3',
                                    'attr'  => 'id="updatePrecioBase"',
                                ),
                                array(
                                    'title' => 'Actualizar Pr.Publico',
                                    'url'   => '#',
                                    'icon'  => 'icon-loop3',
                                    'attr'  => 'id="updatePrecioPublico"',
                                ),
                                array(
                                    'title' => 'Actualizar Stock',
                                    'url'   => '#',
                                    'icon'  => 'icon-loop3',
                                    'attr'  => 'id="updateStock"',
                                ),
                                array(
                                    'title' => 'Importar',
                                    'url'   => '#',
                                    'icon'  => 'icon-file-excel',
                                    'attr'  => 'data-toggle="modal" data-target="#modal_import"',
                                ),
                                array(
                                    'title' => 'Importar Simple',
                                    'url'   => '#', 
                                    'icon'  => 'icon-file-excel',
                                    'attr'  => 'data-toggle="modal" data-target="#modal_import_simple"',
                                ),
                            ],  
            "breadcumbs"=>  array(
                                'Inicio' => base_url(),
                                'Productos' => null,
                            ),
            "modals" =>  'manager/products/modal-import',
            "familias"  => $familias,
            "marcas"    => $marcas,            
        );

        // enviamos dentro de $data array con js y css espeíficos de la hoja
        //Cargamos la vista correspondiente
        $this->load->view('manager/products/index', $data);         

    } 

    public function get_list_dt(){
        
        $allInputs = json_decode(trim(file_get_contents('php://input')), true);

        //Paginado
        $start = $allInputs["start"];   

        //Ver
        $length = $allInputs["length"];

        //Busqueda
        $search = $allInputs["search"]['value'];

       
        if ($allInputs["order"][0] != ""){
            switch($allInputs["order"][0]["column"]){
                case 0:
                    $column = "pr.sku";
                break;

                case 1:
                    $column = "pr.idTipo";
                break;

                case 2:
                    $column = "fm.nombre";
                break;
                
                case 3:
                    $column = "mr.nombre";
                break;
                
                case 4:
                    $column = "pr.modelo";
                break;

                case 5:
                    $column = "pr.imagen";
                break;
                
                case 6:
                    $column = "pr.precio";
                break; 

                case 7:
                    $column = "pr.visibilidad";
                break;                                
            }
            
            $order = array(
                "column"        => $column,
                "direction"     => $allInputs["order"][0]["dir"]
            );
        
        }

        // var_dump($allInputs["idFamilia"]);die;
        $filtro = array('familia'     => $allInputs["idFamilia"], //$allInputs["status"]
                        'marca'       => $allInputs["idMarca"],
                        'show'        => $allInputs["show"],
                        'promo'       => $allInputs["promo"]);
        
        $result = $this->Products_model->getAllProducts($start, $length, $search, $filtro, $order);
        
        $resultado = $result["datos"];

        $numrows = $result["numRows"];
        
        $numrowstotal = $this->Products_model->getNumRowAll();

        $data = array();

        foreach ($resultado->result_array() as $result_row) {

            switch ($result_row["visibilidad"]) {
                case 'publicar':
                    $visibilidad = '<input  type="checkbox" data-on-text="Si" data-off-text="No" class="visibilidad" name="visibilidad" value="' . $result_row["idProducto"] . '" checked>';
                    break;

               default:
                    $visibilidad = '<input type="checkbox" data-on-text="Si" data-off-text="No" class="visibilidad" name="visibilidad" value="' . $result_row["idProducto"] . '" >';
                    break;
            }

            if($result_row["is_promo_active"] == 1) {
                $promo = '<input  type="checkbox" data-on-text="Si" data-off-text="No" class="is_promo_active" name="is_promo_active" value="' . $result_row["idProducto"] . '" checked>';
            }else{
                $promo = '<input type="checkbox" data-on-text="Si" data-off-text="No" class="is_promo_active" name="is_promo_active" value="' . $result_row["idProducto"] . '" >';
            }


            switch($result_row["idTipo"]){
                case 0: 
                    $tipo = "Simple";
                break;
                
                case 1:
                    $tipo = "Variable";
                break;

                case 2:
                    $tipo = "Variación";
                break;
                }
            
            
            $images = "";
            if($result_row['imagen'] !=""){
                $images = '<i class="icon-images3"></i>';
            }

            $actions = '<div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">

                                <a href="'.base_url('manager/Product/index/'). $result_row["idProducto"]. '" class="dropdown-item" title="Ver Producto">
                                    <i class="icon-file-eye"></i>Ver Producto
                                </a> 
                                <a  onclick="updateOneStock('.$result_row["sku"].')" class="dropdown-item" title="Actualizar Stock">
                                    <i class="icon-loop3"></i>Actualizar Stock
                                </a> 
                                </div>
                            </div>
                        </div>'; 
            
            $row_of_data = array();

            $row_of_data[] = $result_row["sku"];
            $row_of_data[] = $tipo;
            $row_of_data[] = $result_row["familiaTxt"];
            $row_of_data[] = $result_row["marcaTxt"];
            $row_of_data[] = $result_row["modelo"];
            $row_of_data[] = $images;
            $row_of_data[] = "$ " . number_format($result_row["precio"],2, ",", ".");            
            $row_of_data[] = $promo;
            $row_of_data[] = $visibilidad;
            $row_of_data[] = $actions;          

            $data[] = $row_of_data;

        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($numrowstotal),
            "recordsFiltered" => intval($numrows),//intval($numrows),
            "data"            => $data
        );
        echo json_encode($json_data);
    }

    function get_import_templates(){

        $data = $this->Familia_model->get_familias();
        echo json_encode( [ 'status' => 200, 'families' => $data ] );

    }

    /* function import(){
        
        $fileName=$this->input->post('fileName');

		$file					 = 'file';
		$config['upload_path']   = 'uploads/import/';
		$config['file_name']     = $fileName;
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size']      = 10000;
        $config['max_width']     = 2048;
        $config['max_height']    = 2048;
        
        $this->load->library('upload', $config);
        
        $data=$this->upload->do_upload($file);
        
		if (!$data) {
            //Se muestra error en pantalla si no se puede subir el archivo
			$data = $this->upload->display_errors();
            $response=[
				'status'	=>400,
				'type'		=>'danger',
				'message'	=>'Error en el archivo: '.$data
			];

        }else{

			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
			
            $file_name = $upload_data['file_name'];
        	$full_path = $config['upload_path'] . $file_name;

            // Identify the type of $inputFileName 
            $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($full_path);

            //  Create a new Reader of the type that has been identified 
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
            $reader->setReadDataOnly(false);
            $reader->setReadEmptyCells(false);
            //  Load $inputFileName to a Spreadsheet Object
            $spreadsheet = $reader->load($full_path);

            // Convert Spreadsheet Object to an Array for ease of use 
            $sheetDataImport = $spreadsheet->getActiveSheet()->toArray(null, true, false, true);
            $iterators = array_values($sheetDataImport);
            
            foreach ($iterators as $key => $value) {
                $sheetData[] = array_values($value);
            }

            $products = array();
            $responseSkuCreated = array();
            $responseSkuUpdated = array();
            $responseRowsFailed = array();

            //Cantidad total de campos, menos la cantidad de campos base, nos da la cantidad de atributos 
            // que tiene el excel para esta familia
            $cant_atributos = count($sheetData[0]) - 9;

            // var_dump($sheetData);die;
            $linea = 0;

            //Se loopea el array con la data, con el slice se evita la primer linea con titulos
            foreach (array_slice($sheetData,2) as $key => $value) {
                $linea ++;
                if(empty($value[7])){
                    $responseRowsFailed[] = 'La linea ' . $linea . ' tiene un SKU invalido';
                    continue;
                }
                
                //Su busca el SKU en la BD
                $sku_valid = $this->Products_model->check_sku_exist($value[7]);

                //Si no esta en la BD, se crea el producto
                if(!isset($sku_valid[0]['idProducto'])){

                    $nombre = $value[4];
                    if($nombre == ''){
                        $nombre = $value[1].' '.$value[3].' '.$value[5];
                    }

                    $row = array(
                        'idTipo'        => 0,
                        'idFamilia'     => $value[0],
                        'idMarca'       => $value[2],
                        'nombre'        => $nombre,
                        'slug'          => $this->slugify($value[4]),
                        'modelo'        => $value[5],
                        'descripcion'   => $value[6],
                        'imagen'        => "[]",
                        'sku'           => $value[7],
                        'precio'        => $value[8],
                        'visibilidad'   => 'publicar',
                        'state'         => 1,
                        'idParent'      => 0
                    );

                   
                    $idProduct = $this->Products_model->insertProductSimple($row);

                    $atributos = array();
                    $idx = 9;

                    for($i = 0; $i < $cant_atributos; $i++ ){
                        
                        if($sheetData[0][$idx] != NULL && $value[$idx] != NULL){
                            $attr_row = array();
                            $attr_row['idProducto'] = $idProduct;
                            $attr_row['idAtributo'] = $sheetData[0][$idx];
                            $attr_row['valores'] = $value[$idx];    

                            $atributos[] = $attr_row;                            
                        } 

                        $idx++;
                    }

                    if (count($atributos) > 0 ){
                        $resultadoAtributos   = $this->ProductoAtributo->insertBatch($atributos);
                    }
                    
                    $row['atributos']     = $atributos;
                    $responseSkuCreated[] = $row;

                }else{
                    //Si existe en la BD, se actualiza el producto con los campos que tengan valores
                    $row = array();

                    if($value[0] != 0){
                        $row['idFamilia'] = $value[0];
                    }

                    if($value[2] != 0){
                        $row['idMarca'] = $value[2];
                    }

                    if($value[4] != ""){
                        $row['nombre'] = $value[4];
                        $row['slug']   = $this->slugify($value[4]);
                    }

                    if($value[5] != ""){
                        $row['modelo'] = $value[5];
                    }

                    if($value[6] != ""){
                        $row['descripcion'] = $value[6];
                    }

                    if($value[8] != ""){
                        $row['precio'] = $value[8];
                    }                    

                    if(count($row) > 0 ){
                        $responseUpdate = $this->Products_model->updateProductSimple($sku_valid[0]['idProducto'], $row);                        
                    }
                    
                    $idx = 9;

                    $atributos = array();
                    
                    for($i = 0; $i < $cant_atributos; $i++ ){
                        
                        if($sheetData[0][$idx] != NULL 
                            && $value[$idx] != NULL 
                            && $value[$idx] != "" ){
                            
                            $attr_row = array();
                            $attr_row['idProducto'] = $sku_valid[0]['idProducto'];
                            $attr_row['idAtributo'] = $sheetData[0][$idx];
                            $attr_row['valores']    = $value[$idx];    

                            $atributos[] = $attr_row;                            
                        } 

                        $idx++;
                    }

                    $this->ProductoAtributo->deleteAtributos($sku_valid[0]['idProducto']);
                    
                    if (count($atributos) > 0 ){
                        $resultadoAtributos = $this->ProductoAtributo->insertBatch($atributos);    
                    }
                    
                    $row['atributos']       = $atributos;
                    $row['sku']             = $value[7];
                    $responseSkuUpdated[]   = $row;
                }
            } 
            
        	$response=[
				'status'	      => 200,
				'title'		      => 'Import finalizado',
                'message'	      => 'Archivo Procesado con éxito',
                'productCreated'  => $responseSkuCreated,
                'productUpdated'  => $responseSkuUpdated,
                'rowsFailed'      => $responseRowsFailed
			];

        }

        //convertimos la respuesta a json
        echo json_encode($response);

    } */

    // Importador de productos de manera dinamica.. 
    function import(){
        
        $fileName=$this->input->post('fileName');

		$file					 = 'file';
		$config['upload_path']   = 'uploads/import/';
		$config['file_name']     = $fileName;
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size']      = 10000;
        $config['max_width']     = 2048;
        $config['max_height']    = 2048;
        
        $this->load->library('upload', $config);
        
        $data=$this->upload->do_upload($file);
        
		if (!$data) {
            //Se muestra error en pantalla si no se puede subir el archivo
			$data = $this->upload->display_errors();
            $response=[
				'status'	=>400,
				'type'		=>'danger',
				'message'	=>'Error en el archivo: '.$data
			];

        }else{
            try {
                //code...
            } catch (\Throwable $th) {
                //throw $th;
            }
			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
			
            $file_name = $upload_data['file_name'];
            $full_path = $config['upload_path'] . $file_name;

            /**  Identify the type of $inputFileName  **/
            $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($full_path);

            /**  Create a new Reader of the type that has been identified  **/
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
            $reader->setReadDataOnly(false);
            $reader->setReadEmptyCells(false);
            /**  Load $inputFileName to a Spreadsheet Object  **/
            $spreadsheet = $reader->load($full_path);

            /**  Convert Spreadsheet Object to an Array for ease of use  **/
            $sheetDataImport = $spreadsheet->getActiveSheet()->toArray(null, true, false, true);
            $iterators = array_values($sheetDataImport);
            
            // Recuperamos las cabeceras..
            foreach ($iterators as $key => $value) {
                $sheetData[] = array_values($value);
            }

            $products = array();
            $responseSkuCreated = array();
            $responseSkuUpdated = array();
            $responseRowsFailed = array();

            //Cantidad total de campos, menos la cantidad de campos base, nos da la cantidad de atributos 
            // que tiene el excel para esta familia
            $cant_atributos = count($sheetData[0]) - 17;

            // var_dump($sheetData);die;
            $linea = 0;

            //Se loopea el array con la data, con el slice se evita la primer linea con titulos
            try {
                foreach (array_slice($sheetData,1) as $key => $value) {
                    //Preparo registro LOG
                    $data_log = array(
                        'table'    => 'products',
                        'idUser'   => $this->user['id'],
                        'observation'   => $file_name,
                    );

                    $linea ++;
                    if(empty($value[6])){
                        $responseRowsFailed[] = 'La linea ' . $linea . ' tiene un SKU invalido';
                        continue;
                    }
                    
                    //Su busca el SKU en la BD
                    $sku_valid = $this->Products_model->check_sku_exist($value[6]);

                    $stock_type = null;
                    $stock_fijo = null;
                    $stock_limit_1 = null;
                    $stock_limit_2 = null;
    
                    if($value[9] === 'formula'){
                        $stock_type = 'formula';
                        $stock_limit_1 = $value[11];
                        $stock_limit_2 = $value[12];
                    }elseif($value[9] === 'fijo'){
                        $stock_type = 'fijo';
                        $stock_fijo = $value[10];
                    }
    
                    // Parametros de busqueda.
                    $search = '';
                    if(strlen($value[8])>0){
                        $searchs = $value[8];

                        // Dividir el string en un array
                        $array = explode(';', $searchs);

                        // Convertir todos los valores a mayúsculas
                        $array = array_map('strtoupper', $array);

                        // Eliminar valores duplicados
                        $array = array_unique($array);

                        // Volver a unir el array en un string
                        $search = implode(';', $array);
                        
                        /* $parametros = explode(',', str_replace("'",'',$value[8]));
                        
                        if(isset($sku_valid[0]['search'])){
                            $busqueda = $sku_valid[0]['search'];
                            $busqueda = str_replace(array("[", "\"", "]"), '', $busqueda);
                            $parametros2 = explode(',', $busqueda);
                            foreach($parametros2 as $p){
                                array_push($parametros, $p);
                            }
                        }
                        $parametros = array_unique($parametros, SORT_STRING);
                        $search = '[';
                        foreach($parametros as $p){
                            $search .= '"'.$p.'",';
                        }
                        $search = substr($search, 0, -1);
                        $search .= ']'; */
                    }else{
                        $search = null;
                    }
    
                    //Si no esta en la BD, se crea el producto
                    if(!isset($sku_valid[0]['idProducto'])){

                        $nombre = $value[4];
                        if($nombre == ''){
                            $nombre = $value[1].' '.$value[3].' '.$value[5];
                        }
                        $marca = $this->Marcas->get_marca_by_name($value[2]);
    
                        $row = array(
                            'idTipo'        => 0,
                            'idFamilia'     => $value[0],
                            'idMarca'       => $marca['idMarca'],
                            'nombre'        => $nombre,
                            'slug'          => $this->slugify($value[3]),
                            'modelo'        => $value[4],
                            'descripcion'   => $value[5],
                            'imagen'        => "[]",
                            'sku'           => $value[6],
                            'precio'        => $value[7],
                            'peso'          => $value[13],
                            'alto'          => $value[14],
                            'ancho'         => $value[15],
                            'largo'         => $value[16],
                            'visibilidad'   => 'borrador',
                            'state'         => 1,
                            'idParent'      => 0,
                            'search'        => $search,
                            'stock_type'    => $stock_type,
                            'stock_fijo'    => $stock_fijo,
                            'stock_limit2'    => $stock_limit_1,
                            'stock_limit1'    => $stock_limit_2
                        );
                    
                        $idProduct = $this->Products_model->insertProductSimple($row);
    
                        $atributos = array();
                        $idx = 17;
    
                        for($i = 0; $i < $cant_atributos; $i++ ){
                            if($sheetData[0][$idx] != NULL && $value[$idx] != NULL){
                                //Obtengo el ID del Atributo
                                $atributo = $this->Atributo_model->getAtributosByFamiliaAndNombre($value[0],$sheetData[0][$idx]);
                                $attr_row = array();
                                $attr_row['idProducto'] = $idProduct;
                                $attr_row['idAtributo'] = $atributo[0]['idAtributo'];
                                $attr_row['valores'] = $value[$idx];    
    
                                $atributos[] = $attr_row;     
                                
                                $valoresActuales = json_decode($atributo[0]['valor'], true);

                                $valorSearch = $value[$idx];
                                $trx = array_filter($valoresActuales, function($valor) use ($valorSearch) {
                                    return $valor === $valorSearch;
                                });

                                if($trx === 0){
                                    $valoresActuales[] = $value[$idx];
                                    $this->Atributo_model->agregarValor($atributo[0]['idAtributo'], $value[$idx]);
                                }
    
                                //$pos = array_search($value[$idx], $valoresActuales);
                                
                                /*if(!array_search($value[$idx], $valoresActuales)){
                                    print_r("NO EXISTE ATRIBUTO");exit();
                                    $valoresActuales[] = $value[$idx];
                                    
                                    $this->Atributo_model->agregarValor($atributo[0]['idAtributo'], $value[$idx]);
                                }
                                print_r("EXISTE ATRIBUTO");exit();*/
                            } 
    
                            $idx++;
                        }
    
                        if (count($atributos) > 0 ){
                            $resultadoAtributos   = $this->ProductoAtributo->insertBatch($atributos);
                        }
                        
                        $row['atributos']     = $atributos;
                        $responseSkuCreated[] = $row;

                        $data_log['idRegistro'] = $idProduct;
                        $data_log['action'] = 'create';
                        $data_log['jsonNew'] = json_encode($row);
                        insertLog($data_log);
                    }else{
                        //Si existe en la BD, se actualiza el producto con los campos que tengan valores
                        $row = array();
    
                        if($value[0] != 0){
                            $row['idFamilia'] = $value[0];
                        }
    
                        $marca = $this->Marcas->get_marca_by_name($value[2]);
                        if($value[2] != 0){
                            $row['idMarca'] = $marca['idMarca'];
                        }
    
                        if($value[3] != ""){
                            $row['nombre'] = $value[3];
                            $row['slug']   = $this->slugify($value[3]);
                        }
    
                        if($value[4] != ""){
                            $row['modelo'] = $value[4];
                        }
    
                        if($value[5] != ""){
                            $row['descripcion'] = $value[5];
                        }
    
                        if($value[7] != "" || $value[7] == 0){
                            $row['precio'] = $value[7];
                        }                    
                        
                        if($value[8] != ""){
                            $row['search'] = $search;
                        }
                        
                        if($value[9] === 'formula'){
                            $row['stock_type'] = 'formula';
                            $row['stock_limit1'] = $value[11];
                            $row['stock_limit2'] = $value[12];
                        }elseif($value[9] === 'fijo'){
                            $row['stock_type'] = 'fijo';
                            $row['stock_fijo'] = $value[10];
                        }
    
                        if($value[13] != ""){
                            $row['peso'] = $value[13];
                        } 
    
                        if($value[14] != ""){
                            $row['alto'] = $value[14];
                        } 
    
                        if($value[15] != ""){
                            $row['ancho'] = $value[15];
                        }
    
                        if($value[16] != ""){
                            $row['largo'] = $value[16];
                        } 
    
                        if(count($row) > 0 ){
                            $responseUpdate = $this->Products_model->updateProductSimple($sku_valid[0]['idProducto'], $row);                        
                        }
                        
                        $idx = 17;
    
                        $atributos = array();
                        for($i = 0; $i < $cant_atributos; $i++ ){
                            if($sheetData[0][$idx] != NULL 
                                && $value[$idx] != NULL 
                                && $value[$idx] != "" ){        
                                
                                    //Obtengo el ID del Atributo
                                    $atributo = $this->Atributo_model->getAtributosByFamiliaAndNombre($value[0],$sheetData[0][$idx]);

                                    $attr_row = array();
                                    $attr_row['idProducto'] = $sku_valid[0]['idProducto'];
                                    $attr_row['idAtributo'] = $atributo[0]['idAtributo'];
                                    $attr_row['valores'] = $value[$idx];    
                                    
                                    $atributos[] = $attr_row;  
                                    $valoresActuales = json_decode($atributo[0]['valor'], true);
                                    
                                    $valorSearch = $value[$idx];
                                    $trx = array_filter($valoresActuales, function($valor) use ($valorSearch) {
                                        return $valor === $valorSearch;
                                    });

                                    if($trx === 0){
                                        $valoresActuales[] = $value[$idx];
                                        $this->Atributo_model->agregarValor($atributo[0]['idAtributo'], $value[$idx]);
                                    }
                                    /*if(!array_search($value[$idx],$valoresActuales)){
                                        print_r("NO EXISTE ATRIBUTO UPDATE - ".$value[$idx] ." ---- ");die();
                                        $valoresActuales[] = $value[$idx];
                                    
                                        $this->Atributo_model->agregarValor($atributo[0]['idAtributo'], $value[$idx]);
                                    }
                                    print_r("EXISTE ATRIBUTO UPDATE");exit();*/
                            } 
    
                            $idx++;
                        }
    
                        $this->ProductoAtributo->deleteAtributos($sku_valid[0]['idProducto']);
                        //print_r($atributos);exit();
                        if (count($atributos) > 0 ){
                            $resultadoAtributos = $this->ProductoAtributo->insertBatch($atributos);    
                        }
                        
                        $row['atributos']       = $atributos;
                        $row['sku']             = $value[7];
                        $responseSkuUpdated[]   = $row;

                        $data_log['idRegistro'] = $sku_valid[0]['idProducto'];
                        $data_log['jsonOld'] = json_encode($sku_valid[0]);
                        $data_log['action'] = 'update';
                        $data_log['jsonNew'] = json_encode($row);
                        insertLog($data_log);
                    }
                }
            } catch (\Throwable $th) {
               echo $th;
            } 
            
        	$response=[
				'status'	      => 200,
				'title'		      => 'Import finalizado',
                'message'	      => 'Archivo Procesado con éxito',
                'productCreated'  => $responseSkuCreated,
                'productUpdated'  => $responseSkuUpdated,
                'rowsFailed'      => $responseRowsFailed
			];

        }

        echo json_encode($response);

    }

    function importsimple(){
        $fileName=$this->input->post('fileName');
        
		$file					 = 'file';
		$config['upload_path']   = 'uploads/import/';
		$config['file_name']     = $fileName;
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size']      = 10000;
        $config['max_width']     = 2048;
        $config['max_height']    = 2048;
        
        $this->load->library('upload', $config);
        
        
        $data=$this->upload->do_upload($file);
        
		if (!$data) {
            //Se muestra error en pantalla si no se puede subir el archivo
			$data = $this->upload->display_errors();
            
            $response=[
                'status'  => 400,
				'type'	  => 'danger',
				'message' => 'Error en el archivo: '.$data
			];
            
        }else{
            
			$upload_data = $this->upload->data(); 
			
            $file_name = $upload_data['file_name'];
        	$full_path = $config['upload_path'] . $file_name;
            
            /**  Identify the type of $inputFileName  **/
            $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($full_path);
            
            /**  Create a new Reader of the type that has been identified  **/
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
            $reader->setReadDataOnly(false);
            $reader->setReadEmptyCells(false);
            
            /**  Load $inputFileName to a Spreadsheet Object  **/
            $spreadsheet = $reader->load($full_path);
            
            /**  Convert Spreadsheet Object to an Array for ease of use  **/
            $sheetDataImport = $spreadsheet->getActiveSheet()->toArray(null, true, false, true);

            $iterators = array_values($sheetDataImport);
            
            foreach ($iterators as $key => $value) {
                $sheetData[] = array_values($value);
            }

            $products = array();
            $responseSkuCreated = array();
            $responseSkuUpdated = array();
            $responseRowsFailed = array();
            
            $linea = 0;
            $updated = 0;
            //Se loopea el array con la data, con el slice se evita la primer linea con titulos
            foreach (array_slice($sheetData,1) as $key => $value) {
                
                $linea ++;
                if(empty($value[0])){
                    $responseRowsFailed[] = 'La linea ' . $linea . ' tiene un SKU invalido';
                    continue;
                }
                
                //Su busca el SKU en la BD
                $sku_valid = $this->Products_model->check_sku_exist($value[0]);
                if(isset($sku_valid[0]['idProducto'])){
                    //Si existe en la BD, se actualiza el producto con los campos que tengan valores
                    $row = array();
                    
                    if($value[1] != 0){
                        $row['precio'] = $value[1];
                    }

                    // Modifica el precio Minorista del producto.
                    if($value[2] != 0){
                        $row['price_public'] = $value[2];
                    }
                    
                    // stock new
                    if($value[4] === 'formula'){
                        
                        $row['stock_type'] = 'formula';
                        $row['stock_quantity'] = $value[3];
                        $row['stock_limit1'] = $value[6];
                        $row['stock_limit2'] = $value[7];

                    }elseif($value[4] === 'fijo'){

                        $row['stock_type'] = 'fijo';
                        $row['stock_fijo'] = strtolower($value[5]);
                    }

                    // Parametros de busqueda.
                    if(strlen($value[8])>0){
                        $busqueda = $sku_valid[0]['search'];
                        $busqueda = str_replace(array("[", "\"", "]"), '', $busqueda);
                        $parametros = explode(',', $value[8]);
                        $parametros2 = explode(',', $busqueda);
                        foreach($parametros2 as $p){
                            array_push($parametros, $p);
                        }
                        $parametros = array_unique($parametros, SORT_STRING);
                        $search = '[';
                        foreach($parametros as $p){
                            $search .= '"'.$p.'",';
                        }
                        $search = substr($search, 0, -1);
                        $search .= ']';
                        $row['search'] = $search;
                    }
                    
                    if($value[9] > 0){
                        $row['is_promo_active'] = 1;
                        $row['price_promo'] = $value[9];
                    }
                    
                    //etiqueta de promocion
                    if(strlen($value[10]) > 0){
                        $tag = $this->Tag_model->getOrCreateTagByName($value[10]);
                        if(isset($tag['id'])){
                            $row['idTag'] = $tag['id'];
                        }
                    }

                    //array_unique(array $array, int $sort_flags = SORT_STRING)
                    
                    if(count($row) > 0 ){
                        $responseUpdate = $this->Products_model->updateProductSimple($sku_valid[0]['idProducto'], $row);                        
                    }

                    $row['sku']             = $value[0];
                    $responseSkuUpdated[]   = $row;
                    $updated++;

                }else{
                    $responseRowsFailed[] = 'La linea ' . $linea . ' tiene un SKU invalido - SKU: ' . $value[0];
                    continue; 
                }
            }
        } 
            
        $response = [
            'status'	      => 200,
            'title'		      => 'Import finalizado',
            'message'	      => 'Archivo Procesado con éxito',
            'productUpdated'  => $responseSkuUpdated,
            'rowsFailed'      => $responseRowsFailed
        ];

        $params = array(
            'filename' => $file_name,
            'updated'  => $updated,
            'success'  => json_encode($responseSkuUpdated),
            'errors'   => json_encode($responseRowsFailed)
        );
        
        $idBulkUpdateLogs = $this->BulkUpdateLogs_model->add_bulk_update_logs($params);

        //convertimos la respuesta a json
        echo json_encode($response);

    }

    public function slugify($text){

        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // Generar una cadena aleatoria de 4 letras
        $letras = 'abcdefghijklmnopqrstuvwxyz'; // Letras disponibles
        $randomString = substr(str_shuffle($letras), 0, 4); // Mezcla y toma las primeras 4 letras

        // lowercase
        $text = strtolower($text.'_'.$randomString);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    
    }

    public function update_stock(){
        
        $client = new \GuzzleHttp\Client();

        $skus = $this->Products_model->getAllSku();
        $cant_total = count($skus);
        $cant_processed = 0;
        $cant_sku_enviados = 0;
        $url_endpoint = '';
        date_default_timezone_set('America/Argentina/Buenos_Aires'); 
        $date_start = date('d-m-Y H:i:s');

        $process_config = $this->Sysoptions->getProcessState();
        
        foreach ($process_config as $r) {
            switch ($r['opt_name']) {
                case 'stock_update_status':
                    $process_status = $r['opt_value'];
                    break;
                case 'stock_update_batch':
                    $process_batch = $r['opt_value'];
                    break;
                case 'stock_update_endpoint':
                    $url_endpoint = $r['opt_value'];
                    break;
            }
        }
        if($process_status == 'false'){
            $this->Sysoptions->updateProcessState('stock_update_status', 'true');
            $this->Sysoptions->updateProcessState('stock_update_total', $cant_total);
            $this->Sysoptions->updateProcessState('stock_update_start', $date_start);
            $this->Sysoptions->updateProcessState('stock_update_processed', $cant_processed);
            //Formateo el Body
            foreach ($skus as $sku) {
                $data[] = array("Clave" => str_pad($sku['sku'], 6, 0, STR_PAD_LEFT));
            }
            while($cant_sku_enviados < $cant_total){

                //$batch = (intval($cant_sku_enviados)+intval($process_batch[0]['opt_value']));
            // $batch_skus ;
                $batch_skus = array_slice($data, $cant_sku_enviados, intval($process_batch));
                
                $cant_sku_enviados = intval($cant_sku_enviados) + intval($process_batch);
                $header = array('Content-Type' => 'application/json');
                $batch_skus = json_encode($batch_skus);
                
                $url = $url_endpoint;
                try {
                    $response = $client->request('GET', $url,
                                                ['headers' => $header, 
                                                'body'    => $batch_skus]);
                    
                    $r = $response->getBody()->getContents();
                    
                    $list = json_decode($r);
                    //Actualiza
                    foreach($list as $l){
                        if($l->Nombre != 'Codigo Inexistente'){
                            $sku = ltrim($l->Clave, "0"); 
                            $value['stock_quantity'] = $l->Existencias;
                            $this->Products_model->updateProductSku($sku, $value);
                            $cant_processed++;
                        }
                    }
                    
                    $api_data['url'] = $url_endpoint;
                    $api_data['payload'] = strval($batch_skus);
                    $api_data['response'] = strval($r);
                    $api_data['status'] = strval($response->getStatusCode());
                    $this->Apilog->insertApiLog($api_data);
                    $this->Sysoptions->updateProcessState('stock_update_processed', $cant_processed);
                } catch (\Throwable $th) {

                    $api_data['url'] = $url_endpoint;
                    $api_data['payload'] = strval($data);
                    $api_data['response'] = strval($th->getMessage());
                    $api_data['status'] = strval($th->getCode());
                    $this->Apilog->insertApiLog($api_data);
                    $this->Sysoptions->updateProcessState('stock_update_status', 'false');
                    echo json_encode( [ 'code'   => 500, 
                                            'message' => 'El proceso no se pudo ejecutar correctamente'] );
                } 
            }
            $date_finish = date('d-m-Y H:i:s');

            $this->Sysoptions->updateProcessState('stock_update_finish', $date_finish);
            $this->Sysoptions->updateProcessState('stock_update_status', 'false');
            
            echo json_encode( [ 'code'   => 200, 
                                'message' => 'El proceso de actualización finalizo satisfactoriamente'] );
        }else{
            echo json_encode( [ 'code'   => 409, 
                                'message' => 'El proceso de actualización se encuentra en ejecución'] );
        }
    }

    public function update_slug(){

        $slugs_result = $this->db->query("SELECT slug, count(*) as cantidad from producto group by slug HAVING cantidad > 1 ORDER BY `cantidad` DESC")->result_array();
        $slugs_repetidos = array_column($slugs_result, 'slug');

        //$product_list = $this->db->query("SELECT idProducto, slug FROM `producto` ")->result_array();

        // Construir la lista de productos que tienen slugs repetidos
        $products = $this->db->query("SELECT idProducto, nombre, slug FROM producto WHERE slug IN ('" . implode("','", $slugs_repetidos) . "')")->result_array();

        foreach ($products as $product) {
            $slug = $this->slugify($product['nombre']);
            $response = $this->Products_model->updateSlug($product['idProducto'], $slug);
           
            echo($response);
            echo( ' ');
            echo($product['idProducto']);
            echo(' ');
            echo($slug);
            echo ('/n');            
           
        }

    }

    public function templateFamilia($idFamilia, $idMarca = null){
       // Crear una instancia de PhpSpreadsheet
       $spreadsheet = new Spreadsheet();

        // Iniciando las configuraciones del archivo excel
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Datos');
        $sheet->getDefaultColumnDimension()->setWidth(18);
        
        
        // Obtengo los datos de la familia.
        $familia = $this->Familia_model->get_familia($idFamilia);

        $sheet->setCellValue('A1', 'IDFAMILIA');
        $idMarca == null ? $sheet->setCellValue('A2', $idFamilia) : ''; // Verifico si requiere marca para cargar Ejemplo
        $sheet->setCellValue('B1', 'FAMILIA');
        $idMarca == null ? $sheet->setCellValue('B2', $familia['nombre']) : ''; // Verifico si requiere marca para cargar Ejemplo
        $sheet->setCellValue('C1', 'MARCA');
        $sheet->setCellValue('D1', 'NOMBRE');
        $sheet->setCellValue('E1', 'MODELO');
        $sheet->setCellValue('F1', 'DESCRIPCION');
        $sheet->setCellValue('G1', 'SKU');
        $sheet->setCellValue('H1', 'PRECIO');
        $sheet->setCellValue('I1', 'PARAMETRO_BUSQUEDA (separador ;)');
        $idMarca == null ?  $sheet->setCellValue('I2', 'motor;turbo;escape') : '';
        $sheet->setCellValue('J1', 'STOCK_TIPO');
        $sheet->setCellValue('K1', 'STOCK_FIJO');
        $sheet->setCellValue('L1', 'STOCK_LIMITE_1');
        $sheet->setCellValue('M1', 'STOCK_LIMITE_2');

        $sheet->setCellValue('N1', 'PESO');
        $sheet->setCellValue('O1', 'ALTO');
        $sheet->setCellValue('P1', 'ANCHO');
        $sheet->setCellValue('Q1', 'LARGO');

        // Obtengo los Atributos de la familia.
        $data = $this->Atributo_model->getAtributosByFamilia($idFamilia);
        // Seteando los titulos de las columnas de manera dinamica.
        if (!empty($data)) {
            $col = 'R';
            foreach ($data as $d) {
                $sheet->setCellValue($col . '1', $d['nombre']);
                $col++;
            }
        }

        $sheet->getStyle('A1:C1')->getFill()
            ->setFillType(Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('477fe0');

        $sheet->getStyle('D1:I1')->getFill()
            ->setFillType(Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('93b267');

        $sheet->getStyle('J1:M1')->getFill()
            ->setFillType(Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('cccccc');

        $sheet->getStyle('N1:Q1')->getFill()
            ->setFillType(Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('006d66');

        $sheet->getStyle('R1:' . $sheet->getHighestColumn() . '1')
            ->getFill()
            ->setFillType(Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('919191'); // Color amarillo claro

        // Establecer altura de la primera fila
        $sheet->getRowDimension(1)->setRowHeight(30); // Ajusta la altura según tus preferencias


        // RECUPERA LOS DATOS DE PRODUCTOS
        $rowData = '2';
        if($idMarca != null){
            $dataProduct = $this->Products_model->getProductByFamilyAndMarca($idFamilia, $idMarca);
            //print_r($dataProduct);exit();
            if (!empty($dataProduct)) {
                foreach ($dataProduct as $d) {
                    $sheet->setCellValue('A' . $rowData, $d['idFamilia']);
                    $sheet->setCellValue('B' . $rowData, $d['nombre_familia']);
                    $sheet->setCellValue('C' . $rowData, $d['nombre_marca']);
                    $sheet->setCellValue('D' . $rowData, $d['nombre']);
                    $sheet->setCellValue('E' . $rowData, $d['modelo']);
                    $sheet->setCellValue('F' . $rowData, $d['descripcion']);
                    $sheet->setCellValue('G' . $rowData, $d['sku']);
                    $sheet->setCellValue('H' . $rowData, $d['precio']);
                    $sheet->setCellValue('I' . $rowData, str_replace('"', "'", str_replace(array('[', ']'), '', $d['search'])));
                    $sheet->setCellValue('J' . $rowData, $d['stock_type']);
                    $sheet->setCellValue('K' . $rowData, $d['stock_fijo']);
                    $sheet->setCellValue('L' . $rowData, $d['stock_limit1']);
                    $sheet->setCellValue('M' . $rowData, $d['stock_limit2']);
                    
                    $sheet->setCellValue('N' . $rowData, $d['peso']);
                    $sheet->setCellValue('O' . $rowData, $d['alto']);
                    $sheet->setCellValue('P' . $rowData, $d['ancho']);
                    $sheet->setCellValue('Q' . $rowData, $d['largo']);

                    // Recuperar los atributos del producto.
                    $atributos = $this->ProductoAtributo->getAtributosByProduct($d['idProducto']);
                    // Recorre los ID de atributo para relacionar con los atributos del producto.
                    if (!empty($data)) {
                        $col = 'R';
                        foreach ($data as $a) {
                            if (!empty($atributos)) {
                                // recorre los atributos del producto.
                                foreach ($atributos as $at) {
                                    if($a['idAtributo'] === $at['idAtributo'] ){
                                        $sheet->setCellValue($col . $rowData, $at['valores']);
                                    }
                                }
                            }
                            $col++;
                        }
                    }
                    $rowData++;           
                }
            }
        }

        $marcas = $this->Marcas->get_marcas();
       
        // Definir las opciones para la lista desplegable
        foreach ($marcas as $marca) {
            $options[] = $marca["nombre"];
        }
        
        // Convertir las opciones en un formato que acepte PhpSpreadsheet
        $optionsString = implode(",", $options);
        // Aplicar la validación de datos al rango de celdas deseado

        // Definir la validación de datos para la lista desplegable
        $validation = $sheet->getCell('C2')->getDataValidation();
        $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST)
                    ->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION)
                    ->setAllowBlank(false)
                    ->setShowInputMessage(true)
                    ->setShowErrorMessage(true)
                    ->setShowDropDown(true)
                    ->setErrorTitle('Error')
                    ->setError('El valor no es válido.')
                    ->setFormula1('"'.$optionsString.'"');

        $validationRange = 'C2'.$rowData; // Cambia esto según tus necesidades
        $sheet->setDataValidation($validationRange, $validation);

        // Lista desplegable para Tipo de Stock
        $optionsTipoString = implode(",", ['fijo','formula']);

        $validation = $sheet->getCell('J2')->getDataValidation();
        $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST)
                    ->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION)
                    ->setAllowBlank(false)
                    ->setShowInputMessage(true)
                    ->setShowErrorMessage(true)
                    ->setShowDropDown(true)
                    ->setErrorTitle('Error')
                    ->setError('El valor no es válido.')
                    ->setFormula1('"'.$optionsTipoString.'"');

        $validationRange = 'J2'.$rowData; // Cambia esto según tus necesidades
        $sheet->setDataValidation($validationRange, $validation);


        // Lista desplegable para tipo de stock fijo
        $optionsFijoString = implode(",", ['bajo','medio','alto']);

        $validation = $sheet->getCell('K2')->getDataValidation();
        $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST)
                    ->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION)
                    ->setAllowBlank(false)
                    ->setShowInputMessage(true)
                    ->setShowErrorMessage(true)
                    ->setShowDropDown(true)
                    ->setErrorTitle('Error')
                    ->setError('El valor no es válido.')
                    ->setFormula1('"'.$optionsFijoString.'"');

        $validationRange = 'K2'.$rowData; // Cambia esto según tus necesidades
        $sheet->setDataValidation($validationRange, $validation);
        //$sheet->setDataValidation($validationRange, $validation);
        
        // Crear una instancia para la segunda hoja
        //$spreadsheet->createSheet();

        // Crear un objeto de escritura y guardar el archivo Excel
        $writer = new Xlsx($spreadsheet);
        $filename = 'template.xlsx';
        $writer->save($filename);

        // Descargar el archivo
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    public function _update_stock(){
        $result = $this->productImport->update_stock();
        // Actualizar solo stock
        $this->output->set_content_type('application/json')->set_output($result);
    }

    public function update_base_price(){
        $result = $this->productImport->update_base_price();
        // Actualizar solo el precio base
        $this->output->set_content_type('application/json')->set_output($result);
    }

    public function update_public_price(){
        $result = $this->productImport->update_public_price();
        // Actualizar solo el precio público
        $this->output->set_content_type('application/json')->set_output($result);
    }

    public function start_update($type) {
        if (!in_array($type, ['stock', 'base_price', 'public_price'])) {
            return $this->output->set_status_header(400)->set_output(json_encode(['error' => 'Tipo de actualización inválido']));
        }

        // Verificar si ya hay un proceso en ejecución
        $status = $this->Sysoptions->getProcessConfig(["{$type}_update_status"]);
        if ($status["{$type}_update_status"] === 'true') {
            return $this->output->set_status_header(409)->set_output(json_encode(['message' => 'Ya hay un proceso de actualización en ejecución']));
        }

        // Iniciar el proceso en segundo plano
       
        $log_file = APPPATH . 'logs/cli_' . date('Y-m-d') . '.log';
        $command = "php " . FCPATH . "index.php cli update {$type} >> {$log_file} 2>&1 &";
        exec($command);


        return $this->output->set_status_header(202)->set_output(json_encode(['message' => 'Proceso de actualización iniciado']));
    }

    public function check_update_status($type) {
        if (!in_array($type, ['stock', 'base_price', 'public_price'])) {
            return $this->output->set_status_header(400)->set_output(json_encode(['error' => 'Tipo de actualización inválido']));
        }

        $status = $this->Sysoptions->getProcessConfig([
            "{$type}_update_status",
            "{$type}_update_total",
            "{$type}_update_processed"
        ]);

        return $this->output->set_status_header(200)->set_output(json_encode($status));
    }

}