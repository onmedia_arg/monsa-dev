<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Customizing extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            // $this->load->model('manager/Downloads_model', 'Downloads');
            $this->load->model('manager/Marca_model', 'Marca', true);   
            $this->load->model('manager/Familia_model', 'Familia', true);    
            $this->load->model('manager/Atributo_model', 'Atributos', true);        
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 


    function index(){

        $script = [
            base_url('assets/manager/js/customizing/customizing.js?v='.time()),
            base_url('assets/manager/js/Sortable.min.js')
        ];
        $csss = [];

        $data = array(
            "scripts"	=>	$script,
            "csss"		=>	$csss,
            "title"		=>	'Configuración',  
            "buttons"	=>	[
                                array(
                                    'title' => 'Familias',
                                    'url'   => '#', //base_url('manager/Products/import'),
                                    'icon'  => 'icon-tree7',
                                    'attr'  => "id='btn-show-families'",
                                ),
                                array(
                                    'title' => 'Marcas',
                                    'url'   => '#', //base_url('manager/Products/import'),
                                    'icon'  => 'icon-trophy4',
                                    'attr'  => "id='btn-show-marcas'",
                                )                                
                            ],  
            "breadcumbs"=>  array(
                                'Inicio' => base_url(),
                                'Configuración' => null,
                            ),
            "view"      => 'manager/customizing/index'
        );

        // $data['view'] = 'manager/customizing/index';
        $this->load->view( 'manager/index', $data );  
    }

    function getFamilies(){

        $families = $this->Familia->get_familias();

        usort($families, function($a, $b){ 
            return strnatcmp($a['orden'], $b['orden']);
        });

        echo json_encode( [ 'status' => 200, 'families' => $families ] );
    }

    function getAtributes($idFamilia){

        $atributos = $this->Atributos->getAtributosByFamilia($idFamilia);

        echo json_encode( [ 'status' => 200, 'atributos' => $atributos ] );

    }

    function getAtributesValues($idAtributo){

        $values = $this->Atributos->getValues($idAtributo);

        echo json_encode( [ 'status' => 200, 'values' => $values ] );

    }

    function addValue($idAtributo, $newValue){

        if ($this->Atributos->agregarValor($idAtributo, $newValue)) {
            // Éxito
            echo "Valor agregado con éxito.";
        } else {
            // Error: No se encontró el registro con el idAtributo especificado
            echo "No se encontró el registro con el ID especificado.";
        }
    }

    function addFamily(){

        $familyName = $this->input->post('familyName');

        if ($this->Familia->agregarFamilia($familyName)) {
            // Éxito
            echo "Familia agregada con éxito.";
        } else {
            // Error: No se encontró el registro con el idAtributo especificado
            echo "No se encontró el registro con el ID especificado.";
        }
    }

    function updateFamily(){

        $idFamilia = $this->input->post('idFamilia');
        $familyName = $this->input->post('familyName');

        $query = $this->Familia->actualizarFamilia($idFamilia, $familyName);
            
        echo json_encode([ 'status' => 200, 'data' => $query]);            
    }

    function updateOrdenFamily(){

        $sorts = $this->input->post('sorts');
        
        $query = $this->Familia->actualizarOrdenFamilia($sorts);
            
        echo json_encode([ 'status' => 200, 'data' => $query]);            
    }





    function addAtribute(){

        $atributeName = $this->input->post('atribute');
        $idFamilia = $this->input->post('idFamilia');

        if ($this->Atributos->agregarAtributo($atributeName, $idFamilia)) {
            // Éxito
            echo "Atributo agregado con éxito.";
        } else {
            // Error: No se encontró el registro con el idAtributo especificado
            echo "No se encontró el registro con el ID especificado.";
        }



    }

    function updateAtribute(){
            
        $idAtributo = $this->input->post('idAtributo');
        $atributeName = $this->input->post('atribute');
    
        $query = $this->Atributos->updateAtributoName($idAtributo, $atributeName);
        echo json_encode([ 'status' => 200, 'data' => $query]);
            
    }

    function deleteFamily(){

        $idFamilia = $this->input->post('idFamilia');
        
        $query = $this->Familia->eliminarFamilia($idFamilia);
        echo json_encode([ 'status' => 200, 'data' => $query]);

    }

    function deleteAtribute(){

        $idAtributo = $this->input->post('idAtributo');
        
        $query = $this->Atributos->deleteAtributo($idAtributo);
        echo json_encode([ 'status' => 200, 'data' => $query]);

    }


    function updateValues(){
        
        $idAtributo = $this->input->post('idAtributo');
        $oldValue = $this->input->post('oldValue');
        $newValue = $this->input->post('newValue');

        $query = $this->Atributos->updateAtributo($idAtributo, $oldValue, $newValue);
        echo json_encode([ 'status' => 200, 'data' => $query]);

    }

    function deleteValue(){

        $idAtributo = $this->input->post('idAtributo');
        $value = $this->input->post('value');
        
        $query = $this->Atributos->deleteValue($idAtributo, $value);
        echo json_encode([ 'status' => 200, 'data' => $query]);

    }

    function getMarcas(){

        $marcas = $this->Marca->get_marcas();

        usort($marcas, function($a, $b){ 
            return strnatcmp($a['orden'], $b['orden']);
        });

        echo json_encode( [ 'status' => 200, 'marcas' => $marcas ] );
    }

    function addMarca(){

        $marcaName = $this->input->post('marcaName');

        if ($this->Marca->agregarMarca($marcaName)) {
            // Éxito
            echo "Marca agregada con éxito.";
        } else {
            // Error: No se encontró el registro con el idAtributo especificado
            echo "No se encontró el registro con el ID especificado.";
        }
    }

    function updateMarca(){

        $idMarca = $this->input->post('idMarca');
        $marcaName = $this->input->post('marcaName');

        $query = $this->Marca->actualizarMarca($idMarca, $marcaName);
            
        echo json_encode([ 'status' => 200, 'data' => $query]);            
    }

    function updateOrdenMarca(){

        $sorts = $this->input->post('sorts');
        
        $query = $this->Marca->actualizarOrdenMarca($sorts);
            
        echo json_encode([ 'status' => 200, 'data' => $query]);            
    }

    function deleteMarca(){

        $idMarca = $this->input->post('idMarca');
        
        $query = $this->Marca->eliminarMarca($idMarca);

        echo json_encode([ 'status' => 200, 'data' => $query]);

    }

}