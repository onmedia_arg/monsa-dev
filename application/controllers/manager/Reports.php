<?php

include_once(FCPATH."/application/controllers/BaseController.php");


class Reports extends BaseController{

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('manager/Orders_model');
            $this->load->model('manager/Reports_model');          

            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }

    } 

	public function index()
	{
		$script = [
            base_url('assets/manager/js/plugins/visualization/echarts/echarts.min.js'),
            base_url('assets/manager/js/reports/reports.js?v='.time())
		];

		$css = [
		];

		$data = [
			"title"		=>	'Reportes', 
            'scripts'	=> $script,
			'csss'		=> $css,
            "view"      => 'manager/reports/charts'
		];

		$this->load->view('manager/reports/index', $data);
	}


	public function get_data($product)
	{
		
		try {

            $data = $this->Reports_model->get_totals($product);

            $created   = array();
            $iniciado   = array();
            $procesando = array();
            $pagado     = array();
            $error      = array();

            foreach ($data as $row) {
                $created[]    = $row["created"];
                $iniciado[]   = $row["iniciado"];
                $procesando[] = $row["procesando"];
                $pagado[]     = $row["pagado"];
                $error[]      = $row["erroremision"];
            };

            $json_data = array(
                'status'	            =>200,
                "data"				=> $data,
                "fecha"           	=>$created,
                "iniciado"         =>$iniciado,
                "procesando"       =>$procesando,
                "pagado"             =>$pagado,
                "erroremision"    =>$error
            );

            echo json_encode($json_data);

		} catch (\Exception $e) {
			return returnJson($e->getMessage());
		}
	}

	public function fetch_all_products()
	{
		try {

			$result_response = $this->Reports_model->getAllProducts();

			$this->output
				->set_status_header('200')
				->set_content_type('application/json')
				->set_output(json_encode($result_response));
			return;
		} catch (\Exception $e) {
			$message = $e->getMessage();

			$this->output
				->set_status_header('400')
				->set_content_type('application/json')
				->set_output(json_encode(['status' => 'ERROR', 'message' => $message]));
			return;
		}
	}



}