<?php

include_once(FCPATH."/application/controllers/BaseController.php");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Orders extends BaseController{

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('manager/Orders_model', 'Orders_model', true);
            $this->load->model('manager/Products_model', 'Products_model', true);
            $this->load->model('manager/Clients_model', 'Clientes', true);

            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    function index(){

        $script = array( 
            base_url('assets/manager/plugins/datatables/datatables.min.js'),
            base_url('assets/manager/js/daterangepicker.js'),
            // base_url('assets/manager/plugins/datatables/datatables_basic.js'),            
            base_url('assets/manager/js/orders/orders.js?v='.time())
        );
                
        $csss = array(
            base_url('assets/manager/plugins/datatables/datatables.min.css'),
            base_url('assets/manager/plugins/datatables/dataTables.bootstrap4.min.css'),
        );


        $data = array(
            "scripts"	=>	$script,
            "csss"		=>	$csss,
            "title"		=>	'Pedidos',  
            // "buttons"	=>	[
            //                     array(
            //                         'title' => 'Acciones',
            //                         'url'   => '#', //base_url('manager/Products/import'),
            //                         'icon'  => 'icon-file-excel',
            //                         'attr'  => null,
            //                     ),
            //                 ],  
            "breadcumbs"=>  array(
                                'Inicio' => base_url(),
                                'Pedidos' => null,
                            ),
            "view"      => 'manager/orders/list'
           // "modals" =>  'manager/products/modal-import',
        );

        //Enviamos dentro de $data array con js y css espeíficos de la hoja
        //Cargamos la vista correspondiente
       $this->load->view('manager/orders/index', $data); 
    } 

    public function get_list_dt(){
        
        $allInputs = json_decode(trim(file_get_contents('php://input')), true);

        //Paginado
        $start = $allInputs["start"];   

        //Ver
        $length = $allInputs["length"];

        //Busqueda
        $search = $allInputs["search"]['value'];;

       
        if ($allInputs["order"][0] != ""){
            switch($allInputs["order"][0]["column"]){
                case 0:
                    $column = "o.idOrderHdr";
                break;

                case 1:
                    $column = "o.updated";
                break;
                
                case 2:
                    $column = "o.idCliente";
                break;
                
                case 3:
                    $column = "o.idOrderStatus";
                break;
            }
            
            $order = array(
                "column"        => $column,
                "direction"     => $allInputs["order"][0]["dir"]
            );
        }

        $filtro = array(
                    'fecha_desde'   => "", //$fecha_desde,
                    'fecha_hasta'   => "", //$fecha_hasta,
                    'idCliente'     => $allInputs["idcliente"], 
                    'idOrderStatus' => $allInputs["idorderstatus"]
        );

        $result = $this->Orders_model->getAllOrders($start, $length, $search, $filtro, $order);

        $resultado = $result["datos"];

        //$numrows = $result["numRows"];
        $numrowstotal = $result["numRows"];
        //$numrowstotal = $this->Orders_model->getNumRowAll();

        $data = array();

        foreach ($resultado->result_array() as $result_row) {

            switch ($result_row["idOrderStatus"]) {
                case '1':
                    $status = "<span class='badge badge-secondary'>" . $result_row['statusText'] . "</span>";
                    break;
                
                case '2':
                    $status = "<span class='badge badge-primary'>" . $result_row['statusText'] . "</span>";
                    break;

                case '3':
                    $status = "<span class='badge badge-warning'>" . $result_row['statusText'] . "</span>";
                    break;
                
                case '4':
                    $status = "<span class='badge badge-success'>" . $result_row['statusText'] . "</span>";
                    break;
            }
            
            $actions = '<div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">

                                    <a href="'. base_url('/manager/Orders/order_detail/') . $result_row["idOrderHdr"] . '" class="dropdown-item" title="Ver Orden">
                                        <i class="icon-file-eye"></i>Ver Pedido
                                    </a>';

                        if($result_row["idOrderStatus"] == 2){

                            $actions .= '<a href="#" class="dropdown-item btnChangeStatus"  data-id="' . $result_row["idOrderHdr"] . '">
                                                <i class="icon-clipboard2"></i>En Preparación</a>';                                                              
                        } else if($result_row["idOrderStatus"] == 3){

                            $actions .= '<a href="#" class="dropdown-item btnChangeFinished"  data-id="' . $result_row["idOrderHdr"] . '">
                                                <i class="icon-clipboard2"></i>Finalizado</a>';
                        }       
                        
            $actions .= '<a href="'. base_url('/uploads/orders/') . $result_row["idOrderHdr"] . '.xlsx' . '" class="dropdown-item" title="Ver Orden">
                            <i class="icon-file-excel"></i>Descargar Excel
                        </a>';

            $actions .=         '</div>
                            </div>
                        </div>'; 
            
            $row_of_data = array();

            $row_of_data[] = $result_row["idOrderHdr"];
            // $row_of_data[] = fecha_es($result_row["created"],"d F a");
            $row_of_data[] = fecha_es($result_row["updated"]);
            $row_of_data[] = $result_row["razon_social"];
            $row_of_data[] = $status; //;
            $row_of_data[] = "$ " . number_format($result_row["total"],2, ",", ".");
            $row_of_data[] = $actions;          

            $data[] = $row_of_data;

        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($numrowstotal),
            "recordsFiltered" => intval($numrowstotal),//intval($numrows),
            "data"            => $data
        );
        echo json_encode($json_data);
    }

    public function get_order_json( $idOrderHdr ){

        $order = $this->Order_model->getById( $idOrderHdr );
        $details = $this->Order_model->getItemsByOrder( $idOrderHdr );
        $client = $this->Clientes->get_cliente( $order['idCliente'] );

        $items = array();

        foreach ($details as $o => $data) {

            $product = $this->Products_model->get_producto( $data->idProducto ); 

            $items[] = array(  
                                'posnr' => $data->posnr,
                                'producto' => $product, 
                                'qty' => $data->qty,
                                'total' => $data->total,
                                'options' => $data->options,
                                'idItemStatus' => $data->idItemStatus,
                                'createdBy' => $data->createdBy,
                                'createdDate' => $data->createdDate,
                                'updatedBy' => $data->updatedBy,
                                'updatedDate' => $data->updatedDate,
                        );
        }

        echo json_encode( [ 'order' => $order, 'details' => $items, 'client' => $client ] );

    }

    public function get_list_home_dt(){
        
        $allInputs = json_decode(trim(file_get_contents('php://input')), true);

        $result = $this->Orders_model->getLastOrders();

        $resultado = $result["datos"];

        $numrows  = $result["numRows"];
        $numrowstotal = $this->Orders_model->getNumRowAll();

        $data = array();

        foreach ($resultado->result_array() as $result_row) {

            $actions = "<div>
                            <a href='" . base_url('/manager/Orders/order_detail/') . $result_row["idOrderHdr"] . "' class='btn btn-sm btn-primary'><i class='icon-file-eye mr-1'></i> Ver Pedido</a>
                            <a data-id=" . $result_row["idOrderHdr"] . " href='#' class='btn btn-sm btn-warning btnChangeStatus'><i class='icon-clipboard2 mr-1'></i> En preparacion</a>
                            <a href='" . base_url('/uploads/orders/') . $result_row["idOrderHdr"] . '.xlsx' . "' class='btn btn-sm btn-success'><i class='icon-file-excel mr-1'></i> Excel</a>
                        </div>";

            $row_of_data = array();

            $row_of_data[] = $result_row["idOrderHdr"];
            $row_of_data[] = fecha_es($result_row["updated"]);
            $row_of_data[] = $result_row["razon_social"];
            $row_of_data[] = "$ " . number_format($result_row["total"],2, ",", ".");
            $row_of_data[] = $actions;          

            $data[] = $row_of_data;

        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($numrowstotal),
            "recordsFiltered" => intval($numrowstotal),//intval($numrows),
            "data"            => $data
        );
        echo json_encode($json_data);
    }    

    public function order_detail($idOrderHdr){
        $script = array( 
                    base_url('assets/manager/js/orders/order_detail.js?v='.time())
                );
                
        $csss = array();


        $data = array(
            "scripts"   =>  $script,
            "csss"      =>  $csss,
            "title"     =>  'Detalle de Pedidos',  
            "buttons"   =>  [ array('title' => 'En Preparación',
                                     'url'   => '#', //base_url('manager/Products/import'),
                                     'icon'  => 'icon-clipboard2',
                                     'attr'  => 'id="chgst-to-prep"'),
                               array( 'title' => 'Finalizado',
                                      'url'  => '#', //base_url('manager/Products/import'),
                                      'icon' => 'icon-truck',
                                      'attr'  => 'id="chgst-to-fin"'),
                          ],
            "breadcumbs"=>  array(
                                'Inicio'         => base_url(),
                                'Pedidos'        => null,
                                'Detalle Pedido' => null
                            ),
            "idOrderHdr" => $idOrderHdr,
            "view"      => 'manager/orders/detail'
           // "modals" =>  'manager/products/modal-import',
        );

        //Enviamos dentro de $data array con js y css espeíficos de la hoja
        //Cargamos la vista correspondiente
       $this->load->view('manager/orders/index', $data); 


    }

    public function getOrderDetail($idOrderHdr){

        $header = $this->Orders_model->getOrderData($idOrderHdr);
        $items  = $this->Orders_model->getAllItemsbyOrderId($idOrderHdr);
        
        $json_data = array(
                        "status"    => 200,
                        "header"    => $header,        
                        "items"     => $items
                    );

        echo json_encode($json_data);

    }

	public function get_info_widget(){

		$result = array(
			"cant"   => $this->Orders_model->info_stat_orders_cant(),
			"total"  => number_format($this->Orders_model->info_stat_orders_tot(),2, ",", "."),
            "prom"   => number_format( $this->Orders_model->info_stat_orders_tot() / 
                                       $this->Orders_model->info_stat_orders_cant()
                                       ,2, ",", ".")
		);

		return returnJson($result);

    }
    
    public function changeOrderStatus(){

        $id = $this->input->post('id');
        $status = $this->input->post('status');

        $r = $this->Orders_model->updateOrderStatus($id, array('idOrderStatus'=> $status));
        $mailData = array(
                      'id' => $id
        );

        $email = $this->Orders_model->getEmailByOrderId($id); 

        if($email != NULL){
            if($status == 3){

                // Enviar Correo
                ob_start(); 
                    $this->load->view( 'layouts/email/email-header' );
                    $this->load->view( 'layouts/email/order-enpreparacion', $mailData );
                    $this->load->view( 'layouts/email/email-footer');
                $body = ob_get_clean(); 

                // Correo usuario
                $send = $this->Base_model->send_email_template( 'Estamos preparando tu pedido!', 
                                                                $email['mail'], 
                                                                $body );

            }else if($status == 4){

                // Enviar Correo
                ob_start(); 
                    $this->load->view( 'layouts/email/email-header' );
                    $this->load->view( 'layouts/email/order-finalizada', $mailData );
                    $this->load->view( 'layouts/email/email-footer');
                $body = ob_get_clean(); 

                // Correo usuario
                $send = $this->Base_model->send_email_template( 'Tu pedido esta en camino!', 
                                                                $email['mail'], 
                                                                $body );
            }
        }
        
        $header = $this->Orders_model->getOrderData($id);

        $response=[
            'status'	=> 200,
            'type'		=> 'success',
            'message'	=> 'Pedido actualizado correctamente!',
            'header'    => $header
        ];

        //convertimos la respuesta a json
        echo json_encode($response);

    }

    
    function getStatusList(){
		echo json_encode( [ 'code'   => 200, 
                            'status' => $this->Orders_model->getStatusList() ] );
	}    

    public function get_xls_order(){

        $path = PATH_UPLOAD . '/orders';

        $list_files = get_filenames( $path, FALSE);

        echo "<pre>";
        print_r($list_files);

        foreach ($list_files as $file) {
            
            $new = PATH_UPLOAD . '/orders' . '/' . trim(substr(substr(str_replace('#', '', $file), 0, -21), 6)) . ".xlsx";
            $new_file[] = $new;
            
            $old = PATH_UPLOAD . '/orders' . '/' . $file;
            // print_r($old);

            rename($old, $new);
        }

        // $search = $idOrderHdr . ' ' ; 
        // $key =  array_find($search, $list_files);

        echo "<pre>";
        var_dump($new_file);
        
        // echo json_encode( 
        //     [ 
        //       'status' => 200, 
        //       'files'  => base_url('/uploads/orders/' . $list_files[$key])
        //     ] );
        
    }

}