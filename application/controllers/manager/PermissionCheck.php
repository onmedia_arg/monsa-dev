
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PermissionCheck extends CI_Controller {
    
    public function index() {
        $this->load->helper('file');
        
        $output = "Verificación de Permisos\n";
        $output .= "========================\n\n";

        // Verificar usuario de PHP
        $php_user = posix_getpwuid(posix_geteuid());
        $output .= "PHP se está ejecutando como usuario: " . $php_user['name'] . "\n\n";

        // Verificar permisos del directorio de logs
        $log_directory = APPPATH . 'logs/';
        $permissions = fileperms($log_directory);
        $output .= "Permisos del directorio de logs: " . substr(sprintf('%o', $permissions), -4) . "\n";
        $output .= "¿Es escribible?: " . (is_writable($log_directory) ? 'Sí' : 'No') . "\n\n";

        // Prueba de escritura
        $test_file = $log_directory . 'test_write.log';
        if (file_put_contents($test_file, "Test de escritura\n")) {
            $output .= "Prueba de escritura exitosa\n";
            unlink($test_file);
        } else {
            $output .= "No se pudo escribir en el directorio de logs\n";
        }
        $output .= "\n";

        // Verificar ejecución de comandos
        if (function_exists('exec')) {
            $exec_output = [];
            exec('whoami', $exec_output);
            $output .= "Comando 'whoami' ejecutado como: " . $exec_output[0] . "\n";
        } else {
            $output .= "La función exec() está deshabilitada\n";
        }
        $output .= "\n";

        // Verificar funciones deshabilitadas
        $disabled_functions = ini_get('disable_functions');
        $output .= "Funciones deshabilitadas: " . ($disabled_functions ? $disabled_functions : "Ninguna") . "\n";

        echo "<pre>" . $output . "</pre>";
    }
}
?>