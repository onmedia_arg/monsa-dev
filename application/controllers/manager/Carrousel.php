<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Carrousel extends BaseController {

    public function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {

            $this->load->model('manager/Carrousel_model', 'Carrousel', true);
        }else{
            redirect('/', 'refresh');
        }        
    }

    public function index()
    {
        
        $script = [
            base_url('assets/manager/js/carrousel/carrousel.js?v='.time()),
            // base_url('assets/manager/js/Sortable.min.js')
        ];
        $csss = [];

        $data = array(
            "scripts"	=>	$script,
            "csss"		=>	$csss,
            "title"		=>	'Carruseles',  
            "buttons"	=>	[
                                // array(
                                //     'title' => 'Crear',
                                //     'url'   => '#', //base_url('manager/Products/import'),
                                //     'icon'  => 'icon-tree7',
                                //     'attr'  => "id='btn-show-families'",
                                // )
                            ],  
            "breadcumbs"=>  array(
                                'Inicio' => base_url(),
                                'Carruseles' => null,
                            ),
            "view"      => 'manager/carrousel/index'
        );

        
        
        $this->load->view('manager/index', $data);
    }

    public function getData()
    {
        $response = $this->Carrousel->getData();
        echo json_encode($response);
    }

    public function create()
    {
        $this->load->view('manager/carrousel/create');
    }

    public function store()
    {
        $data = $this->input->post();
        $response = $this->Carrousel->store($data);
        echo json_encode($response);
    }

    public function edit()
    {
        $this->load->view('manager/carrousel/edit');
    }

    public function update()
    {
        $data = $this->input->post();
        $response = $this->Carrousel->update($data);
        echo json_encode($response);
    }

}