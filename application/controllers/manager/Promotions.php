<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Promotions extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('manager/Promotions_model', 'Promotions');
            $this->load->model('manager/Sysoptions_model', 'Sysoptions', true);            
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 


    function uploadImage(){

        // var_dump($this->input->post());die();
        $currentDate = date('YmdHis');
        $fileName = $this->input->post('fileName') ?? $currentDate;
        $folder = 'uploads/offers/';

            // Check if the directory exists, if not, create it
        if (!is_dir($folder)) {
            mkdir($folder, 0755, true);
        }


		$file					 = 'file';
		$config['upload_path']   = $folder;
		$config['file_name']     = $fileName;
        $config['allowed_types'] = 'jpg|png|jpeg|pdf';
        $config['max_size']      = 40000;
        $config['max_width']     = 2048;
        $config['max_height']    = 2048;
        // var_dump($config);die();

        $this->load->library('upload', $config);

        $data = $this->upload->do_upload($file);

        
		if (!$data) {
			$data = $this->upload->display_errors();
            $response=[
				'status'	=>400,
				'type'		=>'danger',
				'message'	=>'Error en el archivo: '.$data
            ];
           
        }else{

			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name'];
            $type = $this->input->post('type');

            $data = Array(
                'name'           => $file_name,
                'description'    => 'promotion',
                'type'           => $type,
                'section'        => 'HOME',
                'folder'         => $folder,
                'fullpath'       => $folder . $file_name,
                'publicfullpath' => base_url($folder . $file_name),
                'is_active'      => 1,
                'created_by'     => $_SESSION['user_id']
            );

            $r = $this->Promotions->insert_promotion($data);
            $upload = $this->Promotions->get_promotion_by_id($r);

        	$response=[
				'status'	=> 200,
				'type'		=> 'success',
                'message'	=> 'Archivo subido correctamente',
                'upload'    => $upload
			];

        }

        //convertimos la respuesta a json
        echo json_encode($response);

    }

    function deleteImage(){
        $id = $this->input->post('id');

        $promotion = $this->Promotions->get_promotion_by_id($id);
        $path = $promotion->fullpath;

        unlink($path);

        $this->Promotions->delete_promotion($id);

        $response = [
            'status' => 200,
            'message' => 'Imagen eliminada correctamente'
        ];

        echo json_encode($response);
    }


}