<?php

include_once(FCPATH."/application/controllers/BaseController.php");


class Popup extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('manager/Popup_model', 'Popup');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    function index(){

        $script = array( 
            base_url('assets/manager/js/popup/popup.js?v='.time()),
            // 'https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js'
        );
                
        $csss = array();

        $data = array(
            "scripts"	=>	$script,
            "csss"		=>	$csss,
            "title"		=>	'Popup',  
            "breadcumbs"=>  array(
                                'Inicio' => base_url(),
                                'Popup' => null,
                            ),
            "view"      => 'manager/popup/popup'
        );

        //Enviamos dentro de $data array con js y css espeíficos de la hoja
        //Cargamos la vista correspondiente
       $this->load->view('manager/downloads/index', $data); 
    }

    public function get_popup(){
        
        $popup = $this->Popup->get_popup();
        // var_dump($popup);die();
        
        if(isset($popup['image']) && $popup['image'] != null){
            echo json_encode( [ 'status' => 200, 
                                'active' => true, 
                                'image' => $popup['image'],
                                'url'   => $popup['url'] ] );
        }else{
            echo json_encode( [ 'status' => 400, 
                                'active' => false ] );
            
        }
    }

    public function getAll(){
        
        $popup = $this->Popup->get_all();

        echo json_encode( [ 'status' => 200, 
                            'data' => $popup ] );

    }


    public function uploadImage(){

		$fileName = $this->input->post('fileName');
        $folder   = 'uploads/popup/';

		$file					 = 'file';
		$config['upload_path']   = $folder;
		$config['file_name']     = $fileName;
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size']      = 40000;
        $config['max_width']     = 2048;
        $config['max_height']    = 2048;

        $this->load->library('upload', $config);

        $data=$this->upload->do_upload($file);

        // var_dump($config);die();

		if (!$data) {

			$data = $this->upload->display_errors();

            $response=[
				'status'	=>400,
				'type'		=>'danger',
				'message'	=>'Error en el archivo: '.$data
            ];
            
        }else{

			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            
            $file_name = $upload_data['file_name'];
            // $type = $this->input->post('type');

            // $clear = $this->Downloads->deleteFile($type);
            // // var_dump($clear->publicfullpath);die;
            // if($clear != NULL){
            //     delete_files($clear->publicfullpath);
            // } 
            
            $data = Array(
                'image'      => $folder. $fileName,
            );

            $update = $this->Popup->update(1, $data);

        	$response=[
				'status'	=> 200,
				'title'		=> 'Bien Hecho!',
                'message'	=> 'Archivo subido correctamente',
                'upload'    => 'OK' //$upload
			];

        }

        //convertimos la respuesta a json
        echo json_encode($response);
    }

    public function update(){

        $data = Array(
            'active'     => $this->input->post('active'),
            'url'        => $this->input->post('url')
        );

        // $r = $this->Downloads->insert($data);
        $update = $this->Popup->update(1, $data);

        $response=[
            'status'	=> 200,
            'title'		=> 'Bien Hecho!',
            'message'	=> 'Popup actualizado correctamente',
            'upload'    => 'OK' //$upload
        ];

        echo json_encode($response);

    }

    public function new_file(){

		$fileName = $this->input->post('fileName');
        $folder   = 'uploads/descargas/';

		$file					 = 'file';
		$config['upload_path']   = $folder;
		$config['file_name']     = $fileName;
        $config['allowed_types'] = 'xls|xlsx|pdf';
        $config['max_size']      = 10000;
        $config['max_width']     = 2048;
        $config['max_height']    = 2048;

        $this->load->library('upload', $config);

        $data=$this->upload->do_upload($file);

        // var_dump($config);die();

		if (!$data) {

			$data = $this->upload->display_errors();

            $response=[
				'status'	=>400,
				'type'		=>'danger',
				'message'	=>'Error en el archivo: '.$data
            ];
            
        }else{

			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            
            $file_name = $upload_data['file_name'];
            $type = $this->input->post('type');

            if($type == 'XLS'){
                $icon = '/resources/img/xls.png';
            }else{
                $icon = '/resources/img/pdf.png';
            }
            

            // $this->security->xss_clean(trim($this->input->post('title')))
            $data = Array(
                'name'           => $this->input->post('name'),
                'description'    => $this->input->post('description'),
                'icon'           => $icon,
                'type'           => $type,
                'section'        => 'HOME',
                'folder'         => $folder,
                'fullpath'       => $folder . $file_name,
                'publicfullpath' => base_url($folder . $file_name),
                'is_active'      => 1,
                'created_by'     => $_SESSION['user_id']
            );

            $r = $this->Downloads->insert($data);
            
        	$response=[
				'status'	      => 200,
				'type'		      => 'success',
                'message'	      => 'Archivo subido correctamente',
			];

        }

        //convertimos la respuesta a json
        echo json_encode($response);
    }

}