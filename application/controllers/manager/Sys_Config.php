<?php
include_once(FCPATH."/application/controllers/BaseController.php");

class Sys_Config extends BaseController{

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('manager/Sysoptions_model', 'Sysoptions', true);           
            
            $this->user = $this->dataUser();
        
        }else{
        
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of producto
     */
    function getProcessState()
    { 
        $result = $this->Sysoptions->getProcessState();
        
		foreach ($result as $r) {
			switch ($r['opt_name']) {
				case 'stock_update_status':
					$status = $r['opt_value'];
					break;
				case 'stock_update_total':
					$total = $r['opt_value'];
					break;
				case 'stock_update_processed':
					$processed = $r['opt_value'];
					break;
				case 'stock_update_start':
					$start = $r['opt_value'];
					break;
				case 'stock_update_finish':
					$finish = $r['opt_value'];
					break;	
			}
		}
		
			returnJson([ "status"    => $status,
			"total"    	=> $total,        
			"processed" => $processed,
			"start"     => $start,
			"finish"    => $finish ]);
    }

	public function save_sys_config(){ 

		$data = array(
		   array(
		   	  'opt_name'   => 'sys_url',
		      'opt_value' => $this->input->post('sys-url')
		   ),
		   array(
		   	  'opt_name'   => 'sys_title',
		      'opt_value' => $this->input->post('sys-title')
		   ),
		   array(
		   	  'opt_name'   => 'sys_logo',
		      'opt_value' => $this->input->post('sys-logo')
		   ),
		   array(
		   	  'opt_name'   => 'sys_mail_logo',
		      'opt_value' => $this->input->post('sys-mail-logo')
		   ),
		   array(
		   	  'opt_name'   => 'admin_mail',
		      'opt_value' => $this->input->post('admin-mail')
		   ),
		   array(
		   	  'opt_name'   => 'admin_notify',
		      'opt_value' => $this->input->post('admin-notify')
		   ),
		   array(
		   	  'opt_name'   => 'sys_mailsender',
		      'opt_value' => $this->input->post('mail-sender')
		   ),
		   array(
		   	  'opt_name'   => 'smtp_host',
		      'opt_value' => $this->input->post('smtp_host')
		   ),
		   array(
		   	  'opt_name'   => 'smtp_port',
		      'opt_value' => $this->input->post('smtp_port')
		   ),
		   array(
		   	  'opt_name'   => 'smtp_user',
		      'opt_value' => $this->input->post('smtp_user')
		   ),
		   array(
		   	  'opt_name'   => 'smtp_pass',
		      'opt_value' => $this->input->post('smtp_pass')
		   ),
		   array(
			'opt_name'   => 'wsp_number',
			'opt_value' => $this->input->post('wsp_number')
		   ),
		   array(
			'opt_name'   => 'wsp_message',
			'opt_value' => $this->input->post('wsp_message')
		   ),	
		   array(
			'opt_name'   => 'shop_msj_title',
			'opt_value' => $this->input->post('shop_msj_title')
		   ),	
		   array(
			'opt_name'   => 'shop_msj_detail',
			'opt_value' => $this->input->post('shop_msj_detail')
		   ),			   		   	   		   

		);

		$this->db->update_batch('system_options', $data, 'opt_name'); 

		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success_message', 'Configuración Guardada.');
		}else{
			$this->session->set_flashdata('error_message', 'Hubo un problema en la consulta.');
		}

		redirect( base_url('Sys_Config/index'), 'refresh' );  

	}

}