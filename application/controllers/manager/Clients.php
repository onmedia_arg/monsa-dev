<?php
include_once(FCPATH . "/application/controllers/BaseController.php");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Clients extends BaseController
{
    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('manager/Clients_model', 'Clientes', true);
            $this->load->model('manager/Marca_model', 'Marcas', true);
            $this->load->model('manager/TipoCliente_model', 'TipoCliente', true);
            $this->load->model('Base_model');
            $this->load->model('manager/User_model', 'User_model', true);
            $this->load->model('manager/RelClienteUsuario_model', 'RelClienteUsuario_model', true);
            $this->load->model('manager/Order_model', 'Order_model', true);

            $this->user = $this->dataUser();
        } else {
            redirect('/', 'refresh');
        }
    }

    function index()
    {

        $script = array(
            base_url('assets/manager/plugins/datatables/datatables.min.js'),
            base_url('assets/manager/js/clients/list.js?v='.time()),
        );

        $csss = array(
            base_url('assets/manager/plugins/datatables/datatables.min.css'),
            base_url('assets/manager/plugins/datatables/dataTables.bootstrap4.min.css'),
        );

        $data = array(
            "scripts"    =>    $script,
            "csss"        =>    $csss,
            "title"        =>    'Clientes',
            "buttons"    =>    [
                array(
                    'title' => 'Nuevo Cliente',
                    'url'   =>  base_url('manager/clients/add'),
                    'icon'  => 'icon-user',
                    'attr'  => 'id="newClient"',
                ),
                array(
                    'title' => 'Exportar',
                    'url'   => base_url('manager/clients/export_clients'),
                    'icon'  => 'icon-file-excel',
                    'attr'  => 'id="export_clients"',
                ),
            ],
            "breadcumbs" =>  array(
                'Inicio' => base_url(),
                'Clientes' => null,
            ),
        );

        // enviamos dentro de $data array con js y css espeíficos de la hoja
        //Cargamos la vista correspondiente
        $this->load->view('manager/clients/index', $data);
    }

    public function get_list_dt()
    {

        $allInputs = json_decode(trim(file_get_contents('php://input')), true);

        //Paginado
        $start = $allInputs["start"];

        //Ver
        $length = $allInputs["length"];

        //Busqueda
        $search = $allInputs["search"]['value'];


        if ($allInputs["order"][0] != "") {
            switch ($allInputs["order"][0]["column"]) {
                case 0:
                    $column = "cli.idCliente";
                    break;

                case 1:
                    $column = "cli.razon_social";
                    break;

                case 2:
                    $column = "tc.texto";
                    break;

                case 3:
                    $column = "cli.cuit";
                    break;

                case 4:
                    $column = "cli.direccion";
                    break;

                case 5:
                    $column = "cli.telefono";
                    break;

                case 6:
                    $column = "cli.mail";
                    break;

                case 7:
                    $column = "cli.is_activo";
                    break;

                case 8:
                    $column = "cli.fechaCreacion";
                    break;
            }

            $order = array(
                "column"        => $column,
                "direction"     => $allInputs["order"][0]["dir"]
            );
        }

        $resultado = $this->Clientes->getAllClients($start, $length, $search, $order);

        $data = array();
        $numrows = $resultado["numRows"];
        $numrowstotal = $this->Clientes->getNumRowAll();
        foreach ($resultado['datos']->result_array() as $result_row) {

            switch ($result_row["is_activo"]) {
                case 0:
                    $activo = '<input  type="checkbox" data-on-text="Si" data-off-text="No" class="is_activo" name="is_activo" value="' . $result_row["idCliente"] . '">';
                    break;

                case 1:
                    $activo = '<input type="checkbox" data-on-text="Si" data-off-text="No" class="is_activo" name="is_activo" value="' . $result_row["idCliente"] . '" checked>';
                    break;
            }

            $actions = '<div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">

                                <a href="' . base_url('manager/Clients/edit/') . $result_row["idCliente"] . '" class="dropdown-item" title="Editar">
                                    <i class="icon-pencil"></i>Editar
                                </a> 
                                <a href="#" data-id="' . $result_row["idCliente"] . '" data-razonsocial="' . $result_row["razon_social"] . '" class="dropdown-item delete-cliente" title="Eliminar">
                                    <i class="fa fa-trash"></i>Eliminar
                                </a> 

                                </div>
                            </div>
                        </div>';

            $row_of_data = array();
            //print_r($result_row);exit();
            $row_of_data[] = $result_row["idCliente"];
            $row_of_data[] = $result_row["razon_social"];
            $row_of_data[] = $result_row["texto"];
            $row_of_data[] = $result_row["cuit"];
            $row_of_data[] = $result_row["direccion"];
            $row_of_data[] = $result_row["telefono"];
            $row_of_data[] = $result_row["mail"];
            $row_of_data[] = $activo;
            $row_of_data[] = $result_row["fechaCreacion"];

            $row_of_data[] = $actions;

            $data[] = $row_of_data;
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($numrowstotal),
            "recordsFiltered" => intval($numrows), //intval($numrows),
            "data"            => $data
        );
        echo json_encode($json_data);
    }


    public function get_list_home_dt()
    {

        $allInputs = json_decode(trim(file_get_contents('php://input')), true);

        $result = $this->Clientes->get_last_clients();

        $resultado = $result["datos"];

        $data = array();

        foreach ($resultado->result_array() as $result_row) {

            $row_of_data = array();
            $row_of_data[] = '' . $result_row["razon_social"] . '<br><span class="text-muted">' . fecha_es($result_row["fechaCreacion"]) . '</span>';
            $row_of_data[] = '<a href="' . base_url('/clientes/edit/') . $result_row["idCliente"] . '" class="badge badge-primary ml-2">Ver Cliente</a>';

            $data[] = $row_of_data;
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "data"            => $data
        );
        echo json_encode($json_data);
    }

    function getAll()
    {

        $clientes = $this->Clientes->getAll();

        echo json_encode(['status' => 200, 'clientes' => $clientes]);
    }

    function add()
    {

        $tipos_usuarios = $this->TipoCliente->get_all_tc();
        $script = array(
            base_url('assets/manager/plugins/forms/validation/validate.min.js'),
            base_url('assets/manager/js/clients/client.js?v='.time()),
        );

        $csss = array();

        $data = array(
            "scripts"    =>    $script,
            "csss"        =>    $csss,
            "title"        =>    'Nuevo Usuario',
            "buttons"    =>    [
                array(
                    'title' => 'Guardar',
                    'url'   => '#', //base_url('manager/Products/import'),
                    'icon'  => 'icon-floppy-disk',
                    'attr'  => 'id="btnAddCliente"',
                ),
            ],
            "breadcumbs" =>  array(
                'Inicio' => base_url(),
                'Cliente' => base_url('manager/clients'),
                'Nuevo' => null,
            ),
            "tipos_usuarios"  => $tipos_usuarios,
        );

        $this->load->view('manager/clients/add/add', $data);
    }

    function insertUser()
    {

        // Generación de Contraseña.
        $password = $this->User_model->generate_password();

        $params_user['username'] = $this->input->post('usuario');
        $params_user['email'] = $this->input->post('mail');
        //$params_user['passwd'] = $this->input->post('password_usuario');
        //$params_user['passwd'] = sha1($params_user['passwd']);
        $params_user['passwd'] = sha1($password);
        $params_user['auth_level'] = 1;

        try {

            $verify = $this->Base_model->verifyUserAndEmail($params_user['username'], $params_user['email']);
            if ($verify != false) {
                //$this->session->set_flashdata('alert_message', "Usuario y/o email ya esta registrado, intenta recuperar contraseña o crea un nuevo usuario.");
                echo returnJson(['status' => 500, 
                                    'title' => 'Cliente', 
                                    'message' => 'Usuario y/o email ya esta registrado, intenta recuperar contraseña o crea un nuevo usuario.']);
                //redirect(base_url('Start/showRegisterForm'), 'refresh');
            } else {
                // Usuario
                $user_id = $this->user_model->add_user($params_user);

                // Cliente
                $client_data = array(
                    'razon_social' => $this->input->post('razon_social'),
                    'idTipoCliente' => $this->input->post('tipo_cliente'),
                    'cuit' => $this->input->post('cuit'),
                    'direccion' => $this->input->post('direccion'),
                    'telefono' => $this->input->post('telefono'),
                    'mail' => $params_user['email'],
                    'is_activo' => 0,
                    'id_cliente_interno' => $this->input->post('id_cliente_interno'),
                );

                $client_id = $this->Clientes->add_cliente($client_data);

                // Relacion Cliente Usuario
                $rel_data = array(
                    'idCliente' => $client_id,
                    'idUsuario' => $user_id,
                );

                $rel = $this->RelClienteUsuario_model->add_rel($rel_data);

                if ($user_id && $client_id && $rel) {
                    //$this->input->post('password') = $password;

                    // Enviar Correo
                    $data = array('password' => $password);
                    ob_start();
                    $this->load->view('layouts/email/email-header');
                    $this->load->view('layouts/email/new-user', $data);
                    $this->load->view('layouts/email/email-footer');
                    $body = ob_get_clean();

                    // Correo usuario
                    $send = $this->Base_model->send_email_template('Gracias por registrarte!', $params_user['email'], $body);

                    // Enviar Correo
                    ob_start();
                    $this->load->view('layouts/email/email-header');
                    $this->load->view('layouts/email/new-user-admin');
                    $this->load->view('layouts/email/email-footer');
                    $bodyAdmin = ob_get_clean();

                    // Correo usuario
                    $sendAdmin = $this->Base_model->send_email_template('Nuevo usuario registrado', $this->Base_model->get_sysopt_value('admin_mail'), $bodyAdmin);
                }
                echo returnJson(['status' => 200, 
                                    'title' => 'Cliente', 
                                    'message' => 'Cliente agregado correctamente!', 
                                    'user' => $this->input->post('mail'), 
                                    'password' => $password]);
                //redirect( 'clientes/index' );
            }
        } catch (\Throwable $th) {
            echo returnJson(['status' => 500, 'title' => 'Cliente', 'message' => 'Error al momento de registrar el usuario!']);
        }
    }

    function edit($idCliente)
    {
        // check if the clientes exists before trying to edit it
        $cliente = $this->Clientes->get_cliente($idCliente);
        $usuarios = $this->Clientes->get_client_users($idCliente);
        $ordenes = $this->Order_model->get_orders_by_client($idCliente);
        $tipos_usuarios = $this->TipoCliente->get_all_tc();


        $script = array(
            base_url('assets/manager/plugins/datatables/datatables.min.js'),
            base_url('assets/manager/plugins/forms/validation/validate.min.js'),
            base_url('assets/manager/js/clients/client_edit.js?v='.time()),
        );

        $csss = array(
            base_url('assets/manager/plugins/datatables/datatables.min.css'),
            base_url('assets/manager/plugins/datatables/dataTables.bootstrap4.min.css'),
        );
        $data = array(
            "scripts"    =>    $script,
            "csss"        =>    $csss,
            "title"        =>    'ID: ' . $idCliente . ' - ' . $cliente['razon_social'],
            "buttons"    =>    [
                /* array(
                    'title' => 'Ver Movimiento',
                    'url'   => '#', //base_url('manager/Products/import'),
                    'icon'  => 'icon-file-zip',
                    'attr'  => 'id="btnViewMovimientos"',
                ), */
                array(
                    'title' => 'Guardar',
                    'url'   => '#', //base_url('manager/Products/import'),
                    'icon'  => 'icon-loop3',
                    'attr'  => 'id="btnUpdateCliente"',
                )
            ],
            "breadcumbs" =>  array(
                'Inicio' => base_url(),
                'Cliente' => base_url('manager/clients'),
                'Editar' => null,
            ),
            "tipos_usuarios"  => $tipos_usuarios,
            "usuarios"  => $usuarios,
            "ordenes"   => $ordenes,
            "cliente"   => $cliente,
            "user" => $this->user
        );

        $this->load->view('manager/clients/edit/edit', $data);
    }

    function updateUser()
    {

        // Cliente
        $client_data = array(
            'razon_social' => $this->input->post('razon_social'),
            'idTipoCliente' => $this->input->post('tipo_cliente'),
            'cuit' => $this->input->post('cuit'),
            'direccion' => $this->input->post('direccion'),
            'telefono' => $this->input->post('telefono'),
            'mail' => $this->input->post('mail'),
            //'is_activo' => 0,
            'id_cliente_interno' => $this->input->post('id_cliente_interno'),
            'email_vendedor' => $this->input->post('email_vendedor'),
        );
        $idCliente = $this->input->post('id_cliente');

        $this->Clientes->update_cliente($idCliente, $client_data);
        echo returnJson(['status' => 200, 'title' => 'Cliente', 'message' => 'Cliente modificado exitosamente']);
    }

    public function get_lists_usuarios_ajax()
    {

        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = array();

        $idCliente = 3;

        $usuarios = $this->Clientes->get_client_users($idCliente);

        foreach ($usuarios as $r) {

            ob_start();
?>
            <div class="list-icons">
                <div class="list-icons-item dropdown">
                    <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false">
                        <i class="icon-menu7"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-164px, 16px, 0px);">

                    </div>
                </div>
            </div>

<?php

            // preparo la accion editar
            $acciones = ob_get_clean();

            // formateo la fecha 
            $data[] = array(
                $r['user_id'],
                $r['username'],
                $r['email'],
                $r['auth_level'],
                $r['banned'],
                $r['created_at'],
                $r['modified_at'],
                $acciones,
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => 1,
            "recordsFiltered" => 1,
            "data" => $data
        );

        echo json_encode($output);
        exit();
    }

    /*
     * Deleting clientes
     */
    function remove($idCliente)
    {

        $clientes = $this->Clientes->get_cliente($idCliente);

        // check if the clientes exists before trying to delete it
        if (isset($clientes['idCliente'])) {
            if (count($this->Order_model->get_orders_by_client_pending($idCliente)) == 0) {
                $this->Clientes->delete_cliente($idCliente);
                echo returnJson(['status' => 200, 'title' => 'Cliente', 'message' => 'Cliente eliminado correctamente!']);
            } else {
                echo returnJson(['status' => 500, 'title' => 'Cliente', 'message' => 'El Cliente posee ordenes pendientes!']);
            }
        } else {
            echo returnJson(['status' => 500, 'title' => 'Cliente', 'message' => 'Cliente no se ha podido eliminar!']);
        }
    }

    public function update_client_status($idCliente = null, $status = null)
    {

        if ($idCliente !== null && $status !== null) {

            $update = $this->Clientes->update_cliente($idCliente, ['idCliente' => $idCliente, 'is_activo' => $status]);

            if ($this->db->affected_rows() >= 1) {
                $update_user = $this->User_model->active_users_from_client($idCliente, $status);
                $return = json_encode(['code' => 1, 'message' => 'Cliente actualizado correctamente.']);
            } else {

                $return = json_encode(['code' => 0, 'message' => 'Hubo un error y no se pudo actualizar.']);
            }
        } else {

            $return = json_encode(['code' => 0, 'message' => 'No hay datos.']);
        }

        echo $return;
    }

    function export_clients(){
         // Crear una instancia de PhpSpreadsheet
       $spreadsheet = new Spreadsheet();

       // Iniciando las configuraciones del archivo excel
       $spreadsheet->setActiveSheetIndex(0);
       $sheet = $spreadsheet->getActiveSheet();
       $sheet->setTitle('Clientes');
       $sheet->getDefaultColumnDimension()->setWidth(25);

       $sheet->setCellValue('A1', 'ID Cliente');
       $sheet->setCellValue('B1', 'Razon Social');
       $sheet->setCellValue('C1', 'Tipo Cliente');
       $sheet->setCellValue('D1', 'CUIT');
       $sheet->setCellValue('E1', 'Dirección');
       $sheet->setCellValue('F1', 'Telefono');
       $sheet->setCellValue('G1', 'Email');
       $sheet->setCellValue('H1', 'Activo');
       $sheet->setCellValue('I1', 'Eliminado');
       $sheet->setCellValue('J1', 'ID Cliente Interno');
       $sheet->setCellValue('K1', 'Fecha Creación');

       $sheet->getStyle('A1:K1')->getFill()
            ->setFillType(Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('477fe0');

        // Establecer altura de la primera fila
        $sheet->getRowDimension(1)->setRowHeight(30); // Ajusta la altura según tus preferencias

       // Recupero los Clientes
       $clientes = $this->Clientes->getAllExport();
       
       if (!empty($clientes)) {
            $rowData = '2';
            foreach ($clientes as $c) {
                $sheet->setCellValue('A' . $rowData, $c['idCliente']);
                $sheet->setCellValue('B' . $rowData, $c['razon_social']);
                $sheet->setCellValue('C' . $rowData, $c['texto']);
                $sheet->setCellValue('D' . $rowData, $c['cuit']);
                $sheet->setCellValue('E' . $rowData, $c['direccion']);
                $sheet->setCellValue('F' . $rowData, $c['telefono']);
                $sheet->setCellValue('G' . $rowData, $c['mail']);
                $sheet->setCellValue('H' . $rowData, $c['is_activo'] ? 'Activo' : 'Inactivo');
                $sheet->setCellValue('I' . $rowData, $c['deleted'] ? 'Elimiando' : 'Activo');
                $sheet->setCellValue('J' . $rowData, $c['id_cliente_interno'] ? $c['id_cliente_interno'] : '-');
                $sheet->setCellValue('K' . $rowData, date('d/m/Y', strtotime($c['fechaCreacion'])));
                
                $rowData++;           
            }
        }

       // Crear un objeto de escritura y guardar el archivo Excel
       $writer = new Xlsx($spreadsheet);
       $filename = 'detalle_clientes.xlsx';
       $writer->save($filename);

       // Descargar el archivo
       header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
       header('Content-Disposition: attachment;filename="' . $filename . '"');
       header('Cache-Control: max-age=0');

       $writer->save('php://output');
    }

    /* function generate_password()
    {
        $marcas = $this->Marcas->get_nombres_marcas();
        $indice = random_int(0 , count($marcas)-1);
        return (str_replace(" ","*",ucwords(strtolower($marcas[$indice]['nombre']))).random_int(10000,99999));
        
    } */

}
