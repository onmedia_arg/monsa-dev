<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class User extends BaseController{
    private $user;
    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('manager/User_model', 'User_model');
            $this->load->model('manager/Marca_model', 'Marcas');
            $this->load->model('manager/Clients_model', 'Clients_model');
            $this->load->model('manager/Order_model', 'Order_model');
            $this->load->model('manager/RelClienteUsuario_model', 'RelClienteUsuario_model');
            // Load form validation library
            $this->load->library('form_validation');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of users
     */
    function index()
    {
        //$data['users'] = $this->User_model->get_all_users();
        $script = array(
            base_url('assets/manager/plugins/datatables/datatables.min.js'),
            base_url('assets/manager/js/users/list.js?v='.time()),
        );

        $csss = array(
            base_url('assets/manager/plugins/datatables/datatables.min.css'),
            base_url('assets/manager/plugins/datatables/dataTables.bootstrap4.min.css'),
        );

        $data = array(
            "scripts"    =>    $script,
            "csss"        =>    $csss,
            "title"        =>    'Usuarios',
            "buttons"    =>    [
                array(
                    'title' => 'Nuevo Usuario',
                    'url'   =>  base_url('user/add'),
                    'icon'  => 'icon-user',
                    'attr'  => 'id="newUser"',
                )
            ],
            "breadcumbs" =>  array(
                'Inicio' => base_url(),
                'Usuarios' => null,
            ),
        );

        // enviamos dentro de $data array con js y css espeíficos de la hoja
        //Cargamos la vista correspondiente
        $this->load->view('manager/users/index', $data);
    }

    public function get_list_dt()
    {

        $allInputs = json_decode(trim(file_get_contents('php://input')), true);

        //Paginado
        $start = $allInputs["start"];

        //Ver
        $length = $allInputs["length"];

        //Busqueda
        $search = $allInputs["search"]['value'];


        if ($allInputs["order"][0] != "") {
            switch ($allInputs["order"][0]["column"]) {
                case 0:
                    $column = "u.user_id";
                    break;

                case 1:
                    $column = "u.username";
                    break;

                case 2:
                    $column = "u.email";
                    break;

                case 3:
                    $column = "u.auth_level";
                    break;

                case 4:
                    $column = "u.banned";
                    break;

                case 5:
                    $column = "u.is_active";
                    break;

                case 6:
                    $column = "cli.razon_social";
                    break;

                case 7:
                    $column = "u.created_at";
                    break;

                case 8:
                    $column = "u.modified_at";
                    break;
            }

            $order = array(
                "column"        => $column,
                "direction"     => $allInputs["order"][0]["dir"]
            );
        }

        $resultado = $this->User_model->getAllUsers($start, $length, $search, $order);

        $data = array();
        $numrows = $resultado["numRows"];
        $numrowstotal = $this->User_model->getNumRowAll();
        foreach ($resultado['datos']->result_array() as $result_row) {

            switch ($result_row["is_active"]) {
                case 0:
                    $activo = '<input  type="checkbox" data-on-text="Si" data-off-text="No" class="is_activo" name="is_activo" value="' . $result_row["user_id"] . '">';
                    break;

                case 1:
                    $activo = '<input type="checkbox" data-on-text="Si" data-off-text="No" class="is_activo" name="is_activo" value="' . $result_row["user_id"] . '" checked>';
                    break;
            }

            $actions = '<div class="list-icons">
                            <div class="list-icons-item dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                                <div class="dropdown-menu dropdown-menu-right">

                                <a href="' . base_url('user/edit/' . $result_row['user_id'] ) . '" class="dropdown-item" title="Editar">
                                    <i class="icon-pencil"></i>Editar
                                </a> 
                                <a href="#" data-id="' . $result_row['user_id']  . '" id="updatePassword" data-username="' . $result_row['username']  . '" class="dropdown-item" title="Actualizar Contraseña">
                                    <i class="fa fa-key"></i>Actualizar Contraseña
                                </a>
                                <a href="#" data-id="' . $result_row["user_id"] . '" data-username="' . $result_row["username"] . '" class="dropdown-item delete-user" title="Eliminar">
                                    <i class="fa fa-trash"></i>Eliminar
                                </a> 

                                </div>
                            </div>
                        </div>';

            $row_of_data = array();
            //print_r($result_row);exit();
            $row_of_data[] = $result_row["user_id"];
            $row_of_data[] = $result_row["username"];
            $row_of_data[] = $result_row["email"];
            $row_of_data[] = $result_row["auth_level"];
            $row_of_data[] = $result_row["banned"];
            $row_of_data[] = $activo;
            $row_of_data[] = $result_row["razon_social"];
            $row_of_data[] = $result_row["created_at"];
            $row_of_data[] = $result_row["modified_at"];

            $row_of_data[] = $actions;

            $data[] = $row_of_data;
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($numrowstotal),
            "recordsFiltered" => intval($numrows), //intval($numrows),
            "data"            => $data
        );
        echo json_encode($json_data);
    }

    /*
     * Adding a new user
     */
    function add()
    {   

        $data['clientes'] = $this->Clients_model->get_all_clientes();

        // Si envia formulario
        if ( isset( $_POST ) && ! empty( $_POST ) ) {

            // Check validation for user input in SignUp form
            $this->form_validation->set_rules('username', 'Nombre de Usuario', 'required')
                                    ->set_rules('email', 'Correo', 'required')
                                    // ->set_rules('auth_level', 'Nivel de Usuario', 'required')
                                    ->set_rules('passwd', 'Contraseña', 'required');

            // Si todo esta bien
            if ( $this->form_validation->run() !== FALSE ) {

                $params = array(
                    // 'user_id' => mt_rand(1000000000, 9999999999),
                    'username' => $this->input->post('username'),
                    'email' => $this->input->post('email'),
                    'auth_level' => '1',
                    'passwd' => sha1($this->input->post('passwd')),
                );
                
                $user_id = $this->User_model->add_user($params);

                // Si asigna cliente asociado
                if ( isset( $_POST['cliente'] ) && $_POST['cliente'] !== 0 ) {

                    // Relacion Cliente Usuario
                    $rel_data = array(
                                    'idCliente' => $_POST['cliente'],
                                    'idUsuario' => $user_id,
                                );

                    $rel = $this->RelClienteUsuario_model->add_rel( $rel_data );

                }

                $this->session->set_flashdata( 'success_message', 'Usuario agregado correctamente!' );   
                redirect('user/index');

            }else{

                $this->session->set_flashdata( 'alert_message', validation_errors() );   

                $data['_view'] = 'user/add';
                $data['user'] = $this->user;
                $this->load->view('layouts/main',$data);

            }

        }else{

            $data['_view'] = 'user/add';
            $data['user'] = $this->user;
            $this->load->view('layouts/main',$data);

        }
 
    }  

    /*
     * Editing a user
     */
    function edit($user_id)
    {   
        // check if the user exists before trying to edit it
        $data['user'] = $this->User_model->get_user($user_id);
        $data['clientes'] = $this->Clients_model->get_all_clientes();
        
        if(isset($data['user']['user_id']))
        {

            // Si envia formulario
            if ( isset( $_POST ) && ! empty( $_POST ) ) {

                // Check validation for user input in SignUp form
                $this->form_validation->set_rules('username', 'Nombre de Usuario', 'required')
                                        ->set_rules('email', 'Correo', 'required')
                                        ->set_rules('auth_level', 'Nivel de Usuario', 'required')
                                        ->set_rules('banned', 'Banned', 'required|numeric');

                // Si todo esta bien
                if ( $this->form_validation->run() !== FALSE ) {

                    $fecha = new DateTime();
                    $modified_at = $fecha->format('Y-m-d H:i:s');
                    $params = array(
    					'username' => $this->input->post('username'),
    					'email' => $this->input->post('email'),
    					'auth_level' => $this->input->post('auth_level'),
    					'banned' => $this->input->post('banned'),
    					'modified_at' => $modified_at,
                    );

                    $this->User_model->update_user($user_id,$params);   
                    $this->session->set_flashdata( 'success_message', 'Usuario actualizado correctamente!' );    
                    redirect('user/index');

                }
                else
                {
                    $this->session->set_flashdata( 'alert_message', validation_errors() );   
                    $data['_view'] = 'user/edit';
                    $this->load->view('layouts/main',$data);
                }

            }else{

                $data['_view'] = 'user/edit';
                $this->load->view('layouts/main',$data);

            }  

        }else{
            $this->session->set_flashdata( 'alert_message', 'El usuario seleccionado no existe.' );   
            redirect('user/index', 'refresh');
        }

    } 

    /* 
     * Deleting user
     */
    function remove($user_id)
    {
        $user = $this->User_model->get_user($user_id);

        // check if the user exists before trying to delete it
        if(isset($user['user_id']))
        {
            if(count($this->Order_model->get_orders_by_user_pending($user_id)) == 0){
                $this->User_model->delete_user($user_id);
                echo returnJson( ['status' => 200, 'title' => 'Usuario', 'message' => 'Usuario eliminado correctamente!' ] );
            }else{
                echo returnJson( ['status' => 500, 'title' => 'Usuario', 'message' => 'El usuario posee ordenes pendientes!' ] );
            }

        }else{
            echo returnJson( ['status' => 500, 'title' => 'Usuario', 'message' => 'Usuario no se ha podido eliminar!' ] );
        }
    }

    public function update_user_status($idUser = null, $status = null)
    {

        if ($idUser !== null && $status !== null) {

            $update = $this->User_model->update_user($idUser, ['user_id' => $idUser, 'is_active' => $status]);

            if ($this->db->affected_rows() >= 1) {

                $return = json_encode(['code' => 1, 'message' => 'Usuario actualizado correctamente.']);
            } else {

                $return = json_encode(['code' => 0, 'message' => 'Hubo un error y no se pudo actualizar.']);
            }
        } else {

            $return = json_encode(['code' => 0, 'message' => 'No hay datos.']);
        }

        echo $return;
    }
    /**
     * Log out
     */
    public function logout()
    {
        $this->authentication->logout();

        // Set redirect protocol
        $redirect_protocol = USE_SSL ? 'https' : NULL;

        redirect( site_url( LOGIN_PAGE . '?' . AUTH_LOGOUT_PARAM . '=1', $redirect_protocol ) );
    }   

    function update_password($user_id)
    {
        // Generación de Contraseña.
        $password = $this->User_model->generate_password();

        $user = $this->User_model->get_user($user_id);

        // check if the user exists before trying to delete it
        if(isset($user['user_id']))
        {
            $fecha = new DateTime();
            $modified_at = $fecha->format('Y-m-d H:i:s');
            
            $params = array(
                'passwd' => sha1($password),
                'modified_at' => $modified_at,
            );

            $this->User_model->update_user($user_id,$params);  
            echo returnJson(['status' => 200, 
                                'title' => 'Usuario', 
                                'message' => 'Usuario agregado correctamente!', 
                                'user' => $user['email'], 
                                'password' => $password]);

        }else{
            echo returnJson( ['status' => 500, 'title' => 'Usuario', 'message' => 'Usuario no se ha podido actualizar!' ] );
        }
        
    }
}
