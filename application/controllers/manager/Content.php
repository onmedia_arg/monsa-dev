<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Content extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('manager/Content_model', 'Content');
            $this->load->model('manager/Slider_model', 'Slider');
            $this->load->model('manager/Brands_model', 'Brands');

            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    function index(){
        $script = array( 
            base_url('assets/manager/js/content/slider.js?v='.time()),
            base_url('assets/manager/js/content/brands.js?v='.time()),
        );
                
        $data = array(
            "scripts"	=>	$script,
            "title"		=>	'Contenido',  
            "breadcumbs"=>  array(
                                'Inicio' => base_url(),
                                'Contenido' => null,
                            ),
            "view"      => 'manager/content/index'
        );

        //Enviamos dentro de $data array con js y css espeíficos de la hoja
       $this->load->view('manager/content/index', $data); 
    }


    function get_slider(){
        $slider = $this->Slider->getSlider();
        echo json_encode($slider);
    }

    function uploadImageSlider(){

        $folder = 'uploads/slider/';

        // Check if the directory exists, if not, create it
        if (!is_dir($folder)) {
            mkdir($folder, 0755, true);
        }


		$file					 = 'file';
        $fileName = $_FILES[$file]['name'];
		$config['upload_path']   = $folder;
		$config['file_name']     = $fileName;
        $config['allowed_types'] = 'jpg|png|jpeg|pdf';
        $config['max_size']      = 40000;
        $config['max_width']     = 2048;
        $config['max_height']    = 2048;

        $this->load->library('upload', $config);

        $data = $this->upload->do_upload($file);
        
		if (!$data) {
			$data = $this->upload->display_errors();
            $response=[
				'status'	=>400,
				'type'		=>'danger',
				'message'	=>'Error en el archivo: '.$data
            ];
           
        }else{

			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name'];
            $type = $this->input->post('type');

            $data = Array(
                'image'          => $file_name,
                'order'          => 1,
                'link'           => '',
                'location'       => $this->input->post('location'),
                'folder'         => $folder,
                'fullpath'       => $folder . $file_name,
                'publicfullpath' => base_url($folder . $file_name),
                'created_by'     => $_SESSION['user_id']
            );

            $r = $this->Slider->insert_slider($data);
            $upload = $this->Slider->get_slider_by_id($r);

        	$response=[
				'status'	=> 200,
				'type'		=> 'success',
                'message'	=> 'Archivo subido correctamente',
                'upload'    => $upload
			];

        }

        //convertimos la respuesta a json
        echo json_encode($response);
    }

    function deleteImageSlider(){
        $id = $this->input->post('id');
        try{
            $slider = $this->Slider->get_slider_by_id($id);
            $path = $slider->fullpath;
            unlink($path);

            $this->Slider->delete_slider($id);
            
            $response = [
                'status' => 200,
                'message' => 'Imagen eliminada correctamente'
            ];
    
            echo json_encode($response);
        }catch(Exception $e){
            echo json_encode($e);
        }
    }


    function get_brands(){
        $brands = $this->Brands->getBrands();
        echo json_encode($brands);
    }

    function uploadImageBrands(){

        $folder = 'uploads/brands/';

        // Check if the directory exists, if not, create it
        if (!is_dir($folder)) {
            mkdir($folder, 0755, true);
        }


		$file					 = 'file';
        $fileName = $_FILES[$file]['name'];

        $config['upload_path']   = $folder;
		$config['file_name']     = $fileName;
        $config['allowed_types'] = 'jpg|png|jpeg|pdf';
        $config['max_size']      = 40000;
        $config['max_width']     = 2048;
        $config['max_height']    = 2048;

        $this->load->library('upload', $config);

        $data = $this->upload->do_upload($file);
        
		if (!$data) {
			$data = $this->upload->display_errors();
            $response=[
				'status'	=>400,
				'type'		=>'danger',
				'message'	=>'Error en el archivo: '.$data
            ];
           
        }else{

			$upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            $file_name = $upload_data['file_name'];
            
            $data = Array(
                'image'          => $file_name,
                'order'          => 1,
                'link'           => '',
                'location'       => '',
                'folder'         => $folder,
                'fullpath'       => $folder . $file_name,
                'publicfullpath' => base_url($folder . $file_name),
                'created_by'     => $_SESSION['user_id']
            );

            $r = $this->Brands->insert_brands($data);
            $upload = $this->Brands->get_brands_by_id($r);

        	$response=[
				'status'	=> 200,
				'type'		=> 'success',
                'message'	=> 'Archivo subido correctamente',
                'upload'    => $upload
			];

        }

        //convertimos la respuesta a json
        echo json_encode($response);
    }

    function deleteImageBrands(){
        $id = $this->input->post('id');
        try{
            $slider = $this->Brands->get_brands_by_id($id);
            $path = $slider->fullpath;
            unlink($path);

            $this->Brands->delete_brands($id);
            
            $response = [
                'status' => 200,
                'message' => 'Imagen eliminada correctamente'
            ];
    
            echo json_encode($response);
        }catch(Exception $e){
            echo json_encode($e);
        }
    }



}