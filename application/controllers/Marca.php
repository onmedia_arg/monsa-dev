<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Marca extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Marca_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of marca
     */
    function index()
    {
        $data['marca'] = $this->Marca_model->get_all_marca();

        // Ordenar por orden
        usort($data['marca'], function($a, $b){ 
            return strnatcmp($a['orden'], $b['orden']);
        });

        $data['user'] = $this->user;
        $data['_view'] = 'marca/index';
        $this->load->view('layouts/main',$data);
    } 

    /*
     * Adding a new marca
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
                'idMarca' => $this->input->post('idMarca'),
				'nombre' => $this->input->post('nombre'),
				'slug' => $this->input->post('slug'),
            );
            
            $marca_id = $this->Marca_model->add_marca($params);
            redirect('marca/index');
        }
        else
        {            
            $data['_view'] = 'marca/add';
            $data['user'] = $this->user;
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a marca
     */
    function edit($idMarca)
    {   
        // check if the marca exists before trying to edit it
        $data['marca'] = $this->Marca_model->get_marca($idMarca);
        
        if(isset($data['marca']['idMarca']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
                    'idMarca' => $idMarca,
					'nombre' => $this->input->post('nombre'),
					'slug' => $this->input->post('slug'),
                );

                $this->Marca_model->update_marca($idMarca,$params);            
                redirect('marca/index');
            }
            else
            {
                $data['_view'] = 'marca/edit';
                $data['user'] = $this->user;
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The marca you are trying to edit does not exist.');
    } 

    /*
     * Deleting marca
     */
    function remove($idMarca)
    {
        $marca = $this->Marca_model->get_marca($idMarca);

        // check if the marca exists before trying to delete it
        if(isset($marca['idMarca']))
        {
            $this->Marca_model->delete_marca($idMarca);
            redirect('marca/index');
        }
        else
            show_error('The marca you are trying to delete does not exist.');
    }

    function fillTable(){
        $file = FCPATH.'/application/models/json/makesAndModels.json';

        $data = file_get_contents($file);

        $data_arr = json_decode($data, true);

        $arr_data = [];
        for ($i=0; $i < count($data_arr['Marcas']); $i++) {
            $params = array(
                'idMarca' => $data_arr['Marcas'][$i]['id'],
                'nombre' => $data_arr['Marcas'][$i]['nombre'],
                'slug' => strtolower(str_replace(' ', '-', $data_arr['Marcas'][$i]['nombre'])),
            );
            array_push($arr_data, $params);
        }
        $this->Marca_model->add_marca_batch($arr_data);
    }

    function update_sort(){ 
 
        $data = json_decode( $_POST['json'], true );

        $updateOld = $this->Marca_model->update_sort( $data ); 

        if ( $this->db->affected_rows() >= 1 ) {
            echo json_encode( [ 'code' => 1, 'message' => 'Orden actualizada exitosamente' ] );
        }else{
            echo json_encode( [ 'code' => 0, 'message' => 'No se pudo actualizar el orden' ] );
        }

    }

}
