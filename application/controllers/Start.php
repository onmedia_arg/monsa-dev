<?php
defined('BASEPATH') or exit('No direct script access allowed');

include_once(FCPATH."/application/controllers/BaseController.php");
require_once APPPATH . '/core/Auth_Controller.php';

class Start extends Auth_Controller

{
    // private $cart = null;
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->model('user_model');
        $this->load->model('Base_model');
        $this->load->model('RelClienteUsuario_model');
        $this->load->model('Clientes_model');
        $this->load->model('TipoCliente_model');
        $this->load->model('Order_itm_model');

        $this->load->model('Auth_model', 'auth', true);

        // Load form validation library
        $this->load->library('form_validation');

        if ($this->is_monsa_login()) {
            $this->load->model('Categorium_model');
            $this->user = $this->dataUser();
            // $this->cart = $this->get_user_cart($_SESSION['id'], 1);
        }
    }

    // public function get_user_cart($id_user, $state=''){
    //     $this->load->model('Order_model');
    //     return $this->Order_model->get_order_by_user($id_user, $state);
    // }

    // -----------------------------------------------------------------------
    public function index($messageArr = '')
    {

        if ( $this->is_monsa_login() ) {

            $data['user'] = $this->dataUser();

            if ( $data['user']['role'] == 'Cliente' ){

                $this->session->unset_userdata('idOrderHdr');
                
                //  Revisa si el cliente esta activo
                $activeClient = $this->Clientes_model->client_is_active_by_user( $data['user']['id'] );
                // Revisa si el usuario esta activo
                $activeUser = $this->user_model->user_is_active( $data['user']['id'] ); 

                if( ! $activeClient || ! $activeUser ){

                    $this->session->set_flashdata( 'alert_message', 'El usuario que ingresaste aún no esta activo en el sistema. Espera un minimo de 24 horas y te llegara un correo de notificación.' );
                    $this->logout(); 

                }else{

                    $this->fill_cart_initial_order($data['user']['id']);
                    redirect('FrontController');  

                }

            }else{
                // $data['_view'] = 'dashboard';
                // $this->load->view( 'layouts/main', $data );                

                $script = array( 
                    base_url('assets/manager/plugins/datatables/datatables.min.js'),
                    base_url('assets/manager/js/dashboard/dashboard.js?v='.time()),
                );
                        
                $csss = array(
                    base_url('assets/manager/plugins/datatables/datatables.min.css'),
                    base_url('assets/manager/plugins/datatables/dataTables.bootstrap4.min.css'),
                );
        
        
                $data = array(
                    "scripts"	=>	$script,
                    "csss"		=>	$csss,
                    "view"      => 'manager/orders/list'
                );

                $data['view'] = 'manager/dashboard/start';
                $this->load->view( 'manager/index', $data );                                
            }

        }else{
            
            $data['_view'] = 'user/login/login';
            $this->load->view('user/login/index',$data);
        }
    }

    function fill_cart_initial_order($user_id){
       
        $this->cart->destroy();
        
        $order = $this->Order_itm_model->getItemsFromOrderStatus1($user_id);

        if ($order != ""){
            $this->session->set_userdata("idOrderHdr",$order['idOrderHdr']);
       
            if( count($order['items']) > 0 ){
                
                $i=0;
                foreach ($order['items'] as $item) {
                    $name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $item['nombre']);
                    $name = preg_replace('!\s+!', ' ', $name);
                    
                    $data = array(
                        "id"     => $item['idProducto'],
                        "name"   => $name,
                        "qty"    => $item['qty'],
                        "price"  => $item['precio'],
                        "sku"    => $item['sku'],
                    ); 

                    $this->cart->insert($data);
                }
            }       
        }
    }

	public function recover()
	{
		// Load resources
		// $this->load->model('examples/examples_model');

		/// If IP or posted email is on hold, display message
		if( $on_hold = $this->authentication->current_hold_status( TRUE ) )
		{
			$view_data['disabled'] = 1;
		}
		else
		{
			// If the form post looks good
			if( $this->input->post('email') )
			{
                
                if( $user_data = $this->auth->get_recovery_data( $this->input->post('email') ) )
				{
                    
                    // Check if user is banned
					if( $user_data->banned == '1' )
					{
						// Log an error if banned
						$this->authentication->log_error( $this->input->post('email', TRUE ) );

						// Show special message for banned user
						$view_data['banned'] = 1;
					}
					else
					{
                        
                        /**
						 * Use the authentication libraries salt generator for a random string
						 * that will be hashed and stored as the password recovery key.
						 * Method is called 4 times for a 88 character string, and then
						 * trimmed to 72 characters
						 */
						$recovery_code = substr( $this->authentication->random_salt() 
							. $this->authentication->random_salt() 
							. $this->authentication->random_salt() 
							. $this->authentication->random_salt(), 0, 72 );

						// Update user record with recovery code and time
						$this->auth->update_user_raw_data(
							$user_data->user_id,
							[
								'passwd_recovery_code' => $this->authentication->hash_passwd($recovery_code),
								'passwd_recovery_date' => date('Y-m-d H:i:s')
							]
						);

						// Set the link protocol
						$link_protocol = USE_SSL ? 'https' : NULL;

						// Set URI of link
						$link_uri = 'Start/recovery_verification/' . $user_data->user_id . '/' . $recovery_code;

						$view_data['special_link'] = anchor( 
							site_url( $link_uri, $link_protocol ), 
							'LINK', 
							'target ="_blank"' 
						);

                        // Enviar Correo
                        ob_start(); 
                            $this->load->view( 'layouts/email/email-header' );
                                $this->load->view( 'layouts/email/recovery_password', $view_data );
                            $this->load->view( 'layouts/email/email-footer');
                        $body = ob_get_clean(); 

                        // Correo usuario
                        $send = $this->Base_model->send_email_template( 'Recuperar Clave', $this->input->post('email'), $body );

						$view_data['confirmation'] = 1;
					}
				}

				// There was no match, log an error, and display a message
				else
				{
					// Log the error
					$this->authentication->log_error( $this->input->post('email', TRUE ) );

					$view_data['no_match'] = 1;
				}
			}
        }

        $view_data['_view'] = 'user/login/recover';
        $this->load->view('user/login/index',$view_data);  

	}

	/**
	 * Verification of a user by email for recovery
	 * 
	 * @param  int     the user ID
	 * @param  string  the passwd recovery code
	 */
	public function recovery_verification( $user_id = '', $recovery_code = '' )
	{
		/// If IP is on hold, display message
		if( $on_hold = $this->authentication->current_hold_status( TRUE ) )
		{
			$view_data['disabled'] = 1;
		}
		else
		{
			// Load resources

			if( 
				/**
				 * Make sure that $user_id is a number and less 
				 * than or equal to 10 characters long
				 */
				is_numeric( $user_id ) && strlen( $user_id ) <= 10 &&

				/**
				 * Make sure that $recovery code is exactly 72 characters long
				 */
				strlen( $recovery_code ) == 72 &&

				/**
				 * Try to get a hashed password recovery 
				 * code and user salt for the user.
				 */
				$recovery_data = $this->auth->get_recovery_verification_data( $user_id ) )
			{
				/**
				 * Check that the recovery code from the 
				 * email matches the hashed recovery code.
				 */
				if( $recovery_data->passwd_recovery_code == $this->authentication->check_passwd( $recovery_data->passwd_recovery_code, $recovery_code ) )
				{
					$view_data['user_id']       = $user_id;
					$view_data['username']     = $recovery_data->username;
					$view_data['recovery_code'] = $recovery_data->passwd_recovery_code;
				}

				// Link is bad so show message
				else
				{
					$view_data['recovery_error'] = 1;

					// Log an error
					$this->authentication->log_error('');
				}
			}

			// Link is bad so show message
			else
			{
				$view_data['recovery_error'] = 1;

				// Log an error
				$this->authentication->log_error('');
			}

			/**
			 * If form submission is attempting to change password 
			 */
			// var_dump($this->tokens->match);die;
			//  if( $this->tokens->match )
			// {
				
            // }
            if ( $this->input->post('passwd') !== NULL  && 
                 $this->input->post('passwd_confirm') !== NULL ){
                $this->auth->recovery_password_change();
            }
            // var_dump();die;
            // var_dump('pase por aca');die;
			
		}

        $view_data['_view'] = 'user/login/choose_password_form';
        $this->load->view('user/login/index',$view_data);


		// echo $this->load->view('examples/page_header', '', TRUE);

		// echo $this->load->view( 'examples/choose_password_form', $view_data, TRUE );

		// echo $this->load->view('examples/page_footer', '', TRUE);
	}



    use startFuntions;
}

trait startFuntions {
    
    // -----------------------------------------------------------------------
    public function login(){
        $login = false;

        $this->form_validation->set_rules('login_string', 'Usuario', 'required')
                                ->set_rules('login_pass', 'Password', 'required');

        if ( $this->form_validation->run() == FALSE ) {

            $this->session->set_flashdata( 'error_message', validation_errors() );

            redirect( base_url(), 'refresh' );

        }else{

            $params['user_string'] = $this->input->post('login_string');
            $params['passwd'] = sha1($this->input->post('login_pass')); 

            $found = $this->Base_model->verifyUserAndEmail($params['user_string'] , $params['user_string'] ); 
            if ($found != false) {
                
                if (isset($found['user'])){
                //if ($found['user'] == true) {
                    if($found['user_row']['is_active'] == 1){
                        if ($params['passwd'] == $found['user_row']['passwd']) {
                            $login = true;
                            $newdata = array(
                                'username'  => $found['user_row']['username'], 
                                'user_id'   => $found['user_row']['user_id'], 
                                'email'     => $found['user_row']['email'],
                                'level'     => $found['user_row']['auth_level'],
                                'logged_in' => TRUE
                            );
                        }else{ 
                            
                            $this->session->set_flashdata( 'error_message', 'Usuario o contraseña no coincide. Intente nuevamente.' );
                            redirect( base_url(), 'refresh' );
                            
                        }
                    }else{
                        $this->session->set_flashdata( 'error_message', 'Verifique si su usuario se encuentra activo.' );
                        redirect( base_url(), 'refresh' );
                    }
                    
                }else if($found['email'] == true){
                    if($found['email_row']['is_active'] == 1){
                        if ($params['passwd'] == $found['email_row']['passwd']) {
                            $login = true;
                            $newdata = array(
                                'username'  => $found['email_row']['username'], 
                                'user_id'        => $found['email_row']['user_id'], 
                                'email'     => $found['email_row']['email'],
                                'level'     => $found['email_row']['auth_level'],
                                'logged_in' => TRUE
                            );
                        }else{ 
                            
                            $this->session->set_flashdata( 'error_message', 'Usuario o contraseña no coincide. Intente nuevamente.' );
                            redirect( base_url(), 'refresh' );
                            
                        }
                    }else{
                        $this->session->set_flashdata( 'error_message', 'Verifique si su usuario se encuentra activo.' );
                        redirect( base_url(), 'refresh' );
                    }
                    //print_r("adentro");
                    //exit();
                    
                }else{                     

                    $this->session->set_flashdata( 'error_message', 'Error inesperado intenta de nuevo o comunicate con el administrador.' );
                    redirect( base_url(), 'refresh' );

                }

                if ( $login ) {  

                    // print_r( $newdata );
                    // die();

                    $this->session->set_userdata($newdata);    
                    redirect( base_url(), 'refresh' ); 

                }else{
                    $this->session->set_flashdata( 'error_message', 'Usuario o contraseña no coincide. Intente nuevamente.' );
                    redirect( base_url(), 'refresh' );
                }

            }else{

                $this->session->set_flashdata( 'error_message', 'Usuario o contraseña no coincide. Intente nuevamente.' );
                redirect( base_url(), 'refresh' );

            }

        }

    }
    
    // -----------------------------------------------------------------------
    public function registerUser(){ 

            // Check validation for user input in SignUp form
            $this->form_validation->set_rules('razon_social', 'Nombre / Razón Social', 'required')
                                    ->set_rules('cuit', 'CUIT', 'required')
                                    ->set_rules('telefono', 'Telefono', 'required')
                                    ->set_rules('mail', 'Mail', 'required|valid_email')
                                    ->set_rules('direccion', 'Dirección', 'required')
                                    ->set_rules('usuario', 
                                                'Usuario', 
                                                'required|min_length[5]|max_length[12]|is_unique[users.username]',
                                                 array(
                                                        'min_length' => 'El USUARIO debe contener al menos 5 caracteres',
                                                        'max_length' => 'El USUARIO debe contener como máximo 12 caracteres'
                                                     )
                                                )
                                    ->set_rules('nombre_usuario', 'Nombre y Apellido del Usuario', 'required')
                                    ->set_rules('password_usuario', 'Password', 'required')
                                    ->set_rules('r_password_usuario', 'Confirmar Password', 'required|matches[password_usuario]');

            if ($this->form_validation->run() == FALSE) {

                $this->session->set_flashdata( 'alert_message', validation_errors() );   
                $data['_view'] = 'user/login/register';
                $this->showRegisterForm();

            }else{

                $params['username'] = $this->input->post('usuario');
                $params['email'] = $this->input->post('mail');
                $params['passwd'] = $this->input->post('password_usuario');
                $params['passwd'] = sha1($params['passwd']);
                // $params['user_id'] = mt_rand(1000000000, 9999999999);
                $params['auth_level'] = 1;

                $verify = $this->Base_model->verifyUserAndEmail($params['username'] , $params['email']);
                $verify_client = $this->Base_model->verifyClientAndEmail($params['email']);

                if ($verify != false || $verify_client != false) {

                    $this->session->set_flashdata( 'alert_message', "Usuario y/o email ya esta registrado, intenta recuperar contraseña o crea un nuevo usuario.</br> <a href=".base_url('Start/recover').">Resetear Clave</a>" );   
                    redirect( base_url('Start/showRegisterForm'), 'refresh' );

                }else{

                    // Usuario
                    $user_id = $this->user_model->add_user($params);

                    // Cliente
                    $client_data = array(
                                        'razon_social' => $this->input->post('razon_social'),
                                        'idTipoCliente' => $this->input->post('tipo_cliente'),
                                        'cuit' => $this->input->post('cuit'),
                                        'direccion' => $this->input->post('direccion'),
                                        'telefono' => $this->input->post('telefono'),
                                        'mail' => $params['email'],
                                        'is_activo' => 0,  
                                    ); 

                    $client_id = $this->Clientes_model->add_cliente( $client_data );

                    // Relacion Cliente Usuario
                    $rel_data = array(
                                    'idCliente' => $client_id,
                                    'idUsuario' => $user_id,
                                );

                    $rel = $this->RelClienteUsuario_model->add_rel( $rel_data );
                    
                    if ( $user_id && $client_id && $rel ) {

                        // Enviar Correo
                        $data = array('password' => $this->input->post('password_usuario'));
                        ob_start(); 
                            $this->load->view( 'layouts/email/email-header' );
                            $this->load->view( 'layouts/email/new-user', $data);
                            $this->load->view( 'layouts/email/email-footer');
                        $body = ob_get_clean(); 

                        // Correo usuario
                        $send = $this->Base_model->send_email_template( 'Gracias por registrarte!', $params['email'], $body );

                        // Enviar Correo
                        ob_start(); 
                            $this->load->view( 'layouts/email/email-header' );
                                $this->load->view( 'layouts/email/new-user-admin' );
                            $this->load->view( 'layouts/email/email-footer');
                        $bodyAdmin = ob_get_clean(); 

                        // Correo usuario
                        $sendAdmin = $this->Base_model->send_email_template( 'Nuevo usuario registrado', $this->Base_model->get_sysopt_value('admin_mail'), $bodyAdmin );

                        $this->session->set_flashdata( 'success_message', 'Felicidades! Registro existoso. Tu usuario debe ser activado, espera un minimo de 24 horas y te llegara un correo de notificación.' );

                        redirect( base_url(), 'refresh' );

                    }else{

                        $this->session->set_flashdata( 'error_message', 'Hubo un error y no se pudo registrar el usuario.' );

                        redirect( base_url('Start/showRegisterForm'), 'refresh' );

                    }

                } 

            } 

    }

    // -----------------------------------------------------------------------
    public function showRegisterForm($messageArr = ''){

        if ($messageArr != '') {
            $data['message'] = $messageArr; 
        }

        $data['tipos_usuarios'] = $this->TipoCliente_model->get_all_tc();
        $data['_view'] = 'user/login/register';
        $this->load->view( 'user/login/index', $data );

    }

    // -----------------------------------------------------------------------
    public function logout($message = ''){
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('level');
        $this->session->unset_userdata('logged_in');
        $message = [
            "type" => "info", 
            "text" => ($message == '') ? "Session Cerrada correctamente." : $message
        ];
        $this->index($message);
    }

    // -----------------------------------------------------------------------
    public function reset_key($user_id){
        $key = bin2hex( $this->encryption->create_key( 20 ) );
        $fecha = new DateTime();
        $recovery_date = $fecha->format('Y-m-d H:i:s');
        $params = array(
                    'passwd_recovery_code' => $key,
                    'passwd_recovery_date' => $recovery_date,
                );

        $this->user_model->update_user($user_id,$params);   

        // $this->email->clear();

        // $this->email->to('erickorso@gmail.com');
        // $this->email->from('monsa@monsa-srl.com.ar');
        // $this->email->subject('Here is your info '.$user_id);
        // $this->email->message('Hi '.$user_id.' Here is the info you requested.<br>'. $key);
        // $this->email->send();

            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.google.com',
                'smtp_port' => 465,
                'smtp_user' => 'erickorso@gmail.com',
                'smtp_pass' => 'R0drig0_A', 
                'smtp_crypto' => 'ssl',
                'mailtype' => 'html', 
                'charset' => 'iso-8859-1', 
                'wordwrap' => true
            );

        $this->load->library('email',$config);
        $this->email->set_newline("\r\n");

        $this->email->from("sender@gmail.com", 'admin');
        $this->email->to("erickorso@gmail.com");
        $this->email->subject("Email with Codeigniter");
        $this->email->message("This is email has been sent with Codeigniter");

        if($this->email->send())
        {
            echo "Your email was sent.!";
        } else {
            show_error($this->email->print_debugger());
        }

        // redirect('start/reset_key_form/' . $user_id . '/' . $key);
    }

    // -----------------------------------------------------------------------
    public function reset_key_form($user_id, $key){
        if (isset($_POST) && count($_POST) > 0) {

        }else{
            $data['_view'] = 'user/login/recovery_pass';
            $data['userObj'] = $this->user_model->get_user($user_id);
            $data['user'] = $data['userObj']['username'];
            $data['key'] = $key;
            $data['message'] = [
                        "type" => "warning", 
                        "text" => "Tu clave "
                    ];
            $this->load->view('layouts/login',$data);
        }
    }

    public function generate_password()
    {
        // Generación de Contraseña.
        $data = $this->user_model->generate_password();
        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));      
    }

}

