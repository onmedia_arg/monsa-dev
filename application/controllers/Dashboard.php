<?php

// require_once FCPATH . 'application\controllers\BaseController.php';
include_once(FCPATH."/application/controllers/BaseController.php");
class Dashboard extends BaseController{
    function __construct()
    {
        parent::__construct();   
    }

    function index()
    {
        $data['_view'] = 'dashboard';
        $this->load->view('layouts/main',$data);
    }

}
