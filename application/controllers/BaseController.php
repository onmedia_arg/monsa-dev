<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
	Este clase es para incluir metodos genericos

	Todas las tablas deben tener un CRUD y los metodos especificos en cada uno de los controladores

	Uso:

		1 - Debes incluir este srchivo en el controlador
			de la siguiente forma
				include_once(FCPATH."/application/controllers/BaseController.php");

		2 - Debes declarar el controlador como una exten-
			sion del controlador BaseController y no de CI_Controller

		3 - De igual forma debes llamar al construcctor
			padre
			parent::__construct();

		4 - Es una clase abstracta por lo que no puedes
			usarla sola, siempre debe ser llamada desde
			otro controlador
*/
// require_once FCPATH . 'application\core\Auth_Controller.php';

// class BaseController extends Auth_Controller{
class BaseController extends CI_Controller{

	public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('base_model');
	}
	
	// -----------------------------------------------------------------------
    public function is_monsa_login(){
        $login = false;
        $logged_in = $this->session->userdata('logged_in');
        if ($logged_in && $logged_in == true) {
            $login = true;
        }
        return $login;
    }

    public function getRoleString($role){
        switch ($role) {
            case 9:
                return 'Superadmin';
                break;
            case 6:
                return 'Admin';
                break;
            
            default:
                return 'Cliente';
                break;
        }
    }

    public function dataUser (){
        $userdata = null;
        if ($this->session->userdata('logged_in') == true) {
            $userdata = array(
                    'id'       => $this->session->userdata('user_id'), 
                    'username' => $this->session->userdata('username'), 
                    'email'    => $this->session->userdata('email'),
                    'level'    => $this->session->userdata('level'),
                    'role'     => $this->getRoleString($this->session->userdata('level'))
            );
        }
        return $userdata;
    }

    public function limpiar_valor($value){
    	$value                   = explode('.', $value);
    	$value_entero            = $value[0];
    	$quitar                  = ['$', ','];
    	$remplazar               = '';
    	$value_sin_dolar_ni_coma = str_replace($quitar, $remplazar, $value_entero);
    	return ($value_sin_dolar_ni_coma != '') ? $value_sin_dolar_ni_coma : null;
    }

    public function create_slug($subject){
		$subject = strtolower($subject);
		$subject = str_replace(' ', '-', $subject);

	    $patterns[0]     = '/[á|â|à|å|ä]/';
	    $patterns[1]     = '/[ð|é|ê|è|ë]/';
	    $patterns[2]     = '/[í|î|ì|ï]/';
	    $patterns[3]     = '/[ó|ô|ò|ø|õ|ö]/';
	    $patterns[4]     = '/[ú|û|ù|ü]/';
	    $patterns[5]     = '/[^a-zA-Z1-9\-]/';
	    
	    $replacements[0] = ('a');
	    $replacements[1] = ('e');
	    $replacements[2] = ('i');
	    $replacements[3] = ('o');
	    $replacements[4] = ('u');
	    $replacements[5] = rand(0, 9);

	    $subject = preg_replace($patterns, $replacements, $subject);
	    return $subject;
    }

    public function build_src($images){        
        $max     = 1;
        $img_src = build_img_url($images, $max); 

        return $img_src;
        //return "'" . $img_src . "'";
    }
}
