<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Order_item extends BaseController{
    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Order_item_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of order_item
     */
    function index()
    {
        $data['order_item'] = $this->Order_item_model->get_all_order_item();
        
        $data['_view'] = 'order_item/index';
        // $data['user'] = $this->user;
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new order_item
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'order_item_name' => $this->input->post('order_item_name'),
				'order_item_type' => $this->input->post('order_item_type'),
				'idOrder' => $this->input->post('idOrder'),
            );
            
            $order_item_id = $this->Order_item_model->add_order_item($params);
            redirect('order_item/index');
        }
        else
        {            
            $data['_view'] = 'order_item/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a order_item
     */
    function edit($idOrderItem)
    {   
        // check if the order_item exists before trying to edit it
        $data['order_item'] = $this->Order_item_model->get_order_item($idOrderItem);
        
        if(isset($data['order_item']['idOrderItem']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'order_item_name' => $this->input->post('order_item_name'),
					'order_item_type' => $this->input->post('order_item_type'),
					'idOrder' => $this->input->post('idOrder'),
                );

                $this->Order_item_model->update_order_item($idOrderItem,$params);            
                redirect('order_item/index');
            }
            else
            {
                $data['_view'] = 'order_item/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The order_item you are trying to edit does not exist.');
    } 

    /*
     * Deleting order_item
     */
    function remove($idOrderItem)
    {
        $order_item = $this->Order_item_model->get_order_item($idOrderItem);

        // check if the order_item exists before trying to delete it
        if(isset($order_item['idOrderItem']))
        {
            $this->Order_item_model->delete_order_item($idOrderItem);
            redirect('order_item/index');
        }
        else
            show_error('The order_item you are trying to delete does not exist.');
    }
    
}
