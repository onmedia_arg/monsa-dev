<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
    public function __construct() {
        /**
         * @property CI_Loader $load
         */
        parent::__construct();
        // $this->load->model('Product_model'); // Asumiendo que tienes un modelo llamado Product_model para interactuar con la base de datos
            $this->load->model('Familium_model');
            $this->load->model('Marca_model');
            $this->load->model('Producto_model');
            $this->load->model('Atributo_model');        
    }

    public function get_products() {

        // Configurar los encabezados CORS
        header("Access-Control-Allow-Origin: " .  $this->config->item('cors_allow_origin') );
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, X-Requested-With");

        $query = $this->db->query("SELECT   pr.idProducto, 
                                            pr.idFamilia,
                                            pr.idMarca,
                                            pr.nombre,
                                            pr.slug,
                                            pr.modelo,
                                            pr.descripcion,
                                            pr.imagen,
                                            pr.sku,
                                            pr.precio,
                                            pr.price_public,
                                            pr.dimensiones,
                                            pr.peso,
                                            pr.stock,
                                            pr.stock_quantity,
                                            pr.visibilidad,
                                            pr.state,
                                            pr.show,
                                            pr.search,
                                            pr.alto,
                                            pr.ancho,
                                            pr.largo,
                                            atr.idAtributo,
                                            atr.valores
                                    FROM producto as pr 
                                    INNER JOIN producto_atributo as atr ON pr.idProducto = atr.idProducto");

        $raw_products = $query->result_array();

        // Preparar un arreglo para almacenar los productos agrupados
        $products = [];

        foreach ($raw_products as $row) {
            $idProducto = $row['idProducto'];

            // Si el producto aún no está en el arreglo, inicialízalo
            if (!isset($products[$idProducto])) {
                $products[$idProducto] = [
                    'idProducto' => $row['idProducto'],
                    'idFamilia' => $row['idFamilia'],
                    'idMarca' => $row['idMarca'],
                    'nombre' => $row['nombre'],
                    'slug' => $row['slug'],
                    'modelo' => $row['modelo'],
                    'descripcion' => $row['descripcion'],
                    'imagen' => $row['imagen'],
                    'sku' => $row['sku'],
                    'precio' => $row['precio'],
                    'price_public' => $row['price_public'],
                    'dimensiones' => $row['dimensiones'],
                    'peso' => $row['peso'],
                    'stock' => $row['stock'],
                    'stock_quantity' => $row['stock_quantity'],
                    'visibilidad' => $row['visibilidad'],
                    'state' => $row['state'],
                    'show' => $row['show'],
                    'search' => $row['search'],
                    'alto' => $row['alto'],
                    'ancho' => $row['ancho'],
                    'largo' => $row['largo'],
                    'atributos' => []  // Inicializa el arreglo de atributos
                ];
            }

            // Añade el atributo al producto
            $products[$idProducto]['atributos'][] = [
                                                        'idAtributo' => $row['idAtributo'],
                                                        'valores'    => $row['valores']    
                                                    ];
        }

        // Re-indexa para eliminar las claves basadas en idProducto
        $products = array_values($products);

        $this->output->set_content_type('application/json')->set_output(json_encode($products));

    }

    public function get_brands(){
        // Configurar los encabezados CORS
        header("Access-Control-Allow-Origin: " .  $this->config->item('cors_allow_origin') );
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, X-Requested-With");

        $query = $this->db->query("SELECT * from marca " );

        $marcas = $query->result_array(); 

        $this->output->set_content_type('application/json')->set_output(json_encode($marcas)); 

    }

    public function get_families(){
        // Configurar los encabezados CORS
        header("Access-Control-Allow-Origin: " .  $this->config->item('cors_allow_origin') );
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, X-Requested-With");

        $query = $this->db->query("SELECT * from familia " );

        $familias = $query->result_array(); 

        $this->output->set_content_type('application/json')->set_output(json_encode($familias)); 

    }

    public function get_atributos(){
        // Configurar los encabezados CORS
        header("Access-Control-Allow-Origin: " .  $this->config->item('cors_allow_origin') );
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, X-Requested-With");

        $query = $this->db->query("SELECT * from atributo " );

        $atributo = $query->result_array(); 

        $this->output->set_content_type('application/json')->set_output(json_encode($atributo)); 
        
    }

}
