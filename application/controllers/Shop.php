<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Shop extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Producto_model');
            $this->load->model('Familium_model');
            $this->load->model('Marca_model');
            $this->load->helper("globalFunctions");
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 


    public function fetch_data()
    {
        sleep(1);

        $marca         = $this->input->post('marca');
        $familia       = $this->input->post('familia');
        $atributo      = json_decode( urldecode( $this->input->post('atributo') ), true );
        $shop_layout   = $this->input->post('shop_layout');
        $search        = $this->input->post('search');

        $countData = array(
                            'marca'     => $marca,
                            'familia'   => $familia,
                            'atributo'  => $atributo,
                            'vista'     => $shop_layout, 
                            'search'    => $search
                        );

        $this->load->library('pagination');

        $config = array();
        $config['base_url']         = '#';
        $config['total_rows']       = $this->Producto_model->countAll( $countData );
        $config['per_page']         = $per_page = ( isset( $_POST['cantidad'] ) ) ? (int) $_POST['cantidad'] : 12 ;
        $config['uri_segment']      = 3;
        $config['use_page_numbers'] = TRUE;
        $config['full_tag_open']    = '<ul class="pagination">';
        $config['full_tag_close']   = '</ul>';
        $config['first_link']       = 'Primero';
        $config['first_tag_open']   = '<li>';
        $config['first_tag_close']  = '</li>';
        $config['last_link']        = 'Ultimo';
        $config['last_tag_open']    = '<li>';
        $config['last_tag_close']   = '</li>';
        $config['next_link']        = '&gt;';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['prev_link']        = '&lt;';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';
        $config['cur_tag_open']     = "<li class='active'><a href='#'>";
        $config['cur_tag_close']    = '</a></li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $config['num_links']        = 3;

        $this->pagination->initialize($config);
        $page   = $this->uri->segment(3);
        $start  = ($page - 1) * $config['per_page']; 

        $queryData = array(
                            'limit'     => $config["per_page"],
                            'start'     => $start,
                            'marca'     => $marca,
                            'familia'   => $familia, 
                            'atributo'  => $atributo, 
                            'vista'     => $shop_layout, 
                            'search'    => $search
                        );

        $data = $this->Producto_model->fetch_data( $queryData );

        foreach ($data as &$item) 
        {
            $item["imagen"] = $this->build_src($item["imagen"]);
        }

        $output = array(
                    'pagination_link' => $this->pagination->create_links(),
                    'product_list'    => $data,
                    'query'           => $this->db->last_query(),
                  );
        
        echo json_encode($output);

    }
    
    public function list_attr( $product_id ){

        $atributo = $this->db->get_where( 'producto_atributo', [ 'idProducto' => $product_id ] )->result_array();

        echo json_encode( $atributo ); 

    }

}
?>