<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Order extends BaseController{
    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->helper('price');
            $this->load->model('Order_hdr_model');
            $this->load->model('Order_itm_model');
            $this->load->model('Producto_model');
            $this->load->model('Clientes_model');

            $this->user = $this->dataUser();

        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of order
     */
    function index(){
        $data['_view'] = 'order/index';
        $this->load->view('layouts/main',$data);

    } 

    public function orderDetail($idOrderHdr){
        
        $data['_view'] = 'order/detail';
        $data['order'] = $idOrderHdr;
        
        $this->load->view('layouts/main',$data);   
    }

    public function getOrderDetail( $idOrderHdr ){

        //Paginado
        $start = $this->input->post('start');

        //Ver
        $length = $this->input->post("length");
        
        //Busqueda
        $search = $this->input->post("search")['value'];
 
        $result = $this->Order_itm_model->getItemsByOrderDT( $idOrderHdr, $start, $length, $search);
        
        $resultado = $result["datos"];

        $numrows   = $result["numRows"];
        $numrowstotal = $this->Order_itm_model->getNumRowAllDT( $idOrderHdr ); 

        $data = array();
        
        foreach ($resultado as $result_row) {
            
            $product = $this->Producto_model->get_producto( $result_row["idProducto"] );
            $row_of_data = array(); 
 
            // $row_of_data[] = $result_row["idOrderHdr"];
            $row_of_data[] = $result_row["posnr"];
            $row_of_data[] = $product['nombre'];
            $row_of_data[] = $result_row["precio"];
            $row_of_data[] = $result_row["qty"];
            $row_of_data[] = $result_row["total"];
            $row_of_data[] = $result_row["options"];
            $row_of_data[] = $result_row["idItemStatus"];
            $row_of_data[] = $result_row["createdBy"];
            $row_of_data[] = $result_row["createdDate"];
            $row_of_data[] = $result_row["updatedBy"];
            $row_of_data[] = $result_row["updatedDate"];
            $row_of_data[] = '<a href="#" data-id="'.$result_row["idOrderItm"].'" data-toggle="modal" data-target="#edit-item" class="btn btn-info btn-xs"><span class="fa fa-pencil" ></span> Editar</a> 
                              <a href="#" data-id="'.$result_row["idOrderItm"].'" class="btn btn-danger btn-xs delete-item"><span class="fa fa-trash"></span> Borrar</a>';
                       
            $data[] = $row_of_data;

        }
        
        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($numrowstotal),
            "recordsFiltered" => intval($numrows),
            "data"            => $data,
            "last_query"      => $this->db->last_query()
        );

        echo json_encode($json_data);  

    }
    
    public function get_order_json( $idOrderHdr ){

        $order = $this->Order_hdr_model->getById( $idOrderHdr );
        $details = $this->Order_itm_model->getItemsByOrder( $idOrderHdr );
        $client = $this->Clientes_model->get_cliente( $order['idCliente'] );

        $items = array();

        foreach ($details as $o => $data) {

            $product = $this->Producto_model->get_producto( $data->idProducto ); 

            $items[] = array(  
                                'posnr' => $data->posnr,
                                'producto' => $product, 
                                'qty' => $data->qty,
                                'total' => $data->total,
                                'options' => $data->options,
                                'idItemStatus' => $data->idItemStatus,
                                'createdBy' => $data->createdBy,
                                'createdDate' => $data->createdDate,
                                'updatedBy' => $data->updatedBy,
                                'updatedDate' => $data->updatedDate,
                        );
        }

        echo json_encode( [ 'order' => $order, 'details' => $items, 'client' => $client ] );

    }

    public function delete_order_item( $idOrderHdr, $idOrderItm ){

        $delete = $this->Order_itm_model->delete_order_item( $idOrderItm );

        if ( $delete ) {
            $return = json_encode( [ 'code' => 1, 'message' => 'Item eliminado correctamente.'] );
        }else{
            $return = json_encode( [ 'code' => 0, 'message' => 'Hubo un error y no se pudo eliminar el item.'] );
        }

        echo $return;

    } 

    

    //get_orders_by_client_pending

}
