<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cli extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        // Asegurarse de que solo se pueda acceder desde la línea de comandos
        if (!is_cli()) {
            show_error("Este controlador solo puede ser accedido desde la línea de comandos.");
            exit(1);
        }
        $this->load->library('ImportProducts');
    }

    public function update($type = null) {
        if (!in_array($type, ['stock', 'base_price', 'public_price'])) {
            echo "Tipo de actualización inválido. Use: stock, base_price, o public_price\n";
            return;
        }

        log_message('info', "Iniciando actualización de {$type}...");

        $method = "update_{$type}";
        $result = $this->importproducts->$method();
        
        log_message('info', "Actualización de {$type} completada.");
        log_message('info', "Resultado: " . json_encode($result));

        
    }
}