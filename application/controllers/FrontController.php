<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class FrontController extends BaseController{

    // private $cart;
    
	function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Familium_model');
            $this->load->model('Marca_model');
            $this->load->model('Producto_model');
            $this->load->model('Atributo_model');
            $this->load->helper("globalFunctions");
            $this->load->model('ProductoCategoria_model');
            $this->load->model('ProductoAtributo_model');
            $this->load->model('ProductoAplicacion_model');
            $this->load->model('moto_model');   
            $this->load->model('manager/Sysoptions_model', 'Sysoptions', true);
            $this->load->model('Order_itm_model');
            $this->load->model('manager/Promotions_model', 'Promotions', true);
            $this->load->model('manager/Slider_model', 'Slider', true);
            $this->load->model('manager/Brands_model', 'Brands', true);
            $this->load->model('manager/Carrousel_model', 'Carrousel', true);
        }else{
            redirect('/', 'refresh');
        }

    }

    public function index(){
    	$this->data['_view']  = 'front/home';
        
        $this->data['user']  = $this->dataUser();
        $this->data['nav']   = menuFront();
        $this->data['slider'] = $this->Slider->getSlider();
        $this->data['brands'] = $this->Brands->getBrands();
        $this->data['carrouselList'] = $this->Carrousel->getActives();

        foreach ($this->data['carrouselList'] as $carrousel) {
            $this->data['carrouselData'][$carrousel['id']] = $this->Carrousel->getProductsbyCarrousel($carrousel['id']);

            foreach ($this->data['carrouselData'][$carrousel['id']] as &$item) 
            {
                $item["imagen"] = $this->build_src($item["imagen"]);
            }
        }
        
        
        $this->load->view('front/front',$this->data);
    }


    public function detalle($strProduct){

        $script = [
            'assets/front/js/product-single.js?v='.time(),
        ];

        $css = [
            'assets/front/css/product-single.css?v='.time(),
        ];

    	$data['products'] = $this->Producto_model->get_product_by_str($strProduct);
    	$data['product'] = (is_array($data['products']))?$data['products'][0]: false;
       
        if (!(isset($data['product']) && $data['product'] != '')) {
            redirect($this->inicio, 'refresh');
       
        }else{
            
            if(!isset($data['product']['idProducto'])){
                redirect($this->inicio, 'refresh');
            
            }else{
                $idProducto = $data['product']['idProducto'];
                
                $data['product']['cats'] = $this->ProductoCategoria_model->get_cats_by_idProduct($idProducto);
                
                $data['product']['atributos'] = $this->ProductoAtributo_model->get_all_atributo_by_product($idProducto);
                
                if($data['product']['idTipo'] == 1){

                    $data['product']['atr_value_list']  = $this->Atributo_model->get_atributo_for_prod_var($idProducto);
                    $data['product']['test_atributos']  = $this->Atributo_model->get_atributo_for_variables($idProducto);

                    echo "<pre>";
                    print_r($data['product']['test_atributos']);
                    echo "<br>";
                    print_r($data['product']['atr_value_list']);
                    echo "</pre>";
                    
                }


                $data['_view']   = 'front/detail';
                $data['user']    = $this->dataUser();
                $data['nav']     = $this->menuFront();
                $data['scripts'] = $script;
                $data['csss']    = $css;

                $this->load->view('front/productos/index', $data);
                
            }
        }
    }

    public function menuFront(){
        $this->load->model('Categorium_model');
        $this->load->model('Familium_model');
        $this->load->model('Marca_model');
        $data['nav_cats'] = $this->Categorium_model->get_all_categoria_join();
        $data['nav_family'] = $this->Familium_model->get_all_familia();
        $data['nav_marcas'] = $this->Marca_model->get_all_marca();
        return $data;
    }

    function set_product_filter(){
        $this->session->userdata();
        foreach ($_POST as $key => $value) {
            if($key == "mostrar18")
                $this->session->set_userdata('mostrar', '18');
            if($key == "mostrar9")
                $this->session->set_userdata('mostrar', '9');
            if($key == "mostrar6")
                $this->session->set_userdata('mostrar', '6');
            if($key == "ordasc")
                $this->session->set_userdata('ord', 'asc');
            if($key == "orddesc")
                $this->session->set_userdata('ord', 'desc');
            if($key == "preciomin")
                $this->session->set_userdata('preciomin', $_POST['preciomin']);
            if($key == "preciomax")
                $this->session->set_userdata('preciomax', $_POST['preciomax']);
        }
    }

    public function productos($fam=null,$marca=null){

        $this->load->model('Producto_model');

        $data['_view'] = 'front/home';
        $data['user'] = 'Invitado';
        $data['nav'] = $this->menuFront();

        $data['title'] = 'Productos según categoría';
        $data['fam'] = $fam;
        if($marca && !is_numeric($marca))
        $data['marca'] = $marca;

        // $base = 'http://monsa-dev.onmedia.com.ar/frontController/productos/';
        $base = base_url() . 'frontController/productos';
    
        if ($fam  && !is_numeric($fam)){ 
            $base .= "/".$fam;
        }
        if ($marca && !is_numeric($marca)){
            $base .= "/".$marca;
        }
        $config['total_rows'] = count($this->Producto_model->get_all_producto_count($fam,$marca)); 
        if($this->session->userdata('mostrar'))
           $config['per_page'] = $this->session->userdata('mostrar');
        else
            $config['per_page'] = 18;
        include('partials/pagination.php');
        
        $data['products']      = $this->Producto_model->get_all_producto($page,$config["per_page"],$fam,$marca);
        $data['marcas']        = $this->Producto_model->get_all_producto(null,null,$fam,$marca);
        $data["featured_prod"] = $this->Producto_model->get_featured_prod();
        $data['total_rows']    = $config['total_rows'];

        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

        $this->mybreadcrumb->add('Productos', base_url('frontController/productos'));
        if($fam)
            $this->mybreadcrumb->add($fam, base_url('frontController/productos'.'/'.$fam));
        if($marca && !is_numeric($marca))
            $this->mybreadcrumb->add($marca, base_url('frontController/productos'.'/'.$fam.'/'.$marca));

            $data['breadcrumbs'] = $this->mybreadcrumb->render();

            $this->load->view('front/productos',$data);
    }

    public function productoslista($fam=null,$marca=null){

        $this->load->model('Producto_model');

        $data['_view'] = 'front/home';
        $data['user'] = 'Invitado';
        $data['nav'] = $this->menuFront();

        $data['title'] = 'Productos según categoría';


        $data['fam'] = $fam;
        if($marca && !is_numeric($marca))
            $data['marca'] = $marca;
        
        $base = base_url() . 'frontController/productoslista/';        
        // $base = 'http://monsa-dev.onmedia.com.ar/frontController/productoslista/';
        
        if ($fam  && !is_numeric($fam)){ 
            $base .= "/".$fam;
        }
        if ($marca && !is_numeric($marca)){
            $base .= "/".$marca;
        }
        $config['total_rows'] = count($this->Producto_model->get_all_producto_count($fam,$marca)); 
        if($this->session->userdata('mostrar'))
           $config['per_page'] = $this->session->userdata('mostrar');
        else
            $config['per_page'] = 4;
        include('partials/pagination.php');
        
        $data['products'] = $this->Producto_model->get_all_producto($page,$config["per_page"],$fam,$marca);
        $data['marcas'] = $this->Producto_model->get_all_producto(null,null,$fam,$marca);
        $data['total_rows'] = $config['total_rows'];

        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

            $this->mybreadcrumb->add('Productos', base_url('frontController/productos'));
            if($fam)
                $this->mybreadcrumb->add($fam, base_url('frontController/productos'.'/'.$fam));
            if($marca && !is_numeric($marca))
                $this->mybreadcrumb->add($marca, base_url('frontController/productos'.'/'.$fam.'/'.$marca));

                $data['breadcrumbs'] = $this->mybreadcrumb->render();

                $this->load->view('front/productoslista',$data);
    }

    public function producto(){
        $this->load->model('Producto_model');
        $data['products'] = $this->Producto_model->get_all_producto();
        $data['_view'] = 'front/home';
        $data['user'] = 'Invitado';
        $data['nav'] = $this->menuFront();
        $data['title'] = 'Detalle del producto';

        $this->load->view('front/producto',$data);
    }

    public function marcas($marca=null,$fam=null){

        $this->load->model('Producto_model');

        $data['_view'] = 'front/home';
        $data['user'] = 'Invitado';
        $data['nav'] = $this->menuFront();
        $data['title'] = 'Productos según marcas';
        $data['fam'] = $fam;
        if($marca && !is_numeric($marca))
            $data['marca'] = $marca;
        $base = base_url() . 'frontController/marcas/';
        // $base = 'http://monsa-dev.onmedia.com.ar/frontController/marcas/';
        if ($fam  && !is_numeric($fam)){ 
            $base .= "/".$fam;
        }
        if ($marca && !is_numeric($marca)){
            $base .= "/".$marca;
        }
        $config['total_rows'] = count($this->Producto_model->get_all_producto_count($fam,$marca)); 
        if($this->session->userdata('mostrar'))
            $config['per_page'] = $this->session->userdata('mostrar');
        else
            $config['per_page'] = 18;
        include('partials/pagination.php');
        $data['products'] = $this->Producto_model->get_all_producto($page,$config["per_page"],$fam,$marca);
        $data['marcas'] = $this->Producto_model->get_all_producto(null,null,$fam,$marca);
        $data['total_rows'] = $config['total_rows'];

        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();

        $this->mybreadcrumb->add('Productos', base_url('frontController/productos'));
        if($fam)
            $this->mybreadcrumb->add($fam, base_url('frontController/productos'.'/'.$fam));
        if($marca && !is_numeric($marca))
            $this->mybreadcrumb->add($marca, base_url('frontController/productos'.'/'.$fam.'/'.$marca));

            $data['breadcrumbs'] = $this->mybreadcrumb->render();

            $this->load->view('front/productos',$data);
    }    

    public function shop()
    {
        $script = [
            'assets/front/js/shop.js?v='.time(),
        ];

        $css = [
            'assets/front/css/shop.css?v='.time(),
        ];

        $data['user']         = $this->dataUser();
        $data['marca_data']   = $this->Marca_model->get_all_marca(); 
        // Ordenar por orden
        usort($data['marca_data'], function($a, $b){ 
            return strnatcmp($a['orden'], $b['orden']);
        });

        $data['family_data']  = $this->Familium_model->get_all_familia();
        // Ordenar por orden
        usort($data['family_data'], function($a, $b){ 
            return strnatcmp($a['orden'], $b['orden']);
        });

        $data['children_atributos'] = array();

        foreach( $this->Familium_model->get_all_familia() as $familia ) {

            $f = $familia['idFamilia'];

            $data['children_atributos'][$f] = $this->Atributo_model->get_all_atributo_by_family( $f );

        }
        //print_r($data['children_atributos']);exit();
        $message = $this->Sysoptions->getShopMsj();
        $data['flash_message'] = 
        [
            'titulo' => $message['shop_msj_title'],
            'detail' => $message['shop_msj_detail']
        ];

        $data['nav']          = menuFront();
        $data['title']        = "Tienda"; 
        $data['scripts']      = $script;
        $data['csss']      = $css;

        $this->load->view('front/shop/index', $data);
    }
 
//  Fn para mostrar la vista Carrito
    
    function carrito()
    {

        $script = [
            'assets/front/js/cartlist.js?v='.time(),
        ];

        $data['user']         = $this->dataUser();
        $data['nav']          = menuFront();
        $data['title']        = "Carrito";
        $data['idOrderHdr']   = $this->session->userdata("idOrderHdr");
        $data['scripts'] = $script;

        $this->load->view('front/cart/index', $data);
    }

    function customer(){

        $data['nav']          = menuFront();
        $data['user']         = $this->dataUser();
        $data['title']        = "Mi Cuenta";
        $data['_view']        = 'front/customer/orders_list';

        $this->load->view('front/customer/index', $data);

    }

    function descargas(){

        $data['nav']          = menuFront();
        $data['user']         = $this->dataUser();
        $data['title']        = "Descargas";

        $data['promotions']   = $this->Promotions->getAll();

        // $products = $this->Producto_model->get_products_promotions();
        // foreach ($products as &$item) 
        // {   
        //     $item["imagen"] = $this->build_src($item["imagen"]);
        // }

        // $data['products']     = $products;
        
        // var_dump($data['products']);die();

        $this->load->view('front/descargas/index', $data);

    }    
    function promociones(){

        $data['nav']          = menuFront();
        $data['user']         = $this->dataUser();
        $data['title']        = "Promociones";

        $data['promotions']   = $this->Promotions->getAll();

        $this->load->view('front/promociones/index', $data);

    }    

    function getProdDestacados(){
        $productos = $this->Producto_model->get_products_destacado();
        foreach ($productos as &$item) 
        {
            $item["imagen"] = $this->build_src($item["imagen"]);
        }
        echo json_encode($productos);
    }
    
    function get_whatsapp_config(){

        $r = $this->Sysoptions->getWhatsappConfig();
        
        echo json_encode( ['code' => 200, 'data' => $r ] );

    }

    function getCarrouselData(){
        $carrouselData = $this->Carrousel->getProductsCarrousel();
        foreach ($carrouselData as &$item) 
        {
            $item["imagen"] = $this->build_src($item["imagen"]);
        }
        return $carrouselData;
    }

}