<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Home extends BaseController{

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Producto_model');
            $this->load->helper("globalFunctions");

        }else{
            redirect('/', 'refresh');
        }
    }

    function getProductosHome(){

        $productos = $this->Producto_model->get_products_home();
        foreach ($productos as &$item) 
        {
            $item["imagen"] = $this->build_src($item["imagen"]);
        }
        echo json_encode($productos);

    }

    function getProductosMasVendidos(){

        $productos = $this->Producto_model->get_products_masvendidos();

        foreach ($productos as &$item) 
        {
            $item["imagen"] = $this->build_src($item["imagen"]);
        }
        echo json_encode($productos);

    }
}

