<script>
	var fillSelect = (attr, target, url) => {
		$.get( url)
		 .done( data => {
		 	target.html('');
		 	var option = $('<option>',  {
				value : 0, 
				text  : 'Seleccione ' + attr 
			});
			target.append(option);
			for (var i = 0; i < data[attr].length; i++) {
				option = $('<option>',  {
					value : data[attr][i].idFamilia, 
					text  : data[attr][i].nombre 
				});
				target.append(option);
				target.select2({placeholder: 'Seleccione ...', allowClear: true});
			}
		}).error( err => {
			let option = $('<option>',  {
				value : 0, 
				text  : 'Error al cargar ' + attr
			});
			target.append(option);
			target.select2({placeholder: 'Error al cargar',allowClear: true});
		})
	}

	var firstUpper = (string) =>{
		return string.charAt(0).toUpperCase() + string.slice(1);
	}
</script>
