<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Uploads extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Marca_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of marca
     */
    function index()
    {

        $this->load->helper('directory');
        $this->load->helper('file');
        $this->load->model('Producto_model');
        $this->load->model('Familium_model');

        $map = directory_map('./resources/img/auto-uploads');  
        $folders = array_keys($map); 

        $valid = ["png", "bmp", "jpg", "jpeg", "gif"]; 
        $this->session->set_flashdata('info_message', 'Para subir imagenes masivamente, debe ubicar las carpetas que desee en <pre style="display: inline;">' . FCPATH . 'resources/img/auto-uploads</pre> y el formulario buscara y mostrara las carpetas agregadas en el campo "Carpetas".');

        if ( isset($_POST['sku']) ) { 

            $length = $_POST['sku_lenght'];
            $updated = 0;
            $errors = 0; 

            ob_start();

            foreach ( $_POST['folders'] as $folder ) { 

                foreach ( $map[$folder] as $file ) {  

                    if ( in_array( pathinfo( $file, PATHINFO_EXTENSION ), $valid ) ) { 

                        $sku = explode( '.', $file );
                        $sku = $sku[0]; 
                        $newSKU = substr($sku, 0, $length);

                        if ( $this->Producto_model->get_producto_by_sku( $newSKU, 'num_rows' ) >= 1 ) {

                            $producto = $this->Producto_model->get_producto_by_sku( $newSKU, 'row_array' );
                            $familia = $this->Familium_model->get_familium( $producto['idFamilia'] ); 
                            $currentPath = FCPATH . 'resources/img/auto-uploads/' . substr($folder, 0, -1) . '/' . $file;
                            $familiaFolder = ( $familia['slug'] !== '' ) ? $familia['slug'] : 'sin_familia' ;
                            
                            $newPath = FCPATH . 'resources/img/uploads/' . $familiaFolder . '/' . $this->rename_img( $file );
                            $savePath = 'resources/img/uploads/' . $familiaFolder . '/' . $this->rename_img( $file );

                            $currentImages = json_decode( $producto['imagen'] );
                            $currentImages[] = $savePath;

                            $params = array(
                                            'imagen' => json_encode( $currentImages ),
                                        );

                            if ( file_exists( $currentPath ) ) {

                                if ( ! file_exists( dirname( $newPath ) ) ) {
                                    mkdir($newPath, 0777, true);
                                }

                                $rename = rename( $currentPath, $newPath );

                                if ( $rename ) {

                                    // Update
                                    $this->db->where( 'sku', $newSKU );
                                    $this->db->update( 'producto', $params );

                                    if ( $this->db->affected_rows() >= 1 ) { 
                                        $updated++;
                                        ?>
                                        <pre style="background: #2ecc71; border-color: #27ae60;">
                                            Status: <?= $file .' - <b style="color: green;">OK</b>' ?><br>
                                            ID: <?= $producto['idProducto'] ?><br>
                                            SKU: <?= $newSKU ?><br>
                                            Value: <?= json_encode( $currentImages ) ?><br>  
                                            Path: <?= $newPath ?>
                                        </pre>
                                        <?php  
                                    }else{
                                        $errors++;
                                        ?>
                                        <pre>
                                            Status: <?= $file .' - NO SE PUDO ACTUALIZAR LA BD' ?><br>
                                            ID: <?= $producto['idProducto'] ?><br>
                                            SKU: <?= $newSKU ?><br> 
                                            Path: <?= $newPath ?>
                                        </pre>
                                        <?php  
                                    }

                                }else{
                                    $errors++;
                                    ?>
                                    <pre>
                                        Status: <?= $file .' No se pudo reubicar <b style="color: red;">ERROR</b>' ?><br>
                                        ID: <?= $producto['idProducto'] ?><br>
                                        SKU: <?= $newSKU ?><br> 
                                        Path: <?= $newPath ?>
                                    </pre>
                                    <?php  
                                }

                            }else{
                                $errors++;
                                ?>
                                <pre style="background: #e74c3c; border-color: #c0392b;">
                                    Status: <?= $file . ' no existe <br>' ?> 
                                    SKU: <?= $newSKU ?><br>
                                </pre>
                                <?php  
                            }

                        }else{
                            $errors++;
                            ?>
                            <pre style="background: #e74c3c; border-color: #c0392b;">
                                Status: <?= $file . ' - Sin SKU <br>' ?> 
                                SKU: <?= $newSKU ?><br>
                            </pre>
                            <?php 
                        } 

                    } 

                }

            }

            $data['output'] = ob_get_clean();

            if ( $updated >= 1 ) {
                $this->session->set_flashdata('success_message', $updated . ' actualizados exitosamente!');
                if ( $errors >= 1 ) {
                    $this->session->set_flashdata('error_message', $errors . ' productos no pudieron ser actualizados');
                }
            }else{
                $this->session->set_flashdata('error_message', 'No se pudo actualizar ningun producto, revise el log para mas detalle'); 
            } 

        } 

        elseif ( isset($_POST['slug']) ) {

            $updated = 0;
            $errors = 0; 

            ob_start();

            foreach ( $_POST['folders'] as $folder ) { 

                foreach ( $map[$folder] as $file ) {  

                    if ( in_array( pathinfo( $file, PATHINFO_EXTENSION ), $valid ) ) {

                        $slug = explode( '.', $file );
                        $slug = $slug[0];  

                        if ( $this->Producto_model->get_product_by_slug( $slug, 'num_rows' ) >= 1 ) {

                            $producto = $this->Producto_model->get_product_by_slug( $slug, 'row_array' );
                            $familia = $this->Familium_model->get_familium( $producto['idFamilia'] ); 
                            $currentPath = FCPATH . 'resources/img/auto-uploads/' . substr($folder, 0, -1) . '/' . $file;
                            $familiaFolder = ( $familia['slug'] !== '' ) ? $familia['slug'] : 'sin_familia' ;
                            
                            $newPath = FCPATH . 'resources/img/uploads/' . $familiaFolder . '/' . $this->rename_img( $file );
                            $savePath = 'resources/img/uploads/' . $familiaFolder . '/' . $this->rename_img( $file );

                            $currentImages = json_decode( $producto['imagen'] );
                            $currentImages[] = $savePath;

                            $params = array(
                                            'imagen' => json_encode( $currentImages ),
                                        );

                            if ( file_exists( $currentPath ) ) {

                                if ( ! file_exists( dirname( $newPath ) ) ) {
                                    mkdir($newPath, 0777, true);
                                }

                                $rename = rename( $currentPath, $newPath );

                                if ( $rename ) {

                                    // Update
                                    $this->db->where( 'idProducto', $producto['idProducto'] );
                                    // $this->db->where( 'idTipo', 1 );
                                    $this->db->update( 'producto', $params );

                                    if ( $this->db->affected_rows() >= 1 ) { 
                                        $updated++;
                                        ?>
                                        <pre style="background: #2ecc71; border-color: #27ae60;">
                                            Status: <?= $file .' - <b style="color: green;">OK</b>' ?><br>
                                            ID: <?= $producto['idProducto'] ?><br>
                                            Value: <?= json_encode( $currentImages ) ?><br>  
                                            Path: <?= $newPath ?>
                                        </pre>
                                        <?php  
                                    }else{
                                        $errors++;
                                        ?>
                                        <pre>
                                            Status: <?= $file .' - NO SE PUDO ACTUALIZAR LA BD' ?><br>
                                            ID: <?= $producto['idProducto'] ?><br>
                                            Path: <?= $newPath ?>
                                        </pre>
                                        <?php  
                                    }

                                }else{
                                    $errors++;
                                    ?>
                                    <pre>
                                        Status: <?= $file .' No se pudo reubicar <b style="color: red;">ERROR</b>' ?><br>
                                        ID: <?= $producto['idProducto'] ?><br>
                                        Path: <?= $newPath ?>
                                    </pre>
                                    <?php  
                                }

                            }else{
                                $errors++;
                                ?>
                                <pre style="background: #e74c3c; border-color: #c0392b;">
                                    Status: <?= $file . ' no existe <br>' ?> 
                                </pre>
                                <?php  
                            }

                        }else{
                            $errors++;
                            ?>
                            <pre style="background: #e74c3c; border-color: #c0392b;">
                                Status: <?= $file . ' - Sin SKU <br>' ?> 
                            </pre>
                            <?php 
                        }

                    } 

                }

            }

            $data['output'] = ob_get_clean();

            if ( $updated >= 1 ) {
                $this->session->set_flashdata('success_message', $updated . ' actualizados exitosamente!');
                if ( $errors >= 1 ) {
                    $this->session->set_flashdata('error_message', $errors . ' productos no pudieron ser actualizados');
                }
            }else{
                $this->session->set_flashdata('error_message', 'No se pudo actualizar ningun producto, revise el log para mas detalle'); 
            } 

        }  

        $data['folders'] = $folders; 
        $data['_view'] = 'uploads/index'; 
        $this->load->view('layouts/main', $data);

        // Limpiar mensajes
        $this->session->set_flashdata('success_message', null);
        $this->session->set_flashdata('error_message', null);

    }   

    function rename_img( $filename ){

        $x = explode('.', $filename );
        $x = array_reverse( $x );

        $ext = $x[0];
        unset( $x[0] );

        $x = array_reverse( $x );

        if ( count($x) ) {
            $str = implode( '-' , $x );
        }else{
            $str = $x[0];
        }

        $str = str_replace( ' ', '-', $str );
        $str = str_replace( '(', '', $str );
        $str = str_replace( ')', '', $str );

        return $str . '.' . $ext;

    } 

}
