<?php
include_once(FCPATH."/application/controllers/BaseController.php");
 
class Familium extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();

        if ($this->is_monsa_login()) {
            $this->load->model('Familium_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of familia
     */
    function index()
    {
        $data['familia'] = $this->Familium_model->get_all_familia();

        // sort by priority
        usort($data['familia'], function($a, $b){ 
            return strnatcmp($a['orden'], $b['orden']);
        });

        $data['user'] = $this->user;
        $data['_view'] = 'familium/index';
        $this->load->view('layouts/main',$data);
    }
    /*
    * Functions Package
    */
    use FamilyFuntions;
}

trait FamilyFuntions {
    /*
     * Adding a new familium
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
                'nombre' => $this->input->post('nombre'),
                'slug' => $this->input->post('slug'),
            );
            
            $familium_id = $this->Familium_model->add_familium($params);
            redirect('familium/index');
        }
        else
        {            
            $data['_view'] = 'familium/add';
            $data['user'] = $this->user;
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a familium
     */
    function edit($idFamilia)
    {   
        // check if the familium exists before trying to edit it
        $data['familium'] = $this->Familium_model->get_familium($idFamilia);
        
        if(isset($data['familium']['idFamilia']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
                    'nombre' => $this->input->post('nombre'),
                    'slug' => $this->input->post('slug'),
                );

                $this->Familium_model->update_familium($idFamilia,$params);            
                redirect('familium/index');
            }
            else
            {
                $data['_view'] = 'familium/edit';
                $data['user'] = $this->user;
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The familium you are trying to edit does not exist.');
    } 

    /*
     * Deleting familium
     */
    function remove($idFamilia)
    {
        $familium = $this->Familium_model->get_familium($idFamilia);

        // check if the familium exists before trying to delete it
        if(isset($familium['idFamilia']))
        {
            $this->Familium_model->delete_familium($idFamilia);
            redirect('familium/index');
        }
        else
            show_error('The familium you are trying to delete does not exist.');
    }

    function getFamiliaJson()
    {
        $data['familia'] = $this->Familium_model->get_all_familia();
        return $this->json_ouput($data);
    }


    function update_sort(){ 
 
        $data = json_decode( $_POST['json'], true );

        $updateOld = $this->Familium_model->update_sort( $data ); 

        if ( $this->db->affected_rows() >= 1 ) {
            echo json_encode( [ 'code' => 1, 'message' => 'Orden actualizada exitosamente' ] );
        }else{
            echo json_encode( [ 'code' => 0, 'message' => 'No se pudo actualizar el orden' ] );
        }

    }

}
