<?php
 
class Login_error extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Login_error_model');
    } 

    /*
     * Listing of login_errors
     */
    function index()
    {
        $data['login_errors'] = $this->Login_error_model->get_all_login_errors();
        
        $data['_view'] = 'login_error/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new login_error
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'username_or_email' => $this->input->post('username_or_email'),
				'ip_address' => $this->input->post('ip_address'),
				'time' => $this->input->post('time'),
            );
            
            $login_error_id = $this->Login_error_model->add_login_error($params);
            redirect('login_error/index');
        }
        else
        {            
            $data['_view'] = 'login_error/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a login_error
     */
    function edit($ai)
    {   
        // check if the login_error exists before trying to edit it
        $data['login_error'] = $this->Login_error_model->get_login_error($ai);
        
        if(isset($data['login_error']['ai']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'username_or_email' => $this->input->post('username_or_email'),
					'ip_address' => $this->input->post('ip_address'),
					'time' => $this->input->post('time'),
                );

                $this->Login_error_model->update_login_error($ai,$params);            
                redirect('login_error/index');
            }
            else
            {
                $data['_view'] = 'login_error/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The login_error you are trying to edit does not exist.');
    } 

    /*
     * Deleting login_error
     */
    function remove($ai)
    {
        $login_error = $this->Login_error_model->get_login_error($ai);

        // check if the login_error exists before trying to delete it
        if(isset($login_error['ai']))
        {
            $this->Login_error_model->delete_login_error($ai);
            redirect('login_error/index');
        }
        else
            show_error('The login_error you are trying to delete does not exist.');
    }
    
}
