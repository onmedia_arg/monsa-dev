<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Descargas extends BaseController{

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('manager/Downloads_model', 'Downloads');
            $this->load->model('manager/Sysoptions_model', 'Sysoptions', true);       
            $this->load->model('manager/Promotions_model', 'Promotions', true);
            $this->load->model('Producto_model', 'Producto');
            $this->load->helper("globalFunctions");
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    }

    public function get_content(){

		$files = $this->Downloads->getAll();
        $msj   = $this->db->get_where('system_options', array('opt_name' => 'home_dwnl_msj'))->row();
        $promotions = $this->Promotions->getAll();

        $products = $this->Producto->get_products_promotions();
        foreach ($products as &$item) 
        {   
            $item["imagen"] = $this->build_src($item["imagen"]);
        }

		echo json_encode( [ 'status' => 200, 'files' => $files, "msj" => $msj, "promotions" => $promotions, "products" => $products ] );

	}


    public function get_products_promotions(){

        $products = $this->Producto->get_products_promotions();
        foreach ($products as &$item) 
        {   
            $item["imagen"] = $this->build_src($item["imagen"]);
        }

        // Obtener las marcas únicas de los productos
        $brands = array_unique(array_column($products, 'idMarca', 'marca'));

        // Obtener las marcas únicas de los productos
        $familia = array_unique(array_column($products, 'idFamilia', 'familia'));

        // $brands = $products->groupBy('idMarca');
        // $brands = $brands->toArray();
        // var_dump($familia);

        echo json_encode( [ 'status' => 200, 
                                   'products' => $products,
                                   'brands'   => $brands,
                                   'familia'  => $familia ] );
    }

}
