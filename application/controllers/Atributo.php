<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Atributo extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Atributo_model');
            $this->load->model('manager/Productosingle_model', 'Productsingle', true);
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of atributo
     */
    function index()
    {
        $data['atributo'] = $this->Atributo_model->get_all_atributo_join();
        $data['user'] = $this->user;
        $data['_view'] = 'atributo/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new atributo
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'idfamilia' => $this->input->post('idfamilia'),
                'nombre' => $this->input->post('nombre'),
				'slug' => $this->input->post('slug'),
				'valor' => json_encode($this->input->post('valor')),
            );
            $atributo_id = $this->Atributo_model->add_atributo($params);
            redirect('atributo/index');
        }
        else
        {      
            $this->load->model('Familium_model');
            $data['all_familia'] = $this->Familium_model->get_all_familia();
            $data['user'] = $this->user;      
            $data['_view'] = 'atributo/add';

            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a atributo
     */
    function edit($idAtributo)
    {   
        // check if the atributo exists before trying to edit it
        $data['atributo'] = $this->Atributo_model->get_atributo($idAtributo);
        if(isset($data['atributo']['idAtributo']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $filtered_values = array_filter($this->input->post('valor'), "strlen");
                $params = array(
                    'idfamilia' => $this->input->post('idfamilia'),
                    'nombre' => $this->input->post('nombre'),
                    'valor' => json_encode($filtered_values),
                    'slug' => $this->input->post('slug'),
                );

                $this->Atributo_model->update_atributo($idAtributo,$params);            
                redirect('atributo/index');
            }
            else
            {
                $this->load->model('Familium_model');
                $data['all_familia'] = $this->Familium_model->get_all_familia();
                $data['user'] = $this->user; 
                $data['_view'] = 'atributo/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The atributo you are trying to edit does not exist.');
    } 

    /*
     * Deleting atributo
     */
    function remove($idAtributo)
    {
        $atributo = $this->Atributo_model->get_atributo($idAtributo);

        // check if the atributo exists before trying to delete it
        if(isset($atributo['idAtributo']))
        {
            $this->Atributo_model->delete_atributo($idAtributo);
            redirect('atributo/index');
        }
        else
            show_error('The atributo you are trying to delete does not exist.');
    }

    function get_attr_by_family($idfamilia = '')
    {

        if ($idfamilia != '') {
            $data['atributo'] = $this->Atributo_model->get_all_atributo_by_family($idfamilia);
        }else{
            $data['atributo'] = $this->Atributo_model->get_all_atributo_join();
        }
        if (count($data['atributo']) > 0) {
            for ($i=0; $i < count($data['atributo']); $i++) { 
                $data['atributo'][$i]['valor'] = json_decode($data['atributo'][$i]['valor']);
            }
        }

        return $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));

        // $this->json_ouput($data);
    }

    function get_attr_for_family_all($str = NULL)
    {

        $data['atributo'] = $this->Atributo_model->get_all_atributo_join($str);

        if (count($data['atributo']) > 0) {
            for ($i=0; $i < count($data['atributo']); $i++) { 
                $data['atributo'][$i]['valor'] = json_decode($data['atributo'][$i]['valor']);
            }
        }
        return $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
        
        // return $this->json_ouput($data);
    }

    function get_all_attr()
    {

        $this->load->model('ProductoAtributo_model');
        $params = [ 'idAtributo' => $_REQUEST['idAtributo'], 'idProducto' => $_REQUEST['idProducto'] ];
        $atributos = $this->ProductoAtributo_model->get_all_producto_atributo( $params );
        $return = array(); 
 
        foreach ($atributos as $atributo) {
            $return[] = $atributo;
        }
 
        return $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));
        
        // return $this->json_ouput($data);
    }

    function nuevo_valor_atributo( $idAtributo = null, $idfamilia = null, $valor = null )
    {

        $idAtributo = ( isset( $_REQUEST['idAtributo'] ) ) ? $_REQUEST['idAtributo'] : $idAtributo ;
        $valor = ( isset( $_REQUEST['valor'] ) ) ? $_REQUEST['valor'] : $valor ;

        $json = $this->db->where( 'idAtributo', $idAtributo )
                         ->get( 'atributo' )
                         ->result_array();

        $json = $json[0]['valor'];

        if ( $json !== '' && $json !== null && $json !== 0 && $json !== '0' ) {
            $array = json_decode($json); 
        }else{
            $array = array();
        }

        array_push( $array, $valor );

        $update = $this->db->where( 'idAtributo', $idAtributo )
                           ->update( 'atributo', [ 'valor' => json_encode( $array ) ] );

        if ( $this->db->affected_rows() >= 1 ) {
            $return = [ 'code' => 1, 'message' => null ];
        }else{
            $return = [ 'code' => 0, 'message' => 'No se pudo actualizar la base de datos' ];
        }

        return $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));

    }

    /*SE UTILIZA PARA ARMAR EL CAMPO VALUES DE LA TABLA PRODUCTOS_ATRIBUTO PARA TODOS LOS PADRES, CON 
    LOS VALORES QUE TIENEN SUS HIJOS EN CADA UNO DE LOS ATRIBUTOS*/

    function cocinero()
    {

        // ini_set('max_execution_time', 0); 
        // ini_set('memory_limit','2048M');

        $parent_list = $this->db->query("SELECT * FROM `producto` WHERE `idTipo` = 1")->result_array();
        // var_dump('<pre>', $parent_list, "</pre>");

        $i = 0;        
        foreach ($parent_list as $parent) {
            
            $result = $this->Atributo_model->get_atributo_for_variables($parent["idProducto"]);
            $row = current($result);
            $valores = array();

            while( !empty($row)){
            
                $current_prod = $row["idParent"];
                $current_attr = $row["idAtributo"];

                // echo( "Producto: " . $parent["idProducto"] .  " Atributo:" . $current_attr );
                while( $row["idAtributo"] == $current_attr){
                    $valores[] = $row["valores"];
                    // echo( $row["valores"] . " - ");
                    $row = next( $result );
                }
                $valores_json = json_encode($valores);
                
                // $r =  $this->db->query("UPDATE producto_atributo SET valores = '" . $valores_json . " ', is_variacion = 1 WHERE idProducto = " . $current_prod . " and idAtributo =" . $current_attr . ";");
                
                // if ($this->db->affected_rows() > 0)
                // {
                //     echo "TRUE: ";                
                // }
                // else
                // {
                //     echo "FALSE: ";
                // }   

                echo ("UPDATE producto_atributo SET valores = '" . $valores_json . " ', is_variacion = 1 WHERE idProducto = " . $current_prod . " and idAtributo =" . $current_attr . ";");
                echo "<br>" ; 
                $valores = array();
            }
            echo "<br>" ; 

            $i ++;

         }

         echo $i;

        // for($i=0; $i<=10; $i++){
         
            // $idParent = 1599 + $i;

            // $result = $this->Atributo_model->get_atributo_for_variables($idParent);
            
            // var_dump('<pre>', $result, "</pre>");

            // $current_parent = $result[0]["idParent"];

            // $row = current($result);

            // while( !empty($row)){
            //     while( $row["idParent"] == $current_parent){
                    
            //         $current_attr = $row["idAtributo"];
            //         echo("\n Producto ID:". $current_parent. "\tAtributo:". $current_attr . " variacion:" . $row["is_variacion"]. "\tValores:");

            //         while( $row["idParent"] == $current_parent && $row["idAtributo"] == $current_attr){
            //               echo($row["valores"] . "; ");
            //               $row = next( $result );  
            //         }
            //         echo("\n siguiente atributo");
            //     }
            // }   echo("\nsiguiente producto");
            // var_dump('<pre>', $row, "</pre>");
        // }   

    }

    /*SE UTILIZA PARA CREAR EL CAMPO SEARCH CON MARCA FAMILIA Y VALORES DE SUS ATRIBUTOS */
    function cocinero2()
    {

        // ini_set('max_execution_time', 0); 
        // ini_set('memory_limit','2048M');


        $product_list = $this->db->query("SELECT pr.*, 
                                                 fa.nombre as familia_nombre,
                                                 ma.nombre as marca_nombre
                                           FROM `producto` as pr 
                                           left join familia as fa on fa.idfamilia = pr.idFamilia
                                           left join marca as ma on ma.idMarca = pr.idMarca
                                          WHERE `idTipo` != 1
                                          and search IS NULL")->result_array();


        // var_dump('<pre>', $product_list, "</pre>"); die;

        $i = 0;        
               
        foreach ($product_list as $product) {
            
 
            $r = $this->db->query("SELECT  pa.idAtributo, 
                                               at.nombre,
                                               at.slug,
                                               pa.idProducto, 
                                               pr.idParent, 
                                               pa.valores
                                      FROM `producto_atributo` AS pa 
                                     INNER JOIN producto AS pr ON pr.idProducto = pa.idProducto
                                     INNER JOIN atributo AS at ON at.idAtributo = pa.idAtributo
                                     WHERE pr.idProducto = " . $product["idProducto"] );

            $result  = $r->result_array();
            
            if(count($result) > 0){
                $row     = current($result);
                $valores = array();
               
                // echo( "Producto: " . $product["idProducto"]);
                
                $valores[] = $product["familia_nombre"];
                $valores[] = $product["marca_nombre"];
                
                while( !empty($row)){
                    $valores[] = $row["valores"];
                    $row = next( $result );
                }
                
                $valores_json = json_encode($valores);

                // var_dump('<pre>', $valores_json, "</pre>"); die;
                
                // echo ($valores_json . "<br>" ) ;
                echo ("UPDATE producto SET search = '" . $valores_json . " ' WHERE idProducto = " . $product["idProducto"] . ";</br>"); 
            }else{
                $valores = array();
                $valores[] = $product["familia_nombre"];
                $valores[] = $product["marca_nombre"];
                
                $valores_json = json_encode($valores);
                echo ("UPDATE producto SET search = '" . $valores_json . " ' WHERE idProducto = " . $product["idProducto"] . ";</br>"); 

            }

         }
    }

    /*SE UTILIZA PARA CREAR EL CAMPO SEARCH DE LOS PRODUCTOS PADRES, QUE TIENEN TODOS LOS VALORES DE LOS HIJOS */
    function cocinero3()
    {

        $parent_list = $this->db->query("SELECT pr.*, 
                                                                        fa.nombre as familia_nombre,
                                                                        ma.nombre as marca_nombre
                                                                FROM `producto` as pr 
                                                                left join familia as fa on fa.idfamilia = pr.idFamilia
                                                                left join marca as ma on ma.idMarca = pr.idMarca
                                                                WHERE `idTipo` = 1")->result_array();

        $i = 0;        
        foreach ($parent_list as $parent) {
            
            $result = $this->Atributo_model->get_atributo_for_variables($parent["idProducto"]);
            // $result = $this->Atributo_model->get_atributo_for_variables('1971');
            $row = current($result);
            $valores = array();
            // var_dump( "<pre>" , $result , "</pre>");

            $valores[] = $parent["familia_nombre"];
            $valores[] = $parent["marca_nombre"];
            while( !empty($row)){
                
                $valores[] = $row["valores"];
                $row = next( $result );
            
            }
            
            $children  = $this->db->get_where('producto',array('idParent'=>$parent["idProducto"]))->result_array();

            foreach($children as $child){
                
                $valores[] = $child["sku"];
            }

            $valores_json = json_encode($valores);
            
            echo ("UPDATE producto SET search = '" . $valores_json . " ' WHERE idProducto = " . $parent["idProducto"] . ";</br>"); 
 
            $i ++;
            // die();  //just for test
         }

         echo $i;

    }

    // SE ACTUALIZAN LOS VALORES DE LOS ATRIBUTOS EN LA TABLA ATRIBUTOS
    function cocinero4()
    {

        $r = $this->db->query("SELECT * from atributo" )->result_array();

        foreach($r as $atributo){

            $v = $this->db->query("SELECT valores 
                                                        FROM producto_atributo
                                                        WHERE idAtributo = ". $atributo["idAtributo"].
                                                        " AND is_variacion IS NULL 
                                                        group by valores order by valores asc")->result_array();
            
            // var_dump($v);
            // echo("<br><br><br>");
            $valores = array();
            
            foreach ($v as $value) {
                $valores[] = $value["valores"];

            }
            $valores_json = json_encode($valores);

            echo("UPDATE atributo set valor ='" . $valores_json . "' where idAtributo = " .$atributo["idAtributo"] . ";");
            echo("<br>");
        }
    }

    // ACTUALIZA NOMBRE DE CUBIERTAS PIRELLI
    function cocinero5($update = 0)
    {
        $product_list = $this->db->query("SELECT pr.*, 
                                                fa.nombre as familia_nombre,
                                                ma.nombre as marca_nombre
                                            FROM `producto` as pr 
                                            left join familia as fa on fa.idfamilia = pr.idFamilia
                                            left join marca as ma on ma.idMarca = pr.idMarca
                                                WHERE pr.idMarca = 19
                                                and pr.idFamilia = 8
                                                and pr.modelo LIKE 'CUBIERTA PIRELLI %'
                                                or pr.modelo LIKE 'CUBIERTAS PIRELLI %'
                                                ")->result_array();


        // var_dump('<pre>', $product_list, "</pre>"); die;

        if($update != 1){
            echo "</br>-------PROCESO DE ACTUALIZACION FINALIZADO | ENVIE 1 PARA HABILITAR-------</br>";
            echo "-------------------------------------------------------------------------------------------------------------------</br>";
        }

        $i = 0;        
        echo "</br>SE HAN DETECTADO: ". count($product_list)." CUBIERTAS PIRELLI.</br></br>";
        foreach ($product_list as $product) {
            echo ("OLD_____ ID: ".$product['idProducto']." - ".$product['nombre']."</br>"); 
            $search  = array("CUBIERTA PIRELLI ", "CUBIERTAS PIRELLI ", "cubiertas pirelli ");
            $product['modelo'] = str_replace($search,'',$product['modelo']);
            echo ("MODELO: ".  $product['modelo']."</br>");
            $product['nombre'] = $product['familia_nombre']." ".$product['marca_nombre']." ".$product['modelo'];
            echo ("PRODUCT ID: ".$product['idProducto']." - ".$product['nombre']."</br></br>"); 

            
            if($update == 1){
                //UPDATE
                $params = array(
                    'nombre' => $product['nombre'],
                    'modelo' => $product['modelo'],
                );
                $this->Productsingle->updateProduct($product['idProducto'],$params);  
            }
            $i ++;
        }

        echo "FIN CUBIERTA PIRRELLI</br>";

        $product_list = $this->db->query("SELECT pr.*, 
                                                fa.nombre as familia_nombre,
                                                ma.nombre as marca_nombre
                                            FROM `producto` as pr 
                                            left join familia as fa on fa.idfamilia = pr.idFamilia
                                            left join marca as ma on ma.idMarca = pr.idMarca
                                                WHERE pr.idMarca = 5
                                                and pr.idFamilia = 33
                                                and pr.modelo LIKE 'DISCO DE FRENO RIFFEL %'
                                                or pr.modelo LIKE 'DISCO DE FRENO %'
                                                ")->result_array();

        echo "</br>SE HAN DETECTADO: ". count($product_list)." DISCO DE FRENO.</br></br>";
        foreach ($product_list as $product) {
            echo ("OLD_____ ID: ".$product['idProducto']." - ".$product['nombre']."</br>"); 
            $search  = array("DISCO DE FRENO RIFFEL ","DISCO DE FRENO ");
            $product['modelo'] = str_replace($search,'',$product['modelo']);
            echo ("MODELO: ".  $product['modelo']."</br>");
            $product['nombre'] = $product['familia_nombre']." ".$product['marca_nombre']." ".$product['modelo'];
            echo ("PRODUCT ID: ".$product['idProducto']." - ".$product['nombre']."</br></br>"); 

            
            if($update == 1){
                //UPDATE
                $params = array(
                    'nombre' => $product['nombre'],
                    'modelo' => $product['modelo'],
                );
                $this->Productsingle->updateProduct($product['idProducto'],$params);  
            }
            $i ++;
        }

        echo "FIN DISCO DE FRENO</br>";


        $product_list = $this->db->query("SELECT pr.*, 
                                                fa.nombre as familia_nombre,
                                                ma.nombre as marca_nombre
                                            FROM `producto` as pr 
                                            left join familia as fa on fa.idfamilia = pr.idFamilia
                                            left join marca as ma on ma.idMarca = pr.idMarca
                                                WHERE pr.idMarca = 3
                                                and pr.idFamilia = 5
                                                and pr.modelo LIKE 'KIT DID %'
                                                ")->result_array();

        echo "</br>SE HAN DETECTADO: ". count($product_list)." KIT DID.</br></br>";
        
        foreach ($product_list as $product) {
            echo ("OLD_____ ID: ".$product['idProducto']." - ".$product['nombre']."</br>"); 
            $search  = array("KIT DID ");
            $product['modelo'] = str_replace($search,'',$product['modelo']);
            echo ("MODELO: ".  $product['modelo']."</br>");
            $product['nombre'] = $product['familia_nombre']." ".$product['marca_nombre']." ".$product['modelo'];
            echo ("PRODUCT ID: ".$product['idProducto']." - ".$product['nombre']."</br></br>"); 

            
            if($update == 1){
                //UPDATE
                $params = array(
                    'nombre' => $product['nombre'],
                    'modelo' => $product['modelo'],
                );
                $this->Productsingle->updateProduct($product['idProducto'],$params);  
            }
            $i ++;
        }

        echo "FIN KIT DID</br>";

        $product_list = $this->db->query("SELECT pr.*, 
                                                fa.nombre as familia_nombre,
                                                ma.nombre as marca_nombre
                                            FROM `producto` as pr 
                                            left join familia as fa on fa.idfamilia = pr.idFamilia
                                            left join marca as ma on ma.idMarca = pr.idMarca
                                                WHERE pr.idMarca = 5
                                                and pr.idFamilia = 5
                                                and pr.modelo LIKE 'KIT RIFFEL %'
                                                ")->result_array();

        echo "</br>SE HAN DETECTADO: ". count($product_list)." KIT RIFFEL.</br></br>";
        
        foreach ($product_list as $product) {
            echo ("OLD_____ ID: ".$product['idProducto']." - ".$product['nombre']."</br>"); 
            $search  = array("KIT RIFFEL ");
            $product['modelo'] = str_replace($search,'',$product['modelo']);
            echo ("MODELO: ".  $product['modelo']."</br>");
            $product['nombre'] = $product['familia_nombre']." ".$product['marca_nombre']." ".$product['modelo'];
            echo ("PRODUCT ID: ".$product['idProducto']." - ".$product['nombre']."</br></br>"); 

            
            if($update == 1){
                //UPDATE
                $params = array(
                    'nombre' => $product['nombre'],
                    'modelo' => $product['modelo'],
                );
                $this->Productsingle->updateProduct($product['idProducto'],$params);  
            }
            $i ++;
        }

        echo "FIN KIT RIFFEL</br>";


        $product_list = $this->db->query("SELECT pr.*, 
                                                fa.nombre as familia_nombre,
                                                ma.nombre as marca_nombre
                                            FROM `producto` as pr 
                                            left join familia as fa on fa.idfamilia = pr.idFamilia
                                            left join marca as ma on ma.idMarca = pr.idMarca
                                                WHERE pr.idMarca = 2
                                                and pr.idFamilia = 8
                                                and pr.modelo LIKE 'CUBIERTA METZELER %'
                                                ")->result_array();

        echo "</br>SE HAN DETECTADO: ". count($product_list)." CUBIERTA METZELER .</br></br>";
        
        foreach ($product_list as $product) {
            echo ("OLD_____ ID: ".$product['idProducto']." - ".$product['nombre']."</br>"); 
            $search  = array("CUBIERTA METZELER ");
            $product['modelo'] = str_replace($search,'',$product['modelo']);
            echo ("MODELO: ".  $product['modelo']."</br>");
            $product['nombre'] = $product['familia_nombre']." ".$product['marca_nombre']." ".$product['modelo'];
            echo ("PRODUCT ID: ".$product['idProducto']." - ".$product['nombre']."</br></br>"); 

            
            if($update == 1){
                //UPDATE
                $params = array(
                    'nombre' => $product['nombre'],
                    'modelo' => $product['modelo'],
                );
                $this->Productsingle->updateProduct($product['idProducto'],$params);  
            }
            $i ++;
        }

        echo "FIN CUBIERTA METZELER FEELFREE</br>";

        $product_list = $this->db->query("SELECT pr.*, 
                                                fa.nombre as familia_nombre,
                                                ma.nombre as marca_nombre
                                            FROM `producto` as pr 
                                            left join familia as fa on fa.idfamilia = pr.idFamilia
                                            left join marca as ma on ma.idMarca = pr.idMarca
                                                WHERE pr.idMarca = 3
                                                and pr.idFamilia = 5
                                                and pr.modelo LIKE 'CUBIERTA METZELER FEELFREE %'
                                                ")->result_array();

        echo "</br>SE HAN DETECTADO: ". count($product_list)." CUBIERTA METZELER FEELFREE.</br></br>";

        foreach ($product_list as $product) {
        echo ("OLD_____ ID: ".$product['idProducto']." - ".$product['nombre']."</br>"); 
        $search  = array("CUBIERTA METZELER FEELFREE ");
        $product['modelo'] = str_replace($search,'',$product['modelo']);
        echo ("MODELO: ".  $product['modelo']."</br>");
        $product['nombre'] = $product['familia_nombre']." ".$product['marca_nombre']." ".$product['modelo'];
        echo ("PRODUCT ID: ".$product['idProducto']." - ".$product['nombre']."</br></br>"); 


        if($update == 1){
        //UPDATE
        $params = array(
        'nombre' => $product['nombre'],
        'modelo' => $product['modelo'],
        );
        $this->Productsingle->updateProduct($product['idProducto'],$params);  
        }
        $i ++;
        }

        echo "FIN CUBIERTA METZELER FEELFREE</br>";

        $product_list = $this->db->query("SELECT pr.*, 
                                                fa.nombre as familia_nombre,
                                                ma.nombre as marca_nombre
                                            FROM `producto` as pr 
                                            left join familia as fa on fa.idfamilia = pr.idFamilia
                                            left join marca as ma on ma.idMarca = pr.idMarca
                                                WHERE pr.idMarca = 0
                                                and pr.idFamilia = 1
                                                and pr.modelo LIKE 'YUASA %'
                                                ")->result_array();

        echo "</br>SE HAN DETECTADO: ". count($product_list)." YUASA.</br></br>";
        
        foreach ($product_list as $product) {
            echo ("OLD_____ ID: ".$product['idProducto']." - ".$product['nombre']."</br>"); 
            $search  = array("YUASA ");
            $product['modelo'] = str_replace($search,'',$product['modelo']);
            echo ("MODELO: ".  $product['modelo']."</br>");
            $product['nombre'] = $product['familia_nombre']." ".$product['marca_nombre']." ".$product['modelo'];
            echo ("PRODUCT ID: ".$product['idProducto']." - ".$product['nombre']."</br></br>"); 

            
            if($update == 1){
                //UPDATE
                $params = array(
                    'nombre' => $product['nombre'],
                    'modelo' => $product['modelo'],
                );
                $this->Productsingle->updateProduct($product['idProducto'],$params);  
            }
            $i ++;
        }

        echo "FIN YUASA</br>";

        $product_list = $this->db->query("SELECT pr.*, 
                                                fa.nombre as familia_nombre,
                                                ma.nombre as marca_nombre
                                            FROM `producto` as pr 
                                            left join familia as fa on fa.idfamilia = pr.idFamilia
                                            left join marca as ma on ma.idMarca = pr.idMarca
                                                WHERE pr.idMarca = 1
                                                and pr.idFamilia = 2
                                                ")->result_array();

        echo "</br>SE HAN DETECTADO: ". count($product_list)." AGV.</br></br>";
        
        foreach ($product_list as $product) {
            echo ("OLD_____ ID: ".$product['idProducto']." - ".$product['nombre']."</br>"); 
            //$search  = array("YUASA ");
            //$product['modelo'] = str_replace($search,'',$product['modelo']);
            echo ("MODELO: ".  $product['modelo']."</br>");
            $product['nombre'] = $product['familia_nombre']." ".$product['marca_nombre']." ".$product['modelo'];
            echo ("PRODUCT ID: ".$product['idProducto']." - ".$product['nombre']."</br></br>"); 

            
            if($update == 1){
                //UPDATE
                $params = array(
                    'nombre' => $product['nombre'],
                    'modelo' => $product['modelo'],
                );
                $this->Productsingle->updateProduct($product['idProducto'],$params);  
            }
            $i ++;
        }

        echo "FIN AGV</br>";

    }

    // ACTUALIZA LOS SLUG DE LOS PRODUCTOS AGREGANDO EL ID AL FINAL DEL SLUG
    function cocinero6($update = 0)
    {
        $product_list = $this->db->query("SELECT idProducto, slug FROM `producto` ")->result_array();

        if($update == 1){
            echo" Se Actualizarán los SLUG";
            echo" Se Actualizarán los SLUG: </br>
            ==================================================================== <br>
            Listado de SLUG a Actualizar <br><br>";

            foreach ($product_list as $product) {
                // Realiza la comparacion del final de la cadena, para verficar que finalice con el ID, la funcion retorna 0 si coincide.
                if(substr_compare($product['slug'], $product['idProducto'], -strlen($product['idProducto']))){
                    echo "SLUG: ".$product['slug']."_".$product['idProducto']." UPDATE <br>";
                    $params = array(
                        'slug' => $product['slug']."_".$product['idProducto']
                    );
                    
                    $this->Productsingle->updateProduct($product['idProducto'],$params); 
                }
            }

        }else{
            echo" Vista Previa: </br>
            ==================================================================== <br>
            Listado de SLUG a Actualizar<br><br>";

            foreach ($product_list as $product) {
                // Realiza la comparacion del final de la cadena, para verficar que finalice con el ID, la funcion retorna 0 si coincide.
                if(substr_compare($product['slug'], $product['idProducto'], -strlen($product['idProducto']))){
                    echo "SLUG: ".$product['slug']." ==> NEW: ".$product['slug']."_".$product['idProducto']."<br>";
                }
            }
        }
    }

    // function cocinero7(){
    //     $slugs = $this->db->query("SELECT slug, count(*) as cantidad from producto group by slug HAVING cantidad > 1 ORDER BY `cantidad` DESC")->result_array();
    //     $slugs_repetidos = array_column($slugs_result, 'slug');

    //     $product_list = $this->db->query("SELECT idProducto, slug FROM `producto` ")->result_array();

    //     // Construir la lista de productos que tienen slugs repetidos
    //     $product_list = $this->db->query("SELECT idProducto, slug FROM producto WHERE slug IN ('" . implode("','", $slugs_repetidos) . "')")->result_array();

    //     foreach ($product_list as $product) {

           
    //     }

    // SE ACTUALIZAN LOS VALORES DE LOS ATRIBUTOS EN LA TABLA ATRIBUTOS
    function cocinero8($update = 0){

        $atributos = $this->db->query("SELECT * from atributo" )->result_array();

        if($update == 1){
            
            echo" Se Actualizarán los ATRIBUTOS DUPLICADOS</br>";
            echo" Se Actualizarán los ATRIBUTOS: </br>
            ==================================================================== <br>
            Listado de ATRIBUTOS a Actualizar <br><br>";

            foreach ($atributos as $atributo) {
                //Borrar los duplicados
                $list = str_replace(array("[", "\"", "]",'\\'), '', $atributo['valor']);
                $list = explode(',', $list);
                $list = array_unique($list, SORT_STRING);
                $listSinDuplicados = '[';
                foreach($list as $l){
                    $listSinDuplicados .= '"'.$l.'",';
                }
                $listSinDuplicados = substr($listSinDuplicados, 0, -1);
                $listSinDuplicados .= ']';

                $params = array(
                    'valor' => $listSinDuplicados,
                );

                $this->Atributo_model->update_atributo($atributo['idAtributo'],$params); 

                echo"ID FAMILIA: ".$atributo['idfamilia']. " => NOMBRE: ".$atributo['nombre'] ."</br>VALORES OLD: ".$atributo['valor'] ."</br>VALORES NEW: ".$listSinDuplicados."</br></br>";
            }

        }else{
            echo" Vista Previa: </br>
            ==================================================================== <br>
            Listado de ATRIBUTOS a Actualizar<br><br>";

            foreach ($atributos as $atributo) {
                //Borrar los duplicados
                $list = str_replace(array("[", "\"", "]",'\\'), '', $atributo['valor']);
                $list = explode(',', $list);
                $list = array_unique($list, SORT_STRING);
                $listSinDuplicados = '[';
                foreach($list as $l){
                    $listSinDuplicados .= '"'.$l.'",';
                }
                $listSinDuplicados = substr($listSinDuplicados, 0, -1);
                $listSinDuplicados .= ']';

                echo"ID FAMILIA: ".$atributo['idfamilia']. " => NOMBRE: ".$atributo['nombre'] ."</br>VALORES OLD: ".$atributo['valor'] ."</br>VALORES NEW: ".$listSinDuplicados."</br></br>";
            }
        }
    }

    // ACTUALIZA LOS SLUG DE LOS PRODUCTOS AGREGANDO EL ID AL FINAL DEL SLUG [ID_PRODUCTO-FAMILIA-MARCA-MODELO]
    function cocinero9($update = 0)
    {
        //$product_list = $this->db->query("SELECT idProducto, slug, idFamilia,  FROM `producto` ")->result_array();
        $product_list = $this->db->query("SELECT p.slug as slug, p.idProducto as idProducto, f.nombre as familia, m.nombre as marca, p.modelo as modelo FROM producto p LEFT JOIN familia f ON p.idFamilia = f.idFamilia LEFT JOIN marca m ON p.idMarca = m.idMarca")->result_array();
        // Caracteres a buscar en productos
        $buscarProducto = array('/', '(', ')');

        // Definir los caracteres de reemplazo
        $reemplazarProducto = array('_', '-', '-');
        if($update == 1){
            echo" Se Actualizarán los SLUG</br>";
            echo"====================================================================</br>";
            echo" Formato SLUG: [ID_PRODUCTO-FAMILIA-MARCA-MODELO]</br>";
            echo"====================================================================</br>";
            echo" Se Actualizarán los SLUG: </br>
            ==================================================================== <br>
            Listado de SLUG a Actualizar <br><br>";

            foreach ($product_list as $product) {
                // Realiza la comparacion del final de la cadena, para verficar que finalice con el ID, la funcion retorna 0 si coincide.
                if(substr_compare($product['slug'], $product['idProducto'], -strlen($product['idProducto']))){
                    $model = preg_replace('/[^a-zA-Z0-9]/', '', $product['modelo']);
                    echo "SLUG: ".str_replace($buscarProducto,$reemplazarProducto,$product['idProducto']."-".str_replace(' ','',strtolower($product['familia']))."-".str_replace(' ','',strtolower($product['marca']))."-".str_replace(' ','',strtolower($model)))." UPDATE <br>";
                    $params = array(
                        'slug' => str_replace($buscarProducto,$reemplazarProducto,$product['idProducto']."-".str_replace(' ','',strtolower($product['familia']))."-".str_replace(' ','',strtolower($product['marca']))."-".str_replace(' ','',strtolower($model)))
                    );
                    $this->Productsingle->updateProduct($product['idProducto'],$params); 
                }
            }

        }else{
            echo" Vista Previa: </br>
            ==================================================================== <br>
            Formato SLUG: [ID_PRODUCTO-FAMILIA-MARCA-MODELO]</br>
            ==================================================================== <br>
            Listado de SLUG a Actualizar<br><br>";

            foreach ($product_list as $product) {
                // Realiza la comparacion del final de la cadena, para verficar que finalice con el ID, la funcion retorna 0 si coincide.
                
                if(substr_compare($product['slug'], $product['idProducto'], -strlen($product['idProducto']))){
                    $model = preg_replace('/[^a-zA-Z0-9]/', '', $product['modelo']);
                    echo "SLUG: ".$product['slug']." ==> NEW: ".str_replace($buscarProducto,$reemplazarProducto,$product['idProducto']."-".str_replace(' ','',strtolower($product['familia']))."-".str_replace(' ','',strtolower($product['marca']))."-".str_replace(' ','',strtolower($model)))."<br>";
                }
            }
        }
    }

    // SE ACTUALIZAN LOS VALORES SEARCH DE LOS PRODUCTOS
    function cocinero10($update = 0){

        $product_list = $this->db->query("SELECT idProducto, search  FROM `producto` ")->result_array();

        if($update == 1){
            
            echo" Se Actualizarán los PARAMETROS DE BUSQUEDAS DUPLICADOS</br>";
            echo" ==================================================================== <br>
            Listado de PARAMETROS DE BUSQUEDAS a Actualizar <br><br>";

            foreach ($product_list as $product) {
                //Borrar los duplicados
                $list = str_replace(array("[", "\"", "]",'\\',"'"), '', $product['search']);
                $list = explode(',', $list);
                $list = array_unique($list, SORT_STRING);
                $searchList = implode(';', $list);
                /* $listSinDuplicados = '[';
                foreach($list as $l){
                    $listSinDuplicados .= '"'.$l.'",';
                }
                $listSinDuplicados = substr($listSinDuplicados, 0, -1);
                $listSinDuplicados .= ']'; */

                $params = array(
                    'search' => $searchList,
                );

                $this->Productsingle->updateProduct($product['idProducto'],$params);

                echo"ID PRODUCTO: ".$product['idProducto']. " => SEARCH: ".$searchList."</br></br>";
            }

        }else{
            echo" Vista Previa: </br>
            ==================================================================== <br>
            Listado de PARAMETROS DE BUSQUEDAS a Actualizar<br><br>";

            foreach ($product_list as $product) {
                //Borrar los duplicados
                $list = str_replace(array("[", "\"", "]",'\\',"'"), '', $product['search']);
                $list = explode(',', $list);
                $list = array_unique($list, SORT_STRING);
                $searchList = implode(';', $list);
                //echo "ID PRODUCTO: ".$product['idProducto']. " - <b>SERACH OLD</b>: ".$searchList." </br></br>";
                /* $listSinDuplicados = '[';
                foreach($list as $l){
                    $listSinDuplicados .= '"'.$l.'",';
                }
                $listSinDuplicados = substr($listSinDuplicados, 0, -1);
                $listSinDuplicados .= ']'; */

                echo"ID PRODUCTO: ".$product['idProducto']. " </br></br>- <b>SERACH OLD</b>: ".$product['search']." </br></br>- <b>SERACH NEW:</b> ".$searchList."</br></br>";
            }
        }
    }

}

     

    