<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Moto extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Moto_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of moto
     */
    function index()
    {
        $data['moto'] = $this->Moto_model->get_all_moto_join();
        
        $data['_view'] = 'moto/index';
        $data['user'] = $this->user;
        $this->load->view('layouts/main',$data);
    }

    function get_moto_with_params($make, $model='', $year=''){
        $data['motos'] = $this->Moto_model->get_moto_by_make_model_year($make, $model, $year);
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    /*
     * Adding a new moto
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
                'sku' => $this->input->post('sku'),
                'idMarca' => $this->input->post('idMarca'),
                'idModelo' => $this->input->post('modelo'),
                'year' => $this->input->post('year'),
                'name' => $this->input->post('nombre'),
                'descripcion' => $this->input->post('descripcion'),
            );
            $moto_id = $this->Moto_model->add_moto($params);
            redirect('moto/index');
        }
        else
        {     
            $this->load->model('Marca_model');
            $data['all_marca'] = $this->Marca_model->get_all_marca();   
            $data['_view'] = 'moto/add';
            $data['user'] = $this->user;
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a moto
     */
    function edit($idMoto)
    {   
        // check if the moto exists before trying to edit it
        $data['moto'] = $this->Moto_model->get_moto($idMoto);
        if(isset($data['moto']['idMoto']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
                    'sku' => $this->input->post('sku'),
                    'idMarca' => $this->input->post('idMarca'),
                    'idModelo' => $this->input->post('modelo'),
                    'year' => $this->input->post('year'),
                    'name' => $this->input->post('nombre'),
                    'descripcion' => $this->input->post('descripcion'),
                );

                $this->Moto_model->update_moto($idMoto,$params);            
                redirect('moto/index');
            }
            else
            {
                $this->load->model('Marca_model');
                $data['all_marca'] = $this->Marca_model->get_all_marca();   
                $data['_view'] = 'moto/edit';
                $data['user'] = $this->user;
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The moto you are trying to edit does not exist.');
    } 

    /*
     * Deleting moto
     */
    function remove($idMoto)
    {
        $moto = $this->Moto_model->get_moto($idMoto);

        // check if the moto exists before trying to delete it
        if(isset($moto['idMoto']))
        {
            $this->Moto_model->delete_moto($idMoto);
            redirect('moto/index');
        }
        else
            show_error('The moto you are trying to delete does not exist.');
    }

}
