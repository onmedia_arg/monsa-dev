<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Customer extends BaseController{
    
    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Producto_model');
            $this->load->model('Order_hdr_model');
            $this->load->model('Order_itm_model');
            $this->load->helper("globalFunctions");
            $this->load->helper("price");            

        }else{
            redirect('/', 'refresh');
        }
    }

    function index(){
        $data['nav']   = menuFront();
        $data['title'] = "Mi Cuenta";
        $data['_view'] = 'front/customer/orders_list';
        $data['user']         = $this->dataUser();
        
        $this->load->view('front/customer/index', $data);
    }

    function cuentaCorriente(){

        $script = array(
            'assets/front/js/customer.js?v='.time(),
        );

        $data['scripts']   = $script;
        $data['nav']   = menuFront();
        $data['title'] = "Cuenta Corriente";
        $data['_view'] = 'front/customer/cuenta_corriente';
        $data['user']         = $this->dataUser();
        
        $this->load->view('front/customer/index', $data);
    }


    function listOrders(){

        $result = $this->Order_hdr_model->getAllByCustomer($this->session->userdata('user_id'));
        
        $data   = array();
       
        foreach ($result as $result_row){
            $row = array();
            
            $row[] = $result_row->idOrderHdr;
            if (trim($result_row->created) && $result_row->created != null) {
                 $result_row->created = DateTime::createFromFormat("Y-m-d", $result_row->created);
                 $result_row->created = $result_row->created->format("d-m-Y");
            }
            $row[] = $result_row->created;
            
            $row[] = $result_row->statusText;
            $row[] = show_price($result_row->total);
            $row[] = $result_row->idOrderStatus;            
            $row[] = $result_row->nota;

            $data[] = $row;
        }

        $output = array(
            "data" => $data,
        );

        echo json_encode($output);
    }

    function listItems(){
        $idOrderHdr = $this->input->post("idOrderHdr");
        $result = $this->Order_itm_model->getItemsByOrder($idOrderHdr);
        $data   = array();
        foreach ($result as $result_row){
            $atributos = $this->Producto_model->get_product_atributes($result_row->id_producto);
            $atr = '';
            foreach ($atributos as $atributo) {
                if($atributo['valores'] != ''){
                    $atr .= $atributo['valores'].' - ';
                }
            }
            $product = $result_row->nombre ."<br><p class='text-muted'>".substr($atr, 0, -2)."</p>";
            $row = array();
            $row[] = $result_row->posnr;
            $row[] = $result_row->sku;
            $row[] = $product;
            $row[] = show_price($result_row->precio);
            $row[] = $result_row->qty;
            $row[] = show_price($result_row->total);

            $data[] = $row;            
        }
        
        $output = array(
            "data" => $data,
        );

        echo json_encode($output);

    }

    function getOrder(){
        $idOrderHdr = $this->input->post("idOrderHdr");
        $result = $this->Order_hdr_model->getById($idOrderHdr);

        $data = array();
        $data[] = $result["created"];
        $data[] = $result["nota"];
        $data[] = show_price($result["total"]);
        $data[] = $result["idOrderStatus"];
        
        echo json_encode($data);
    }

    function viewProfile(){

        
    }

    function viewAccount(){

        
    }

}