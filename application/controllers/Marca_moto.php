<?php

include_once(FCPATH."/application/controllers/BaseController.php");
class Marca_moto extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Marca_moto_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of marca_moto
     */
    function index()
    {
        $data['marca_moto'] = $this->Marca_moto_model->get_all_marca_moto();
        
        $data['_view'] = 'marca_moto/index';
        $data['user'] = $this->user;
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new marca_moto
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'nombre' => $this->input->post('nombre'),
				'slug' => $this->input->post('slug'),
				'descripcion' => $this->input->post('descripcion'),
            );
            
            $marca_moto_id = $this->Marca_moto_model->add_marca_moto($params);
            redirect('marca_moto/index');
        }
        else
        {            
            $data['_view'] = 'marca_moto/add';
            $data['user'] = $this->user;
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a marca_moto
     */
    function edit($idMarcaMoto)
    {   
        // check if the marca_moto exists before trying to edit it
        $data['marca_moto'] = $this->Marca_moto_model->get_marca_moto($idMarcaMoto);
        
        if(isset($data['marca_moto']['idMarcaMoto']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'nombre' => $this->input->post('nombre'),
					'slug' => $this->input->post('slug'),
					'descripcion' => $this->input->post('descripcion'),
                );

                $this->Marca_moto_model->update_marca_moto($idMarcaMoto,$params);            
                redirect('marca_moto/index');
            }
            else
            {
                $data['_view'] = 'marca_moto/edit';
                $data['user'] = $this->user;
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The marca_moto you are trying to edit does not exist.');
    } 

    /*
     * Deleting marca_moto
     */
    function remove($idMarcaMoto)
    {
        $marca_moto = $this->Marca_moto_model->get_marca_moto($idMarcaMoto);

        // check if the marca_moto exists before trying to delete it
        if(isset($marca_moto['idMarcaMoto']))
        {
            $this->Marca_moto_model->delete_marca_moto($idMarcaMoto);
            redirect('marca_moto/index');
        }
        else
            show_error('The marca_moto you are trying to delete does not exist.');
    }

    function fillTable(){
        $file = FCPATH.'/application/models/json/makesAndModels.json';

        $data = file_get_contents($file);

        $data_arr = json_decode($data, true);

        $arr_data = [];
        for ($i=0; $i < count($data_arr['Marcas']); $i++) {
            $params = array(
                // 'idMarcaMoto' => $data_arr['Marcas'][$i]['id'],
                'nombre' => $data_arr['Marcas'][$i]['nombre'],
                'slug' => $this->create_slug($data_arr['Marcas'][$i]['nombre']),
                'descripcion' => 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor',
            );
            array_push($arr_data, $params);
        }
        
        $this->Marca_moto_model->add_marca_batch($arr_data);
    }
    
}
