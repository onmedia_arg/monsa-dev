<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Modelo extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Modelo_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of modelo
     */
    function index()
    {
        $data['modelo'] = $this->Modelo_model->get_all_modelo();
        
        $data['_view'] = 'modelo/index';
        $data['user'] = $this->user;
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new modelo
     */
    function add()
    {   
        $this->load->library('form_validation');

        $this->form_validation->set_rules('nombre','Nombre','max_length[255]');
        $this->form_validation->set_rules('slug','Slug','max_length[255]');
        $this->form_validation->set_rules('year','Year','max_length[255]');
        
        if($this->form_validation->run())     
        {   
            $params = array(
                'idMarca' => $this->input->post('idMarca'),
                'nombre' => $this->input->post('nombre'),
                'slug' => $this->input->post('slug'),
                'year' => $this->input->post('year'),
            );
            
            $modelo_id = $this->Modelo_model->add_modelo($params);
            redirect('modelo/index');
        }
        else
        {
            $this->load->model('Marca_model');
            $data['all_marca'] = $this->Marca_model->get_all_marca();
            
            $data['_view'] = 'modelo/add';
            $data['user'] = $this->user;
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a modelo
     */
    function edit($idModelo)
    {   
        // check if the modelo exists before trying to edit it
        $data['modelo'] = $this->Modelo_model->get_modelo($idModelo);
        
        if(isset($data['modelo']['idModelo']))
        {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('nombre','Nombre','max_length[255]');
            $this->form_validation->set_rules('slug','Slug','max_length[255]');
            $this->form_validation->set_rules('year','Year','max_length[255]');
        
            if($this->form_validation->run())     
            {   
                $params = array(
                    'idMarca' => $this->input->post('idMarca'),
                    'nombre' => $this->input->post('nombre'),
                    'slug' => $this->input->post('slug'),
                    'year' => $this->input->post('year'),
                );

                $this->Modelo_model->update_modelo($idModelo,$params);            
                redirect('modelo/index');
            }
            else
            {
                $this->load->model('Marca_model');
                $data['all_marca'] = $this->Marca_model->get_all_marca();

                $data['_view'] = 'modelo/edit';
                $data['user'] = $this->user;
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The modelo you are trying to edit does not exist.');
    } 

    /*
     * Deleting modelo
     */
    function remove($idModelo)
    {
        $modelo = $this->Modelo_model->get_modelo($idModelo);

        // check if the modelo exists before trying to delete it
        if(isset($modelo['idModelo']))
        {
            $this->Modelo_model->delete_modelo($idModelo);
            redirect('modelo/index');
        }
        else
            show_error('The modelo you are trying to delete does not exist.');
    }
    
    
    /*
    * function batch for fill and empty table
    */

    function fillTable(){
        $file = FCPATH.'/application/models/json/makesAndModels.json';

        $data = file_get_contents($file);

        $data_arr = json_decode($data, true);

        $arr_data = [];
        for ($i=0; $i < count($data_arr['Marcas']); $i++) { 

            for ($j=0; $j < count($data_arr['Marcas'][$i]['Modelos']); $j++) { 
                $params = array(
                    'idMarca' => $data_arr['Marcas'][$i]['id'],
                    'nombre' => $data_arr['Marcas'][$i]['Modelos'][$j]['nombre'],
                    'slug' => strtolower(str_replace(' ', '-', $data_arr['Marcas'][$i]['Modelos'][$j]['nombre'])),
                    'year' => '1900',
                );
                array_push($arr_data, $params);
            }
            
        }
        $this->Modelo_model->add_modelo_batch($arr_data);
    }
    
    function getModeloJson()
    {
        $id = $this->uri->segment(3);
        $data['modelo'] = $this->Modelo_model->get_all_modelo($id);
        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }
}