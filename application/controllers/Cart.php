<?php

include_once(FCPATH."/application/controllers/BaseController.php");

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Cart extends BaseController{
    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {

            // $this->load->library("cart");   
            $this->load->library('email');
            $this->load->helper("price");    
            $this->load->model('Order_hdr_model');
            $this->load->model('Order_itm_model');

            $this->load->model('Base_model', 'Base', true );
            $this->load->model('User_model', 'User', true );
            $this->load->model('Producto_model', 'Producto', true );
            $this->load->model('Clientes_model', 'Clientes', true );
            $this->load->model('RelClienteUsuario_model', 'Rel', true );            

        }else{
            redirect('/', 'refresh');
        }
    }

    public function add_to_cart(){

        if($this->input->post("is_promo_active") == 0 || $this->input->post("is_promo_active") == null){
            $price = $this->input->post("product_price");
        }else{
            $price = $this->input->post("price_promo");
        }
        
        
        $data = array(
            "id"    => $this->input->post("product_id"),
            "name"  => $this->input->post("product_name"),
            "qty"   => $this->input->post("quantity"),
            "price" => $price, //$this->input->post("product_price"),
            "sku" => $this->input->post("sku"),
            "attributes"=> $this->input->post("attributes")
        );          

        $r = $this->cart->insert($data);
        
        $this->save_cart_draft();
        
        if ( $r ) {

            echo json_encode( ['code' => 1, 'message' => 'Producto Agregado Correctamente!', 'console' => null, 'count' => $this->cart->total_items() ] );

        }else{

            echo json_encode( ['code' => 0, 'message' => 'No se pudo agregar el producto al pedido. Contacte al administrador', 'console' => $this->db->error() ] );
        }

    }

    public function get_cart_content(){

        $count = 0;
        $output = '<tr>
                        <th>SKU</th>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th>Total</th>
                        <th>Acciones</th>
                    </tr>';
        
                    
        foreach($this->cart->contents() as $items)
        {
            $producto = $this->Producto->get_producto( $items["id"] );
            $count++;
            $output .= '<tr> 
                            <td>'.$items["sku"].'</td>
                            <td class="cart-item-name">'.$items["name"] . '<br/>' . (isset($items["attributes"]) ? $items["attributes"] : '') .'</td>
                            <td>
                                <input class="qty" type="number" 
                                        value="'.$items["qty"].'" 
                                        id="'.$items["rowid"].'"  
                                        min="'.$producto["cantidad_minima"].'"
                                        step="'.$producto["rango"].'" >                                     
                            </td>
                            <td>'. show_price($items["price"]).'</td>
                            <td>'. show_price($items["subtotal"]).'</td>
                            <td class="cart-item-actions"><button type="button" name="remove" class="btn btn-outline-danger btn-sm remove_item" id="'.$items["rowid"].'" data-toggle="tooltip" data-placement="bottom" title="Borrar Item" ><i class="far fa-trash-alt"></i></button>
                            </td>
                        </tr>';
        }

        $output .= '<tr>
                        <td colspan="4" align="right">Total</td>
                        <td colspan="4"> '.show_price($this->cart->total()).' 
                        <span style="float: right;
                                     color: #666;
                                     font-style: italic;
                                     font-size: 0.8em;">Los precios no incluyen IVA</span></td>
                    </tr>';

        if($count == 0)
        {
            $output = '<h3 align="center">Su pedido aún esta vacio</h3>';
        }
        
        echo json_encode(['items'=>$output, 'cantidad' => $this->cart->total_items() ]); ;

    }
        
    public function remove_from_cart(){

        $row_id = $_POST["row_id"];
        
        $data = array(
                  'rowid'  => $row_id,
                   'qty'  => 0
                );

        $r = $this->cart->update($data);

        $this->save_cart_draft();

        if ( $r ) {

            echo json_encode( ['code' => 1, 'message' => 'Producto eliminado', 'console' => null, 'count' => $this->cart->total_items() ] );

        }else{

            echo json_encode( ['code' => 0, 'message' => 'No se pudo remover el producto del pedido. Contacte al administrador', 'console' => $this->db->_error_message() ] );
        }

    }

    public function update_cart(){

        $row_id = $_POST["row_id"];
        $qty    = $_POST["qty"];
        
        $data = array(
                  'rowid' => $row_id,
                   'qty'  => $qty
                );

        // return $this->cart->update($data);
        
        $r = $this->cart->update($data);

        $this->save_cart_draft();

        echo json_encode( ['code' => 1, 'message' => 'Producto agregado', 'console' => null, 'count' => $this->cart->total_items() ] );
    }    

    public function process_order(){

        if(empty($this->cart->contents())){
            echo json_encode( [ 'code' => 400, 'message' =>  'El pedido no contiene productos asociados'] );
            die;
        }

        $order_r    = $this->session->userdata("idOrderHdr");
        
        $userData   = $this->User->get_user( $_SESSION['user_id'] );
        $rel        = $this->Rel->get_rel_by_user( $_SESSION['user_id'] );
        
        if($rel['idCliente'] != 0){
            $clientData = $this->Clientes->get_cliente( $rel['idCliente'] );
        }else{
            $clientData['idCliente'] = '0';
            $clientData['razon_social'] = 'N/A';
        }
        
        if($order_r == NULL){

            $order_hdr = array(
                    'idUser'         => $_SESSION['user_id'],
                    'idCliente'      => $rel['idCliente'],
                    'total'          => $this->cart->total(),
                    'nota'           => $this->input->post('nota'),
                    'idOrderStatus'  => '2', //ENVIADO
                    'createdBy'      => $_SESSION['user_id'],
                    'created'        => date("Y-m-d"), 
                    'updatedBy'      => $_SESSION['user_id'] 
                    );
            
            $order_r = $this->Order_hdr_model->addOrder($order_hdr);

        }else{
            $order_hdr = array(
                    'idUser'         => $_SESSION['user_id'],
                    'idCliente'      => $rel['idCliente'],
                    'total'          => $this->cart->total(),
                    'nota'           => $this->input->post('nota'),
                    'idOrderStatus'  => '2', //ENVIADO
                    );
            
            $order_r = $this->Order_hdr_model->updateOrder($order_r, $order_hdr);
            $items_r = $this->Order_itm_model->clearItemsFromOrder($order_r);

        }

        $data = array();
        $excel = array();
        $i = 0;
        $total = 0;
        $totalArticulos = 0;

        foreach ($this->cart->contents() as $items){

            $i++;    
            $total = $total + $items["subtotal"];
            $totalArticulos = $totalArticulos + $items["qty"];

            $item = array(
                        'idOrderHdr '   => $order_r,
                        'posnr'         => $i,
                        'idProducto'    => $items["id"],
                        'precio'        => $items["price"],
                        'qty'           => $items["qty"],
                        'total'         => $items["subtotal"],
                        'options'       => '',
                        'idItemStatus'  => 1, //ACTIVO
                        'createdBy'     => $_SESSION['user_id'],
                        'updatedBy'     => $_SESSION['user_id']
                    );    

            // Producto para excel
            $producto = $this->Producto->get_producto( $items["id"] );

            $excel[] = array(
                            'posnr'         => $i,
                            'idOrderHdr '   => $order_r,
                            'sku '          => $producto['sku'],
                            'desc_producto' => $producto['nombre'],
                            'precio'        => $items["price"],
                            'qty'           => $items["qty"],
                            'total'         => $items["subtotal"]
                        ); 

            $items_mail[] = array(
                            'posnr'         => $i,
                            'idOrderHdr '   => $order_r,
                            'sku'           => $producto['sku'],
                            'desc_producto' => $producto['nombre'],
                            'precio'        => $items["price"],
                            'qty'           => $items["qty"],
                            'total'         => $items["subtotal"]
                        ); 


            $data[] = $item;

        }

        // Total
        $excel[] = array( null, null, null, null, null, 'Total: ', $total ); 
        // var_dump($data);die;
        $items_r = $this->Order_itm_model->addItems($data);

        // Excel
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet(); 
        $spreadsheet->getProperties()
                    ->setCreator( 'Monsa Web' )
                    ->setLastModifiedBy( 'Monsa Web' )
                    ->setTitle( "Orden Monsa ".date('Y-m-d').".xlsx" )
                    ->setSubject( "Orden Monsa" ) 
                    ->setKeywords( "office 2007 monsa openxml php" );

        $spreadsheet->setActiveSheetIndex(0);

        $spreadsheet->getActiveSheet()->setCellValue('A5', '# Item')
                                      ->setCellValue('B5', 'ID Orden')
                                      ->setCellValue('C5', 'SKU')
                                      ->setCellValue('D5', 'Desc. Producto')
                                      ->setCellValue('E5', 'Precio')
                                      ->setCellValue('F5', 'Cantidad') 
                                      ->setCellValue('G5', 'Total Item');
     
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); 

        $spreadsheet->getActiveSheet()->mergeCells('A1:C1')
                                      ->mergeCells('E1:G1');

        $spreadsheet->getActiveSheet()->mergeCells('A2:C2')
                                      ->mergeCells('E2:G2');

        $spreadsheet->getActiveSheet()->mergeCells('A3:C3')
                                      ->mergeCells('E3:G3');

        $spreadsheet->getActiveSheet()->mergeCells('A4:G4');

        // var_dump($clientData);die;
        // HEADER
        $spreadsheet->getActiveSheet()->setCellValue( 'A1', 'Cliente #' . $clientData['idCliente'] )
                                        ->setCellValue( 'A2', 'Razon Social: ' . $clientData['razon_social'] )
                                        ->setCellValue( 'A3', 'Usuario: ' . $userData['username'] )

                                        ->setCellValue('E1', 'Cantidad de Items: ' . $i )
                                        ->setCellValue('E2', 'Cantidad de articulos: ' . $totalArticulos )
                                        ->setCellValue('E3', 'Fecha de creación: ' . date('Y-m-d') );
        // Resumen
        $spreadsheet->getActiveSheet()->setCellValue( 'A4', 'DETALLE');
        $spreadsheet->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal('center');
        $spreadsheet->getActiveSheet()->getStyle('A4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('fdcb6e'); 

        // DATA
        $spreadsheet->getActiveSheet()
            ->fromArray(
                $excel,  // The data to set
                NULL,        // Array values with this value will not be set
                'A6'         // Top left coordinate of the worksheet range where
                             //    we want to set these values (default is A1)
            ); 

        // Guarda en uploads
        // $filenameExcel = PATH_UPLOAD . "/orders/Orden #" . $order_r . " Monsa " . date('Y-m-d') . ".xlsx";
        $filenameExcel = PATH_UPLOAD . "/" . "orders/" . $order_r . ".xlsx";
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save( $filenameExcel );


        $mailData = array(
                    'userData'  => $userData,
                    'order_r'   => $order_r,
                    'excel'     => $items_mail,
                    'total'     => $total,
                    'client'    => $clientData,
                    'nota'      => $this->input->post('nota')
                );

        // Si se agrega el pedido
        if ( $items_r !== false ) {

            // Enviar Correo
            ob_start(); 
            $this->load->view( 'layouts/email/email-header' );
            $this->load->view( 'layouts/email/new-order', $mailData );
            $this->load->view( 'layouts/email/email-footer' );
        
            $body = ob_get_clean(); 

            // Correo usuario
            $send = $this->Base->send_email_template( 'Nueva Orden #' . $order_r, 
                                                    $userData['email'], 
                                                    $body, 
                                                    $filenameExcel );

            // Correo Admin
            $sendAdmin = $this->Base->send_email_template( 'Tienes una nueva orden #' . $order_r, 
                                                            $this->Base->get_sysopt_value('admin_mail'), 
                                                            $body, 
                                                            $filenameExcel );
            
            if($clientData['email_vendedor'] != ''){
                $sendVendedor = $this->Base->send_email_template( 'Tienes una nueva orden #' . $order_r, 
                                                                $clientData['email_vendedor'], 
                                                                $body, 
                                                                $filenameExcel );
            }


            $this->cart->destroy();
            $this->session->unset_userdata('idOrderHdr');

            $this->session->set_flashdata( 'success_message', 'Pedido #' . $order_r . ' generado correctamente.' );

            echo json_encode( [ 'code' => 1, 'url' => base_url( 'customer?pedido=' . $order_r ) ] ); 

        }

    }

    public function send_order_by_mail(){

    }

    public function get_user_cart($id_user, $state=''){
        // return $this->Order_model->get_order_by_user($id_user, $state);
    }

    public function save_cart_draft(){

        $idOrderHdr = $this->session->userdata("idOrderHdr");
        // var_dump($idOrderHdr);die;

        if($idOrderHdr == NULL){

            $userData   = $this->User->get_user( $_SESSION['user_id'] );
            $rel        = $this->Rel->get_rel_by_user( $_SESSION['user_id'] );
            $clientData = $this->Clientes->get_cliente( $rel['idCliente'] ); 

            $order_hdr = array(
                    'idUser'         => $_SESSION['user_id'],
                    'idCliente'      => $rel['idCliente'],
                    'total'          => $this->cart->total(),
                    'nota'           => $this->input->post('nota'),
                    'idOrderStatus'  => '1', //BORRADOR
                    'createdBy'      => $_SESSION['user_id'],
                    'created'        => date("Y-m-d"), 
                    'updatedBy'      => $_SESSION['user_id'] 
                    );
            
            $idOrderHdr = $this->Order_hdr_model->addOrder($order_hdr);

            $this->session->set_userdata("idOrderHdr",$idOrderHdr);

        }else{
            $order_hdr = array(
                    'total'          => $this->cart->total(),
                    'nota'           => $this->input->post('nota'),
                    );
            
            $idOrderHdr = $this->Order_hdr_model->updateOrder($idOrderHdr, $order_hdr);
        }

        $data = array();
        $i = 0;

        // var_dump($this->cart->contents());die;
        if( count($this->cart->contents()) > 0){

            foreach ($this->cart->contents() as $items){

                $i++;    
                $item = array(
                            'idOrderHdr '   => $idOrderHdr,
                            'posnr'         => $i,
                            'idProducto'    => $items["id"],
                            'precio'        => $items["price"],
                            'qty'           => $items["qty"],
                            'total'         => $items["subtotal"],
                            'options'       => '',
                            'idItemStatus'  => 1, //ACTIVO
                            'createdBy'     => $_SESSION['user_id'],
                            'updatedBy'     => $_SESSION['user_id']
                        );    
    
                $data[] = $item;
            }
    
            // Total
            $items_r = $this->Order_itm_model->clearItemsFromOrder($idOrderHdr);
            $items_r = $this->Order_itm_model->addItems($data);

        }else{
            $items_r = $this->Order_itm_model->clearItemsFromOrder($idOrderHdr);
        }

        $response = array(
            'idOrderHdr' => $idOrderHdr,
            'items'      => $items_r

        );
        return ($response);

        //echo json_encode($response);

    }


    public function fill_cart_from_draft(){
       
        $this->cart->destroy();

        $idOrderHdr = $this->input->post('id');

        $items = $this->Order_itm_model->getItemsByOrder($idOrderHdr);

        $this->session->set_userdata("idOrderHdr",$idOrderHdr);

        foreach ($items as $item) {
          
            $data = array(
                "id"     => $item->idProducto,
                "name"   => $item->nombre,
                "qty"    => $item->qty,
                "price"  => $item->precio,
                "sku"    => $item->sku,
            );          

            $r = $this->cart->insert($data);

        }


        if ( $r ) {

            echo json_encode( ['code' => 1, 'message' => 'El pedido fue cargado en el carrito correctamente', 'console' => null ] );

        }else{

            echo json_encode( ['code' => 0, 'message' => 'No se pudo agregar el producto al pedido. Contacte al administrador', 'console' => $this->db->_error_message() ] );
        }

    }

    public function fill_cart_from_order(){

        $this->cart->destroy();
        // $this->session->unset_userdata('idOrderHdr');

        $idOrderHdr = $this->input->post('id');

        $items = $this->Order_itm_model->getItemsByOrder($idOrderHdr);


        foreach ($items as $item) {

            $name = preg_replace('/[^A-Za-z0-9\-]/', ' ',  $item->nombre);
            $name = preg_replace('!\s+!', ' ', $name);

            $data = array(
                "id"     => $item->idProducto,
                "name"   => $name,
                "qty"    => $item->qty,
                "price"  => $item->precio,
                "sku"    => $item->sku,
            );          

            $r = $this->cart->insert($data);

        }


        if ( $r ) {

            echo json_encode( ['code' => 1, 'message' => 'El pedido fue cargado en el carrito correctamente', 'console' => null ] );

        }else{

            echo json_encode( ['code' => 0, 'message' => 'No se pudo agregar el producto al pedido. Contacte al administrador', 'console' => $this->db->_error_message() ] );
        }



    }
    
    public function new_cart(){

        $this->cart->destroy();
        $this->session->unset_userdata('idOrderHdr');

        echo json_encode( ['code' => 200, 'message' => 'Se limpio el carrito con éxito', 'console' => null ] );

    }
    public function clear_cart(){

        $this->cart->destroy();
        
        $this->save_cart_draft();

        echo json_encode( ['code' => 200, 'message' => 'Se limpio el carrito con éxito', 'console' => null ] );

    }    
    
    
    // public function update_cart($idOrder, $params){
    //     // return $this->Order_model->update_order($idOrder, $params);
    // }
}