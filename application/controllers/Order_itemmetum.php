<?php
include_once(FCPATH."/application/controllers/BaseController.php");
class Order_itemmetum extends BaseController{
    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Order_itemmetum_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of order_itemmeta
     */
    function index()
    {
        $data['order_itemmeta'] = $this->Order_itemmetum_model->get_all_order_itemmeta();
        
        $data['_view'] = 'order_itemmetum/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new order_itemmetum
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'idOrderItem' => $this->input->post('idOrderItem'),
				'metaKey' => $this->input->post('metaKey'),
				'metaValue' => $this->input->post('metaValue'),
            );
            
            $order_itemmetum_id = $this->Order_itemmetum_model->add_order_itemmetum($params);
            redirect('order_itemmetum/index');
        }
        else
        {            
            $data['_view'] = 'order_itemmetum/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a order_itemmetum
     */
    function edit($idMeta)
    {   
        // check if the order_itemmetum exists before trying to edit it
        $data['order_itemmetum'] = $this->Order_itemmetum_model->get_order_itemmetum($idMeta);
        
        if(isset($data['order_itemmetum']['idMeta']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'idOrderItem' => $this->input->post('idOrderItem'),
					'metaKey' => $this->input->post('metaKey'),
					'metaValue' => $this->input->post('metaValue'),
                );

                $this->Order_itemmetum_model->update_order_itemmetum($idMeta,$params);            
                redirect('order_itemmetum/index');
            }
            else
            {
                $data['_view'] = 'order_itemmetum/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The order_itemmetum you are trying to edit does not exist.');
    } 

    /*
     * Deleting order_itemmetum
     */
    function remove($idMeta)
    {
        $order_itemmetum = $this->Order_itemmetum_model->get_order_itemmetum($idMeta);

        // check if the order_itemmetum exists before trying to delete it
        if(isset($order_itemmetum['idMeta']))
        {
            $this->Order_itemmetum_model->delete_order_itemmetum($idMeta);
            redirect('order_itemmetum/index');
        }
        else
            show_error('The order_itemmetum you are trying to delete does not exist.');
    }
    
}
