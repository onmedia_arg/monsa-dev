<?php
include_once(FCPATH."/application/controllers/BaseController.php");
define('RECORDS_PER_PAGE', 30);
class Modelo_moto extends BaseController{

    private $user;

    function __construct()
    {
        parent::__construct();
        if ($this->is_monsa_login()) {
            $this->load->model('Modelo_moto_model');
            $this->user = $this->dataUser();
        }else{
            redirect('/', 'refresh');
        }
    } 

    /*
     * Listing of modelo_moto
     */
    function index()
    {
        $params['limit'] = RECORDS_PER_PAGE; 
        $params['offset'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
        
        $config = $this->config->item('pagination');
        $config['base_url'] = site_url('modelo_moto/index?');
        $config['total_rows'] = $this->Modelo_moto_model->get_all_modelo_moto_count();
        $this->pagination->initialize($config);

        $data['modelo_moto'] = $this->Modelo_moto_model->get_all_modelo_moto($params);
        
        $data['_view'] = 'modelo_moto/index';
        $data['user'] = $this->user;
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new modelo_moto
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'idMarcaMoto' => $this->input->post('idMarcaMoto'),
				'nombre' => $this->input->post('nombre'),
				'descripcion' => $this->input->post('descripcion'),
				'year' => $this->input->post('year'),
            );
            
            $modelo_moto_id = $this->Modelo_moto_model->add_modelo_moto($params);
            redirect('modelo_moto/index');
        }
        else
        {
			$this->load->model('Marca_moto_model');
			$data['all_marca_moto'] = $this->Marca_moto_model->get_all_marca_moto();
            
            $data['_view'] = 'modelo_moto/add';
            $data['user'] = $this->user;
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a modelo_moto
     */
    function edit($idModeloMoto)
    {   
        // check if the modelo_moto exists before trying to edit it
        $data['modelo_moto'] = $this->Modelo_moto_model->get_modelo_moto($idModeloMoto);
        
        if(isset($data['modelo_moto']['idModeloMoto']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'idMarcaMoto' => $this->input->post('idMarcaMoto'),
					'nombre' => $this->input->post('nombre'),
					'descripcion' => $this->input->post('descripcion'),
					'year' => $this->input->post('year'),
                );

                $this->Modelo_moto_model->update_modelo_moto($idModeloMoto,$params);            
                redirect('modelo_moto/index');
            }
            else
            {
				$this->load->model('Marca_moto_model');
				$data['all_marca_moto'] = $this->Marca_moto_model->get_all_marca_moto();

                $data['_view'] = 'modelo_moto/edit';
                $data['user'] = $this->user;
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The modelo_moto you are trying to edit does not exist.');
    } 

    /*
     * Deleting modelo_moto
     */
    function remove($idModeloMoto)
    {
        $modelo_moto = $this->Modelo_moto_model->get_modelo_moto($idModeloMoto);

        // check if the modelo_moto exists before trying to delete it
        if(isset($modelo_moto['idModeloMoto']))
        {
            $this->Modelo_moto_model->delete_modelo_moto($idModeloMoto);
            redirect('modelo_moto/index');
        }
        else
            show_error('The modelo_moto you are trying to delete does not exist.');
    }

    function fillTable(){
        $file = FCPATH.'/application/models/json/makesAndModels.json';

        $data = file_get_contents($file);

        $data_arr = json_decode($data, true);

        $arr_data = [];
        for ($i=0; $i < count($data_arr['Marcas']); $i++) {
            $params = array(
                'idMarcaMoto' => $data_arr['Marcas'][$i]['id'],
                'nombre' => $data_arr['Marcas'][$i]['nombre'],
                'slug' => strtolower(str_replace(' ', '-', $data_arr['Marcas'][$i]['nombre'])),
                'descripcion' => 'lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor',
            );
            array_push($arr_data, $params);
        }
        $this->Marca_model->add_marca_batch($arr_data);
    }
    
}
