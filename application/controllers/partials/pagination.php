<?php         

    $this->load->helper('url');
    $this->load->library('pagination');
    $config['base_url'] = $base;
    

    if($this->uri->segment($this->uri->total_segments())){
        $config["uri_segment"] = $this->uri->total_segments();
    }

   

    $config["full_tag_open"] = '<ul class="pagination">';
    $config["full_tag_close"] = '</ul>';    
    $config["first_link"] = "&laquo;";
    $config["first_tag_open"] = "<li>";
    $config["first_tag_close"] = "</li>";
    $config["last_link"] = "&raquo;";
    $config["last_tag_open"] = "<li>";
    $config["last_tag_close"] = "</li>";
    $config['next_link'] = '&gt;';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '<li>';
    $config['prev_link'] = '&lt;';
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '<li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';

$config['reuse_query_string'] = false;



    $choice = $config["total_rows"] / $config["per_page"];
    $config["num_links"] = 5;
    $config['use_page_numbers'] = true;
    $page = (is_numeric($this->uri->segment($config["uri_segment"] ))) ? $this->uri->segment($config["uri_segment"] ) : 0;

?>