<?php
 
class Login_error_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get login_error by ai
     */
    function get_login_error($ai)
    {
        return $this->db->get_where('login_errors',array('ai'=>$ai))->row_array();
    }
        
    /*
     * Get all login_errors
     */
    function get_all_login_errors()
    {
        $this->db->order_by('ai', 'desc');
        return $this->db->get('login_errors')->result_array();
    }
        
    /*
     * function to add new login_error
     */
    function add_login_error($params)
    {
        $this->db->insert('login_errors',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update login_error
     */
    function update_login_error($ai,$params)
    {
        $this->db->where('ai',$ai);
        return $this->db->update('login_errors',$params);
    }
    
    /*
     * function to delete login_error
     */
    function delete_login_error($ai)
    {
        return $this->db->delete('login_errors',array('ai'=>$ai));
    }
}
