<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Order_itm_model extends CI_Model{

	function __construct(){
		parent::__construct();


	}
 
    function addItems($data){
        $this->db->insert_batch('order_itm',$data);
        $this->db->error(); 
        return $this->db->insert_id();
    }

    /*
     * function to update user
     */
    function update_order_item( $idOrderItm, $params )
    {
        $this->db->where( 'idOrderItm', $idOrderItm );
        return $this->db->update( 'order_itm', $params );
    }
    
    /*
     * function to delete user
     */
    function delete_order_item( $idOrderItm )
    {
        
        $this->db->where( 'idOrderItm', $idOrderItm );
        $this->db->delete('order_itm');

        if ( $this->db->affected_rows() >= 1 ) {
            return true;
        }else{
            return false;
        }

    }

	function getItemsByOrder($orderid){

		// $r = $this->db->get_where('order_itm', array('idOrderHdr'=>$orderid));


        $r = $this->db->query("select i.*, p.sku, p.nombre, p.idProducto as id_producto, f.nombre as nombre_familia, m.nombre as nombre_marca
                     from order_itm as i
                     join producto as p on i.idProducto = p.idProducto
                     join familia as f on f.idFamilia = p.idFamilia
                     join marca as m on m.idMarca = p.idMarca
                    where i.idOrderHdr =" . $orderid );

		return $r->result();
    }
    
	function getItemsFromOrderStatus1($user_id){

        $response = "";

        $idOrderHdr = $this->db->query("SELECT idOrderHdr 
                                          from order_hdr
                                        where idOrderStatus = 1
                                        and createdBy = $user_id")->row();

        if($idOrderHdr != NULL){
            $items = $this->db->query("SELECT i.*, p.sku, p.nombre, h.idOrderStatus, h.createdBy 
                                         FROM order_itm as i
                                        INNER JOIN order_hdr as h on h.idOrderHdr = i.idOrderHdr
                                        INNER JOIN producto as p on i.idProducto = p.idProducto
                                        WHERE h.idOrderStatus = 1
                                          AND h.createdBy = $user_id" );

            // var_dump($idOrderHdr->idOrderHdr);die;
            
            $response = array(
                "idOrderHdr" => $idOrderHdr->idOrderHdr, 
                "items"      => $items->result_array()
            );
        }


        return $response;

	}    

    // Obtiene ordenes para Datatables con paginado de AJAX
    public function getItemsByOrderDT( $idOrderHdr, $start, $length, $search ){
      
// //      Query para levantar los datos
        $this->db->select('order_itm.*, producto.nombre')
                 ->from('order_itm')
                 ->join('producto', 'producto.idProducto = order_itm.idProducto')
                 ->where('idOrderHdr', $idOrderHdr );
        
        if( $search != null && trim($search) != "" ){

            $search_like = $this->db->escape("%".$search."%");

            $this->db->like( 'order_itm.idOrderHdr', $search_like )
                     ->or_like( 'order_itm.posnr', $search_like )
                     ->or_like( 'producto.nombre', $search_like )
                     ->or_like( 'order_itm.precio', $search_like )
                     ->or_like( 'order_itm.qty', $search_like )
                     ->or_like( 'order_itm.total', $search_like )
                     ->or_like( 'order_itm.options', $search_like ) ; 

        }

        $this->db->order_by('idOrderHdr', 'DESC')
                 ->limit($length, $start);

        $r = $this->db->get()->result_array();

// //      Misma query sin LIMIT para contar los registros
        $this->db->select('order_itm.*, producto.nombre')
                 ->from('order_itm')
                 ->join('producto', 'producto.idProducto = order_itm.idProducto')
                 ->where('idOrderHdr', $idOrderHdr );

        if( $search != null && trim($search) != "" ){

            $search_like = $this->db->escape("%".$search."%");

            $this->db->like( 'order_itm.idOrderHdr', $search_like )
                     ->or_like( 'order_itm.posnr', $search_like )
                     ->or_like( 'producto.nombre', $search_like )
                     ->or_like( 'order_itm.precio', $search_like )
                     ->or_like( 'order_itm.qty', $search_like )
                     ->or_like( 'order_itm.total', $search_like )
                     ->or_like( 'order_itm.options', $search_like ) ; 

        }

        $r2 = $this->db->get();

        $response = array(
                        "numRows" => $r2->num_rows(), 
                        "datos"   => $r
                    );

        return $response;

    } 

    public function getNumRowAllDT( $idOrderHdr ){

        $this->db->select('idOrderHdr')
                 ->from('order_itm')
                 ->where('idOrderHdr', $idOrderHdr );

        return $this->db->count_all_results();

    }

    public function clearItemsFromOrder($idOrderHdr){

        $this->db->where( 'idOrderHdr', $idOrderHdr );
        $this->db->delete('order_itm');

        if ( $this->db->affected_rows() >= 1 ) {
            return true;
        }else{
            return false;
        }


    }

}