<?php
 
class Logs_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    function insertLog($data){
        //print_r($data);exit();
        $this->db->insert('logs', $data);
        $query = $this->db->last_query();
        return $query;
    }

}