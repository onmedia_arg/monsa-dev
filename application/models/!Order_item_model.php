<?php
 
class Order_item_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get order_item by idOrderItem
     */
    function get_order_item($idOrderItem)
    {
        return $this->db->get_where('order_item',array('idOrderItem'=>$idOrderItem))->row_array();
    }
        
    /*
     * Get all order_item
     */
    function get_all_order_item()
    {
        $this->db->order_by('idOrderItem', 'desc');
        return $this->db->get('order_item')->result_array();
    }
        
    /*
     * function to add new order_item
     */
    function add_order_item($params)
    {
        $this->db->insert('order_item',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update order_item
     */
    function update_order_item($idOrderItem,$params)
    {
        $this->db->where('idOrderItem',$idOrderItem);
        return $this->db->update('order_item',$params);
    }
    
    /*
     * function to delete order_item
     */
    function delete_order_item($idOrderItem)
    {
        return $this->db->delete('order_item',array('idOrderItem'=>$idOrderItem));
    }
}
