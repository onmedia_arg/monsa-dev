<?php
 
class Marca_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get marca by idMarca
     */
    function get_marca($idMarca)
    {
        return $this->db->get_where('marca',array('idMarca'=>$idMarca))->row_array();
    }
        
    /*
     * Get all marca
     */
    function get_all_marca()
    {
        $this->db->order_by('nombre', 'asc');
        return $this->db->get('marca')->result_array();
    }
        
    /*
     * function to add new marca
     */
    function add_marca($params)
    {
        $this->db->insert('marca',$params);
        return $this->db->insert_id();
    }

    function add_marca_batch($arr)
    {
        $this->db->insert_batch('marca',$arr);
    }
    
    /*
     * function to update marca
     */
    function update_marca($idMarca,$params)
    {
        $this->db->where('idMarca',$idMarca);
        return $this->db->update('marca',$params);
    }
    
    /*
     * function to delete marca
     */
    function delete_marca($idMarca)
    {
        return $this->db->delete('marca',array('idMarca'=>$idMarca));
    }

    /*
     * return count
     */
    function marca_has_products( $idMarca )
    {

        $query = $this->db->get_where( 'producto', array( 'idMarca' => $idMarca ) ); 

        if ( $query->num_rows() >= 1 ) {
            return true;
        }else{
            return false;
        }

    } 

    function update_sort( $data ){ 

        $this->db->update_batch('marca', $data, 'idMarca');

        if ( $this->db->affected_rows() >= 1 ) {
            return true;
        }else{
            return false;
        }

    }

}
