<?php
 
class clientes_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('RelClienteUsuario_model');
        $this->load->model('user_model');
    }
    
    /*
     * Get clientes by idCliente
     */
    function get_cliente($idCliente)
    {
        return $this->db->get_where( 'clientes', array( 'idCliente'=>$idCliente, 'deleted' => 0 ) )->row_array();
    }

    /*
     * Get clientes by idCliente
     */
    function get_client_by_user( $idUsuario )
    {
        
        $rel = $this->db->get_where( 'relclienteusuario', array( 'idUsuario' => $idUsuario ) )->row_array();

        return $this->db->get_where( 'clientes', array( 'idCliente'=> $rel['idCliente'], 'deleted' => 0 ) )->row_array();

    }

    /*
     * Obtiene los usuarios de un cliente
     */
    function get_client_users( $idCliente )
    {
        
        $result = $this->db->select('*')
                        ->from( 'relclienteusuario' )
                        ->join( 'users', 'users.user_id = relclienteusuario.idUsuario' )
                        ->where( array( 'idCliente' => $idCliente ) )
                        ->where( array( 'users.deleted' => 0 ) ) 
                        ->get()
                        ->result_array();

        return $result;

    }

    /*
     * Get clientes by idCliente
     */
    function user_has_client( $idUsuario )
    {
        
        $count = $this->db->get_where('relclienteusuario', array( 'idUsuario' => $idUsuario ) )->num_rows();

        if ( $count >= 1 ) {
            return true;
        }else{
            return false;
        }

    }

    /*
     * Get all clientes
     */
    function get_all_clientes( $show_deleted = false )
    {
        $this->db->order_by('idCliente', 'desc');

        if ( $show_deleted == false ) {
            $this->db->where('deleted', 0 );
        }

        return $this->db->get('clientes')->result_array();
    }
        
    /*
     * function to add new clientes
     */
    function add_cliente($params)
    {
        $this->db->insert('clientes',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update clientes
     */
    function update_cliente($idCliente,$params)
    {
        $this->db->where('idCliente',$idCliente);
        return $this->db->update('clientes',$params);
    }
    
    /*
     * function to delete clientes
     */
    function delete_cliente($idCliente)
    {

        $this->update_cliente( $idCliente, array( 'deleted' => 1, 'is_activo' => 0 ) );

        $rel = $this->RelClienteUsuario_model->get_rel_by_client( $idCliente );

        foreach ($rel as $r => $data) {
            
            $this->user_model->update_user( $data['idUsuario'], array( 'deleted' => 1, 'is_active' => 0 ) );

        }

        if ( $this->db->affected_rows() >= 1 ) {
            return true;
        }else{
            return false;
        }

    }   

    public function client_is_active( $client_id ){

        $client = $this->Clientes_model->get_cliente( $client_id );

        if ( $client['is_activo'] == 1 ) {

            return true;

        }else{
            
            return false;

        }

    }

    public function client_is_active_by_user( $user_id ){

        $rel = $this->RelClienteUsuario_model->get_rel_by_user( $user_id );
        $client = $this->Clientes_model->get_cliente( $rel['idCliente'] );

        if ( $client['is_activo'] == 1 ) {

            return true;

        }else{
            
            return false;

        }

    }
}
