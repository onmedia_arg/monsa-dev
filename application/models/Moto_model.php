<?php
 
class Moto_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get moto by idMoto
     */
    function get_moto($idMoto)
    {
        return $this->db->get_where('moto',array('idMoto'=>$idMoto))->row_array();
    }
        
    /*
     * Get all moto
     */
    function get_all_moto()
    {
        $this->db->order_by('idMoto', 'desc');
        return $this->db->get('moto')->result_array();
    }

    function get_all_moto_join()
    {
        $this->db->select('
                            mo.idMoto,
                            mo.sku,
                            ma.idMarca,
                            ma.nombre as marca,
                            md.nombre as modelo,
                            md.idModelo,
                            mo.year,
                            mo.name as nombre,
                            mo.descripcion
                        ');
        $this->db->join('marca as ma', 'ma.idMarca = mo.idMarca', 'OUTER LEFT');
        $this->db->join('modelo as md', 'md.idModelo = mo.idModelo', 'OUTER LEFT');
        $this->db->order_by('mo.idMoto', 'desc');
        $query = $this->db->get('moto as mo');
        return $query->result_array();
    }

    function get_moto_by_make_model_year($make, $model, $year)
    {
        if ($make !='') {
            $make = strtolower($make);
        }
        if ($model !='') {
            $model = strtolower($model);
        }
        $this->db->select('
                            mo.idMoto,
                            mo.sku,
                            mo.idMarca,
                            ma.nombre as marca,
                            md.nombre as modelo,
                            mo.idModelo,
                            mo.year,
                            mo.name as nombre,
                            mo.descripcion
                        ');
        $this->db->join('marca as ma', 'ma.idMarca = mo.idMarca', 'OUTER LEFT');
        $this->db->join('modelo as md', 'md.idModelo = mo.idModelo', 'OUTER LEFT');
        $this->db->where('mo.idMarca',$make);
        if ($model !='') {
            $this->db->where('mo.idModelo',$model);
        }
        if ($year !='') {
            $this->db->where('mo.year',$year);
        }
        $this->db->order_by('mo.idMoto', 'desc');
        $query = $this->db->get('moto as mo');
        return $query->result_array();
    }

    /*
     * function to add new moto
     */
    function add_moto($params)
    {
        $this->db->insert('moto',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update moto
     */
    function update_moto($idMoto,$params)
    {
        $this->db->where('idMoto',$idMoto);
        return $this->db->update('moto',$params);
    }
    
    /*
     * function to delete moto
     */
    function delete_moto($idMoto)
    {
        return $this->db->delete('moto',array('idMoto'=>$idMoto));
    }
}
