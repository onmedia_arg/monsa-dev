<?php
 
class Marca_moto_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get marca_moto by idMarcaMoto
     */
    function get_marca_moto($idMarcaMoto)
    {
        return $this->db->get_where('marca_moto',array('idMarcaMoto'=>$idMarcaMoto))->row_array();
    }
        
    /*
     * Get all marca_moto
     */
    function get_all_marca_moto()
    {
        $this->db->order_by('idMarcaMoto', 'desc');
        return $this->db->get('marca_moto')->result_array();
    }
        
    /*
     * function to add new marca_moto
     */
    function add_marca_moto($params)
    {
        $this->db->insert('marca_moto',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update marca_moto
     */
    function update_marca_moto($idMarcaMoto,$params)
    {
        $this->db->where('idMarcaMoto',$idMarcaMoto);
        return $this->db->update('marca_moto',$params);
    }
    
    /*
     * function to delete marca_moto
     */
    function delete_marca_moto($idMarcaMoto)
    {
        return $this->db->delete('marca_moto',array('idMarcaMoto'=>$idMarcaMoto));
    }

    function add_marca_batch($arr)
    {
        $this->db->insert_batch('marca_moto',$arr);
    }
}
