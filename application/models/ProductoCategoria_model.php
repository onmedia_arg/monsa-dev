<?php
 
class ProductoCategoria_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get productCat by idProductCat
     */
    function get_productCat($idProdCat)
    {
        return $this->db->get_where('producto_categoria',array('idProdCat'=>$$idProdCat))->row_array();
    }

    function get_cats_by_idProduct($idProducto)
    {
        $this->db->select('
                            pc.idProdCat,
                            pc.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ca.idCategoria,
                            ca.nombre as categoria,
                            ca.slug
                        ');
        $this->db->join('categoria as ca', 'ca.idCategoria = pc.idCategoria', 'OUTER LEFT');
        $this->db->join('familia as fa', 'fa.idFamilia = ca.idFamilia', 'OUTER LEFT');
        $this->db->where('pc.idProducto', $idProducto );
        $this->db->order_by('ca.nombre', 'asc');
        $query = $this->db->get('producto_categoria as pc');
        
        return $query->result_array();
    }
        
    /*
     * Get all productCat
     */
    function get_all_productCat()
    {
        $this->db->order_by('idProdCat', 'desc');
        return $this->db->get('producto_categoria')->result_array();
    }
        
    /*
     * function to add new productCat
     */
    function add_productCat($params)
    {
        $this->db->insert('producto_categoria',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update productCat
     */
    function update_productCat($idProdCat,$params)
    {
        $this->db->where('idProdCat',$idProdCat);
        return $this->db->update('producto_categoria',$params);
    }
    
    /*
     * function to delete productCat
     */
    function delete_productCat($idProdCat)
    {
        return $this->db->delete('producto_categoria',array('idProdCat'=>$idProdCat));
    }
}
