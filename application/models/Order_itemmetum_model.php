<?php
 
class Order_itemmetum_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get order_itemmetum by idMeta
     */
    function get_order_itemmetum($idMeta)
    {
        return $this->db->get_where('order_itemmeta',array('idMeta'=>$idMeta))->row_array();
    }
        
    /*
     * Get all order_itemmeta
     */
    function get_all_order_itemmeta()
    {
        $this->db->order_by('idMeta', 'desc');
        return $this->db->get('order_itemmeta')->result_array();
    }
        
    /*
     * function to add new order_itemmetum
     */
    function add_order_itemmetum($params)
    {
        $this->db->insert('order_itemmeta',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update order_itemmetum
     */
    function update_order_itemmetum($idMeta,$params)
    {
        $this->db->where('idMeta',$idMeta);
        return $this->db->update('order_itemmeta',$params);
    }
    
    /*
     * function to delete order_itemmetum
     */
    function delete_order_itemmetum($idMeta)
    {
        return $this->db->delete('order_itemmeta',array('idMeta'=>$idMeta));
    }
}
