<?php

class Producto_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_products_promotions()
    {

        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            ma.slug as marcaSlug,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.stock_type,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones,
                            pr.is_promo_active,
                            pr.price_promo,
                            pr.idTag,
                            tags.name as tag,
                            pr.cantidad_minima,
                            pr.rango
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->join('tags', 'pr.idTag = tags.id', 'LEFT');
        $this->db->where('pr.is_promo_active', 1);


        $query = $this->db->get('producto as pr');
        return $query->result_array();

    }

    function get_producto($idProducto, $return = 'row_array')
    {

        switch ($return) {
            case 'row_array':
                return $this->db->get_where('producto', array('idProducto' => $idProducto))->row_array();
                break;

            case 'num_rows':
                return $this->db->get_where('producto', array('idProducto' => $idProducto))->num_rows();
                break;

            default:
                return $this->db->get_where('producto', array('idProducto' => $idProducto))->row_array();
                break;
        }

    }

    function get_producto_by_sku($sku = '', $return = 'row_array')
    {

        switch ($return) {
            case 'row_array':
                return $this->db->get_where('producto', array('sku' => $sku))->row_array();
                break;

            case 'num_rows':
                return $this->db->get_where('producto', array('sku' => $sku))->num_rows();
                break;

            default:
                return $this->db->get_where('producto', array('sku' => $sku))->row_array();
                break;
        }

    }

    function get_product_by_slug($slug = '', $return = 'row_array')
    {

        switch ($return) {
            case 'row_array':
                return $this->db->get_where('producto', array('slug' => $slug))->row_array();
                break;

            case 'num_rows':
                return $this->db->get_where('producto', array('slug' => $slug))->num_rows();
                break;

            default:
                return $this->db->get_where('producto', array('slug' => $slug))->row_array();
                break;
        }

    }

    function get_product_by_str($str)
    {
        $this->db->select('
                            pr.idProducto,
                            pr.idTipo,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones,
                            pr.idParent,
                            pr.alto,
                            pr.ancho,
                            pr.largo,
                            pr.rango,
                            pr.cantidad_minima,
                            pr.idTag,
                            tags.name as tag,
                            pr.is_promo_active,
                            pr.price_promo
                        ');
        $this->db->from('producto as pr');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->join('tags', 'pr.idTag = tags.id', 'LEFT');
        $this->db->order_by('pr.idProducto', 'desc');
        $this->db->where('pr.slug', rawurldecode($str));
        // var_dump( $this->db->get_compiled_select() );
        // die();
        $query = $this->db->get();

        return $query->result_array();

    }

    function get_all_producto($page = 0, $perpage = 20, $fam = '', $marca = '')
    {
        $page = $page - 1;
        if ($page < 0) {
            $page = 0;
        }
        $from = $page * $perpage;

        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            ma.slug as marcaSlug,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->limit($perpage, $from);
        if ($this->session->userdata('preciomin') && $this->session->userdata('preciomax'))
            $this->db->where('pr.precio BETWEEN "' . $this->session->userdata('preciomin') . '" AND "' . $this->session->userdata('preciomax') . '"');
        if ($this->session->userdata('ord'))
            $this->db->order_by('pr.idProducto', $this->session->userdata('ord'));
        else
            $this->db->order_by('pr.idProducto', 'desc');
        if ($fam != '' && $fam != "todas") {
            $this->db->where('fa.slug', $fam);
        }
        if ($marca != '' && !is_numeric($marca)) {
            $this->db->where('ma.slug', $marca);
        }

        $query = $this->db->get('producto as pr');
        return $query->result_array();

    }

    public function getAllProducto($start, $length, $search)
    {

        $where = "";

        if ($search != null && trim($search) != "") {

            $search_like = $this->db->escape("%" . $search . "%");

            $where = " where (fa.nombre  LIKE " . $search_like . " OR
                              ma.nombre  LIKE " . $search_like . " OR
                              pr.modelo  LIKE " . $search_like . " OR
                              pr.sku     LIKE " . $search_like . " OR
                              pr.stock   LIKE " . $search_like . " )";
        }

        // //      Query para levantar los datos
        $r = $this->db->query("SELECT pr.idProducto,
                                fa.idFamilia,
                                fa.nombre AS familia,
                                fa.slug AS familiaSlug,
                                ma.idMarca,
                                ma.nombre AS marca,
                                ma.slug AS marcaSlug,
                                pr.nombre,
                                pr.slug,
                                pr.modelo,
                                pr.descripcion,
                                pr.sku,
                                pr.precio,
                                pr.stock,
                                pr.peso,
                                pr.visibilidad,
                                pr.state,
                                pr.imagen,
                                pr.dimensiones
                            FROM producto AS pr
                            LEFT OUTER JOIN familia AS fa ON fa.idFamilia = pr.idFamilia 
                            LEFT OUTER JOIN marca   AS ma ON ma.idMarca = pr.idMarca
                            " . $where
            . " ORDER BY pr.idProducto ASC LIMIT $start, $length");

        // //      Misma query sin LIMIT para contar los registros
        $r2 = $this->db->query("SELECT pr.idProducto,
                                fa.idFamilia,
                                fa.nombre AS familia,
                                fa.slug AS familiaSlug,
                                ma.idMarca,
                                ma.nombre AS marca,
                                ma.slug AS marcaSlug,
                                pr.nombre,
                                pr.slug,
                                pr.modelo,
                                pr.descripcion,
                                pr.sku,
                                pr.precio,
                                pr.stock,
                                pr.peso,
                                pr.visibilidad,
                                pr.state,
                                pr.imagen,
                                pr.dimensiones
                            FROM producto AS pr
                            LEFT OUTER JOIN familia AS fa ON fa.idFamilia = pr.idFamilia 
                            LEFT OUTER JOIN marca   AS ma ON ma.idMarca = pr.idMarca
                            " . $where);

        $response = array(
            "numRows" => $r2->num_rows(),
            "datos" => $r
        );

        return $response;

    }

    public function getNumRowAll()
    {

        $r = $this->db->get('producto');

        return $r->num_rows();

    }

    function get_all_producto_count($fam = '', $marca = '')
    {


        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            ma.slug as marcaSlug,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');

        if ($this->session->userdata('ord'))
            $this->db->order_by('pr.idProducto', $this->session->userdata('ord'));
        else
            $this->db->order_by('pr.idProducto', 'desc');
        if ($fam != '' && $fam != "todas") {
            $this->db->where('fa.slug', $fam);
        }
        if ($this->session->userdata('preciomin') && $this->session->userdata('preciomax'))
            $this->db->where('pr.precio BETWEEN "' . $this->session->userdata('preciomin') . '" AND "' . $this->session->userdata('preciomax') . '"');
        if ($marca != '' && !is_numeric($marca)) {
            $this->db->where('ma.slug', $marca);
        }
        $query = $this->db->get('producto as pr');
        return $query->result_array();
    }

    function get_all_producto_con_img($page = 0, $perpage = 20, $fam = '')
    {
        $page = $page - 1;
        if ($page < 0) {
            $page = 0;
        }
        $from = $page * $perpage;

        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->where('pr.imagen != ""');
        $this->db->where('pr.imagen != "[]"');
        $this->db->limit($perpage, $from);
        $this->db->order_by('pr.idProducto', 'desc');
        if ($fam != '') {
            $this->db->where('fa.slug', $fam);
        }
        $query = $this->db->get('producto as pr');

        return $query->result_array();
    }

    function get_all_producto_con_img_count($fam = '')
    {
        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->where('pr.imagen != ""');
        $this->db->where('pr.imagen != "[]"');
        $this->db->order_by('pr.idProducto', 'desc');
        if ($fam != '') {
            $this->db->where('fa.slug', $fam);
        }
        $query = $this->db->get('producto as pr');
        return $query->result_array();
    }

    function get_producto_by_cat($cat = '')
    {
        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones, 
                            ca.idCategoria, 
                            ca.nombre as categoria,
                            ca.slug as categoriaSlug,
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->join('producto_categoria as pc', 'pc.idProducto = pr.idProducto', 'OUTER LEFT');
        $this->db->join('categoria as ca', 'ca.idCategoria = pc.idCategoria', 'OUTER LEFT');
        $this->db->order_by('pr.idProducto', 'desc');
        if ($cat != '') {
            $this->db->where('ca.slug', $cat);
        }
        $query = $this->db->get('producto as pr');
        return $query->result_array();
    }

    function get_producto_by_id($id = '')
    {
        $this->db->select('
                            pr.idProducto,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            ma.idMarca,
                            ma.nombre as marca,
                            pr.nombre,
                            pr.slug,
                            pr.modelo,
                            pr.descripcion,
                            pr.sku,
                            pr.precio,
                            pr.stock,
                            pr.peso,
                            pr.visibilidad,
                            pr.state,
                            pr.imagen,
                            pr.dimensiones, 
                            ca.idCategoria, 
                            ca.nombre as categoria,
                            ca.slug as categoriaSlug,
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = pr.idFamilia', 'OUTER LEFT');
        $this->db->join('marca as ma', 'ma.idMarca = pr.idMarca', 'OUTER LEFT');
        $this->db->join('producto_categoria as pc', 'pc.idProducto = pr.idProducto', 'OUTER LEFT');
        $this->db->join('categoria as ca', 'ca.idCategoria = pc.idCategoria', 'OUTER LEFT');
        $this->db->order_by('pr.idProducto', 'desc');
        if ($id != '') {
            $this->db->where('pr.idProducto', $id);
        }
        $query = $this->db->get('producto as pr');
        return $query->result_array();
    }

    /*
     * function to add new producto
     */
    function add_producto($params)
    {
        $this->db->insert('producto', $params);
        $sql = $this->db->last_query();
        return $this->db->insert_id();
    }

    /*
     * function to update producto
     */
    function update_producto($idProducto, $params)
    {
        $this->db->where('idProducto', $idProducto);
        return $this->db->update('producto', $params);
    }

    /*
     * function to delete producto
     */
    function delete_producto($idProducto)
    {
        return $this->db->delete('producto', array('idProducto' => $idProducto));
    }

    function make_query($marca, $familia, $atributo = null, $vista = 'default', $search)
    {
        // Left join with tags to handle null tag IDs
        $this->db->select('producto.*, familia.orden, COALESCE(tags.name, "") as tag')
            ->from('producto')
            ->join('familia', 'producto.idFamilia = familia.idFamilia')
            ->join('tags', 'producto.idTag = tags.id', 'left')
        ;

        // $this->db->select('producto.*, familia.orden, tags.name as tag')
        //          ->from('producto')
        //          ->join('familia', 'producto.idFamilia = familia.idFamilia')
        //          ->join('tags', 'producto.idTag = tags.id')
        //          ; 

        // Visibilidad
        $this->db->where('visibilidad', "publicar");

        switch ($vista) {

            // Simple y variacion
            case 'list':
                $this->db->where_in('producto.idTipo', [2, 0]);
                break;

            // Simple y variable
            case 'default':
                $this->db->where_in('producto.idTipo', [1, 0]);
                break;

            // Simple y variable
            default:
                $this->db->where_in('producto.idTipo', [1, 0]);
                break;

        }

        if (isset($marca) && $marca !== null) {
            $this->db->where_in('producto.idMarca', $marca);

        }

        if (isset($familia) && $familia !== null) {
            $this->db->where_in('producto.idFamilia', $familia);
        }



        if (isset($search) && $search !== null && $search !== "") {

            // if(str_word_count($search, 0) > 0 ) {
            //     $terminos = explode(" ", $search);
            // }
            $where_like = "";
            if ($terminos = explode(" ", $search)) {
                $where_like .= " or ( ";

                foreach ($terminos as $valor) {
                    $where_like .= " producto.search like '%" . $valor . "%' and";
                }

                $where_like = preg_replace("/and$/", ' ', $where_like, -1);
                $where_like .= ")";
            }


            $this->db->where(
                "( producto.modelo   like '%" . $search . "%' 
                                OR producto.sku    like '%" . $search . "%'
                               " . $where_like . ")",
                NULL,
                FALSE
            );

        }


        $this->db->order_by('familia.orden', 'ASC');

        return $this->db->get_compiled_select();
    }

    function countAll($data)
    {

        $query = $this->make_query($data['marca'], $data['familia'], $data['atributo'], $data['vista'], $data['search']);
        $return = $this->db->query($query)->result_array();

        // Filtra el resultado segun atributos 
        if (isset($data['atributo']) && $data['atributo'] !== null) {
            $return = $this->filter_attributes($return, $data['atributo']);
        }

        return count($return);

    }

    // $items = QUERY->result_array()
    // $valores = array
    public function filter_attributes($items, $atributos)
    {

        // Contador
        $count = 0;
        $nuevo = array();

        // Recorre el arreglo y compara si tiene el filtro configurado
        foreach ($items as $item => $data) {

            $existe = false;

            // Si no existe procede  
            foreach ($atributos as $atributo) {

                // Revisa si tiene el atributo
                $check = $this->product_has_atribute($data['idProducto'], $atributo['idAttribute'], $atributo['value']);
                // Revisa si existe el producto en el nuevo array
                foreach ($nuevo as $o => $v) {
                    if ((int) $v['idProducto'] == (int) $data['idProducto']) {
                        $existe = true;
                    }
                }
                // Si tiene el atributo agrega a nuevo array
                if ($check && $existe == false) {
                    $nuevo[] = $items[$count];
                }

            }

            $count++;

        }

        return $nuevo;

    }

    // $items = QUERY->result_array()
    // $filter = array
    public function product_has_atribute($idProducto, $idAtributo, $valor)
    {

        $row = $this->db->select('*')
            ->from('producto_atributo')
            ->where('idAtributo', $idAtributo)
            ->where('idProducto', $idProducto)
            ->get()
            ->row_array();

        if (isset($row)) {
            switch ($row['is_variacion']) {

                case 1:
                case '1':

                    $valores = json_decode($row['valores'], true);

                    break;

                case 0:
                case '0':
                case '':
                case null:
                default:
                    $valores = [$row['valores']];
                    break;

            }
        }

        if (!empty($valores) && is_array($valores)) {

            if (in_array($valor, $valores)) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }

    }

    function fetch_data($data)
    {

        $query = $this->make_query($data['marca'], $data['familia'], $data['atributo'], $data['vista'], $data['search']);
        // $query .= ' LIMIT '. $data['start'].', ' . $data['limit']; 

        $return = $this->db->query($query)->result_array();

        // print_r( $this->db->last_query() );

        // Filtra el resultado segun atributos 
        if (isset($data['atributo']) && $data['atributo'] !== null) {
            $return = $this->filter_attributes($return, $data['atributo']);
            $return = array_values($return);
            // Si no hay atributos devuelve el array
        } else {
            $return = $return;
        }

        // Valores de paginacion
        $output = array();

        // Ordenar paginacion
        // Limite de paginacion
        $limit = $data['start'] + $data['limit'];
        // Revisar si la cantidad de elementos es superior a la paginacion, si no usa la cantidad de elementos
        $limitCheck = (count($return) > $limit) ? $limit : count($return);

        for ($i = (int) $data['start']; $i < (int) $limitCheck; $i++) {
            $output[] = $return[$i];
        }

        return $output;

    }

    function get_products_home()
    {

        $r = $this->db->query('SELECT pr.*,
                                 fa.nombre as familia,
                                 ma.nombre as marca,
                                 tags.name as tag
                             from producto as pr
                            inner join familia as fa on fa.idFamilia = pr.idFamilia
                            inner join marca as ma on ma.idMarca = pr.idMarca
                            left join tags on tags.id = pr.idTag
                            where pr.show = "destacado"
                              and pr.visibilidad = "publicar"
                            limit 6');

        return $r->result_array();
    }

    function get_products_masvendidos()
    {

        $r = $this->db->query('SELECT pr.*,
                                 fa.nombre as familia,
                                 ma.nombre as marca,
                                 tags.name as tag
                          from producto as pr
                          inner join familia as fa on fa.idFamilia = pr.idFamilia
                          inner join marca as ma on ma.idMarca = pr.idMarca
                          left join tags on tags.id = pr.idTag
                          where pr.show = "masvendido"
                           and pr.visibilidad = "publicar"
                           limit 6');

        return $r->result_array();
    }

    function get_products_destacado()
    {
        $this->db->select('
                            pr.idProducto,
                            pr.nombre,
                            pr.modelo,
                            pr.slug,
                            pr.precio,
                            pr.imagen,
                        ');
        // $this->db->where('destacado',1); 
        $this->db->where('show', 'destacado');
        $this->db->limit(8);
        $query = $this->db->get('producto as pr');
        return $query->result_array();
    }

    function get_child_by_parent($idParent)
    {

        $query = " SELECT pr.idProducto, 
                          pr.nombre as prod_nombre,
                          pr.sku,
                          pr.precio, 
                          at.idAtributo, 
                          at.valores, attr.nombre, attr.slug 
                     FROM producto as pr
                    inner join producto_atributo as at on pr.idProducto = at.idProducto
                    inner join atributo as attr on at.idAtributo = attr.idAtributo
                    where pr.idParent = " . $idParent .
            " order by at.idProducto asc";

        $r = $this->db->query($query);

        // $r  = $this->db->get_where('producto',array('idParent'=>$idParent));

        return $r->result_array();


    }

    function get_childs_by_parent($idParent)
    {

        // $query = " SELECT idProducto, 
        //                   nombre,
        //                   sku,
        //                   precio
        //              FROM producto
        //             where idParent = " . $idParent ;

        // $r = $this->db->query($query);

        $r = $this->db->get_where('producto', array('idParent' => $idParent));

        return $r->result_array();
    }


    /*
     * function to add new producto
     * @return Insert ID
     */
    public function add_variacion($data)
    {

        $query = $this->db->insert('producto_atributo', $data);

        if ($query !== true) {
            return false;
        } else {
            $sql = $this->db->last_query();
            return $this->db->insert_id();
        }

    }

    /*
     * function to edit variacion
     * @return Insert ID
     */
    public function update_variacion($idProducto, $idAtributo, $data)
    {
        $this->db->where('idProducto', $idProducto)
            ->where('idAtributo', $idAtributo);
        $delete = $this->db->delete('producto_atributo');

        if ($delete) {

            // Guardar
            $insert = $this->db->insert('producto_atributo', $data);

            if ($insert !== true) {
                // No se pudo guardar
                return false;
            } else {
                $sql = $this->db->last_query();
                return $this->db->insert_id();
            }

        } else {
            // No se pudo borrar
            return false;
        }

    }


    public function get_variations_options($idProducto = null, $idAtributo = null)
    {

        $atributos = $this->db->where([
            'idProducto' => $idProducto,
            'idAtributo' => $idAtributo,
            'is_variacion' => 1
        ])
            ->get('producto_atributo')
            ->result_array();

        $options = array();

        foreach ($atributos as $atributo) {

            if (is_array(json_decode($atributo['valor'], true))) {

                $json = json_decode($atributo['valor']);

                foreach ($json as $value) {
                    $options[] = $value;
                }

            }

        }

    }

    function get_product_atributes($id)
    {

        $r = $this->db->query('SELECT valores
                          from producto_atributo 
                          where idProducto = ' . $id);

        return $r->result_array();
    }

}
