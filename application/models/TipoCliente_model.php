<?php
 
class TipoCliente_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get tc by idTipoCliente
     */
    function get_tc($idTipoCliente)
    {
        return $this->db->get_where('tipocliente',array('idTipoCliente'=>$idTipoCliente))->row_array();
    }
        
    /*
     * Get all tc
     */
    function get_all_tc()
    {
        $this->db->order_by('texto', 'asc');
        return $this->db->get('tipocliente')->result_array();
    }
        
    /*
     * function to add new tc
     */
    function add_tc($params)
    {
        $this->db->insert('tipocliente',$params);
        return $this->db->insert_id();
    }

    function add_tc_batch($arr)
    {
        $this->db->insert_batch('tipocliente',$arr);
    }
    
    /*
     * function to update tc
     */
    function update_tc($idTipoCliente,$params)
    {
        $this->db->where('idTipoCliente',$idTipoCliente);
        return $this->db->update('tipocliente',$params);
    }
    
    /*
     * function to delete tc
     */
    function delete_tc($idTipoCliente)
    {
        return $this->db->delete('tipocliente',array('idTipoCliente'=>$idTipoCliente));
    }

    /*
     * return count
     */
    function tc_has_products( $idTipoCliente )
    {

        $query = $this->db->get_where( 'producto', array( 'idTipoCliente' => $idTipoCliente ) ); 

        if ( $query->num_rows() >= 1 ) {
            return true;
        }else{
            return false;
        }

    } 

    function update_sort( $data ){ 

        $this->db->update_batch('tipocliente', $data, 'idTipoCliente');

        if ( $this->db->affected_rows() >= 1 ) {
            return true;
        }else{
            return false;
        }

    }

}
