<?php
 
class Productosingle_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
        $this->load->model('manager/ProductCarrousel_model', 'ProductCarrousel', true);
    }

    function get_detail($idProducto){

        $r = $this->db->query("SELECT *, tags.name as tag_name from producto 
                                left join tags on producto.idTag = tags.id
                                where idProducto = $idProducto");

        return $r->row_array();

    }

    function updateProduct($id, $data){

        if ( $data == null ) {return false;}

        $update =  $this->db->where( 'idProducto', $id )
                            ->update( 'producto', $data );

        if ( $update ) {
            return true;
        }else{
            return false;
        }    
    }

    function delete_producto($idProducto){
        return $this->db->delete('producto', array('idProducto' => $idProducto));   
    }

}