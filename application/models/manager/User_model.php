<?php
 
class User_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('manager/Marca_model', 'Marcas');

    }
    
    /*
     * Get user by user_id
     */
    function get_user($user_id)
    {
        return $this->db->get_where('users', array('user_id'=> $user_id, 'deleted' => 0 ) )->row_array();
    }

    function verify_user_nick($nick)
    {
        return $this->db->get_where('users', array('username'=> $nick, 'deleted' => 0 ) )->row_array();
    }
    function verify_user_email($nick)
    {
        return $this->db->get_where('users', array('email'=> $nick, 'deleted' => 0 ) )->row_array();
    }
        
    /*
     * Get all users
     */
    function get_all_users( $show_deleted = false )
    {
        $this->db->order_by('user_id', 'desc');
        if ( $show_deleted == false ) {
            $this->db->where('deleted', 0 );
        }
        return $this->db->get('users')->result_array();
    }
        
    /*
     * function to add new user
     */
    function add_user($params)
    {
        $this->db->insert('users',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update user
     */
    function update_user($user_id,$params)
    {
        $this->db->where('user_id',$user_id);
        return $this->db->update('users',$params);
    }
    
    /*
     * function to delete user
     */
    function delete_user($user_id)
    {
        
        $this->update_user( $user_id, array( 'deleted' => 1 ) );

        if ( $this->db->affected_rows() >= 1 ) {
            return true;
        }else{
            return false;
        }

    }

    public function user_is_active( $user_id ){
 
        $user = $this->user_model->get_user( $user_id );

        if ( $user['is_active'] == 1 ) {

            return true;

        }else{
            
            return false;

        }

    }

    // Funciones para la vista de usuarios.
    function getAllUsers($start, $length, $search, $order){
        
        $where = "";

        if($search != null && trim($search) != ""){
            $search_like = $this->db->escape("%".$search."%");
            $where = " where (u.user_id  LIKE " .$search_like . " OR
                                u.username  LIKE " .$search_like . " OR
                                u.email  LIKE " .$search_like . " OR
                                u.auth_level  LIKE " .$search_like . " OR
                                u.banned  LIKE " .$search_like . " OR
                                u.is_active  LIKE " .$search_like . " OR
                                cli.razon_social  LIKE " .$search_like . " OR
                                u.created_at  LIKE " .$search_like . " OR
                                u.modified_at     LIKE " .$search_like . " ) AND
                                u.deleted != 1";
         }else{
            $where = " where u.deleted != 1";
         }

        $query = "SELECT    u.user_id,
                            u.username,
                            u.email,
                            u.auth_level,
                            u.banned,
                            u.is_active,
                            cli.razon_social,
                            u.created_at,
                            u.modified_at
                    from users u
                    left join relclienteusuario rcu on u.user_id = rcu.idUsuario
                    left join clientes cli on cli.idCliente = rcu.idCliente
                    ". $where;

        $query .= " ORDER BY ". $order["column"] . " "  .  $order["direction"] . " LIMIT " . $start . "," . $length;                            

        $r = $this->db->query($query);

        $query2 = "SELECT    u.user_id,
                            u.username,
                            u.email,
                            u.auth_level,
                            u.banned,
                            u.is_active,
                            cli.razon_social,
                            u.created_at,
                            u.modified_at
                    from users u
                    inner join relclienteusuario rcu on u.user_id = rcu.idUsuario
                    inner join clientes cli on cli.idCliente = rcu.idCliente
                    ". $where;

        $r2 = $this->db->query($query2);                                                    

        $response = array(
                    "numRows" => $r2->num_rows(), 
                    "datos"   => $r,
                    "query" => $query
                    );

        return $response;
    }

    public function getNumRowAll(){

        $r = $this->db->query(" SELECT *
                                FROM users"); 

        return $r->num_rows();
    }

    public function generate_password(){
        $marcas = $this->Marcas->get_nombres_marcas();
        $indice = random_int(0 , count($marcas)-1);
        return (str_replace(" ","*",ucwords(strtolower($marcas[$indice]['nombre']))).random_int(10000,99999));
    }

    public function active_users_from_client($id_client, $status = 0){

        $r = $this->db->query(" UPDATE users SET is_active = ".$status." WHERE user_id IN (select idUsuario FROM relclienteusuario WHERE idCliente = ".$id_client.")"); 
        
        return $r;
    }

}
