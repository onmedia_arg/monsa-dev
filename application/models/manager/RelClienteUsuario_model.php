<?php
 
class relclienteusuario_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get rel by idrelclienteusuario
     */
    function get_rel($idrelclienteusuario)
    {
        return $this->db->get_where('relclienteusuario',array('idrelclienteusuario'=>$idrelclienteusuario))->row_array();
    }

    /*
     * Get rel by idrelclienteusuario
     */
    function get_rel_by_user( $idUsuario )
    {
        return $this->db->get_where('relclienteusuario', array( 'idUsuario' => $idUsuario ) )->row_array();
    }

    /*
     * Get rel by idrelclienteusuario
     */
    function get_rel_by_client( $idCliente )
    {
        return $this->db->get_where('relclienteusuario', array( 'idCliente' => $idCliente ) )->result_array();
    }

    /*
     * Get all rel
     */
    function get_all_rel()
    {
        $this->db->order_by('idrelclienteusuario', 'desc');
        return $this->db->get('relclienteusuario')->result_array();
    }
        
    /*
     * function to add new rel
     */
    function add_rel($params)
    {
        $this->db->insert('relclienteusuario',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update rel
     */
    function update_rel($idrelclienteusuario,$params)
    {
        $this->db->where('idrelclienteusuario',$idrelclienteusuario);
        return $this->db->update('relclienteusuario',$params);
    }
    
    /*
     * function to delete rel
     */
    function delete_rel($idrelclienteusuario)
    {
        return $this->db->delete('relclienteusuario',array('idrelclienteusuario'=>$idrelclienteusuario));
    }   

}
