<?php
 
class Marca_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    public function get_marcas(){
        return $this->db->query("select * from marca order by nombre")->result_array();
    }

    public function get_nombres_marcas(){
        return $this->db->query("select nombre from marca")->result_array();
    }

    function get_marca_by_name($name)
    {
        return $this->db->get_where('marca',array('nombre'=>$name))->row_array();
    }

    public function agregarMarca($nombre){

        $query = $this->db->query("SELECT MAX(idMarca) AS idMarca FROM marca");
        $result = $query->result_array();
        $max_id = $result[0]['idMarca'];

        $data = array(
            'idMarca' => $max_id + 1 ,
            'nombre' => $nombre,
            'slug' => $this->_generate_unique_slug($nombre),
        );


        $this->db->insert('marca', $data);
        return $this->db->insert_id();

    }

    public function actualizarMarca($id, $nombre){

        $data = array(
            'nombre' => $nombre,
            'slug' => $this->_generate_unique_slug($nombre)
        );

        $result = $this->db->where('idMarca', $id)->update('marca', $data);

        if($result){
            return "Marca actualizada correctamente";
        }else{
            return "Error al actualizar marca";
        }
    }

    public function actualizarOrdenMarca($sorts){

        $ids = explode(",", $sorts);
        $orden = 1;
        $return = true;
        foreach ($ids as $id) {
            $data = array(
                'orden' => $orden
            );
            $result = $this->db->where('idMarca', $id)->update('marca', $data);
            if(!$result){
                $return = false;
            }
            $orden ++;
        }

        if($return){
            return "Orden marca actualizado correctamente";
        }else{
            return "Error al actualizar orden marca";
        }
    }

    public function eliminarMarca($id){
        
        $products = $this->db->where('idMarca', $id)->get('producto')->result_array();
        if(count($products) > 0){
            return "No se puede eliminar la marca porque tiene productos asociados";
        }

        $result = $this->db->where('idMarca', $id)->delete('marca');

        if($result){
            return "Marca eliminada correctamente";
        }else{
            return "Error al elminiar marca";
        }

    }


    function _generate_unique_slug($title) {
        // Reemplazar espacios por guiones medios y formatear el texto
        $slug = url_title($title, 'dash', true);

        // Verificar si el slug ya existe en la base de datos
        $count = $this->db->where('slug', $slug)->count_all_results('marca');

        // Si ya existe, agrega un sufijo numérico para hacerlo único
        if ($count > 0) {
            $suffix = 1;
            do {
                $new_slug = $slug . '-' . $suffix;
                $count = $this->db->where('slug', $new_slug)->count_all_results('marca');
                $suffix++;
            } while ($count > 0);
            $slug = $new_slug;
        }
    
        return $slug;
    }


}