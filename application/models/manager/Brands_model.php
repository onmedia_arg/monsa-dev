<?php

class Brands_model extends CI_Model{

   function getBrands(){
        $this->db->select('*');
        $this->db->from('brands');
        $query = $this->db->get();
        return $query->result();
    }

    function insert_brands($data){
        $this->db->insert('brands', $data);
        return $this->db->insert_id();
    }

    function get_brands_by_id($id){
        $this->db->select('*');
        $this->db->from('brands');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    function delete_brands($id){
        $this->db->where('id', $id);
        return $this->db->delete('brands');
    }

}