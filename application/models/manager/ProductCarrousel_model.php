<?php

class ProductCarrousel_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getCarrouselByProduct($idProducto) {
        $this->db->where('idProducto', $idProducto);
        $this->db->join('carrouseles', 'producto_carrousel.carrousel_id = carrouseles.id');
        $query = $this->db->get('producto_carrousel');

        return $query->result_array();
    }
    
    function syncCarrousel($idProducto, $carrousel) {
        $this->db->where('idProducto', $idProducto);
        $this->db->delete('producto_carrousel');

        foreach($carrousel as $c){
             $this->db->insert('producto_carrousel', array('idProducto' => $idProducto, 'carrousel_id' => $c));
        }
    }

    function deleteCarrousel($idProducto) {
        $this->db->where('idProducto', $idProducto);
        $this->db->delete('producto_carrousel');
    }

}