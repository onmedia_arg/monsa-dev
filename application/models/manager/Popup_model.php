<?php
 
class Popup_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    public function get_popup(){
        return $this->db->query("select * from popup where active = true")->row_array();
    }
    
    public function get_all(){
        return $this->db->query("select * from popup")->row_array();
    }

    function update($id, $data){

        if ( $data == null ) {return false;}

        $update =  $this->db->where( 'id_popup', $id )
                            ->update( 'popup', $data );

        if ( $update ) {
            return true;
        }else{
            return false;
        }    
    }    

}