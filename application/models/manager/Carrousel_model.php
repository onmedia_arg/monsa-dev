<?php

class Carrousel_model extends CI_Model {

    public function getData()
    {
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('carrouseles');
        return $query->result_array();
    }
    public function getProductsCarrousel()
    {
        $this->db->order_by('id', 'DESC');
        $this->db->join('producto_carrousel', 'producto_carrousel.carrousel_id = carrouseles.id');
        $this->db->join('producto', 'producto.idProducto = producto_carrousel.idProducto');
        $query = $this->db->get('carrouseles');
        return $query->result_array();
    }


    public function store($data)
    {
        $this->db->insert('carrouseles', $data);
        return $this->db->affected_rows();
    }

    public function getProductsbyCarrousel($idCarrousel)
    {
        $sql = "SELECT *, p.slug as slug_producto, f.nombre as familia, m.nombre as marca, t.name as tag
                FROM carrouseles c
                JOIN producto_carrousel pc ON pc.carrousel_id = c.id 
                JOIN producto p ON p.idProducto = pc.idProducto
                JOIN familia f ON p.idFamilia = f.idFamilia
                JOIN marca m ON p.idMarca = m.idMarca
                left JOIN tags t ON p.idTag = t.id
                WHERE pc.carrousel_id = $idCarrousel
                ORDER BY c.id DESC";
                
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function update($data)
    {
        $this->db->where('id', $data['id']);
        $this->db->update('carrouseles', $data);
        return $this->db->affected_rows();
    }


    public function getActives()
    {
        $this->db->where('activo', 1);
        $query = $this->db->get('carrouseles');
        return $query->result_array();
    }
}
