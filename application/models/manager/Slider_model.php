<?php

class Slider_model extends CI_Model{


    function getSlider(){
        $this->db->select('*');
        $this->db->from('slider');
        $query = $this->db->get();
        return $query->result();
    }

    function insert_slider($data){
        $this->db->insert('slider', $data);
        return $this->db->insert_id();
    }

    function get_slider_by_id($id){
        $this->db->select('*');
        $this->db->from('slider');
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    function delete_slider($id){
        $this->db->where('id', $id);
        return $this->db->delete('slider');
    }

}