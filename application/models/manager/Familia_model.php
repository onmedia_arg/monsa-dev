<?php
 
class Familia_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    public function get_familias(){
        return $this->db->query("select * from familia order by nombre")->result_array();
    }

    public function get_familia($idFamilia){
        return $this->db->query("select * from familia where idFamilia = $idFamilia")->row_array();
    }

    public function agregarFamilia($nombre){

        $data = array(
            'nombre' => $nombre,
            'slug' => $this->_generate_unique_slug($nombre)
        );

        $this->db->insert('familia', $data);
        return $this->db->insert_id();

    }

    public function actualizarFamilia($id, $nombre){

        $data = array(
            'nombre' => $nombre,
            'slug' => $this->_generate_unique_slug($nombre)
        );

        $result = $this->db->where('idFamilia', $id)->update('familia', $data);

        if($result){
            return "Familia actualizada correctamente";
        }else{
            return "Error al actualizar familia";
        }
    }

    public function actualizarOrdenFamilia($sorts){

        $ids = explode(",", $sorts);
        $orden = 1;
        $return = true;
        foreach ($ids as $id) {
            $data = array(
                'orden' => $orden
            );
            $result = $this->db->where('idFamilia', $id)->update('familia', $data);
            if(!$result){
                $return = false;
            }
            $orden ++;
        }

        if($return){
            return "Orden familia actualizado correctamente";
        }else{
            return "Error al actualizar orden familia";
        }
    }

    public function eliminarFamilia($id){
        
        $products = $this->db->where('idFamilia', $id)->get('producto')->result_array();
        if(count($products) > 0){
            return "No se puede eliminar la familia porque tiene productos asociados";
        }

        $result = $this->db->where('idFamilia', $id)->delete('familia');

        if($result){
            $resultAttr = $this->db->where('idFamilia', $id)->delete('atributo');
            return "Familia eliminada correctamente";
        }else{
            return "Error al elminiar familia";
        }

    }


    function _generate_unique_slug($title) {
        // Reemplazar espacios por guiones medios y formatear el texto
        $slug = url_title($title, 'dash', true);

        // Verificar si el slug ya existe en la base de datos
        $count = $this->db->where('slug', $slug)->count_all_results('familia');

        // Si ya existe, agrega un sufijo numérico para hacerlo único
        if ($count > 0) {
            $suffix = 1;
            do {
                $new_slug = $slug . '-' . $suffix;
                $count = $this->db->where('slug', $new_slug)->count_all_results('familia');
                $suffix++;
            } while ($count > 0);
            $slug = $new_slug;
        }
    
        return $slug;
    }


}