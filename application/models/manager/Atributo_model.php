<?php
 
class Atributo_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    public function getAtributosByFamilia($idFamilia){
        return $this->db->query("select * from atributo where idFamilia = $idFamilia")->result_array();
    }

    public function getAtributosByFamiliaAndNombre($idFamilia, $nombre){
        return $this->db->query("select * from atributo where idFamilia = $idFamilia AND nombre = '$nombre'")->result_array();
    }

    public function getAtributo($idAtributo){
        return $this->db->query("select * from atributo where idAtributo = $idAtributo")->row_array();
    }

    public function getValues($idAtributo){
        $atributo = $this->getAtributo($idAtributo);
        return json_decode($atributo['valor']);
    }

    public function agregarValor($idAtributo, $nuevoValor) {
       
        // Recuperar el registro de la base de datos utilizando el idAtributo
        $atributo = $this->db->where('idAtributo', $idAtributo)->get('atributo')->row();

        if ($atributo) {
            // Decodificar el JSON en el campo "valor" para obtener un array
            $valoresActuales = json_decode($atributo->valor, true);

            // Agregar el nuevo valor al array
            $valoresActuales[] = $nuevoValor;
            //var_dump($valoresActuales);
            
            // Codificar el array actualizado de nuevo como JSON
            $nuevoValorJSON = json_encode($valoresActuales);
            
            //print_r($nuevoValorJSON);exit();

            // Actualizar el campo "valor" en la base de datos con el nuevo JSON
            $this->db->where('idAtributo', $idAtributo)
                        ->update('atributo', array('valor' => $nuevoValorJSON));

            return true; // Éxito
        } else {
            return false; // No se encontró el registro con el idAtributo especificado
        }
    }

    public function agregarAtributo($nombre, $idFamilia){

        $data = array(
            'nombre' => $nombre,
            'idFamilia' => $idFamilia,
            'valor' => '[]'
        );

        $this->db->insert('atributo', $data);
        return $this->db->insert_id();

    }

    //Actualiza el nombre de un atributo
    public function updateAtributoName($idAtributo, $atributeName){

        $result = $this->db->where('idAtributo', $idAtributo)
                        ->update('atributo', ['nombre' => $atributeName]);
        
        if($result){
            return 'El nombre del atributo ha sido actualizado con éxito.';
        }else{
            // Error: No se encontró el registro con el idAtributo especificado
            return 'No se encontró el registro con el idAtributo especificado';
            
        }

    }

    // Actualiza los valores de un atributo
    public function updateAtributo($idAtributo, $oldValue, $newValue){

        //get atributo by id
        $atributo = $this->db->where('idAtributo', $idAtributo)->get('atributo')->row();
        if ($atributo) {
            // Decodifica el valor actual del atributo, asumiendo que está en formato JSON
            $currentValues = json_decode($atributo->valor, true);
    
            // Verifica si el valor antiguo existe en el array
            $index = array_search($oldValue, $currentValues);
    
            if ($index !== false) {
                // Reemplaza el valor antiguo por el nuevo
                $currentValues[$index] = $newValue;
    
                // Codifica nuevamente los valores a JSON
                $newValues = json_encode($currentValues);
    
                // Actualiza el campo "valor" en la base de datos
                $this->db->where('idAtributo', $idAtributo)->update('atributo', ['valor' => $newValues]);
                
                // Retornar un mensaje de éxito o realizar otras acciones necesarias
                $result =  "El valor ha sido actualizado con éxito.";
            } else {
                $result =  "El valor antiguo no se encontró en la lista de valores.";
            }
        } else {
            return "No se encontró el atributo con el ID especificado.";
        }        

        $result .= ' Productos Actualizados: ' . $this->updateProductoAtributo($idAtributo, $oldValue, $newValue);
        return $result;
    }

    // Función para actualizar las entradas en la tabla "producto_atributo"
    private function updateProductoAtributo($idAtributo, $oldValue, $newValue) {
        // Actualiza las entradas en "producto_atributo" donde coinciden "idAtributo" y "valor"
        $this->db->where('idAtributo', $idAtributo)->where('valores', $oldValue)
            ->update('producto_atributo', ['valores' => $newValue]);

        return $this->db->affected_rows();
    }

    public function deleteValue($idAtributo, $value){

        //get atributo by id
        $atributo = $this->db->where('idAtributo', $idAtributo)->get('atributo')->row();
        if ($atributo) {
            // Decodifica el valor actual del atributo, asumiendo que está en formato JSON
            $currentValues = json_decode($atributo->valor, true);

            // Verifica si el valor antiguo existe en el array
            $index = array_search($value, $currentValues);

            if ($index !== false) {
                // Elimina el valor del array
                unset($currentValues[$index]);

                // Codifica nuevamente los valores a JSON
                $newValues = json_encode(array_values($currentValues)); // Reindexa el array

                // Actualiza el campo "valor" en la base de datos
                $this->db->where('idAtributo', $idAtributo)->update('atributo', ['valor' => $newValues]);

                // Retornar un mensaje de éxito o realizar otras acciones necesarias
                $result = "El valor ha sido eliminado con éxito.";
            } else {
                $result =  "El valor antiguo no se encontró en la lista de valores.";
            }
        } else {
            return "No se encontró el atributo con el ID especificado.";
        }        

        $result .= ' Productos Actualizados: ' . $this->deleteProductoAtributo($idAtributo, $value);
        return $result;

    }

    public function deleteProductoAtributo($idAtributo, $value){
        // Elimina las entradas en "producto_atributo" donde coinciden "idAtributo" y "valor"
        $this->db->where('idAtributo', $idAtributo)->where('valores', $value)
            ->delete('producto_atributo');

        return $this->db->affected_rows();

    }

    public function deleteAtributo($idAtributo){

        //delete atributo by id
        $result = $this->db->where('idAtributo', $idAtributo)->delete('atributo');

        if($result){
            return 'El atributo ha sido eliminado con éxito.';
        }
        else{
            return 'No se encontró el registro con el idAtributo especificado';
        }

    }


}