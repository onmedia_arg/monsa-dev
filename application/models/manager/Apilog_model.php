<?php
 
class Apilog_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertApiLog($data){

        $r = $this->db->insert('api_logs', $data);
        return $this->db->insert_id();

    }

}