<?php
 
class Downloads_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    public function getAll(){
       
        $r = $this->db->query("SELECT *
                                FROM downloads");

        return $r->result();                                                
    }

    function insert($data){

        $r = $this->db->insert('downloads', $data);
        return $this->db->insert_id();

    }

    function getLastActive(){

        $r = $this->db->query('SELECT *  
                                 FROM downloads 
                                where is_active = 1
                                group by type
                                ORDER BY updated_at DESC
                                limit 2');

        return $r->result_array();                               
    }


    function deleteFile($type){

        $r = $this->db->query("SELECT * 
                               FROM downloads
                              WHERE type = '$type'")->row();

        if($r){
            $this->db->delete('downloads', array('iddownloads' => $r->iddownloads));
            return $r;
        }                              

    }

    function getFileByType($type){
        return $this->db->get_where('downloads', array('type' => $type))->row();
    }
}