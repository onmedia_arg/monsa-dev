<?php
 
class Clients_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('manager/Marca_model', 'Marcas');
    }

    /*
     * function to add new clientes
     */
    function add_cliente($params)
    {
        $this->db->insert('clientes',$params);
        return $this->db->insert_id();
    }
    
    public function get_single_client($idCliente){

        $this->db->select('*')
                 ->where('idCliente', $idCliente)
                 ->from('clientes');
        $response = $this->db->get()->row_array();

        return $response;

    }

    public function get_last_clients(){

        $query = "SELECT * 
                    FROM clientes
                   where is_activo = 0
                     and deleted = 0
                    order by idCliente DESC 
                     limit 5";

        //Se ejecuta la query
        $r = $this->db->query($query);

        $response = array(
                    "numRows" => $r->num_rows(), 
                    "datos"   => $r,
                    "query"   => $query
                    );

        return $response;
    }

    function getAll(){
        return $this->db->query("select * from clientes where deleted = 0 order by razon_social")->result_array();
    }
    
    function getAllExport(){
        return $this->db->query("select 
                                        cli.idCliente,
                                        cli.razon_social,
                                        tc.texto,
                                        cli.cuit,
                                        cli.direccion,
                                        cli.mail,
                                        cli.telefono,
                                        cli.fechaCreacion,
                                        cli.is_activo,
                                        cli.deleted,
                                        cli.id_cliente_interno
                                    from clientes cli
                                    inner join tipocliente tc on cli.idTipoCliente = tc.idTipoCliente
                                    order by cli.idCliente")->result_array();
    }

    function getAllClients($start, $length, $search, $order){
        
        $where = "";

        if($search != null && trim($search) != ""){
            $search_like = $this->db->escape("%".$search."%");
            $where = " where (cli.idCliente  LIKE " .$search_like . " OR
                                cli.razon_social  LIKE " .$search_like . " OR
                                tc.texto  LIKE " .$search_like . " OR
                                cli.cuit  LIKE " .$search_like . " OR
                                cli.direccion  LIKE " .$search_like . " OR
                                cli.mail  LIKE " .$search_like . " OR
                                cli.telefono  LIKE " .$search_like . " OR
                                cli.fechaCreacion  LIKE " .$search_like . " OR
                                cli.is_activo     LIKE " .$search_like . " ) AND
                                deleted != 1";
         }else{
            $where = " where deleted != 1";
         }

        $query = "SELECT    cli.idCliente,
                            cli.razon_social,
                            tc.texto,
                            cli.cuit,
                            cli.direccion,
                            cli.mail,
                            cli.telefono,
                            cli.fechaCreacion,
                            cli.is_activo
                    from clientes cli
                    inner join tipocliente tc on cli.idTipoCliente = tc.idTipoCliente
                    ". $where;

        $query .= " ORDER BY ". $order["column"] . " "  .  $order["direction"] . " LIMIT " . $start . "," . $length;                            

        $r = $this->db->query($query);

        $query2 = "SELECT   cli.idCliente,
                            cli.razon_social,
                            tc.texto,
                            cli.cuit,
                            cli.direccion,
                            cli.mail,
                            cli.telefono,
                            cli.fechaCreacion,
                            cli.is_activo
                    from clientes cli
                    inner join tipocliente tc on cli.idTipoCliente = tc.idTipoCliente
                    ". $where;

        $r2 = $this->db->query($query2);                                                    

        $response = array(
                    "numRows" => $r2->num_rows(), 
                    "datos"   => $r,
                    "query" => $query
                    );

        return $response;
    }

    public function getNumRowAll(){

        $r = $this->db->query(" SELECT *
                                FROM clientes"); 

        return $r->num_rows();
    }

    /*
     * Get clientes by idCliente
     */
    function get_cliente($idCliente)
    {
        return $this->db->get_where( 'clientes', array( 'idCliente'=>$idCliente, 'deleted' => 0 ) )->row_array();
    }

    /*
     * Obtiene los usuarios de un cliente
     */
    function get_client_users( $idCliente )
    {
        
        $result = $this->db->select('*')
                        ->from( 'relclienteusuario' )
                        ->join( 'users', 'users.user_id = relclienteusuario.idUsuario' )
                        ->where( array( 'idCliente' => $idCliente ) )
                        ->where( array( 'users.deleted' => 0 ) ) 
                        ->get()
                        ->result_array();

        return $result;
    }

    /*
     * Get all clientes
     */
    function get_all_clientes( $show_deleted = false )
    {
        $this->db->order_by('idCliente', 'desc');

        if ( $show_deleted == false ) {
            $this->db->where('deleted', 0 );
        }

        return $this->db->get('clientes')->result_array();
    }

    /*
     * function to update clientes
     */
    function update_cliente($idCliente,$params)
    {
        $this->db->where('idCliente',$idCliente);
        return $this->db->update('clientes',$params);
    }


    /*
     * function to delete clientes
     */
    function delete_cliente($idCliente)
    {

        $this->update_cliente( $idCliente, array( 'deleted' => 1, 'is_activo' => 0 ) );

        $rel = $this->RelClienteUsuario_model->get_rel_by_client( $idCliente );

        foreach ($rel as $r => $data) {
            
            $this->user_model->update_user( $data['idUsuario'], array( 'deleted' => 1, 'is_active' => 0 ) );

        }

        if ( $this->db->affected_rows() >= 1 ) {
            return true;
        }else{
            return false;
        }

    }   

    function get_id_interno_cliente_by_user($idUser)
    {
        $result = $this->db->select('clientes.id_cliente_interno')
                        ->from( 'clientes' )
                        ->join( 'relclienteusuario', 'clientes.idCliente = relclienteusuario.idCliente' )
                        ->where( array('relclienteusuario.idUsuario' => $idUser) ) 
                        ->get()
                        ->result_array();

        return $result;
    }

    function get_id_interno_cliente_by_client($idCliente)
    {
        $result = $this->db->select('clientes.id_cliente_interno')
                        ->from( 'clientes' )
                        ->where( array('clientes.idCliente' => $idCliente) ) 
                        ->get()
                        ->result_array();

        return $result;
    }

    
}