<?php
 
class Orders_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    public function getAllOrders($start, $length, $search, $filtro, $order){
        //print_r($search);exit();

        if($search != null && trim($search) != ""){
            $search_like = $this->db->escape("%".$search."%");

            $where = " where (o.idOrderHdr  LIKE " .$search_like . " OR
                                o.idCliente  LIKE " .$search_like . " OR
                                cl.razon_social  LIKE " .$search_like . " OR
                                o.idOrderStatus  LIKE " .$search_like . " OR
                                os.statusText  LIKE " .$search_like . ") AND
                                isDeleted = 0";
         }else{
            $where = " where isDeleted = 0";
         }

        
        $where_created = "";
        if($filtro["fecha_desde"]  !=  ""){
            $where_created = " AND ( created >= '" . $filtro["fecha_desde"] . "'"; 
            $where_created .= " and created <= '" . $filtro["fecha_hasta"] . "' ) ";
        }
        
        $where_status = "";
        if($filtro["idOrderStatus"] != ""){
            $where_status = " AND o.idOrderStatus = " . $filtro["idOrderStatus"];
        }

        $where_cliente = "";
        if($filtro["idCliente"] != ""){
            $where_cliente = " AND o.idCliente = " . $filtro["idCliente"];
        }        

         $query = "SELECT o.idOrderHdr,
						  o.idCliente,
						  cl.razon_social,	
						  o.idOrderStatus,
						  os.statusText,
                          o.total,
						  o.created,
                          o.updated
					 FROM order_hdr     AS o
				     LEFT JOIN clientes AS cl ON cl.idCliente     = o.idCliente
				    INNER JOIN order_status AS os ON os.idOrderStatus = o.idOrderStatus " .$where;
       
        // //Se agrega el filtro de fecha si se creo
        // if($where_created != ""){
        //     $query .= $where_created;
        // }

        //Se agrega el filtro de Estado
        if($where_status != ""){
            $query .= $where_status;
        }        

        //Se agrega el filtro de Cliente
        if($where_cliente != ""){
            $query .= $where_cliente;
        }                

        // Se agrega a la query el orden y los parametros de paginado    
        // $query .= " ORDER BY idOrder DESC LIMIT " . $start . "," . $length;                                
        $query .= " ORDER BY ". $order["column"] . " "  .  $order["direction"] . " LIMIT " . $start . "," . $length;                                
        
        // var_dump($query);exit();

        //Se ejecuta la query
        $r = $this->db->query($query);

        $r2 = $this->db->query("SELECT o.idOrderHdr,
                                        o.idCliente,
                                        cl.razon_social,	
                                        o.idOrderStatus,
                                        os.statusText,
                                        o.total,
                                        o.created,
                                        o.updated
                                FROM order_hdr     AS o
                                LEFT JOIN clientes AS cl ON cl.idCliente     = o.idCliente
                                INNER JOIN order_status AS os ON os.idOrderStatus = o.idOrderStatus 
							     " . $where
                                 . $where_created 
                                 . $where_status);                                                    

        $response = array(
                    "numRows" => $r2->num_rows(), 
                    "datos"   => $r,
                    "query" => $query
                    );

        return $response;
  } 
    
    public function getLastOrders(){

        $query = "SELECT o.idOrderHdr,
                        o.idCliente,
                        cl.razon_social,	
                        o.idOrderStatus,
                        os.statusText,
                        o.total,
                        o.created,
                        o.updated
                    FROM order_hdr     AS o
                    LEFT JOIN clientes AS cl ON cl.idCliente     = o.idCliente
                    INNER JOIN order_status AS os ON os.idOrderStatus = o.idOrderStatus 
                    WHERE isDeleted = 0
                    and  o.idOrderStatus =  2
                    ORDER BY o.updated DESC LIMIT 10";
    
        //Se ejecuta la query
        $r = $this->db->query($query);

        $response = array(
                    "numRows" => $r->num_rows(), 
                    "datos"   => $r,
                    "query" => $query
                    );

        return $response;
    }   

    public function getNumRowAll(){

        // $r = $this->db->get('orders_hdr');
        $r = $this->db->query(" SELECT *
                                FROM order_hdr
                                WHERE isDeleted = 0"); 

        return $r->num_rows();

    }

    public function getOrderData($idOrderHdr){

        $r = $this->db->query("SELECT oh.idOrderHdr,
                                    oh.idUser,
                                    us.username, 
                                    oh.idCliente,
                                    cl.razon_social,
                                    oh.total,
                                    oh.idOrderStatus,
                                    oh.created,
                                    oh.nota,
                                    os.statusText,
                                    os.color
                                FROM order_hdr as oh
                                LEFT JOIN clientes as cl on cl.idCliente = oh.idCliente
                                INNER JOIN users   as us on us.user_id   = oh.idUser 
                                inner join order_status as os on os.idOrderStatus = oh.idOrderStatus
                                WHERE oh.idOrderHdr = $idOrderHdr");

        return $r->row_array();
    }

    public function getAllItemsbyOrderId($idOrderHdr){

        $r = $this->db->query("SELECT oi.posnr,
                                        oi.idProducto,
                                        oi.precio,
                                        oi.qty,
                                        oi.total,
                                        p.idTipo,
                                        p.nombre as pNombre,
                                        p.sku,
                                        f.nombre as fNombre,
                                        m.nombre as mNombre
                                FROM order_itm as oi
                                inner join producto as p on p.idProducto = oi.idProducto
                                inner join familia  as f on f.idFamilia  = p.idFamilia
                                inner join marca    as m on m.idMarca    = p.idMarca
                                where idOrderHdr = $idOrderHdr
                                order by oi.posnr ASC");

        return $r->result_array();

    }

    public function info_stat_orders_cant(){

        $r= $this->db->query("SELECT count(idOrderHdr) as cant
                                FROM order_hdr
                               WHERE MONTH(created) = MONTH(CURRENT_DATE())
                                 AND YEAR(created)  = YEAR(CURRENT_DATE()) 
                                 AND idOrderStatus  > 1 ");

        return $r->row()->cant;  
    }

    public function info_stat_orders_tot(){

        $r= $this->db->query("SELECT sum(total) as total
                                FROM order_hdr
                            WHERE ( MONTH(created) = MONTH(CURRENT_DATE())
                                    AND YEAR(created) = YEAR(CURRENT_DATE()) )
                                AND idOrderStatus > 1 ");

        return $r->row()->total;  
    }    

    function updateOrderStatus($id, $data){

        if ( $data == null ) {return false;}

        $update =  $this->db->where( 'idOrderHdr', $id )
                            ->update( 'order_hdr', $data );

        if ( $update ) {
            return true;
        }else{
            return false;
        }    
    }

    function getEmailByOrderId($id){

        $query = "SELECT hdr.idOrderHdr, hdr.idUser, hdr.idCliente, cli.mail
                    from order_hdr as hdr
                inner join clientes as cli on cli.idCliente = hdr.idCliente
                where hdr.idOrderHdr = $id";

        $r = $this->db->query($query);
        
        return $r->row_array();     
    }

    function getStatusList(){
        return $this->db->query("SELECT *
                                   from order_status")->result_array();

    }

}