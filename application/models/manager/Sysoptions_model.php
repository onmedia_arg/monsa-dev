<?php
 
class Sysoptions_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();

        $this->table = 'system_options';
    }
    
    function getWhatsappConfig(){

        $r = $this->db->query("SELECT * 
                                FROM system_options
                                WHERE opt_name = 'wsp_number'
                                OR opt_name    = 'wsp_message'");
        
        return $r->result_array();

    }

    function getShopMsj(){

        $r = $this->db->query("SELECT * 
                                FROM system_options
                                WHERE opt_name = 'shop_msj_title' 
                                   or opt_name = 'shop_msj_detail'");

        $params = $r->result_array();

        $result_param = array();

        foreach ($params as $key => $value) {
            $result_param[$value['opt_name']] = $value['opt_value'];
        }
        // var_dump($params);die;
        return $result_param;

    }

    function getProcessState(){

        $r = $this->db->query("SELECT * 
                                FROM system_options
                                WHERE   opt_name = 'stock_update_status'
                                    OR  opt_name    = 'stock_update_batch'
                                    OR  opt_name    = 'stock_update_total'
                                    OR  opt_name    = 'stock_update_processed'
                                    OR  opt_name    = 'stock_update_start'
                                    OR  opt_name    = 'stock_update_finish'
                                    OR  opt_name    = 'stock_update_endpoint'
                                    OR  opt_name    = 'webapi_client_getSaldo_endpoint'
                                    OR  opt_name    = 'webapi_client_getMovimientos_endpoint'
                                    OR  opt_name    = 'webapi_client_getComposicion_endpoint'");
        
        return $r->result_array();

    }

    // function updateProcessState($name, $value){
    //     $r = $this->db->query(" UPDATE system_options
    //                             SET opt_value = '$value'
    //                             WHERE opt_name = '$name'"); 

    //     return $r;

    // }

    public function updateProcessState($name, $value) {
        if (is_array($name)) {
            // Si $name es un array, actualizamos múltiples registros
            foreach ($name as $key => $val) {
                $this->db->where('opt_name', $key);
                $this->db->update($this->table, ['opt_value' => $val]);
            }
        } else {
            // Si $name es una cadena, actualizamos un solo registro
            $this->db->where('opt_name', $name);
            $this->db->update($this->table, ['opt_value' => $value]);
        }
        
        return $this->db->affected_rows() > 0;
    }    

    function getProcessBatch(){
        $r = $this->db->query("SELECT opt_value 
                                FROM system_options
                                WHERE   opt_name = 'stock_update_batch'");
        
        return $r->result_array();

    }

    public function getProcessConfig($keys) {
        $this->db->select('opt_name, opt_value');
        $this->db->from('system_options');
        $this->db->where_in('opt_name', $keys);
        $query = $this->db->get();
        
        $result = array();
        foreach ($query->result_array() as $row) {
            $result[$row['opt_name']] = $row['opt_value'];
        }
        
        return $result;
    }

    public function incrementProcessState($name, $increment) {

        // $this->db->set('opt_value', "opt_value + $increment", FALSE);
        $this->db->set('opt_value', $increment);
        $this->db->where('opt_name', $name);
        $this->db->update('system_options');
           
        return $this->db->affected_rows() > 0;
    }    

}