<?php
 
class Products_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'producto';
    }

    function get_producto( $idProducto, $return = 'row_array' )
    { 

        switch ($return) {
            case 'row_array':
                return $this->db->get_where('producto',array('idProducto'=>$idProducto))->row_array(); 
                break;
            
            case 'num_rows': 
                return $this->db->get_where('producto',array('idProducto'=>$idProducto))->num_rows(); 
                break;

            default:
                return $this->db->get_where('producto',array('idProducto'=>$idProducto))->row_array(); 
                break;
        } 

    }
    
    function insertBatchSimple($data){

        $r = $this->db->insert_batch('producto', $data);
        $query = $this->db->last_query();
        return $query;

    }


    function insertProductSimple($data){

        $r = $this->db->insert('producto', $data);
        return $this->db->insert_id();;

    }    

    function check_sku_exist($sku){

        $r = $this->db->get_where('producto',array('sku'=>$sku));

        return $r->result_array();
        // return $r->num_rows();

    }

    function updateProductSimple($id, $data){

        $this->db->where('idProducto', $id);
        $r = $this->db->update('producto', $data);
        return $r;

    }     

    function getAllProducts($start, $length, $search, $filtro, $order){
        
        $where = "";
        $where_familia = "";
        $where_marca   = "";
        $where_show    = "";

        $where_promo = "";

        if($search != null && trim($search) != ""){

            $search_like = $this->db->escape("%".$search."%");

            $where = " where (fm.nombre  LIKE " .$search_like . " OR
                              mr.nombre  LIKE " .$search_like . " OR
                              pr.modelo  LIKE " .$search_like . " OR
                              pr.sku     LIKE " .$search_like . " )";
         }

         if($filtro['familia'] !=  ""){
             $where_familia = " AND pr.idFamilia = " . $filtro['familia'];
         }

         if($filtro['marca'] !=  ""){
             $where_familia = " AND pr.idMarca = " . $filtro['marca'];
         }

         if($filtro['show'] !=  ""){
             $where_show = " AND pr.show = '" . $filtro['show'] . "'";
         }

         if($filtro['promo'] ==  true){
             $where_promo = " AND pr.is_promo_active = '1' " ;
         }

        $query = "SELECT pr.idProducto,
                         pr.sku,
                         pr.idTipo, 
                         pr.idFamilia,
                         fm.nombre as familiaTxt,
                         pr.idMarca,
                         mr.nombre as marcaTxt,
                         pr.imagen,
                         pr.modelo,
                         pr.precio,
                         pr.show,
                         pr.visibilidad,
                         pr.is_promo_active
                    from producto as pr 
                    inner join familia as fm on fm.idFamilia = pr.idFamilia
                    inner join marca   as mr on mr.idMarca   = pr.idMarca
                    ". $where;


        if($where_familia != ""){
            $query .= $where_familia;
        }

        if($where_marca != ""){
            $query .= $where_marca;
        }

        if($where_show != ""){
            $query .= $where_show;
        }

        if($where_promo != ""){
            $query .= $where_promo;
        }

        $query .= " ORDER BY ". $order["column"] . " "  .  $order["direction"] . " LIMIT " . $start . "," . $length;                                

        //Se ejecuta la query
        $r = $this->db->query($query);


        $query2 = "SELECT pr.sku,
                          pr.idTipo, 
                          pr.idFamilia,
                          fm.nombre as familiaTxt,
                          pr.idMarca,
                          mr.nombre as marcaTxt,
                          pr.modelo,
                          pr.precio
                    from producto as pr 
                    inner join familia as fm on fm.idFamilia = pr.idFamilia
                    inner join marca   as mr on mr.idMarca   = pr.idMarca
                    ". $where;

        if($where_familia != ""){
            $query2 .= $where_familia;
        }

        if($where_marca != ""){
            $query2 .= $where_marca;
        }

        if($where_show != ""){
            $query2 .= $where_show;
        }


        $r2 = $this->db->query($query2);                                                    

        $response = array(
                    "numRows" => $r2->num_rows(), 
                    "datos"   => $r,
                    "query" => $query
                    );
        // var_dump($response);die;

        return $response;
    }
    
    public function getNumRowAll(){

        $r = $this->db->query(" SELECT *
                                FROM producto"); 

        return $r->num_rows();
    }

    public function getAllSku(){

        $r = $this->db->query(" SELECT sku
                                FROM producto where sku > 0"); 

        return $r->result_array();
    }

    function updateProductSku($sku, $data){

        $this->db->where('sku', $sku);

        $r = $this->db->update('producto', $data);
        return $r;


    }  

    function getProductByFamily($idFamilia){
        $r = $this->db->get_where('producto',array('idFamilia'=>$idFamilia));
        return $r->result_array();
    }

    function getProductByFamilyAndMarca($idFamilia, $idMarca){
       $query = "SELECT pr.*, fm.nombre as nombre_familia, ma.nombre as nombre_marca
                   from producto pr
                   inner join familia as fm on fm.idFamilia = pr.idFamilia
                   left join marca as ma on ma.idMarca = pr.idMArca
                   WHERE pr.idFamilia = ".$idFamilia."";

        if($idMarca != -1){
            $query .= " AND pr.idMarca = ".$idMarca."";
        }
        //Se ejecuta la query
        $r = $this->db->query($query);

        return $r->result_array();
    }

    function updateSlug($idProducto, $slug){
        $this->db->where('idProducto', $idProducto);
        $r = $this->db->update('producto', array('slug' => $slug));
        return $r;
    }     

    public function bulkUpdateField($updates, $field) {
        
        if (empty($updates) || empty($field)) {
            return false;
        }

        $cases = [];
        $ids = [];
        foreach ($updates as $sku => $value) {
            $sku = $this->db->escape($sku);
            $value = $this->db->escape($value);
            $cases[] = "WHEN sku = {$sku} THEN {$value}";
            $ids[] = $sku;
        }

        $ids = implode(',', $ids);
        $field = $this->db->protect_identifiers($field);

        $sql = "UPDATE {$this->table} SET {$field} = CASE " . implode(' ', $cases) . " END WHERE sku IN ({$ids})";

        return $this->db->query($sql);
    }    

}