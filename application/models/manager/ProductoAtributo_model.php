<?php
 
class Productoatributo_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    public function getAtributosByProduct($idProducto){
        return $this->db->query("select * 
                                 from producto_atributo 
                                 where idProducto = $idProducto")->result_array();
    }

    public function updateProductoAtributo($atributos){

        foreach ($atributos as $row) {

            if($row["idProdAtrib"] != 'undefined'){
                //Si el atributo ya existe y tiene IDPRODATRIB, se actualiza de forma simple
            
                $this->db->set('valores', $row["valores"]);
                $this->db->where('idProdAtrib', $row["idProdAtrib"]);
                $this->db->update('producto_atributo');
            }else{
                
                // Si el producto no tiene atributo definido, se agrega el registro sin valor, para asignarle un IDPRODATRIB
                // para luego actualizarlo por ese ID
                // Si se actualiza el valor del atributo sin IDPRODATRIB, se busca el registro por idproducto y idatributo
                $check_insert = $this->db->query("SELECT * 
                                                    FROM producto_atributo
                                                   WHERE idProducto = " . $row['idProducto'] . "
                                                     AND idAtributo = " . $row['idAtributo'] )->row_array();

                
                // Se controla que el atributo no se haya creado, por pulsar varias veces el boton guardar en la misma pantalla.
                if($check_insert == NULL){
                    $this->db->set('idProducto', $row["idProducto"]);
                    $this->db->set('idAtributo', $row["idAtributo"]);
                    $this->db->set('valores', $row["valores"]);
                    $this->db->insert('producto_atributo');
                }else{
                    $this->db->set('valores', $row["valores"]);
                    $this->db->where('idProducto', $row["idProducto"]);
                    $this->db->where('idAtributo', $row["idAtributo"]);
                    $this->db->update('producto_atributo');

                }   
            }
        }
    }
    
    function deleteAtributos($idProducto){
        $this->db->where('idProducto', $idProducto);
        $this->db->delete('producto_atributo');
    }

    function insertBatch($data){
        
        $r = $this->db->insert_batch('producto_atributo', $data);
        $query = $this->db->last_query();
        return $query;

    }    
    
}