<?php

class Order_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*
     * Get order by idOrder
     */
    function get_order($idOrder)
    {
        return $this->db->get_where('order', array('idOrder' => $idOrder))->row_array();
    }

    function get_order_by_user($idUser, $state = "")
    {
        $where = "";
        if ($state != "" && trim($state) != "") {
            $where = " AND  hdr.state = " . $state;
        }

        $r = $this->db->query("SELECT hdr.idOrder, 
                                          hdr.idUser, 
                                          hdr.state, 
                                          hdr.created, 
                                          pos.quantity, 
                                          pos.price 
                                     FROM `order` " . "  AS hdr
                                    LEFT OUTER JOIN order_item AS pos ON hdr.idOrder = pos.idOrder
                                    WHERE hdr.idUser = " . $idUser . $where);


        return $r->result_array();
    }

    function get_current_order_by_user($idUser, $state = "")
    {

        $this->db->select()
            ->order_by('or.idOrder', 'desc');
        if ($idUser != '') {
            $this->db->where('or.idUser', $idUser);
        }
        if ($state != '') {
            $this->db->where('or.state', $state);
        }
        $query = $this->db->get('order as or');
        return $query->result_array();
    }

    /*
     * Get all order
     */
    function get_all_order()
    {
        $this->db->order_by('idOrder', 'desc');
        return $this->db->get('order')->result_array();
    }

    /*
     * function to add new order
     */
    function add_order($params)
    {
        $this->db->insert('order', $params);
        return $this->db->insert_id();
    }

    /*
     * function to update order
     */
    function update_order($idOrder, $params)
    {
        $this->db->where('idOrder', $idOrder);
        return $this->db->update('order', $params);
    }

    /*
     * function to delete order
     */
    function delete_order($idOrder)
    {
        return $this->db->delete('order', array('idOrder' => $idOrder));
    }

    function addItems($data)
    {
        $this->db->insert_batch('order_itm', $data);
        $this->db->error();
        return $this->db->insert_id();
    }

    /*
     * function to update user
     */
    function update_order_item($idOrderItm, $params)
    {
        $this->db->where('idOrderItm', $idOrderItm);
        return $this->db->update('order_itm', $params);
    }

    /*
     * function to delete user
     */
    function delete_order_item($idOrderItm)
    {

        $this->db->where('idOrderItm', $idOrderItm);
        $this->db->delete('order_itm');

        if ($this->db->affected_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }

    function getItemsByOrder($orderid)
    {
        $r = $this->db->query("select i.*, p.sku, p.nombre 
                     from order_itm as i
                     join producto as p on i.idProducto = p.idProducto
                    where i.idOrderHdr =" . $orderid);

        return $r->result();
    }

    function getItemsFromOrderStatus1($user_id)
    {
        $response = "";
        $idOrderHdr = $this->db->query("SELECT idOrderHdr 
                                          from order_hdr
                                        where idOrderStatus = 1
                                        and createdBy = $user_id")->row();

        if ($idOrderHdr != NULL) {
            $items = $this->db->query("SELECT i.*, p.sku, p.nombre, h.idOrderStatus, h.createdBy 
                                         FROM order_itm as i
                                        INNER JOIN order_hdr as h on h.idOrderHdr = i.idOrderHdr
                                        INNER JOIN producto as p on i.idProducto = p.idProducto
                                        WHERE h.idOrderStatus = 1
                                          AND h.createdBy = $user_id");

            // var_dump($idOrderHdr->idOrderHdr);die;

            $response = array(
                "idOrderHdr" => $idOrderHdr->idOrderHdr,
                "items"      => $items->result_array()
            );
        }


        return $response;
    }

    // Obtiene ordenes para Datatables con paginado de AJAX
    public function getItemsByOrderDT($idOrderHdr, $start, $length, $search)
    {

        // //      Query para levantar los datos
        $this->db->select('order_itm.*, producto.nombre')
            ->from('order_itm')
            ->join('producto', 'producto.idProducto = order_itm.idProducto')
            ->where('idOrderHdr', $idOrderHdr);

        if ($search != null && trim($search) != "") {

            $search_like = $this->db->escape("%" . $search . "%");

            $this->db->like('order_itm.idOrderHdr', $search_like)
                ->or_like('order_itm.posnr', $search_like)
                ->or_like('producto.nombre', $search_like)
                ->or_like('order_itm.precio', $search_like)
                ->or_like('order_itm.qty', $search_like)
                ->or_like('order_itm.total', $search_like)
                ->or_like('order_itm.options', $search_like);
        }

        $this->db->order_by('idOrderHdr', 'DESC')
            ->limit($length, $start);

        $r = $this->db->get()->result_array();

        // //      Misma query sin LIMIT para contar los registros
        $this->db->select('order_itm.*, producto.nombre')
            ->from('order_itm')
            ->join('producto', 'producto.idProducto = order_itm.idProducto')
            ->where('idOrderHdr', $idOrderHdr);

        if ($search != null && trim($search) != "") {

            $search_like = $this->db->escape("%" . $search . "%");

            $this->db->like('order_itm.idOrderHdr', $search_like)
                ->or_like('order_itm.posnr', $search_like)
                ->or_like('producto.nombre', $search_like)
                ->or_like('order_itm.precio', $search_like)
                ->or_like('order_itm.qty', $search_like)
                ->or_like('order_itm.total', $search_like)
                ->or_like('order_itm.options', $search_like);
        }

        $r2 = $this->db->get();

        $response = array(
            "numRows" => $r2->num_rows(),
            "datos"   => $r
        );

        return $response;
    }

    public function getNumRowAllDT($idOrderHdr)
    {

        $this->db->select('idOrderHdr')
            ->from('order_itm')
            ->where('idOrderHdr', $idOrderHdr);

        return $this->db->count_all_results();
    }

    public function clearItemsFromOrder($idOrderHdr)
    {

        $this->db->where('idOrderHdr', $idOrderHdr);
        $this->db->delete('order_itm');

        if ($this->db->affected_rows() >= 1) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Get order_itemmetum by idMeta
     */
    function get_order_itemmetum($idMeta)
    {
        return $this->db->get_where('order_itemmeta', array('idMeta' => $idMeta))->row_array();
    }

    /*
     * Get all order_itemmeta
     */
    function get_all_order_itemmeta()
    {
        $this->db->order_by('idMeta', 'desc');
        return $this->db->get('order_itemmeta')->result_array();
    }

    /*
     * function to add new order_itemmetum
     */
    function add_order_itemmetum($params)
    {
        $this->db->insert('order_itemmeta', $params);
        return $this->db->insert_id();
    }

    /*
     * function to update order_itemmetum
     */
    function update_order_itemmetum($idMeta, $params)
    {
        $this->db->where('idMeta', $idMeta);
        return $this->db->update('order_itemmeta', $params);
    }

    /*
     * function to delete order_itemmetum
     */
    function delete_order_itemmetum($idMeta)
    {
        return $this->db->delete('order_itemmeta', array('idMeta' => $idMeta));
    }

    function addOrder($data)
    {
        $this->db->insert('order_hdr', $data);
        return $this->db->insert_id();
    }

    function getAllByCustomer($customer)
    {

        $r = $this->db->query("SELECT oh.*, os.statusText
                                 FROM order_hdr as oh
                                inner join order_status as os on os.idOrderStatus = oh.idOrderStatus 
                                WHERE idUser = $customer
                                order by idOrderHdr desc");
        return $r->result();
    }

    function getById($orderid)
    {
        $r = $this->db->get_where('order_hdr', array('idOrderHdr' => $orderid));
        return $r->row_array();
    }

    public function getAllOrder($start, $length, $search)
    {

        $where = "";

        if ($search != null && trim($search) != "") {

            $search_like = $this->db->escape("%" . $search . "%");

            $where = " where ( idOrderHdr LIKE " . $search_like . " OR
                               idUser     LIKE " . $search_like . " OR
                               created    LIKE " . $search_like . " )";
        }

        // //      Query para levantar los datos
        $r = $this->db->query("SELECT *
                                 FROM order_hdr
                              " . $where
            . " ORDER BY idOrderHdr DESC LIMIT $start, $length");

        // //      Misma query sin LIMIT para contar los registros
        $r2 = $this->db->query("SELECT *
                            FROM order_hdr
                            " . $where);

        $response = array(
            "numRows" => $r2->num_rows(),
            "datos"   => $r
        );

        return $response;
    }

    public function getNumRowAll()
    {

        $r = $this->db->get('order_hdr');

        return $r->num_rows();
    }

    public function get_orders_by_client($idCliente)
    {

        $r = $this->db->get_where('order_hdr', array('idCliente' => $idCliente));
        return $r->result_array();
    }

    public function get_orders_by_user_pending($idUser)
    {

        $r = $this->db->query("SELECT * FROM order_hdr WHERE idUser = " . $idUser . "");
        return $r->result_array();
    }

    public function get_orders_by_client_pending($idCliente)
    {

        $r = $this->db->query("SELECT * FROM clientes cli INNER JOIN relclienteusuario rel ON cli.idCliente = rel.idCliente 
        INNER JOIN users us ON rel.idUsuario = us.user_id
        RIGHT JOIN order_hdr ord ON us.user_id = ord.idUser
        WHERE cli.idCliente = " . $idCliente . "");

        return $r->result_array();
    }

    public function updateOrder($idOrderHdr, $data)
    {

        $this->db->where('idOrderHdr', $idOrderHdr);
        $this->db->update('order_hdr', $data);

        return $idOrderHdr;
    }
}
