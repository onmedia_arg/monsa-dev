<?php

class Tag_model extends CI_Model{

    function __construct(){
        parent::__construct();
    }

    public function get_tags(){
        return $this->db->query("select * from tags")->result_array();
    }

    public function getOrCreateTagByName($name){


        $tag = $this->db->query("select * from tags where name = '$name'")->row_array();
        if(empty($tag)){
            $this->db->query("insert into tags (name) values ('$name')");
            $tag = $this->db->query("select * from tags where name = '$name'")->row_array();
        }
        return $tag;
    }

    public function get_tag_id($tag){

        $id = $this->db->query("select id from tags where name = '$tag'")->row_array();
        
        if(empty($id)){
            $this->db->query("insert into tags (name) values ('$tag')");
            $id = $this->db->query("select id from tags where name = '$tag'")->row_array();
        }
        return $id['id'];
        
    }
}