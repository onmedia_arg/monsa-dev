<?php
 
class Promotions_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function insert_promotion($data) {
        // Insert data into the promotions table
        $r = $this->db->insert('promotions', $data);
        
        return $this->db->insert_id();
    }    

    public function get_promotion_by_id($id) {
        return $this->db->get_where('promotions', array('id' => $id))->row();
    }

    public function delete_promotion($id) {
        return $this->db->delete('promotions', array('id' => $id));
    }

    public function getAll(){
        return $this->db->get('promotions')->result_array();
    }
}
