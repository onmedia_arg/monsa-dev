<?php


class BulkUpdateLogs_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function get_bulk_update_logs($idBulkUpdateLogs)
	{
		return $this->db->get_where('bulk_update_logs', array('idBulkUpdateLogs' => $idBulkUpdateLogs))->row_array();
	}

	public function add_bulk_update_logs($params)
	{
		$this->db->insert('bulk_update_logs', $params);
		$sql = $this->db->last_query();
		return $this->db->insert_id();
	}

	public function get_all_bulk_update_logs($start, $length, $search)
	{

		$where = "";

		if ($search != null && trim($search) != "") {

			$search_like = $this->db->escape("%" . $search . "%");

			$where = " where (filename  LIKE " . $search_like . " OR
                              updated  LIKE " . $search_like . " OR
                              fecha   LIKE " . $search_like . " )";
		}

		$r = $this->db->query("SELECT idBulkUpdateLogs,
								filename,
                                updated,
                                fecha
                            FROM bulk_update_logs
                            " . $where
			. " ORDER BY idBulkUpdateLogs ASC LIMIT $start, $length");

		$response = array(
			"numRows" => $r->num_rows(),
			"datos" => $r
		);

		return $response;

	}

	public function getNumRowAll()
	{

		$r = $this->db->get('bulk_update_logs');

		return $r->num_rows();

	}
}
