<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Order_hdr_model extends CI_Model{

	function __construct(){
		parent::__construct();


	}

    function addOrder($data){
        $this->db->insert('order_hdr',$data);
        return $this->db->insert_id();
    }

    function getAllByCustomer($customer){
    	
        $r = $this->db->query("SELECT oh.*, os.statusText
                                 FROM order_hdr as oh
                                inner join order_status as os on os.idOrderStatus = oh.idOrderStatus 
                                WHERE idUser = $customer
                                order by idOrderHdr desc");
    	return $r->result();
    }

    function getById($orderid){
    	$r = $this->db->get_where('order_hdr',array('idOrderHdr'=>$orderid));
    	return $r->row_array();    	
    }

    public function getAllOrder($start, $length, $search){
        
        $where = "";
        
        if($search != null && trim($search) != ""){

            $search_like = $this->db->escape("%".$search."%");

            $where = " where ( idOrderHdr LIKE " .$search_like . " OR
                               idUser     LIKE " .$search_like . " OR
                               created    LIKE " .$search_like . " )";
         }
      
// //      Query para levantar los datos
        $r = $this->db->query("SELECT *
                                 FROM order_hdr
                              ". $where
                               ." ORDER BY idOrderHdr DESC LIMIT $start, $length");

// //      Misma query sin LIMIT para contar los registros
        $r2 = $this->db->query("SELECT *
                            FROM order_hdr
                            ". $where );

        $response = array(
                    "numRows" => $r2->num_rows(), 
                    "datos"   => $r
                    );

        return $response;

    }    

    public function getNumRowAll(){

        $r = $this->db->get('order_hdr');

        return $r->num_rows();

    }

    public function get_orders_by_client( $idCliente ){

        $r = $this->db->get_where( 'order_hdr', array( 'idCliente' => $idCliente ) );
        return $r->result_array();

    }

    public function get_orders_by_user_pending( $idUser ){

        $r = $this->db->query("SELECT * FROM order_hdr WHERE idUser = ".$idUser."");
        return $r->result_array();

    }

    public function get_orders_by_client_pending( $idCliente ){

        $r = $this->db->query("SELECT * FROM clientes cli INNER JOIN relclienteusuario rel ON cli.idCliente = rel.idCliente 

        INNER JOIN users us ON rel.idUsuario = us.user_id
        
        RIGHT JOIN order_hdr ord ON us.user_id = ord.idUser
        
        WHERE cli.idCliente = ".$idCliente."");

        return $r->result_array();

    }

    public function updateOrder($idOrderHdr, $data) {

       $this->db->where('idOrderHdr', $idOrderHdr);
       $this->db->update('order_hdr', $data);

       return $idOrderHdr;
    }
//-----
}