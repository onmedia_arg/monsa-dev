<?php
 
class User_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('manager/Marca_model', 'Marcas');

    }
    
    /*
     * Get user by user_id
     */
    function get_user($user_id)
    {
        return $this->db->get_where('users', array('user_id'=> $user_id, 'deleted' => 0 ) )->row_array();
    }

    function verify_user_nick($nick)
    {
        return $this->db->get_where('users', array('username'=> $nick, 'deleted' => 0 ) )->row_array();
    }
    function verify_user_email($nick)
    {
        return $this->db->get_where('users', array('email'=> $nick, 'deleted' => 0 ) )->row_array();
    }

    function verify_client_email($nick)
    {
        return $this->db->get_where('clientes', array('mail'=> $nick, 'deleted' => 0 ) )->row_array();
    }
        
    /*
     * Get all users
     */
    function get_all_users( $show_deleted = false )
    {
        $this->db->order_by('user_id', 'desc');
        if ( $show_deleted == false ) {
            $this->db->where('deleted', 0 );
        }
        return $this->db->get('users')->result_array();
    }
        
    /*
     * function to add new user
     */
    function add_user($params)
    {
        $this->db->insert('users',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update user
     */
    function update_user($user_id,$params)
    {
        $this->db->where('user_id',$user_id);
        return $this->db->update('users',$params);
    }
    
    /*
     * function to delete user
     */
    function delete_user($user_id)
    {
        
        $this->update_user( $user_id, array( 'deleted' => 1 ) );

        if ( $this->db->affected_rows() >= 1 ) {
            return true;
        }else{
            return false;
        }

    }

    public function user_is_active( $user_id ){
 
        $user = $this->user_model->get_user( $user_id );

        if ( $user['is_active'] == 1 ) {

            return true;

        }else{
            
            return false;

        }

    }

    public function generate_password(){
        $marcas = $this->Marcas->get_nombres_marcas();
        $indice = random_int(0 , count($marcas)-1);
        return (str_replace(" ","*",ucwords(strtolower($marcas[$indice]['nombre']))).random_int(10000,99999));
    }
}
