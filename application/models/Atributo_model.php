<?php
 
class Atributo_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get atributo by idAtributo
     */
    function get_atributo($idAtributo)
    {
        return $this->db->get_where('atributo',array('idAtributo'=>$idAtributo))->row_array();
    }

    function get_valores_por_atributo($idAtributo)
    { 

        return $this->db->get_where( 'atributo', array( 'idAtributo' => $idAtributo ) )->unbuffered_row('array');

    }

    /*
     * Get all atributo
     */
    function get_all_atributo()
    {
        $this->db->order_by('idAtributo', 'desc');
        return $this->db->get('atributo')->result_array();
    }

    function get_all_atributo_join($str = '')
    {
        $this->db->select('
                            at.idAtributo,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            at.nombre,
                            at.slug,
                            at.valor
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = at.idfamilia', 'OUTER LEFT');
        $this->db->order_by('at.nombre', 'asc');
        if ($str != '') {
            $this->db->where('fa.slug', $str);
        }
        $query = $this->db->get('atributo as at');
        return $query->result_array();

    }

    function get_all_atributo_by_family($idfamilia)
    {
        $this->db->select('
                            at.idAtributo,
                            fa.idFamilia,
                            fa.nombre as familia,
                            fa.slug as familiaSlug,
                            at.nombre,
                            at.slug,
                            at.valor
                        ');
        $this->db->join('familia as fa', 'fa.idFamilia = at.idfamilia', 'OUTER LEFT');
        $this->db->where('fa.idFamilia',$idfamilia);
        $this->db->order_by('at.nombre', 'asc');
        $query = $this->db->get('atributo as at');
        return $query->result_array();
    }
        
    /*
     * function to add new atributo
     */
    function add_atributo($params)
    {
        $this->db->insert('atributo',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update atributo
     */
    function update_atributo($idAtributo,$params)
    {
        $this->db->where('idAtributo',$idAtributo);
        return $this->db->update('atributo',$params);
    }
    
    /*
     * function to delete atributo
     */
    function delete_atributo($idAtributo)
    {
        return $this->db->delete('atributo',array('idAtributo'=>$idAtributo));
    }

    /*
     * Funcion para tomar los valores de los atributos marcados como variables
     */
    function get_atributo_for_prod_var($idProducto){

       $r = $this->db->query(" SELECT pa.idAtributo, pa.is_variacion, at.nombre, at.valor  
                                 FROM producto_atributo as pa 
                                inner join atributo as at on at.idAtributo = pa.idAtributo
                                WHERE pa.is_variacion = true
                                  and pa.idProducto = " . $idProducto);

       return $r->result_array(); 

    }

    function get_atributo_for_variables($idParent){

        $r = $this->db->query("SELECT  pa.idAtributo, 
                                       at.nombre,
                                       at.slug,
                                       pa.idProducto, 
                                       pr.idParent, 
                                       pa.valores
                                  FROM `producto_atributo` AS pa 
                                 INNER JOIN producto AS pr ON pr.idProducto = pa.idProducto
                                 INNER JOIN atributo AS at ON at.idAtributo = pa.idAtributo
                                 WHERE pr.idTipo = 2 
                                   AND pr.idParent = " . $idParent . "
                                 GROUP BY pa.valores
                                 ORDER BY pr.idParent   ASC, 
                                          pa.idAtributo ASC");
       
        return $r->result_array(); 
    } 

}
