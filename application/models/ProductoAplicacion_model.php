<?php
 
class ProductoAplicacion_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get producto_aplicacion by idProdApp
     */
    function get_producto_aplicacion($idProdApp)
    {
        return $this->db->get_where('producto_aplicacion',array('idProdApp'=>$idProdApp))->row_array();
    }
    function get_prodApp_by_idMoto($idMoto)
    {
        return $this->db->get_where('aplicacion_producto',array('idMoto'=>$idMoto))->row_array();
    }
    function get_prodApp_by_idProducto($idProducto)
    {
        return $this->db->get_where('producto_aplicacion',array('idProducto'=>$idProducto))->row_array();
    }
    
    /*
     * Get all producto_aplicacion count
     */
    function get_all_producto_aplicacion_count()
    {
        $this->db->from('producto_aplicacion');
        return $this->db->count_all_results();
    }
        
    /*
     * Get all producto_aplicacion
     */
    function get_all_producto_aplicacion($params = array())
    {
        $this->db->order_by('idProdApp', 'desc');
        if(isset($params) && !empty($params))
        {
            $this->db->limit($params['limit'], $params['offset']);
        }
        return $this->db->get('producto_aplicacion')->result_array();
    }
        
    /*
     * function to add new producto_aplicacion
     */
    function add_producto_aplicacion($params)
    {
        $this->db->insert('producto_aplicacion',$params);
        return $this->db->insert_id();
    }

    function add_aplicacion_producto($params)
    {
        $this->db->insert('aplicacion_producto',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update producto_aplicacion
     */
    function update_producto_aplicacion($idProdApp,$params)
    {
        $this->db->where('idProdApp',$idProdApp);
        return $this->db->update('producto_aplicacion',$params);
    }
    function update_aplicacion_producto($idAppProd,$params)
    {
        $this->db->where('idAppProd',$idAppProd);
        return $this->db->update('aplicacion_producto',$params);
    }
    
    /*
     * function to delete producto_aplicacion
     */
    function delete_producto_aplicacion($idProdApp)
    {
        return $this->db->delete('producto_aplicacion',array('idProdApp'=>$idProdApp));
    }
}
