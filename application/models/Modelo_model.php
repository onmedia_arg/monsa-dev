<?php
 
class Modelo_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get modelo by idModelo
     */
    function get_modelo($idModelo)
    {
        return $this->db->get_where('modelo',array('idModelo'=>$idModelo))->row_array();
    }
        
    /*
     * Get all modelo
     */
    function get_all_modelo($id='')
    {
        $this->db->select('
                            mo.idModelo,
                            ma.nombre as marca,
                            mo.nombre as nombre,
                            mo.slug,
                            mo.year
                        ');
        $this->db->join('marca as ma', 'ma.idMarca = mo.idMarca', 'OUTER LEFT');
        if ($id != '') {
            $this->db->where('mo.idMarca', $id);
        }
        $this->db->order_by('mo.idModelo', 'desc');
        $query = $this->db->get('modelo as mo');
        return $query->result_array();       
    }
        
    /*
     * function to add new modelo
     */
    function add_modelo($params)
    {
        $this->db->insert('modelo',$params);
        return $this->db->insert_id();
    }

    function add_modelo_batch($arr)
    {
        $this->db->insert_batch('modelo',$arr);
    }
    
    /*
     * function to update modelo
     */
    function update_modelo($idModelo,$params)
    {
        $this->db->where('idModelo',$idModelo);
        return $this->db->update('modelo',$params);
    }
    
    /*
     * function to delete modelo
     */
    function delete_modelo($idModelo)
    {
        return $this->db->delete('modelo',array('idModelo'=>$idModelo));
    }

}