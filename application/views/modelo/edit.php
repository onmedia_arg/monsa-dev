<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Editar Modelo</h3>
            </div>
            <?php 
            	$attributes = array('id' => 'form', 'onsubmit' => 'return formOnSubmit()');
            	echo form_open('modelo/edit/'.$modelo['idModelo'], $attributes);
           	?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="nombre" class="control-label">Marca</label>
						<div class="form-group">
							<select name="idMarca" class="form-control" id="idMarca">
								<option value="">select marca</option>
								<?php 
									foreach($all_marca as $marca)
									{
										$selected = ($marca['idMarca'] == $this->input->post('idMarca') || $marca['idMarca'] == $modelo['idMarca']) ? ' selected' : "";
										printf(
												'<option value="%s" %s>%s</option>',
													$marca['idMarca'],
													$selected,
													$marca['nombre']
												);
									} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="nombre" class="control-label">Nombre</label>
						<div class="form-group">
							<input type="text" name="nombre" value="<?php echo ($this->input->post('nombre') ? $this->input->post('nombre') : $modelo['nombre']); ?>" class="form-control" id="nombre" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="slug" class="control-label">Slug</label>
						<div class="form-group">
							<input type="text" name="slug" value="<?php echo ($this->input->post('slug') ? $this->input->post('slug') : $modelo['slug']); ?>" class="form-control" id="slug" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="year" class="control-label">Año</label>
						<div class="form-group">
							<select class="year form-control" name="year" id="year">
								  <option value="0">Seleccione Año</option>
								  <option value="todos">Todos</option>
								  <?php 
								  	$post = ($this->input->post('year')?$this->input->post('year'):$modelo['year']);
									for ($i = date('Y'); $i > (date('Y') - 100); $i--) {
										$selected = ($post == $i)?'selected="selected"': '';
										printf('<option value="%s" %s>%s</option>', $i, $selected,  $i);
									}
								  ?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Guardar
				</button>
				<div style="display: none;">
					<h4 class="text text-danger">Ocurrió un error</h4>
					<ul class="text text-warning list-group" id="form-error"></ul>
				</div>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>

<?php $this->load->view('modelo/script'); ?>