
<!-- Page content -->
<style>
button#btn-up-popup {
    height: 2rem;
}

.h-20{
 height: 20rem;   
}

.w-20{
 width: 20rem;   
}
.text-gray-400{
    color:#e1e1e1;
}

.items-center{
    align-items:center;
}

#img-place{
    cursor:pointer;
}

</style>
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content">
            <div class="row">
                <div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="">Popup</h3>
                            <div class="d-flex items-center"> 
                                <div class="d-flex ">
                                    <input type="file" name="img-popup" id="img-popup" class="my-file">    
                                    <div>
                                        <button id="btn-up-popup" class='btn btn-outline-primary btn-sm mr-4'><i class='icon-image2 mr-2'></i>Subir Imagen</button>
                                        <div style="color:#666; font-size:0.9em; margin-top:10px">Tamaño recomendado:<br> 600px x 600px</div>                       
                                    </div>
                                </div>

                                <div id="image-preview">
                                    <svg id="img-place" xmlns="http://www.w3.org/2000/svg" class="h-20 w-20 text-gray-400" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" /></svg>
                                </div>
                            </div>
                            <legend class="font-weight-semibold"><i class="icon-gear mr-2"></i>Configuración</legend>  

                            <div class="form-group">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input id="active" name="active" type="checkbox" class="form-check-input-styled" data-fouc>
                                        Activo
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row">                                  
                                <div class="col-md-6">
                                    <label for="url">Link</label>
                                    <input id="url" name="url" type="text" placeholder="" class="form-control">
                                </div>
                            </div>                                                                                      
                        </div>
                        <div class='card-footer d-flex justify-content-between'>
                            <div class="d-flex "> 
                                <input type="file" name="file-xls" id="file-xls" class="my-file">    
                                <button id="btn-save-popup" class='btn btn-outline-primary btn-sm'><i class='icon-floppy-disk mr-2'></i>GUARDAR</button>                       
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- <div class="row" id="files-list">
           
                <div class="col-md-4">
                    <div class='card'>
                        <div class='card-body'>
                            <div class='media'>
                                <div class='mr-3'>
                                    <a href='' id="dwnl-icon-xls" ><img style="max-width:45px" src="<?= base_url('/resources/img/xls.png')?>"></a>
                                </div>
                                <div class='media-body'>
                                    <div class='font-weight-bold'>TIPO: XLS<span id="dwnl-date-xls" class='text-muted float-right'></span></div>
                                </div>
                            </div>
                        </div>                        
                        <div class='card-footer d-flex justify-content-between'>
                            <a href="" id="dwnl-btn-xls" type='button' class='btn btn-outline-success btn-sm'><i class='icon-download mr-2'></i>Descargar</a>
                            <div class="d-flex "> 
                                <input type="file" name="file-xls" id="file-xls" class="my-file">    
                                <button id="dwnl-btn-up-xls" class='btn btn-outline-primary btn-sm'><i class='icon-file-upload2 mr-2'></i>Editar</button>                       
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class='card'>
                        <div class='card-body'>
                            <div class='media'>
                                <div class='mr-3'>
                                    <a href='' id="dwnl-icon-pdf" ><img style="max-width:45px" src="<?= base_url('/resources/img/pdf.png')?>"></a>
                                </div>
                                <div class='media-body'>
                                    <div class='font-weight-bold'>TIPO: PDF<span id="dwnl-date-pdf" class='text-muted float-right'></span></div>
                                </div>
                            </div>
                        </div>                        
                        <div class='card-footer d-flex justify-content-between'>
                            <a href="" id="dwnl-btn-pdf" type='button' class='btn btn-outline-success btn-sm'><i class='icon-download mr-2'></i>Descargar</a>
                            <div class="d-flex "> 
                                <input type="file" name="file-pdf" id="file-pdf" class="my-file">    
                                <button id="dwnl-btn-up-pdf" class='btn btn-outline-primary btn-sm'><i class='icon-file-upload2 mr-2'></i>Editar</button>                       
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class='card'>
                        <div class='card-body'>
                            <div class='media'>
                                <div class='mr-3'>
                                    <a href='' id="dwnl-icon-csv" ><img style="max-width:45px" src="<?= base_url('/resources/img/csv.png')?>"></a>
                                </div>
                                <div class='media-body'>
                                    <div class='font-weight-bold'>TIPO: CSV<span id="dwnl-date-csv" class='text-muted float-right'></span></div>
                                </div>
                            </div>
                        </div>                        
                        <div class='card-footer d-flex justify-content-between'>
                            <a href="" id="dwnl-btn-csv" type='button' class='btn btn-outline-success btn-sm'><i class='icon-download mr-2'></i>Descargar</a>
                            <div class="d-flex "> 
                                <input type="file" name="file-csv" id="file-csv" class="my-file">    
                                <button id="dwnl-btn-up-csv" class='btn btn-outline-primary btn-sm'><i class='icon-file-upload2 mr-2'></i>Editar</button>                       
                            </div>
                        </div>
                    </div>
                </div>                                

            </div> -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->

<!-- End of file index.php
Location: ./views/manager/download/files.php -->