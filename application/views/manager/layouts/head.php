<?php
// $current_user_id = $this->ion_auth->get_user_id();
// $current_user = $this->ion_auth->user( $current_user_id )->row_array();
// $user_groups = $this->ion_auth->get_users_groups( $current_user_id )->result_array();
// $groups = current_user_groups(); 
?>

<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>MONSA - onMedia</title>

	<!-- Global stylesheets -->

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
		type="text/css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css"
		integrity="sha256-46qynGAkLSFpVbEBog43gvNhfrOj+BmwXdxFgVK/Kvc=" crossorigin="anonymous" />
	<link href="<?php echo base_url(); ?>assets/manager/template/css/icons/icomoon/styles.min.css" rel="stylesheet"
		type="text/css">
	<link href="<?php echo base_url(); ?>assets/manager/template/css/bootstrap.min.css" rel="stylesheet"
		type="text/css">
	<link href="<?php echo base_url(); ?>assets/manager/template/css/bootstrap_limitless.min.css" rel="stylesheet"
		type="text/css">
	<link href="<?php echo base_url(); ?>assets/manager/template/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/manager/template/css/components.min.css" rel="stylesheet"
		type="text/css">
	<link href="<?php echo base_url(); ?>assets/manager/template/css/colors.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/manager/plugins/iziToast/css/iziToast.min.css" rel="stylesheet"
		type="text/css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
	<link href="<?php echo base_url(); ?>assets/manager/css/common.css" rel="stylesheet" type="text/css">
	<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">

	<!-- /global stylesheets -->

	<!-- Core JS files -->

	<script type="text/javascript"> var baseUrl = '<?= base_url() ?>'; </script>

	<script src="<?php echo base_url(); ?>assets/manager/template/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/manager/template/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/manager/js/plugins/loaders/blockui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/manager/template/js/app.js"></script>
	<script src="<?php echo base_url(); ?>assets/manager/plugins/iziToast/js/iziToast.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/manager/plugins/uniform/uniform.min.js"></script>

	<script src="<?php echo base_url(); ?>assets/manager/js/custom.js"></script>
	<script src="<?php echo base_url(); ?>assets/manager/js/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/manager/js/moment_locales.min.js"></script>

	<script src="<?php echo base_url(); ?>assets/manager/plugins/forms/selects/select2.min.js"></script>
	<!-- Bootstrap switch -->
	<link rel="stylesheet"
		href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/css/bootstrap3/bootstrap-switch.min.css"
		integrity="sha256-sj3qkRTZIL8Kff5fST1TX0EF9lEmSfFgjNvuiw2CV5w=" crossorigin="anonymous" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.4/js/bootstrap-switch.min.js"
		integrity="sha256-AKUJYz2DyEoZYHh2/+zPHm1tTdYb4cmG8HC2ydmTzM4=" crossorigin="anonymous"></script>

	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

	<!-- CSS dinamicos que llegan desde el controlador -->
	<?php
	if (isset($csss)) {
		if (is_array($csss)) {
			foreach ($csss as $css) {
				echo '<link href="' . $css . ' "rel="stylesheet" type="text/css">' . PHP_EOL;
			}
		}
	}
	?>
	<!-- fin CSS dinámicos desde controlador -->

	<!-- <style type="text/css">

		.form-group.has-error.has-danger * {
			color: red;
			border-color: red;
		}

		.form-group * {
			transition: ease .2s;
		}

	</style> -->

	<style>
		a.navbar-nav-link i {
			display: none;
		}

		@media (min-width: 1200px) {
			a.navbar-nav-link i {
				display: block;
			}
		}
	</style>
</head>

<body data-base_url="<?= base_url() ?>">

	<!-- Page header -->
	<div style="margin-bottom:0px" class="page-header page-header-dark">

		<!-- Secondary navbar -->
		<div class="navbar navbar-expand-md navbar-dark border-top-0">
			<div class="d-md-none w-100">
				<button type="button" class="navbar-toggler d-flex align-items-center w-100" data-toggle="collapse"
					data-target="#navbar-navigation">
					<i class="icon-more mr-2"></i>
					Menú
				</button>
			</div>

			<div class="navbar-collapse collapse" id="navbar-navigation">
				<ul class="navbar-nav navbar-nav-highlight">

					<li class="nav-item">
						<a href="<?= base_url() ?>" class="navbar-nav-link">
							<i class="icon-home4 mr-2"></i>
							Inicio
						</a>
					</li>
					<li class="nav-item">
						<a href="<?= base_url('manager/Content') ?>" class="navbar-nav-link">
							<i class="icon-stack-empty mr-2"></i>
							Contenido
						</a>
					</li>

					<li class="nav-item">
						<a href="<?= base_url('manager/Products') ?>" class="navbar-nav-link">
							<i class="icon-price-tag3 mr-2"></i>
							Productos
						</a>
					</li>

					<li class="nav-item">
						<a href="<?= base_url('manager/Orders') ?>" class="navbar-nav-link">
							<i class="icon-cart2 mr-2"></i>
							Pedidos
						</a>
					</li>

					<li class="nav-item">
						<a href="<?= base_url('manager/clients') ?>" class="navbar-nav-link">
							<i class="icon-address-book2 mr-2"></i>
							Clientes
						</a>
					</li>

					<li class="nav-item">
						<a href="<?= base_url('manager/Downloads') ?>" class="navbar-nav-link">
							<i class="icon-cloud-download mr-2"></i>
							Descargas
						</a>
					</li>

					<li class="nav-item">
						<a href="<?= base_url('manager/Popup') ?>" class="navbar-nav-link">
							<i class="icon-image2 mr-2"></i>
							Popup
						</a>
					</li>

					<li class="nav-item">
						<a href="<?= base_url('manager/Reports') ?>" class="navbar-nav-link">
							<i class="icon-stats-growth mr-2"></i>
							Reportes
						</a>
					</li>

				</ul>

				<ul class="navbar-nav navbar-nav-highlight ml-md-auto">
					<li class="nav-item">
						<a href="<?= base_url('FrontController/shop') ?>" class="navbar-nav-link">
							<i class="icon-store mr-2"></i>
							Ver la Tienda
						</a>
					</li>

					<!-- <li class="nav-item">
							<a href="<?= base_url('manager/Customizing') ?>" class="navbar-nav-link">
								<i class="icon-hammer-wrench mr-2"></i>
								Configuración
							</a>
					</li> -->

					<li class="nav-item dropdown dropdown-user">
						<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle"
							data-toggle="dropdown">
							<i class="icon-gear mr-2"></i>
							<span>Configuración</span>
						</a>


						<div class="dropdown-menu dropdown-menu-right">
							<a href="<?= base_url('manager/Carrousel') ?>" class="dropdown-item">
								<i class="icon-more mr-2"></i>
								Carruseles
							</a>
							<a href="<?= base_url('manager/Customizing') ?>" class="dropdown-item">
								<i class="icon-hammer-wrench mr-2"></i>
								Datos Maestros
							</a>
							<a href="<?= base_url('manager/User') ?>" class="dropdown-item">
								<i class="icon-users2 mr-2"></i>
								Usuarios
							</a>
							<a href="<?= base_url('Sys_Config/index') ?>" class="dropdown-item">
								<i class="icon-hammer-wrench mr-2"></i>
								Parametros del Sistema
							</a>


						</div>
					</li>

					<li class="nav-item dropdown dropdown-user">
						<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle"
							data-toggle="dropdown">
							<i class="icon-user mr-2"></i>
							<span>Hola</span>
						</a>

						<div class="dropdown-menu dropdown-menu-right">
							<a href="<?= base_url('manager/Usuarios/profile/') ?>" class="dropdown-item"><i
									class="icon-user-plus"></i> Mi perfil</a>
							<a href="<?= base_url('start/logout') ?>" class="dropdown-item"><i class="icon-switch2"></i>
								Salir</a>
						</div>
					</li>
				</ul>

			</div>
		</div>
		<!-- /secondary navbar -->

	</div>
	<!-- /page header -->

	<div class="page-content p-0">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<div class="page-header page-header-light">

				<div class="page-header-content header-elements-md-inline">
					<div class="page-title d-flex">
						<h4>
							<a href="#" onclick="window.history.go(-1); return false;"><i
									class="icon-arrow-left52 mr-2 text-dark"></i></a>
							<span id="titulo"
								class="font-weight-semibold"><?= (isset($title)) ? $title : 'Inicio' ?></span>
						</h4>
						<!-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> -->
					</div>

					<div class="header-elements">
						<div class="d-flex justify-content-center">

							<?php
							if (isset($buttons)):
								foreach ($buttons as $data):
									?>
									<a href="<?= $data['url'] ?>" class="btn btn-link btn-float text-default" <?= $data['attr'] ?>><i class="<?= $data['icon'] ?> text-primary"></i>
										<span><?= $data['title'] ?></span></a>
								<?php
								endforeach;
							endif;
							?>

						</div>
					</div>
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
						<div class="breadcrumb">
							<?php
							if (isset($breadcumbs)):
								$i = 1;
								foreach ($breadcumbs as $title => $url):

									if (count($breadcumbs) !== $i): ?>
										<a href="<?= $url ?>" class="breadcrumb-item">
											<?= ($i == 1 || $i == '1') ? '<i class="icon-home2 mr-2"></i>' : null; ?>
											<?= $title ?>
										</a>
									<?php else: ?>
										<span class="breadcrumb-item active"><?= $title ?></span>
									<?php
									endif;
									$i++;
								endforeach;
							else:
								?>
								<a href="<?= base_url() ?>" class="breadcrumb-item"><i
										class="icon-home2 mr-2"></i>Inicio</a>
							<?php
							endif;
							?>
						</div>
						<!-- <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a> -->
					</div>
				</div>
			</div>
			<!-- /page header -->