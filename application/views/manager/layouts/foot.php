
			<!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light">
<!--		<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
			            <span class="navbar-text">
							&copy; 2020 - Powered by <a href="http://www.onmedia.com.ar" target="_blank">onMedia</a>
						</span>
						<div class="ml-lg-auto">
							<img class="footer-logo" src="<?= base_url("assets/images/logo-nav.jpg")?>">
						</div>
				</div> -->



				<div class="text-center d-lg-none w-100">
					<div class="navbar-toggler" data-toggle="collapse" data-target="#navbar-footer">
						<div class="ml-lg-auto mt-2">
							<img class="footer-logo" src="<?= base_url("assets/images/logo-nav.jpg")?>">
						</div>
			            <span class="navbar-text">
							&copy; 2021 - Powered by <a href="https://www.onmedia.com.ar" target="_blank">onMedia</a>
						</span>
					</div>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
			            <span class="navbar-text">
							&copy; 2021 - Powered by <a href="https://www.onmedia.com.ar" target="_blank">onMedia</a>
						</span>
						<!-- <div class="ml-lg-auto">
							<img class="footer-logo" src="<?= base_url("assets/images/logo-nav.jpg")?>">
						</div> -->
					<ul class="navbar-nav ml-lg-auto">
						<li class="nav-item"><a href="" class="navbar-nav-link" target="_blank" data-toggle="modal" data-target="#modal_help"><i class="icon-info22  mr-2"></i> Ayuda</a></li>
					</ul>						


				</div>
			</div>
			<!-- /footer -->
		
		</div>
		<!-- /main content -->
			
	</div>
	<!-- /page content -->
	
<!-- Scripts dinamicos que llegan desde el controlador -->
	<?php
		echo "<div class='d-none'><pre>";
		var_dump($this->session->userdata());
		echo "</pre></div>";


		if(isset($scripts)){
			if(is_array($scripts)){
				foreach($scripts as $script){
					echo '<script src="' . $script . '"></script>'.PHP_EOL;
				}
			}
		}
	?>
<!-- fin Scripts dinámicos desde controlador -->

<!-- Modals -->
	<?php if ( isset( $modals ) && $modals !== '' ) {
		$this->load->view( $modals );
	} 
		$this->load->view('manager/modal-help');
	?>


</body>
</html>
