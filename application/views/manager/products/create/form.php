<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <form id="formEditProduct" method="post">
                    <legend class="font-weight-semibold">Datos del Producto</legend>
                    <div class="form-group row ml-2">
                        <div class="col-md-3">
                            <label for="nombre">Nombre</label>
                                <input id="nombre" name="nombre" type="text" class="form-control">
                        </div>
                        <div class="col-md-3">
                            <label for="idFamilia">Familia</label>
                            <select id="idFamilia" name="idFamilia" class="form-control">
                                <option></option>
                                <?php foreach ($familias as $tipo => $info): ?>
                                    <option value="<?php echo $info["idFamilia"] ?>"><?php echo $info["nombre"] ?></option>"
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="idMarca">Marca</label>
                            <select id="idMarca" name="idMarca" class="form-control">
                                <option></option>
                                <?php foreach ($marcas as $tipo => $info): ?>
                                    <option value="<?php echo $info["idMarca"] ?>"><?php echo $info["nombre"] ?></option>"
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <!-- <div class="col-md-3">
                            <div class="d-flex justify-content-between align-items-center">
                                <label for="show">Mostrar en: </label>
                                <button id="clear-show" class="text-danger btn btn-link">Limpiar</button>
                            </div>
                            <select id="show" name="show" class="form-control">
                                <option></option>
                                <option value="destacado">Destacado</option>
                                <option value="masvendido">Más Vendido</option>
                            </select>
                        </div> -->
                    </div>

                    <div class="form-group row ml-2">
                        <!-- <div class="col-md-3">
                            <label for="modelo">Modelo</label>
                            <input id="modelo" name="modelo" type="text" placeholder="Modelo" class="form-control">
                        </div>

                        <div class="col-md-2">
                            <label for="precio">Precio</label>
                            <input id="precio" name="precio" type="text" placeholder="Precio" class="form-control">
                        </div>

                        <div class="col-md-2">
                            <label for="price_public">Precio Minorista</label>
                            <input id="price_public" name="price_public" type="text" placeholder="Precio Minorista"
                                class="form-control">
                        </div> -->

                        <div class="col-md-2">
                            <label for="idTipo">Tipo de Producto</label>
                            <select id="idTipo" name="idTipo" class="form-control">
                                <option></option>
                                <option value="0">Simple</option>
                                <option value="1">Variable</option>
                                <option value="2">Variación</option>
                            </select>
                        </div>

                    </div>

                    <div class="form-group row ml-2">
                        <!-- <div class="col-md-4">
                            <label for="sku">SKU</label>
                            <input id="sku" name="sku" type="text" placeholder="SKU" class="form-control">
                        </div> -->
                        <div class="col-md-5">
                            <label for="slug">Slug</label>
                            <input id="slug" name="slug" type="text" placeholder="Slug" class="form-control">
                        </div>
                    </div>

                    <!-- <div class="form-group row ml-2">
                        <div class="col-md-4">
                            <label for="search">Términos de búsqueda</label>
                            <a onclick="addSearch()" id="add-button"> <i class="fas fa-plus"></i> Agregar Término</a>
                            <input id="search" name="search" type="text" placeholder="Terminos de búsqueda"
                                class="form-control">
                            <div id="badges-container" class="mt-2">

                            </div>
                        </div>

                        <div class="col-md-5">
                            <label for="search">Visibilidad</label>
                            <select id="visibilidad" name="visibilidad" class="form-control">
                                <option></option>
                                <option value="publicar">Publicar</option>
                                <option value="oculto">Ocultar</option>
                            </select>
                        </div>

                    </div> -->


                    <div class="form-group row ml-2">
                        <div class="col-lg-12">
                            <label for="descripcion">Descripción</label>
                            <textarea id="descripcion" name="descripcion" type="text" class="form-control" rows="4"
                                placeholder="Indique aquí observaciones"></textarea>
                        </div>
                    </div>


                    <!-- <legend class="font-weight-semibold"><i class="icon-price-tag3 mr-2"></i>Promociones</legend>
                    <div id="promo-fields" class="form-group row align-items-center ml-2">
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input id="is_promo_active" name="is_promo_active" type="checkbox" class="form-check-input-styled" data-fouc>
                                        Promoción Activa
                                    </label>
                                </div>
                            </div>                            
                        </div>
                        
                        <div class="col-md-3">
                            <label for="price_promo">Precio Promoción</label>
                            <input id="price_promo" name="price_promo" type="text" placeholder="Precio" class="form-control">
                        </div>

                        <div class="col-md-3">
                            <label class="">Etiqueta</label>
                            <div id="etiqueta-container">
                                <select id="etiqueta" name="etiqueta" class="form-control select-multiple-tags"
                                    multiple="multiple" data-fouc>
                                    <option></option>
                                    <?php foreach ($tags as $tag): ?>
                                        <option value="<?php echo $tag["name"] ?>"><?php echo $tag["name"] ?></option>
                                    <?php endforeach; ?>
                                </select>

                            </div>
                        </div>
                    </div> -->

                    <!-- <legend class="font-weight-semibold"><i class="icon-user-lock mr-2"></i>Stock</legend>

                    <div class="form-group row">
                        <div class="col-xl-3 col-md-6">
                            <label for="stock_type">Tipo de Stock</label>
                            <select id="stock_type" name="stock_type" class="form-control">
                                <option></option>
                                <option value="fijo">Fijo</option>
                                <option value="formula">Formula</option>
                            </select>
                        </div>
                    </div>

                    <div id="group-stock-fijo" class="form-group row ml-2">
                        <div class="col-xl-3 col-md-6">
                            <label class="col-lg-12">Stock</label>
                            <div class="row">
                                <div class="col-lg-12">
                                    <select id="stock_fijo" name="stock_fijo" class="form-control">
                                        <option></option>
                                        <option value="alto">Alto</option>
                                        <option value="medio">Medio</option>
                                        <option value="bajo">Bajo</option>
                                    </select>
                                </div>


                            </div>
                        </div>

                    </div> -->

                    <!-- <div id="group-stock-formula" class="form-group row ml-2">
                        <div class="col-md-3">
                            <div class="d-flex justify-content-between">
                                <label for="stock_quantity">Cantidad</label>
                                <button id="update_stock" class="text-success btn btn-link"><i
                                        class=" icon-loop3 mr-1"></i>Actualizar Stock</button>
                            </div>
                            <input id="stock_quantity" name="stock_quantity" type="number" placeholder=""
                                class="form-control">
                        </div>
                        <div class="col-md-3">
                            <label for="stock_limit1">Límite Bajo</label>
                            <input id="stock_limit1" name="stock_limit1" type="number" placeholder=""
                                class="form-control mt-1">
                        </div>

                        <div class="col-md-3">
                            <label for="stock_limit2">Límite Medio</label>
                            <input id="stock_limit2" name="stock_limit2" type="number" placeholder=""
                                class="form-control mt-1">
                        </div>

                    </div> -->

                    <div id="group-stock-formula" class="form-group row ml-2">
                        <div class="col-md-3">
                            <label for="cantidad_minima">Cantidad mínima</label>
                            <input id="cantidad_minima" name="cantidad_minima" type="number"
                                placeholder="Cantidad Mínima" class="form-control">
                        </div>

                        <div class="col-md-3">
                            <label for="rango">Rango de compra</label>
                            <input id="rango" name="rango" type="number" placeholder="Rango de compra"
                                class="form-control">
                        </div>

                    </div>


                    <legend class="font-weight-semibold"><i class="icon-user-lock mr-2"></i>Atributos</legend>
                    <div id="attr-list" class="form-group row ml-2">

                    </div>

                    <!-- <legend class="font-weight-semibold"><i class="icon-user-lock mr-2"></i>Dimensiones</legend>
                    <div class="form-group row ml-2">
                        <div class="col-md-3">
                            <label class="label-input" for="peso">Peso <span class="label-detail">Indicar el peso en
                                    gramos</span></label>
                            <input id="peso" name="peso" type="text" placeholder="Peso" class="form-control">
                        </div>
                        <div class="col-md-3">
                            <label class="label-input" for="alto">Alto <span class="label-detail">Indicar medida en
                                    centímetros</span></label>
                            <input id="alto" name="alto" type="text" placeholder="Alto" class="form-control">
                        </div>
                        <div class="col-md-3">
                            <label class="label-input" for="ancho">Ancho <span class="label-detail">Indicar medida en
                                    centímetros</span></label>
                            <input id="ancho" name="ancho" type="text" placeholder="Ancho" class="form-control">
                        </div>
                        <div class="col-md-3">
                            <label class="label-input" for="largo">Largo <span class="label-detail"> Indicar medida en
                                    centímetros</span></label>
                            <input id="largo" name="largo" type="text" placeholder="Largo" class="form-control">
                        </div>
                    </div>


                    <legend class="font-weight-semibold"><i class="icon-user-lock mr-2"></i>Galería</legend>
                    <div class="col-md-3">
                        <input type="file" name="imagen" id="imagen" class="imageInput float-left">
                        <button id="btnUploadImage" class="btn btn-outline-primary btn-sm">Subir Imagen</button>
                    </div>
                    <div class="thumbs-list" id="thumbs"></div> -->
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    .badge {
        display: inline-flex;
        align-items: center;
        background-color: #28a745;
        /* Verde */
        color: white;
        padding: 5px 10px;
        border-radius: 10px;
        margin: 5px;
    }

    .badge .icon {
        margin-left: 5px;
        cursor: pointer;
    }

    #add-button {
        margin-left: 5px;
        /* Espaciado entre label y botón */
        padding: 5px 10px;
        background-color: #007bff;
        /* Azul */
        color: white;
        border: none;
        border-radius: 5px;
        cursor: pointer;
        font-size: 12px;
        /* Tamaño de fuente pequeño */
        display: inline-flex;
        align-items: center;
    }

    #add-button .fa-plus {
        margin-right: 5px;
        /* Espacio entre icono y texto */
    }
</style>