
<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content">
            <!-- Orders history (static table) -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4>Filtro</h4>
                            <div class="row">
                                <div class="col-4 col-md-6 col-lg-3 col-xl-3">
                                    <div class="form-group">
                                        <label for="filter_marca">Marca:</label>
                                        <select name="filter_marca" id="filter_marca" class="form-control filter-select">
                                            <option></option> 
                                            <?php foreach ( $marcas as $tipo=>$info ):?>
                                                <option value="<?php echo $info["idMarca"] ?>"><?php echo $info["nombre"] ?></option>"
                                            <?php endforeach; ?>                                            
                                        </select> 
                                    </div>
                                </div>

                                <div class="col-4 col-md-6 col-lg-3 col-xl-3">
                                    <div class="form-group">
                                        <label for="filter_familia">Familia:</label>
                                        <select name="filter_familia" id="filter_familia" class="form-control filter-select">
                                            <option></option> 
                                            <?php foreach ( $familias as $tipo=>$info ):?>
                                                <option value="<?php echo $info["idFamilia"] ?>"><?php echo $info["nombre"] ?></option>"
                                            <?php endforeach; ?>                                              
                                        </select> 
                                    </div>
                                </div>

                                <div class="col-4 col-md-6 col-lg-3 col-xl-3">
                                    <div class="form-group">
                                        <label for="filter_show">Mostrar:</label>
                                        <select name="filter_show" id="filter_show" class="form-control filter-select">
                                            <option></option> 
                                            <option value="destacado">Destacado</option>
                                            <option value="masvendido">Más Vendido</option>                                            
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-4 col-md-2">
                                    <div class="form-group">
                                    <label for="filter_show">Promoción:</label>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input id="filter_promo_active" name="filter_promo_active" type="checkbox" class="form-check-input-styled" data-fouc>
                                                Promoción Activa
                                            </label>
                                        </div>                            
                                    </div>
                                </div>

                                <div class="col-4 col-md-6 col-lg-3 col-xl-3">
                                    <div class="form-group row cust-btn-group">
                                        <div class="text-center">
                                            <button type="button" class="btn btn-primary" id="set-filtro">
                                            <i class="icon-search4 mr-2"></i>Filtrar</button>
                                            <button type="button" class="btn btn-outline-warning ml-1" id="btn-clear-flt">
                                            <i class="icon-loop3 mr-2"></i>Limpiar Filtro</button>
                                        </div>
                                    </div>	
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <table class="table" id="products-list">
                       
                            <thead>
                                <tr>
                                    <th style="width:10%">SKU</th>
                                    <th>Tipo</th>
                                    <th>Familia</th>
                                    <th>Marca</th>
                                    <th>Modelo</th>
                                    <th>Imágenes</th>
                                    <th>Precio</th>
                                    <th>Promo</th>
                                    <th>Visibilidad</th>
                                    <th class="text-center" style="width: 20px;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- AJAX -->
                            </tbody>
                        </table> 
                        
                    </div>  
                </div>
            </div>

            <!-- /orders history (static table) -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->

<!-- End of file index.php
Location: ./views/manager/orders/list.php -->



