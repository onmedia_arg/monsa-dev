<!-- MODAL SELECCCIONAR BIENES -->

<div id="modal_import" class="modal fade" tabindex="-1">
	<div class="modal-dialog modal-lg modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Importar Productos</h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<div class="modal-body mb-3">
				<div>
					<h5>Modelo de Archivo</h5>
					<hr>
					<div class="row">
						<div class="col-lg-6 mt-2">
							<span>Familia</span>
							<select id="templates-list" class="form-control"></select>
						</div>
						<br>
						<div class="col-lg-6 mt-2">
							<span>Recuperar datos almacenados</span>
							<select id="recupera_datos" class="form-control">
								<option value="0">NO</option>
								<option value="1">SI</option>
							</select>
						</div>
						<div class="col-lg-6 mt-2 d-none" id="marcas-list">
							<span>Marca</span>
							<select id="marca-id" class="form-control" >
								<option value="" disabled selected hidden>Selecciona una opción</option>
								<option value="-1">todas</option>
								<?php foreach ( $marcas as $tipo=>$info ):?>
									<option value="<?php echo $info["idMarca"] ?>"><?php echo $info["nombre"] ?></option>"
								<?php endforeach; ?>    
							</select>
						</div>
					</div>
					<div class="mt-2">
						<a href="" target="_blank" id="template" type='button' class='btn btn-outline-success btn-sm'><i class='icon-download mr-2'></i>Descargar</a>
					</div>
					
				</div>

				<hr>
				<h5 class="mt-3">Importar</h5>
				<form id="modalFormInput" action="">
					<div class="form-group row">
						<label class="col-form-label col-lg-2">Archivo</label>
						<div class="col-lg-10">
							<input id="modalInputFile" type="file" class="input-xls-file">
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
				<button id="btn_modal_import" type="button" class="btn bg-primary">Importar</button>
			</div>
		</div>
	</div>
</div>

<div id="modal_import_simple" class="modal fade" tabindex="-1">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Importar Productos</h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body mb-3">
				<div>
					<h5>Modelo de Archivo</h5>
					<div class="row">
						<div class="col-lg-4">
							<a href="" target="_blank" id="template_simple" type='button' class='btn btn-outline-success btn-sm'><i class='icon-download mr-2'></i>Descargar</a>
						</div>
					</div>

				</div>
				<hr>
				<h5 class="mt-3">Importar</h5>
				<form id="modalFormInput" action="">
					<div class="form-group row">
						<label class="col-form-label col-lg-2">Archivo</label>
						<div class="col-lg-10">
							<input id="modalInputFileSimple" type="file" class="input-xls-file">
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
				<button id="btn_modal_import_simple" type="button" class="btn bg-primary">Importar</button>
			</div>
		</div>
	</div>
</div>

<!-- FIN MODAL SELECCCIONAR BIENES -->