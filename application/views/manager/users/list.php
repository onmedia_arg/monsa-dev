
<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content">
            <!-- Orders history (static table) -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <table class="table" id="users-list">
                       
                            <thead>
                                <tr>
                                    <th style="width:10%">ID</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Auth Level</th>
                                    <th>Banned</th>
                                    <th>Activo</th>
                                    <th>Cliente</th>
                                    <th>Fecha Creación</th>
                                    <th>Fecha Actualización</th>
                                    <th class="text-center" style="width: 20px;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- AJAX -->
                            </tbody>
                        </table> 
                        
                    </div>  
                </div>
            </div>

            <!-- /orders history (static table) -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->

<!-- End of file index.php
Location: ./views/manager/orders/list.php -->



