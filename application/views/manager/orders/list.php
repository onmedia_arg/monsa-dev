
<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content">
            <!-- Orders history (static table) -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4>Filtro</h4>
                            <div class="row">
                                <div class="col-4 col-md-6 col-lg-4 col-xl-4">
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label" for="idcliente">Cliente:</label>
                                        <div class="col-9">
                                        <select name="idcliente" id="idcliente" class="form-control filter-select select-search" data-fouc>
                                            <option></option> 
                                        </select> 
                                        </div>
                                    </div>
                                </div>

                                <div class="col-4 col-md-6 col-lg-4 col-xl-4">
                                    <div class="form-group row">
                                        <label for="filter_status" class="col-2 col-form-label">Estado:</label>
                                        <div class="col-9">
                                        <select name="filter_status" id="filter_status" class="form-control filter-select">
                                            <option></option> 
                                        </select> 
                                        </div>
                                    </div>
                                </div>

                                <div class="col-4 col-md-6 col-lg-4 col-xl-4">
                                    <div class="form-group row cust-btn-group">
                                        <div class="text-center">
                                            <button type="button" class="btn btn-primary" id="set-filtro">
                                            <i class="icon-search4 mr-2"></i>Filtrar</button>
                                            <button type="button" class="btn btn-outline-warning ml-1" id="btn-clear-flt">
                                            <i class="icon-loop3 mr-2"></i>Limpiar Filtro</button>
                                        </div>
                                    </div>	
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <!-- <table class="table datatable-basic table-hover dataTable no-footer" id="orders-list">                             -->
                        <table class="table" id="orders-list">
                            <thead>
                                <tr>
                                    <th style="width:10%">Order Nº</th>
                                    <th>Fecha</th>
                                    <th>Cliente</th>
                                    <th>Estado</th>
                                    <th>Total</th>
                                    <th class="text-center" style="width: 20px;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table> 
                        
                    </div>  
                </div>
            </div>

            <!-- /orders history (static table) -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->

<!-- End of file index.php
Location: ./views/manager/orders/list.php -->



