<!-- Page content -->
<style>
.border-left-monsa{
    border-left: 2px #f6c145 solid;
}
</style>

<div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Content area -->
        <div class="content">
            <!-- Orders history (static table) -->
            <div class="row">
                <div class="col-md-8">
                    <div class="card border-left-monsa">

                        <div class="card-body">
                        <h4>Datos del Pedido</h4>
                            <dl class="row">
                                <dt class="col-sm-3">Nro Pedido</dt>
                                <dd id="order_idOrderHdr" class="col-sm-3"></dd>

                                <dt class="col-sm-3">Cliente</dt>
                                <dd id="order_cliente" class="col-sm-3"></dd>

                                <!-- <dt class="col-sm-3">Total</dt>
                                <dd id="order_total" class="col-sm-3"></dd>
                                
                                <dt class="col-sm-3">Cantidad Items</dt>
                                <dd id="order_cantidad" class="col-sm-3"></dd> -->

                                <dt class="col-sm-3">Creado</dt>
                                <dd id="order_createdat" class="col-sm-3"></dd>
                                
                                <dt class="col-sm-3">Estado</dt>
                                <dd id="order_status" class="col-sm-3"></dd>
                                
                                <dt class="col-sm-3">Nota:</dt>
                                <dd id="order_nota" class="col-sm-9"></dd>                                

                            </dl>
				        </div>  
                    </div>
                  
                </div>
            </div>                    
            
            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ITEM</th>
                                    <th>SKU</th>
                                    <th>FAMILIA</th>
                                    <th>MARCA</th>
                                    <th>PRODUCTO</th>
                                    <th>CANTIDAD</th>
                                    <th>PRECIO</th>
                                    <th>TOTAL</th>
                                </tr>
                            </thead>
                            <tbody id="detail-body">
                            </tbody>
                        </table>
                    </div>
                       
                    </div>  
                </div>
            </div>
            <!-- /orders history (static table) -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->

<script>
    var idOrderHdr = <?php echo $idOrderHdr ?>
</script>
<!-- End of file index.php
Location: ./views/manager/orders/detail.php -->



