
<div id="modal_help" class="modal fade" tabindex="-1">
	<div class="modal-dialog modal-lg modal-dialog-scrollable">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Ayuda</h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<div class="modal-body mb-3" >
				<div>
					<h5>Estados de Pedidos</h5>	
                    <div class="card border-left-monsa bg-light mb-3">
                        <div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>ID</th>
										<th>Estado</th>
										<th>Descripción</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>INICIADO</td>
										<td>Estado inicial de los pedidos</td>
									</tr>
									<tr>
										<td>2</td>
										<td>NUEVO</td>
										<td>El pedido se ha enviado a procesar</td>
									</tr>
									<tr>
										<td>3</td>
										<td>EN PREPARACIÓN</td>
										<td>Monsa esta preparando el pedido</td>
									</tr>
									<tr>
										<td>4</td>
										<td>FINALIZADO</td>
										<td>Monsa envio el pedido</td>
									</tr>
								</tbody>
							</table>
						</div>
                    </div>                        
				</div>
				
				<!-- <hr>
				<h5 class="mt-3">Importar</h5>
                <form id="modalFormInput" action="">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Archivo</label>
                            <div class="col-lg-10">
                                <input id="modalInputFile" type="file" class="input-xls-file">
                            </div>
                    </div>
                </form> -->
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
				<!-- <button id="btn_modal_import" type="button" class="btn bg-primary">Importar</button> -->
			</div>
		</div>
	</div>
</div>

