<!-- Page content -->
<div class="page-content">
    <div class="content-wrapper">
        <div class="content">
            <!-- /quick stats boxes -->        
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-lg-4">
                            <!-- Members online -->
                            <div class="card bg-teal-400">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <h3 class="font-weight-semibold mb-0" id="widget-pedidos">0</h3>
										<!-- <span>CANTIDAD DE PEDIDOS <br><div class="c_month"></div></span>                                     -->
                                    </div>
                                    
                                    <div>
                                        Cantidad de Pedidos
                                        <div class="font-size-sm opacity-75 c_month" style="text-transform: capitalize;"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- /members online -->
                        </div>

                        <div class="col-lg-4">
                            <div class="card bg-blue-400">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <h3 class="font-weight-semibold mb-0" id="widget-ventas">0</h3>
                                    </div>
                                    
                                    <div>
                                        Total Ventas
                                        <div class="font-size-sm opacity-75" id="avg-ventas"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <!-- Current server load -->
                            <div class="card bg-pink-400">
                                <div class="card-body">
                                    <div class="d-flex">
                                        <h3 class="font-weight-semibold mb-0" id="clie-actv"></h3>
                                    </div>
                                    
                                    <div>
                                        Cant. clientes activos
                                        <div class="font-size-sm opacity-75">0 Agosto 2021</div>
                                    </div>
                                </div>
                            </div>
                            <!-- /current server load -->

                        </div>                        
                    </div> 
                    <div class="card" style="overflow-x: auto;">
                        <div class="card-header">
                            <h5 class="card-title">Últimos Pedidos</h5>                        
                        </div>
                        <table class="table" id="orders-list">
                            <thead>
                                <tr>
                                    <th>Order Nº</th>
                                    <th>Fecha</th>
                                    <th>Cliente</th>
                                    <th>Total</th>
                                    <th class="text-center" style="width:350px">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>                         
                    </div> 
                </div>

                <div class="col-md-3">    
                <div class="card">
			<div class="card-header header-elements-inline">
				<h6 class="card-title">Estado de Procesos</h6>
				<div class="header-elements">
					<div class="form-check form-check-inline form-check-right form-check-switchery form-check-switchery-sm">
					</div>
				</div>
			</div>

			<div class="card-body">
				<div class="chart mb-3" id="bullets"></div>

				<ul class="media-list" id="process-state">
																					
				</ul>
			</div>
		</div>
                    <div class="card">
                        <div class="card-header">
                            <h6 class="card-title">Últimos Clientes</h6>
                        </div>
                        <table class="table" id="clients-list">
                            <thead>
                                <tr>
                                    <!-- <th>ID</th> -->
                                    <th>Razon Social</th>
                                    <th class="text-center" style="width: 20px;">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>                         
                    </div> 
                </div>   
            </div>
        </div>
    </div>
</div>

