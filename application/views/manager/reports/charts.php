<!-- <div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>
                <span class="font-weight-semibold">
                  Reportes
                </span>
            </h4>            
        </div>

        <div class="header-elements d-none">
            
        </div>

    </div>
</div> -->

<div class="page-content">
    <div class="content-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header header-elements-inlin">
                            <h4 class="card-title">Estado de pedidos por día</h4>
                            <div class="header-elements">
                                <label for="">Seleccionar Producto</label>
                                <select class="form-control custom-select" id="product" onchange="EchartsLines.init()">
                                    <option value="0" selected>Todos</option>
                                </select>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="chart-container">
                                <div class="chart has-fixed-height" id="line_basic"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->