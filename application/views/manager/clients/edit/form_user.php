<!-- Page content -->
<div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        
        <!-- Content area -->
        <div class="content">
            <span id="data_saldo"></span>
            <span id="data_movimientos"></span>
            <!-- Orders history (static table) -->
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs nav-tabs-highlight">
                        <li class="nav-item">
                            <a href="#client-data" class="nav-link active" data-toggle="tab">Datos General</a>
                        </li>
                        <li class="nav-item">
                            <a href="#client-users" class="nav-link" data-toggle="tab">Usuarios Asociados</a>
                        </li>
                        <li class="nav-item">
                            <a href="#client-orders" class="nav-link" data-toggle="tab">Ordenes</a>
                        </li>
                        <li class="nav-item">
                            <a href="#cuenta-corriente" class="nav-link" data-toggle="tab">Cuenta Corriente</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <!-- Begin TAB 1 Informacion Gral -->
                        <div class="tab-pane fade show active" id="client-data" role="tabpanel" aria-labelledby="client-data-tab">
                            <form id="formClientData" method="post">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" hidden name="id_cliente" id="id_cliente" class="form-control mb-1" value="<?php echo ($cliente['idCliente']); ?>">
                                        <legend class="font-weight-semibold"><i class="icon-file-text2 mr-2"></i> Información Cliente</legend>
                                        <div class="row">

                                            <div class="form-group col-md-6">
                                                <label for="razon_social">Nombre / Razón Social</label>
                                                <input type="text" name="razon_social" id="razon_social" class="form-control mb-1" placeholder="Nombre / Razón Social" value="<?php echo ($this->input->post('razon_social') ? $this->input->post('razon_social') : $cliente['razon_social']); ?>" autofocus>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="tipo_cliente">Tipo de Usuario</label>
                                                <select type="text" name="tipo_cliente" id="tipo_cliente" class="form-control mb-1">

                                                    <option value="" disabled="" hidden="" selected="">Tipo de Usuario</option>
                                                    <?php foreach ($tipos_usuarios as $tu => $info) : ?>
                                                        <option value="<?= $info['idTipoCliente'] ?>" <?php if ((int) $cliente['idTipoCliente'] == (int) $info['idTipoCliente']) {
                                                                                                            echo 'selected';
                                                                                                        } ?>>
                                                            <?= $info['texto'] ?>
                                                        </option>
                                                    <?php endforeach; ?>

                                                </select>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="cuit">CUIT</label>
                                                <input type="text" name="cuit" id="cuit" class="form-control mb-1" placeholder="CUIT" value="<?php echo ($this->input->post('cuit') ? $this->input->post('cuit') : $cliente['cuit']); ?>">
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="telefono">Telefono</label>
                                                <input type="text" name="telefono" id="telefono" class="form-control mb-1" placeholder="Telefono" value="<?php echo ($this->input->post('telefono') ? $this->input->post('telefono') : $cliente['telefono']); ?>">
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="mail">Mail</label>
                                                <input type="text" name="mail" id="mail" class="form-control mb-1" placeholder="Mail" value="<?php echo ($this->input->post('mail') ? $this->input->post('mail') : $cliente['mail']); ?>">
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="direccion">Dirección</label>
                                                <input type="text" name="direccion" id="direccion" class="form-control mb-1" placeholder="Dirección" value="<?php echo ($this->input->post('direccion') ? $this->input->post('direccion') : $cliente['direccion']); ?>">
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="direccion">ID Cliente Interno</label>
                                                <input type="text" name="id_cliente_interno" id="id_cliente_interno" class="form-control mb-1" placeholder="ID Cliente Interno" value="<?php echo ($this->input->post('id_cliente_interno') ? $this->input->post('id_cliente_interno') : $cliente['id_cliente_interno']); ?>">
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="email_vendedor">Email de Vendedor</label>
                                                <input type="text" name="email_vendedor" id="email_vendedor" class="form-control mb-1" placeholder="Email de Vendedor" value="<?php echo ($this->input->post('email_vendedor') ? $this->input->post('email_vendedor') : $cliente['email_vendedor']); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- End TAB 1 Informacion Gral -->

                        <!-- Begin TAB 2 Direcciones -->
                        <div class="tab-pane fade show" id="client-users" role="tabpanel" aria-labelledby="client-users-tab">
                            <form id="formClientUsers" method="post" action="">
                                <div class="row">
                                    <div class="col-md-12">
                                        <legend class="font-weight-semibold"><i class="icon-user mr-2"></i>Listado de Usuarios</legend>
                                        <div>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Activo</th>    
                                                        <th>User Id</th>
                                                        <th>Username</th>
                                                        <th>Email</th>
                                                        <th>Auth Level</th>
                                                        <th>Banned</th>
                                                        <th>Created At</th>
                                                        <th>Modified At</th>

                                                        <?php if ($user['level'] == '9') : ?>
                                                            <th>Acciones</th>
                                                        <?php endif ?>



                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php foreach ($usuarios as $u) : ?>
                                                        <tr>
                                                            <?php 
                                                                $checked = $u["is_active"] ? "checked" : "" ;
                                                                $activo = '<input type="checkbox" data-on-text="Si" data-off-text="No" class="is_active" name="is_active" value="' . $u["user_id"] . '" ' .
                                                                          $checked . '>';
                                                            ?>
                                                            <td><?php echo $activo; ?> </td>
                                                            <td><?php echo $u['user_id']; ?></td>
                                                            <td><?php echo $u['username']; ?></td>
                                                            <td><?php echo $u['email']; ?></td>
                                                            <td><?php echo $u['auth_level']; ?></td>
                                                            <td><?php echo $u['banned']; ?></td>
                                                            <td><?php echo $u['created_at']; ?></td>
                                                            <td><?php echo $u['modified_at']; ?></td>
                                                            <?php if ($user['level'] == '9') : ?>
                                                                <td>
                                                                    <div class="list-icons">
                                                                        <div class="list-icons-item dropdown">
                                                                            <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false">
                                                                                <i class="icon-menu7"></i>
                                                                            </a>
                                                                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-164px, 16px, 0px);">
                                                                                <a href="<?php echo site_url('manager/user/edit/' . $u['user_id']); ?>" class="dropdown-item" title="Ver Detalle">
                                                                                    <i class="fa fa-edit"></i>Editar
                                                                                </a>
                                                                                <a href="#" data-id="<?= $u['user_id'] ?>" id="updatePassword" data-username="<?= $u['username'] ?>" class="dropdown-item" title="Actualizar Contraseña">
                                                                                    <i class="fa fa-key"></i>Actualizar Contraseña
                                                                                </a>
                                                                                <a href="#" data-id="<?= $u['user_id'] ?>" id="deleteUser" data-username="<?= $u['username'] ?>" class="dropdown-item" title="Eliminar">
                                                                                    <i class="fa fa-trash"></i>Eliminar
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            <?php endif ?>

                                                        </tr>
                                                    <?php endforeach; ?>

                                                </tbody>
                                            </table>


                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- End TAB 2 Direcciones -->

                        <!-- Begin TAB 3 Archivos -->
                        <div class="tab-pane fade show" id="client-orders" role="tabpanel" aria-labelledby="client-orders-tab">
                            <form id="formClientOrdes" method="post" action="">
                                <div class="row">
                                    <div class="col-md-12">
                                        <legend class="font-weight-semibold"><i class="icon-folder-open mr-2"></i>Listado de Ordenes</legend>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Usuario</th>
                                                    <th>Cliente</th>
                                                    <th>Total</th>
                                                    <th>Status</th>
                                                    <th>Nota</th>
                                                    <th>Fecha Creacion</th>
                                                    <th>Fecha Actualizacion</th>

                                                    <?php if ($user['level'] == '9') : ?>
                                                            <th>Acciones</th>
                                                        <?php endif ?>

                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php foreach ($ordenes as $o) : ?>
                                                    <tr>
                                                        <td><?php echo $o['idOrderHdr']; ?></td>
                                                        <td><?php echo $o['idUser']; ?></td>
                                                        <td><?php echo $o['idCliente']; ?></td>
                                                        <td><?php echo $o['total']; ?></td>
                                                        <td><?php echo $o['idOrderStatus']; ?></td>
                                                        <td><?php echo $o['nota']; ?></td>
                                                        <!-- <td><?php echo $o['createdBy']; ?></td> -->
                                                        <td><?php echo $o['created']; ?></td>
                                                        <!-- <td><?php echo $o['updatedBy']; ?></td> -->
                                                        <td><?php echo $o['updated']; ?></td>


                                                        <?php if ($user['level'] == '9') : ?>
                                                        <td>
                                                            <div class="list-icons">
                                                                <div class="list-icons-item dropdown">
                                                                    <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false">
                                                                        <i class="icon-menu7"></i>
                                                                    </a>
                                                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-164px, 16px, 0px);">
                                                                        <a href="#" class="dropdown-item" title="Ver Detalle" data-id="<?= $o['idOrderHdr'] ?>" data-toggle="modal" data-target="#order-details">
                                                                            <i class="fa fa-eye"></i>Detalle
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <?php endif ?>
                                                    </tr>
                                                <?php endforeach; ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- End TAB 3 Files -->

                        <!-- Begin TAB 4 Informacion Gral -->
                        <div class="tab-pane fade show" id="cuenta-corriente" role="tabpanel" aria-labelledby="cuenta-corriente-tab">
                                <div class="row">
                                    <div class="col-md-12">
                                        <legend class="font-weight-semibold"><i class="icon-file-text2 mr-2"></i> Detalle Cuenta Corriente <button id="btnViewMovimientos" type="button" title="Ver cuenta corriente" class="btn btn-outline bg-blue-400 text-blue-800 btn-icon rounded-round"><i class="icon-eye"></i></button></legend>
                                        
                                        

                                        <div class="row d-none" id="details_cuenta_corriente">

                                            <div class="form-group col-md-12">
                                                <label for="saldo">Saldo: <span id="saldo">$</span></label>
                                            </div>

                                            <legend class="font-weight-semibold form-group col-md-12"><i class="icon-cash mr-2"></i> Ultimos Movimientos</legend>
                                        

                                            <div class="table-responsive">
                                                <table id="table-details">
                                                    <thead>
                                                        <th class="text-center">FECHA</th>
                                                        <th class="text-center">TIPO</th>
                                                        <th class="text-center">NRO DOCUMENTO</th>
                                                        <th class="text-center">ESTADO</th>
                                                        <th class="text-center">REFERENCIA</th>
                                                        <th class="text-right">IMPORTE</th>
                                                    </thead>
                                                    <tbody id="details_movimientos">
                                                            
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="fa-3x text-center d-none" id="cliente_not_found">
                                            <h4 class="text-danger">Cliente no identificado, por favor configurar el ID interno.</h4>
                                        </div> 
                                    </div>
                                </div>
                            
                        </div>
                        <!-- End TAB 4 Informacion Gral -->
                    </div>
                </div>
            </div>
            <!-- /orders history (static table) -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->

<!-- End of file index.php
Location: ./views/manager/orders/list.php -->

<div class="modal fade in" id="order-details">
    <div class="modal-dialog modal-lg">

        <section class="modal-content">
              <!-- title row -->
              <div class="row" style="margin-left: 2px; margin-right: 5px;">
                <div class="col-xs-12">
                  <h2 class="page-header">
                    <i class="fa fa-globe"></i> Detalle de Orden
                    <small class="pull-right">Fecha: <span id="order-date"></span></small>
                  </h2>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div style="margin-bottom: 15px;" class="row invoice-info">
 
                    <div class="col-sm-4 invoice-col"> 
                      <b>Orden:</b> #<span id="order-id"></span></b><br> 
                      <b>Razon Social:</b> <span id="order-rs"></span></b><br> 
                      <b>CUIT:</b> <span id="order-cuit"></span>
                    </div> 

                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col"> 

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col"> 
                      <b>Email:</b> <span id="order-email"></span><br> 
                      <b>Telefono:</b> <span id="order-tel"></span><br> 
                      <b>Dirección:</b> <span id="order-address"></span>

                    </div>
                    <!-- /.col -->

              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row" style="margin-left: 5px; margin-right: 5px;">
                <div class="col-xs-12 table-responsive">
                    <table id="order-details-table" class="table table-striped no-dt">
                        <thead>
                            <tr>  
                                <th>posnr</th>
                                <th>idProducto</th>
                                <th>precio</th>
                                <th>qty</th>
                                <th>total</th> 
                            </tr>
                        </thead>
                        <tbody>  

                            <!-- Ajax -->

                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-xs-6">
 
                </div>
                <!-- /.col -->
                <div class="col-xs-6"> 
                  <div class="table-responsive">
                    <table class="table no-dt">
                        <tbody> 
                          <tr>
                            <th>Total:</th>
                            <td><span id="order-total"></span></td>
                          </tr>
                        </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row --> 
            </section>
 
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>
