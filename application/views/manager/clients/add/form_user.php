<!-- Page content -->
<div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">
        <!-- Content area -->
        <div class="content">
            <!-- Orders history (static table) -->
            <div class="card">
                <div class="card-body">
                    <form id="formClientData" method="post">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="razon_social">Nombre / Razón Social</label>
                                <input type="text" name="razon_social" id="razon_social" class="form-control mb-1" placeholder="Nombre / Razón Social" autofocus>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="tipo_cliente">Tipo de Usuario</label>
                                <select type="text" name="tipo_cliente" id="tipo_cliente" class="form-control mb-1">

                                    <option value="" disabled="" hidden="" selected="">Tipo de Usuario</option>
                                    <?php foreach ($tipos_usuarios as $tu => $info) : ?>
                                        <option value="<?= $info['idTipoCliente'] ?>" <?php if (isset($_POST['tipo_cliente']) && $_POST['tipo_cliente'] == $info['idTipoCliente']) {
                                                                                            echo 'selected';
                                                                                        } ?>>
                                            <?= $info['texto'] ?>
                                        </option>
                                    <?php endforeach; ?>

                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="cuit">CUIT</label>
                                <input type="text" name="cuit" id="cuit" class="form-control mb-1" placeholder="CUIT">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="telefono">Telefono</label>
                                <input type="number" name="telefono" id="telefono" class="form-control mb-1" placeholder="Telefono">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="mail">Mail</label>
                                <input type="text" name="mail" id="mail" class="form-control mb-1" placeholder="Mail">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="direccion">Dirección</label>
                                <input type="text" name="direccion" id="direccion" class="form-control mb-1" placeholder="Dirección">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="usuario">Usuario</label>
                                <input type="text" name="usuario" id="usuario" class="form-control mb-1" placeholder="Usuario">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="nombre_usuario">Nombre y Apellido del Usuario</label>
                                <input type="text" name="nombre_usuario" id="nombre_usuario" class="form-control mb-1" placeholder="Nombre y Apellido del Usuario">
                            </div>

                            <!-- <div class="form-group col-md-6">
                                <label for="password_usuario">Password</label>
                                <input type="password" name="password_usuario" id="password_usuario" class="form-control mb-1" placeholder="Password" disabled>
                                <button id="btnViewMovimientos" type="button" title="Ver cuenta corriente" class="btn btn-outline bg-blue-400 text-blue-800 btn-icon rounded-round"><i class="icon-key"></i> Generar Contraseña</button>
                            </div> -->

                            <div class="form-group col-md-6">
                                <label for="id_cliente_interno">ID Cliente Interno</label>
                                <input type="text" name="id_cliente_interno" id="id_cliente_interno" class="form-control mb-1" placeholder="ID Cliente interno">
                            </div>

                            <div class="form-group col-md-6 text-orange">
                                <br>
                                <small for="password_usuario"><i class="icon-key"></i> La contraseña es generado automaticamente por el Sistema.</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /orders history (static table) -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->

<!-- End of file index.php
Location: ./views/manager/orders/list.php -->