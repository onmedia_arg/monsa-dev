
<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content">
            <!-- Orders history (static table) -->
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                    <div class="card family-card">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Familias</h5>
                        </div>
                        <div class="card-body">
                            <div class="row" id="formCreateFamily">
                                <div class="col-md-8">
                                    <input id="createFamily" type="text" class="form-control" placeholder="Familia">
                                </div>
                                <div class="col-md-4">
                                    <button id="btnCreateFamily" class="btn btn-outline-primary btn-tab5">Crear <i class="icon-floppy-disk ml-2"></i></button>
                                </div>
                            </div>
                            <div id="formUpdateFamily" class="row d-none">
                                <div class="col-md-8">
                                    <input id="idFamily" type="hidden" />
                                    <input id="updateFamily" type="text" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <button id="btnUpdateFamily" class="btn btn-outline-primary btn-tab5">Actualizar <i class="icon-floppy-disk ml-2"></i></button>
                                </div>   
                            </div>
                        </div>    

                        <div class="table-responsive">
                            <table class="table" id="table-family">
                                <thead>
                                    <tr>
                                        <th style="max-width:5%">-</th>
                                        <th style="max-width:10%">ID</th>
                                        <th style="max-width:40%">Nombre</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody id="familyList"></tbody>
                            </table>
                        </div>
                    </div>
                </div> 

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                    <div id="atributes-card" class="card d-none family">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Atributos de <span id="family-name" data-idFamilia=""></span></h5>
                        </div>
                        <div class="card-body">
                            <div class="row" id="formCreateAtribute">
                                <div class="col-md-8">
                                    <input id="createAtribute" type="text" class="form-control" placeholder="Atributo">
                                </div>
                                <div class="col-md-4">
                                    <button id="btnCreateAtribute" class="btn btn-outline-primary btn-tab5">Crear <i class="icon-floppy-disk ml-2"></i></button>
                                </div>
                            </div>
                            <div id="formUpdateAtribute" class="row d-none">
                                <div class="col-md-8">
                                    <input id="idAtribute" type="hidden" />
                                    <input id="updateAtribute" type="text" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <button id="btnUpdateAtribute" class="btn btn-outline-primary btn-tab5">Actualizar <i class="icon-floppy-disk ml-2"></i></button>
                                </div>   
                            </div>
                        </div>                         
                        <div class="table-responsive">
                            <table class="table" id="table-atributes">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody id="atributes-list"></tbody>
                            </table>
                        </div>
                    </div>    
                </div> 

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                    <div id="values-card" class="card d-none family">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Valores</h5>
                        </div>
                        <div class="card-body">
                            <div class="row" id="formCreateValue">
                                <div class="col-md-8">
                                    <input id="createValue" type="text" class="form-control" placeholder="Valor">
                                </div>
                                <div class="col-md-4">
                                    <button id="btnCreateValue" class="btn btn-outline-primary btn-tab5">Crear <i class="icon-floppy-disk ml-2"></i></button>
                                </div>
                            </div>
                            <div id="formUpdateValue" class="row d-none">
                                <div class="col-md-8">
                                    <input id="updateValue" type="text" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <button id="btnUpdateValue" class="btn btn-outline-primary btn-tab5">Actualizar <i class="icon-floppy-disk ml-2"></i></button>
                                </div>   
                            </div>
                        </div>                        
                        <div class="table-responsive">
                            <table class="table" id="table-values">
                                <thead>
                                    <tr>
                                        <th>Valor</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody id="values-list"></tbody>
                            </table>
                        </div>
                    </div>     
                </div> 

                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                    <div class="card marcas-card d-none">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Marcas</h5>
                        </div>
                        <div class="card-body">
                            <div class="row" id="formCreateMarca">
                                <div class="col-md-8">
                                    <input id="createMarca" type="text" class="form-control" placeholder="Marca">
                                </div>
                                <div class="col-md-4">
                                    <button id="btnCreateMarca" class="btn btn-outline-primary btn-tab5">Crear <i class="icon-floppy-disk ml-2"></i></button>
                                </div>
                            </div>
                            <div id="formUpdateMarca" class="row d-none">
                                <div class="col-md-8">
                                    <input id="idMarca" type="hidden" />
                                    <input id="updateMarca" type="text" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    <button id="btnUpdateMarca" class="btn btn-outline-primary btn-tab5">Actualizar <i class="icon-floppy-disk ml-2"></i></button>
                                </div>   
                            </div>
                        </div>    

                        <div class="table-responsive">
                            <table class="table" id="table-marcas">
                                <thead>
                                    <tr>
                                        <th style="max-width:5%">-</th>
                                        <th style="max-width:10%">ID</th>
                                        <th style="max-width:40%">Nombre</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody id="marcasList"></tbody>
                            </table>
                        </div>
                    </div>
                </div> 
            </div>

            <!-- /orders history (static table) -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->

<!-- End of file index.php
Location: ./views/manager/orders/list.php -->
