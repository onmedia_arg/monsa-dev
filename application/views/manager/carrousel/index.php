<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content">
            <!-- Orders history (static table) -->
            <div class="row">
                <div class="col-md-8">
                    <!-- /basic layout -->
                    <div class="card carrousel-card">
                        <div class="card-header">
                            <h5 class="card-title">Carruseles</h5>
                        </div>

                        <div class="card-body">
                            <form action="#">
                                <div class="form-group">
                                    <label>Titulo:</label>
                                    <input id="carrouselTitle" type="text" class="form-control" placeholder="">
                                </div>

                                <div class="text-right">
                                    <button type="submit" id="btnCreateCarrousel" class="btn btn-primary">Crear</button>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table" id="table-carrousel">
                                <thead>
                                    <tr>
                                        <th style="max-width:5%">-</th>
                                        <th style="max-width:40%">Nombre</th>
                                        <th style="max-width:20%">Activo</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody id="carrouselList"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /orders history (static table) -->
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->

<!-- End of file index.php
Location: ./views/manager/orders/list.php -->