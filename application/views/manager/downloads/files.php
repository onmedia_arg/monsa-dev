
<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <div class="content">
            <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card">
            <div class="card-body">
                <legend class="font-weight-semibold"><i class="icon-images2 mr-2"></i>Promociones</legend> 
                    <div class="col-md-3">
                        <input type="file" name="imagen" id="imagen" class="imageInput float-left">
                        <button id="btnUploadPromotion" class="btn btn-outline-primary btn-sm">Subir Imagen</button>
                    </div>
                    <div class="thumbs-list" id="thumbs"></div> 
                </div>

            </div>    
            </div>    
            </div>    
        </div>

        <!-- Content area -->
        <div class="content">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <h3 class="mb-3">Mensaje de Actualización <button id="btn_save_msj" class="btn btn-primary float-right">Guardar</button></h3>
                            <textarea id="msj-download"></textarea>        
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" id="files-list">

                <div class="col-md-4">
                    <div class='card'>
                        <div class='card-body'>
                            <div class='media'>
                                <div class='mr-3'>
                                    <a href='' id="dwnl-icon-pdf" ><img style="max-width:45px" src="<?= base_url('/resources/img/pdf.png')?>"></a>
                                </div>
                                <div class='media-body'>
                                    <div class='font-weight-bold'>TIPO: PDF<span id="dwnl-date-pdf" class='text-muted float-right'></span></div>
                                </div>
                                <div class="loading-promotion-pdf d-none">
                                    <div>
                                        <i class="icon-spinner2 icon-2x spinner"></i>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                        <div class='card-footer d-flex justify-content-between'>
                            <a href="" id="dwnl-btn-pdf" type='button' class='btn btn-outline-success btn-sm'><i class='icon-download mr-2'></i>Descargar</a>
                            <div class="d-flex "> 
                                <input type="file" name="file-pdf" id="file-pdf" class="my-file">    
                                <button id="dwnl-btn-up-pdf" class='btn btn-outline-primary btn-sm'><i class='icon-file-upload2 mr-2'></i>Editar</button>                       
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class='card'>
                        <div class='card-body'>
                            <div class='media'>
                                <div class='mr-3'>
                                    <a href='' id="dwnl-icon-csv" ><img style="max-width:45px" src="<?= base_url('/resources/img/csv.png')?>"></a>
                                </div>
                                <div class='media-body'>
                                    <div class='font-weight-bold'>TIPO: CSV<span id="dwnl-date-csv" class='text-muted float-right'></span></div>
                                </div>
                            </div>
                        </div>                        
                        <div class='card-footer d-flex justify-content-between'>
                            <a href="" id="dwnl-btn-csv" type='button' class='btn btn-outline-success btn-sm'><i class='icon-download mr-2'></i>Descargar</a>
                            <div class="d-flex "> 
                                <input type="file" name="file-csv" id="file-csv" class="my-file">    
                                <button id="dwnl-btn-up-csv" class='btn btn-outline-primary btn-sm'><i class='icon-file-upload2 mr-2'></i>Editar</button>                       
                            </div>
                        </div>
                    </div>
                </div>                                

            </div>
        </div>
        <!-- /content area -->
    </div>
    <!-- /main content -->
</div>
<!-- /page content -->

<!-- End of file index.php
Location: ./views/manager/download/files.php -->