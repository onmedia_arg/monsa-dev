<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <div class="content">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <legend class="font-weight-semibold"><i class="icon-images2 mr-2"></i>Carrousel de Marcas
                            </legend>
                            <div class="col-md-3">
                                <input type="file" name="imagenBrands" id="imagenBrands" class="imageInput float-left">
                                <button id="btnUploadBrands" class="btn btn-outline-primary btn-sm">Subir Imagen</button>
                            </div>
                            <ul class="thumbs-list d-flex flex-row align-items-start" id="thumbsBrands"></ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /main content -->
</div>
<!-- /page content -->

<!-- End of file index.php
Location: ./views/manager/download/files.php -->