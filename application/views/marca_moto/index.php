<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Listado Marca Moto</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('marca_moto/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
    						<th>IdMarcaMoto</th>
    						<th>Nombre</th>
    						<th>Slug</th>
    						<th>Descripcion</th>
    						<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($marca_moto as $m){ ?>
                        <tr>
    						<td><?php echo $m['idMarcaMoto']; ?></td>
    						<td><?php echo $m['nombre']; ?></td>
    						<td><?php echo $m['slug']; ?></td>
    						<td><?php echo $m['descripcion']; ?></td>
    						<td>
                                <a href="<?php echo site_url('marca_moto/edit/'.$m['idMarcaMoto']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                                <a href="<?php echo site_url('marca_moto/remove/'.$m['idMarcaMoto']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                                
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('script/dataTable') ?>
