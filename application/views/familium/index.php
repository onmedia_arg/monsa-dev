<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Listar Familias</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('familium/add'); ?>" class="btn btn-success btn-sm">Nueva Familia</a> 
                </div>
            </div>
            <div class="box-body"> 
                <table id="sortable" class="table table-striped">
                    <thead>
                        <tr>
    						<th>IdFamilia</th>
    						<th>Nombre</th>
                            <th>Slug</th>
    						<th>Orden</th>
    						<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                        $i=0; 
                        foreach($familia as $f): 
                    ?>
                            <tr data-id="<?php echo $f['idFamilia']; ?>" data-index="<?= $i ?>" data-orden="<?php echo $f['orden']; ?>">
        						<td><?php echo $f['idFamilia']; ?></td>
        						<td><?php echo $f['nombre']; ?></td>
                                <td><?php echo $f['slug']; ?></td>
        						<td class="priority"><?php echo $f['orden']; ?></td>
        						<td>
                                    <a href="<?php echo site_url('familium/edit/'.$f['idFamilia']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                                    <a href="<?php echo site_url('familium/remove/'.$f['idFamilia']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                                    <span style="cursor: move; margin-left: 5px;" title="Arrastra para cambiar la orden">
                                        <i class="fa fa-arrows"></i>
                                    </span>
                                </td>
                            </tr>
                    <?php 
                        $i++; 
                        endforeach; 
                    ?>
                    </tbody>
                </table>
                                
            </div>
        </div>
    </div>
</div>
<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
<script type="text/javascript"> 

    $(document).on( 'ready', function () { 

        $( "#sortable tbody" ).sortable({
            update: function( event, ui ) { 

                var list = [],
                    i=0;

                $( "#sortable tbody tr" ).each(function(k,v){
                    // Actualiza td con orden nuevo
                    $(this).find('.priority').text( i )
                    list.push( { 'orden': i, 'idFamilia': $(this).attr('data-id') } )
                    i++;
                })

                console.log( list )

                $.ajax({
                    'url': "<?= site_url('familium/update_sort/') ?>",
                    'type': 'post',
                    'data': {'json': JSON.stringify(list) },
                    'dataType': 'json',
                    success: function(response){

                        if( response.code == 1 ){

                            iziToast.success({
                                title: 'OK!',
                                message: response.message,
                            }); 

                            console.log( $('.priority:contains(' + curPosition + ')') )
                             
                            // Sort by column 1 and then re-draw
                            table
                                .order( [[ 3, 'asc' ]] )
                                .draw( false );

                        }else{

                            iziToast.error({
                                title: 'Error!',
                                message: response.message,
                            }); 

                        }

                    }
                })

            }
        });
        $( "#sortable tbody" ).disableSelection();

    } );

</script>
<?php //$this->load->view('script/dataTable') ?>
