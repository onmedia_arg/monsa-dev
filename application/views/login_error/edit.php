<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Login Error Edit</h3>
            </div>
			<?php echo form_open('login_error/edit/'.$login_error['ai']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="username_or_email" class="control-label">Username Or Email</label>
						<div class="form-group">
							<input type="text" name="username_or_email" value="<?php echo ($this->input->post('username_or_email') ? $this->input->post('username_or_email') : $login_error['username_or_email']); ?>" class="form-control" id="username_or_email" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="ip_address" class="control-label">Ip Address</label>
						<div class="form-group">
							<input type="text" name="ip_address" value="<?php echo ($this->input->post('ip_address') ? $this->input->post('ip_address') : $login_error['ip_address']); ?>" class="form-control" id="ip_address" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="time" class="control-label">Time</label>
						<div class="form-group">
							<input type="text" name="time" value="<?php echo ($this->input->post('time') ? $this->input->post('time') : $login_error['time']); ?>" class="has-datetimepicker form-control" id="time" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>