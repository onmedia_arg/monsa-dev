    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tbody>
            <tr>
                <td><b>Razón Social: </b><?= $client['razon_social'] ?> <b> - Usuario: </b><?= $userData['username'] ?>
                  <p style="margin-top:0px;">Orden #<?= $order_r ?></p></td>
                <td align="right" width="100"> <?= date('d/m/Y') ?> </td>
            </tr>
            <tr>
              <td colspan="2" style="padding:20px 0; border-top:1px solid #f6f6f6;">
                <div>
                  <table width="100%" cellpadding="0" cellspacing="0">
                    <tbody>
                      <?php foreach ($excel as $cell): ?>
                          <tr>
                            <td style="font-family: 'arial'; font-size: 14px; vertical-align: middle; margin: 0; padding: 9px 0;"><?= $cell['sku'] ?></td>
                            <td style="font-family: 'arial'; font-size: 14px; vertical-align: middle; margin: 0; padding: 9px 0;"><?= $cell['desc_producto'] ?></td>
                            <td style="font-family: 'arial'; font-size: 14px; vertical-align: middle; margin: 0; padding: 9px 0;"><?= $cell['qty'] ?> u.</td>
                            <td style="font-family: 'arial'; font-size: 14px; vertical-align: middle; margin: 0; padding: 9px 0;"  align="right">$ <?= $cell["total"] ?></td>
                          </tr>
                      <?php endforeach; ?>
                      <tr class="total">
                        <td colspan="2" style="font-family: 'arial'; font-size: 14px; vertical-align: middle; border-top-width: 1px; border-top-color: #f6f6f6; border-top-style: solid; margin: 0; padding: 9px 0; font-weight:bold;" width="80%">Total</td>
                        <td style="font-family: 'arial'; font-size: 14px; vertical-align: middle; border-top-width: 1px; border-top-color: #f6f6f6; border-top-style: solid; margin: 0; padding: 9px 0; font-weight:bold;" align="right">$ <?= $total ?></td>
                      </tr>
                    </tbody>
                  </table>
                  <p><b>Nota:</b> <?= $nota ?></p>
                </div>
              </td>
            </tr> 

            <tr>
                <td colspan="2">
                  <center>
                    <a href="<?= base_url() ?>" style="display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #FF7300; border-radius: 3px; text-decoration:none;">Ver Orden</a>
                  </center>
                </td>
            </tr>
        </tbody>
    </table>