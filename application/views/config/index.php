<?php 

$ci =&get_instance();
$ci->load->model('Base_model');  

?>
<div class="container-fluid">

    <!-- .row -->
<div class="row">
    <div class="col-sm-12">
        
        <section class="content">

<!-- Default box -->
    <div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Ajustes Generales</h3> 
    </div>
    <div class="box-body">
    <?php echo form_open('Sys_Config/save_sys_config', [ 'class' => "form-horizontal",  'role' => "form", 'data-toggle' => "validator" ] ); ?>   
        <h3 class="box-title m-b-0">Configuración de Whatsapp</h3>
        <br>
        <div class="form-group">
            <label class="col-md-12">Número de Whatsapp</label>
            <div class="col-md-6">
                <input type="text" name="wsp_number" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('wsp_number'); ?>">
                <span class="help-block"> Dejar este campo en blanco para ocultar el botón de whatsapp</span> 
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-12">Mensaje</label>
            <div class="col-md-6">
                <input type="text" name="wsp_message" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('wsp_message'); ?>">
                <span class="help-block">Mensaje por defecto</span> 
            </div>
        </div>   


        <h3 class="box-title m-b-0">Mensaje de la Tienda</h3>
        <br>
        <div class="form-group">
            <label class="col-md-12">Título</label>
            <div class="col-md-6">
                <input type="text" name="shop_msj_title" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('shop_msj_title'); ?>">
                <span class="help-block"> Dejar este campo en blanco para ocultar el mensaje</span> 
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-12">Mensaje</label>
            <div class="col-md-6">
                <input type="text" name="shop_msj_detail" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('shop_msj_detail'); ?>">
                <span class="help-block"> Dejar este campo en blanco para ocultar el mensaje</span> 
            </div>
        </div>   

        <hr>
        <h3 class="box-title">Configuración del sistema</h3>
        
        <div class="form-group">
            <label class="col-md-12">URL del Sistema</label>
            <div class="col-md-6">
                <input readonly="" type="text" name="sys-url" class="form-control" value="<?= base_url() ?>">
                    
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-12">Nombre del Sistema</label>
            <div class="col-md-6">
                <input type="text" name="sys-title" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('sys_title'); ?>">
                    
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-12">Logo del Sistema</label>
            <div class="col-md-6">
                <input type="text" name="sys-logo" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('sys_logo'); ?>">
                    
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-12">Logo del Correo</label>
            <div class="col-md-6">
                <input type="text" name="sys-mail-logo" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('sys_mail_logo'); ?>">
                <span class="help-block">Logo de cabecera en notificaciones por correo</span>  
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-12">Correo Remitente</label>
            <div class="col-md-6">
                <input type="text" name="mail-sender" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('sys_mailsender'); ?>">
                <span class="help-block">Correo que envia por defecto las alertas del sistema</span>  
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-12">Correo del Administrador</label>
            <div class="col-md-6">
                <input type="text" name="admin-mail" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('admin_mail'); ?>"> 
                <span class="help-block">Receptores de correos de notificacion de Pedidos <b>(Si es mas de un correo, separelos con una coma ",")</b></span> 
            </div>
        </div>

        <hr>

        <h3 class="box-title m-b-0">Configuracion del Correo</h3>
        <br> 

            <div class="form-group">
                <label class="col-md-12">Host</label>
                <div class="col-md-6">
                    <input type="text" name="smtp_host" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('smtp_host'); ?>">
                    <span class="help-block">Host del Servidor de correo </span> 
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-12">Puerto</label>
                <div class="col-md-6">
                    <input type="text" name="smtp_port" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('smtp_port'); ?>">
                    <span class="help-block">Puerto para la conexion </span> 
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-12">Usuario</label>
                <div class="col-md-6">
                    <input type="text" name="smtp_user" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('smtp_user'); ?>">
                    <span class="help-block">Correo remitente </span> 
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-12">Contraseña</label>
                <div class="col-md-6">
                    <input type="text" name="smtp_pass" class="form-control" value="<?= $ci->Base_model->get_sysopt_value('smtp_pass'); ?>">
                    <span class="help-block">Contraseña del correo remitente </span> 
                </div>
            </div>



        <button type="submit" name="save-config" class="btn btn-success waves-effect waves-light m-r-10">Guardar</button>

        <?php echo form_close(); ?>
                </div>
                <!-- /.box-body --> 
                </div>
                <!-- /.box -->

            </section> 