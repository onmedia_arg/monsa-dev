<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Mostrar Marcas</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('marca/add'); ?>" class="btn btn-success btn-sm">Nueva Marca</a> 
                </div>
            </div>
            <div class="box-body">
                <table id="sortable" class="table table-striped">
                    <thead>
                        <tr>
    						<th>IdMarca</th>
    						<th>Nombre</th>
    						<th>Slug</th>
                            <th>Orden</th>
    						<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($marca as $m){ ?>
                        <tr data-id="<?= $m['idMarca']; ?>">
    						<td><?= $m['idMarca']; ?></td>
    						<td><?= $m['nombre']; ?></td>
                            <td><?= $m['slug']; ?></td>
    						<td><?= $m['orden']; ?></td>
    						<td>
                                <a href="<?php echo site_url('marca/edit/'.$m['idMarca']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                                <a href="<?php echo site_url('marca/remove/'.$m['idMarca']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                                <span style="cursor: move; margin-left: 5px;" title="Arrastra para cambiar la orden">
                                    <i class="fa fa-arrows"></i>
                                </span>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                                
            </div>
        </div>
    </div>
</div>
<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
<script type="text/javascript"> 

    $(document).on( 'ready', function () { 

        $( "#sortable tbody" ).sortable({
            update: function( event, ui ) { 

                var list = [],
                    i=0;

                $( "#sortable tbody tr" ).each(function(k,v){
                    // Actualiza td con orden nuevo
                    $(this).find('.priority').text( i )
                    list.push( { 'orden': i, 'idMarca': $(this).attr('data-id') } )
                    i++;
                })

                console.log( list )

                $.ajax({
                    'url': "<?= site_url('marca/update_sort/') ?>",
                    'type': 'post',
                    'data': {'json': JSON.stringify(list) },
                    'dataType': 'json',
                    success: function(response){

                        if( response.code == 1 ){

                            iziToast.success({
                                title: 'OK!',
                                message: response.message,
                            });   

                        }else{

                            iziToast.error({
                                title: 'Error!',
                                message: response.message,
                            }); 

                        }

                    }
                })

            }
        });
        $( "#sortable tbody" ).disableSelection();

    } );

</script>
<?php //$this->load->view('script/dataTable') ?>
