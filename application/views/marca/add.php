<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Crear Marca</h3>
            </div>
              <?php 
                  $attributes = array('id' => 'form', 'onsubmit' => 'return formOnSubmit()');
                  echo form_open('marca/add', $attributes);
              ?>
              <div class="box-body">
                <div class="row clearfix">
                <div class="col-md-6">
                  <label for="idMarca" class="control-label">Id</label>
                  <div class="form-group">
                    <input type="number" name="idMarca" value="<?php echo $this->input->post('idMarca'); ?>" class="form-control" id="idMarca" />
                  </div>
                </div>
      					<div class="col-md-6">
      						<label for="nombre" class="control-label">Nombre</label>
      						<div class="form-group">
      							<input type="text" name="nombre" value="<?php echo $this->input->post('nombre'); ?>" class="form-control" id="nombre" />
      						</div>
      					</div>
      					<div class="col-md-6">
      						<label for="slug" class="control-label">Slug</label>
      						<div class="form-group">
      							<input type="text" name="slug" value="<?php echo $this->input->post('slug'); ?>" class="form-control" id="slug" />
      						</div>
      					</div>
      				</div>
      			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Guardar Marca
            	</button>
              <div style="display: none;">
                <h4 class="text text-danger">Ocurrió un error</h4>
                <ul class="text text-warning list-group" id="form-error"></ul>
              </div>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>

<?php $this->load->view('marca/script') ?>