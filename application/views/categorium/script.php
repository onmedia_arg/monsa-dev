<script>

	function limpiarSlug(slug){
		slug = slug.toLowerCase();
		slug = slug.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
		slug = slug.replace(/ /g,"-");
		slug = slug.replace((/[^A-Za-z0-9]+/g),"_");
		return slug;
	}

	function formOnSubmit(){
		var valid = validForm();
		if (valid.success) {
			return true;
		}else{
			showErrors(valid.error);
			return false;
		}
	}

	function showErrors(arr){
		$('#form-error').parent().slideUp();
		$('#form-error').html('');
		arr.forEach((item, index) => {
			console.log(item);
			var li = $('<li>', {
				class : 'list-group-item', 
				html : item
			})
			$('#form-error').append(li);
		})
		$('#form-error').parent().slideDown();
	}

	function validForm(){
		var success = {
			error : [], 
			success : true
		};

		if ($('#idfamilia').val() == 0) {
			success.error.push('Familia: Selecciona una familia'); 
			success.success = false; 
		}

		if ($('#nombre').val() == '') {
			success.error.push('Nombre: Escribe un nombre'); 
			success.success = false; 
		}

		if ($('#slug').val() == '') {
			success.error.push('Slug: Slug no valido'); 
			success.success = false; 
		}

		return success;
	}

	$(() =>{
		
		$('#nombre').on('change', () => {
			var name = $('#nombre').val();
			name = limpiarSlug(name);
			$('#slug').val(name);
		})

		$('#idfamilia').select2();

	})
</script>