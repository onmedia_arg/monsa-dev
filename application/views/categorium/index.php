<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Listado de Categorías</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('categorium/add'); ?>" class="btn btn-success btn-sm">Nueva Categoria</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
    						<th>IdCategoria</th>
    						<th>Familia</th>
    						<th>Nombre</th>
    						<th>Slug</th>
    						<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($categoria as $c){ ?>
                            <tr>
        						<td><?php echo $c['idCategoria']; ?></td>
        						<td><?php echo $c['familia']; ?></td>
        						<td><?php echo $c['nombre']; ?></td>
        						<td><?php echo $c['slug']; ?></td>
        						<td>
                                    <a href="<?php echo site_url('categorium/edit/'.$c['idCategoria']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                                    <a href="<?php echo site_url('categorium/remove/'.$c['idCategoria']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                                
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('script/dataTable') ?>
