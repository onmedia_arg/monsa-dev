<style>
	.thumbs{
		text-align: center;
	}
	.list-thumb{
		width: auto;
		height: auto;
		max-height: 50px;
		max-width: 50px;
		border: solid 1px white;
		border-radius: 3px;
	}
	.box-body{
		overflow-x: scroll;
	}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box list">
            <div class="box-header">
                <h3 class="box-title">Producto Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('producto/add'); ?>" class="btn btn-success btn-sm">Agregar</a>
					<a href="<?php echo site_url('producto/updateBulkPrice'); ?>" class="btn btn-primary btn-sm">Subir xls</a>
					<a href="<?php echo site_url('producto/bulk_index'); ?>" class="btn btn-info btn-sm">Masivas</a>
				</div>
            </div>
            <div class="box-body">
				<table class="table table-striped table-bordered" id="table-prod">
					<thead>
						<tr>
							<th>SKU</th>
							<th>FAMILIA</th>
							<th>MARCA</th>
							<th>MODELO</th>
							<th>PRECIO</th>
							<th>STOCK</th>
							<th>VISIBILIDAD</th>
							<th>ACCIONES</th>
			            </tr>
					</thead>
				</table>                
                                
            </div>
        </div>
    </div>
</div>


<script>
	var table = null;

    $(function () {
        
        fn_build_table();

    });	

	function fn_build_table(){

		table = $('#table-prod').DataTable({
			"serverSide": true,
			"processing": true,
			"responsive": true,
			"ajax":{
				url :"<?php echo base_url()?>Producto/get_list_dt", // json datasource
				type: "post", 
			},
	        "columnDefs":[
	                    {
	                    "targets": [ 4 ],
	                    render: $.fn.dataTable.render.number( '.', ',', 0, '$ ' )
	                     },
	            ],			
		})

	};

</script>
