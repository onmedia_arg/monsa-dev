<style>
	/*
		TODO: mover los estilos al main css;
	*/
	h5{
		margin-top: 0px;
		margin-bottom: 5px;
	}
	div.btn{
		display: flex;
		justify-content: center;
		align-items: center;
		min-width: 120px;
	}
	.select2-container{
		max-width: 500px !important;
		width: 100% !important;
	}
	.select2-selection{
		min-height: 34px !important;
	}

	.fileContainer {
	    overflow: hidden;
	    position: relative;
	    height: 40px;
	    width: 100%;
	    max-width: 120px;
	}

	.fileContainer [type=file] {
	    cursor: pointer;
	    opacity: 0;
	    position: absolute;
	    bottom: : 0;
	    right: 0;
	    left: 0;
	    top: 0;
	}
	#images-preview{
		display: flex;
		justify-content: flex-start;
		flex-wrap: wrap;
	}
	#images-preview img{
		max-width: 200px;
		max-height: 200px;
		height: auto;
		width: auto;
		border: solid 3px white;
		box-shadow: 0 0 3px black;
		border-radius: 5px;
		margin: 10px;
	}
	.img-wrapper{
		position: relative;
	}
	.img-wrapper .btn{
		position: absolute;
		top: 5px;
		right: 5px;
		border-radius: 10px;
	}
	.select2-selection__choice {
	    color: #333 !important;
	}
	.list-group-item{
		display: flex;
		justify-content: space-between;
	}
	div.form-control{
		background: #eee;
	}

</style>
<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Agregar Producto</h3>
            </div>
            <?php 
            	$attributes = array('id' => 'form', 'onsubmit' => 'return formOnSubmit()');
            	echo form_open_multipart	('producto/add', $attributes); 
            ?>
          	<div class="box-body">
          		<div class="clearfix">
          			<div class="row datos-generales">
          				<fieldset class="col-md-9">
          					<legend><h2>Datos Generales</h2></legend>
							<div class="col-md-6">
								<label for="nombre" class="control-label">Nombre (es autogenerado)</label>
								<div class="form-group">
									<div class="form-control text-info" id="nombre-label"></div>
									<input type="hidden" name="nombre" value="<?php echo $this->input->post('nombre'); ?>" class="form-control" id="nombre" readonly/>
								</div>
							</div>
							<div class="col-md-6">
								<label for="slug" class="control-label">Slug (es autogenerado)</label>
								<div class="form-group">
									<input type="text" name="slug" value="<?php echo $this->input->post('slug'); ?>" class="form-control" id="slug" readonly/>
								</div>
							</div>
							<div class="col-md-6">
								<label for="idFamilia" class="control-label">Familia</label>
								<div class="form-group">
									<select name="idFamilia" class="form-control" id="idFamilia"/>
										<option value="0">Seleccione Familia</option>
										<?php 
										foreach($all_familia as $family)
											{
												$selected = ($family['idFamilia'] == $this->input->post('idFamilia')) ? ' selected="selected"' : "";
												if ($family['slug'] != 'todas') {
													printf(
														'<option value="%s" %s data-slug="%s">%s</option>',
															$family['idFamilia'],
															$selected,
															$family['slug'],
															$family['nombre']
													);
												}
											} 
										?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<label for="cats" class="control-label">Categorias</label>
								<div class="form-group">
									<select class="tags form-control" name="cats[]" id="cats" multiple="multiple" disabled="disabled">
									  <option value="0">Seleccione Categorías</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<label for="marcaProducto" class="control-label">Marcas del Producto</label>
								<div class="form-group">
									<select name="marcaProducto" class="form-control" id="marcaProducto">
										<option value="">Seleccione Marca</option>
										<?php 
										foreach($all_marca as $marca)
											{
												$selected = ($marca['idMarca'] == $this->input->post('idMarca')) ? ' selected="selected"' : "";

												printf(
														'<option value="%s" %s  data-slug="%s">%s</option>',
															$marca['idMarca'],
															$selected,
															$marca['slug'],
															$marca['nombre']
													);
											} 
										?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<label for="modeloProducto" class="control-label">Modelo del Producto</label>
								<div class="form-group">
									<input type="text" name="modeloProducto" value="<?php echo $this->input->post('modeloProducto'); ?>" class="form-control" id="modeloProducto" />
								</div>
							</div>
							<div class="col-md-12">
								<label for="descripcion" class="control-label">Descripción</label>
								<div class="form-group">
									<textarea name="descripcion" class="form-control" id="descripcion" ><?php echo $this->input->post('descripcion'); ?></textarea>
								</div>
							</div>
          				</fieldset>
          				<fieldset class="col-md-3">
          					<legend><h2>Visibilidad</h2></legend>
							<div class="col-md-12">
								<div class="btn-group visibilidad" role="group">
									<button type="button" class="btn btn-primary btn-visibility" data-visibility="oculto">Ocultar</button>
									<button type="button" class="btn btn-secondary btn-visibility" data-visibility="publicar">Publicar</button>
									<button type="button" class="btn btn-secondary btn-visibility" data-visibility="borrador">Borrador</button>
								</div>
								<input type="hidden" name="visibilidad" id="visibilidad" value="oculto" class="form-control">
							</div>
          				</fieldset>
          			</div>
          			<div class="row venta">	
						<fieldset class="col-md-9">
							<legend><h2>Venta</h2></legend>
							<div class="col-md-6">
								<label for="sku" class="control-label">Sku</label>
								<div class="form-group">
									<input type="text" name="sku" value="<?php echo $this->input->post('sku'); ?>" class="form-control" id="sku" />
								</div>
							</div>
							<div class="col-md-6">
								<label for="precio" class="control-label">Precio (ARS)</label>
								<div class="form-group">
									<input type="number" name="precio" value="<?php echo $this->input->post('precio'); ?>" class="form-control" id="precio" />
								</div>
							</div>
							<div class="col-md-6">
								<label for="peso" class="control-label">Peso (g)</label>
								<div class="form-group">
									<input type="number" name="peso" value="<?php echo $this->input->post('peso'); ?>" class="form-control" id="peso" />
								</div>
							</div>
							<div class="col-md-6">
								<h5><label for="stock" class="control-label">Stock</label></h5>
								<div class="btn-group stock" role="group">
									<button type="button" class="btn btn-primary btn-stock" data-stock="alto">Alto</button>
									<button type="button" class="btn btn-secondary btn-stock" data-stock="medio">Medio</button>
									<button type="button" class="btn btn-secondary btn-stock" data-stock="bajo">Bajo</button>
								</div>
								<input type="hidden" name="stock" id="stock" value="alto" class="form-control">
							</div>
							<div class="col-md-11 col-md-offset-1">
								<fieldset class="row">
									<legend><h5><label for="stock" class="control-label">Dimensiones (cm)</label></h5></legend>
									<div class="col-md-4">
										<label for="alto" class="control-label">Alto</label>
										<div class="form-group">
											<input type="number" name="alto" value="<?php echo $this->input->post('alto'); ?>" class="form-control" id="alto" placeholder="Alto"/>
										</div>
									</div>
									<div class="col-md-4">
										<label for="largo" class="control-label">Largo</label>
										<div class="form-group">
											<input type="number" name="largo" value="<?php echo $this->input->post('largo'); ?>" class="form-control" id="largo" placeholder="Largo"/>
										</div>
									</div>
									<div class="col-md-4">
										<label for="ancho" class="control-label">Ancho</label>
										<div class="form-group">
											<input type="number" name="ancho" value="<?php echo $this->input->post('ancho'); ?>" class="form-control" id="ancho" placeholder="ancho"/>
										</div>
									</div>
								</fieldset>
							</div>

						</fieldset>
          			</div>
          			<div class="row galeria">
          				<fieldset class="col-md-12">

          					<legend><h2>Galería</h2></legend>
          					<div class="col-md-2">
								<div class="fileContainer btn btn-primary">
									Subir Imágenes
									<input type="file" name="imagen[]" class="form-control-file" id="imagen" placeholder="Imagen" multiple/>
								</div>
								<div class="btn btn-warning fileContainer hidden" id="remove-images">Borrar Imágenes</div>
          					</div>
          					<div class="col-md-10">
								<fieldset class="preview hidden">
									<div id="images-preview">
										<div class="img-wrapper"></div>
									</div>
								</fieldset>
          					</div>
          				</fieldset>
          			</div>
          			<div class="row atributos">
          				<fieldset class="col-md-9">
          					<legend><h2>Detalle Atributos</h2></legend>
          					<div class="col-md-6">
								<label for="atributoFamilia" class="control-label">Atributos por Familia</label>
								<div class="form-group">
									<select name="atributoFamilia" class="form-control" id="atributoFamilia" style="display: none !important;" />
										<option value="0">Seleccione Familia</option>
										<?php 
										var_dump($all_familia);

										foreach($all_familia as $family)
											{
												$selected = ($family['idFamilia'] == $this->input->post('idFamilia')) ? ' selected="selected"' : "";

												printf(
													'<option value="%s" %s data-slug="%s">%s</option>',
														$family['idFamilia'],
														$selected,
														$family['slug'],
														$family['nombre']
												);
											} 
										?>
									</select>
									<br><div id="attr-error" class="alert alert-info" style="display: none;"></div>
								</div>
							</div>
							<div class="col-md-6">
								<div id="attr-wrapper"></div>
							</div>
          				</fieldset>
          			</div>

					<div class="row atributos">
					<legend><h2>Detalle Atributos</h2></legend>
					</div>  
				<div class="row aplicacion">
					<fieldset class="col-md-12">
						<legend><h2>Aplicación</h2></legend>
						<div class="col-md-6">
							<div class="col-md-12">
								<label for="idMarca" class="control-label">Marcas de Moto</label>
								<div class="form-group">
									<select name="idMarca" class="form-control" id="idMarca">
										<option value="">Seleccione Marca</option>
										<?php 
										foreach($all_marca as $marca)
											{
												$selected = ($marca['idMarca'] == $this->input->post('idMarca')) ? ' selected="selected"' : "";

												printf(
														'<option value="%s" %s>%s</option>',
															$marca['idMarca'],
															$selected,
															$marca['nombre']
													);
											} 
										?>
									</select>
								</div>
							</div>
							<div class="col-md-12">
								<label for="modelo" class="control-label">Modelos de Moto</label>
								<div class="form-group">
									<select class="modelo form-control" name="modelo[]" id="modelo" disabled="disabled">
									  <option value="">Seleccione Modelos</option>
									</select>
								</div>
							</div>
							<div class="col-md-12">
								<label for="year" class="control-label">Año</label>
								<div class="form-group">
									<select class="year form-control" name="year" id="year">
									  <option value="0">Seleccione Año</option>
									  <option value="todos">Todos</option>
									  <?php 
										for ($i = date('Y'); $i > (date('Y') - 100); $i--) { 
											printf('<option value="%s">%s</option>', $i, $i);
										}
									  ?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="fileContainer btn btn-warning" id="search-moto">Buscar Motos</div>
								<br><div id="moto-error" class="alert alert-info" style="display: none;"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div id="app-add" style="display: none;">
								<div class="col-md-12">
									<label for="year" class="control-label">Motos Encontradas</label>
									<div class="form-group">
										<select class="moto form-control" name="moto" id="moto" disabled="disabled">
										  <option value="0">Seleccione Moto</option>
										  <option value="todos">Todos</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="fileContainer btn btn-primary" id="add-application">Agregar Aplicación</div>
								</div>
							</div>
							<div class="col-md-12">
								<label class="control-label">App Seleccionadas</label>
								<div class="well">
									<ul class="list-app list-group">
										<li class="list-group-item hidden">
											<label for="year" class="control-label"></label>
								 			<input type="hidden" class="form-control moto-input" name="aplicacion[]" value="">
								 			<button type="button" class="btn btn-danger btn-xs right" onclick="borrar(this)"><i class="fa fa-trash"></i></button>
								 		</li>
									</ul>
								</div>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-save"></i> Guardar Producto
            	</button>
            	<div style="display: none;">
	                <h4 class="text text-danger">Ocurrió un error</h4>
	                <ul class="text text-warning list-group" id="form-error"></ul>
	            </div>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>

<?php include_once(FCPATH."/application/controllers/js/monsa-back.php"); ?>

<script>

	$(() =>{
		getAtributesForAllFamily();

		$('#idFamilia').select2(select2Params);
		$('#cats').select2(select2Params);
		$('#idMarca').select2(select2Params);
		$('#modelo').select2(select2Params);
		$('#marcaProducto').select2(select2Params);
		$('#year').select2(select2Params);
		// $('#atributoFamilia').select2({allow-clear: true, disabled: true});
		
		$('#idFamilia').change(() => {
			var idFamilia = ($('#idFamilia').val() == 1 ? '' : $('#idFamilia').val());
			var selectCats = $('select#cats')
			create_slug()
			selectCats.html('<option value="0">Cargando ...</option>').slideUp(()=>{
				fillSelect(
					'categoria', 
					selectCats,
					base_url + 'categorium/getCategoryJson/' + idFamilia 
				);

				$.get( base_url + 'categorium/getCategoryJson/' + idFamilia )
				 .done( data => {
				 	selectCats.html('');
				 	var option = $('<option>',  {
						value : 0, 
						text  : 'Seleccione ' + 'categoria' 
					});
					selectCats.append(option);
					for (var i = 0; i < data['categoria'].length; i++) {
						option = $('<option>',  {
							value : data['categoria'][i]['id' + firstUpper('categoria')], 
							text  : data['categoria'][i].nombre 
						});
						selectCats.append(option);
						selectCats.select2(select2Params);
					}
				}).error( err => {
					let option = $('<option>',  {
						value : 0, 
						text  : 'Error al cargar ' + 'categoria'
					});
					selectCats.append(option);
					selectCats.select2({placeholder: 'Error al cargar',allowClear: true});
				})

				selectCats.slideDown();
				document.getElementById('cats').removeAttribute('disabled');
				// $('select#cats').parentsUntil('.col-md-6').fadeIn();
			});
		})

		$('select#idMarca').change(() => {
			var idMarca = ($('#idMarca').val() == 1 ? '' : $('#idMarca').val());
			var selectModelo = $('select#modelo')
			selectModelo.html('<option value="0">Cargando ...</option>').slideUp(()=>{
				$.get( base_url + "modelo/getModeloJson/" + idMarca)
				 .done( data => {
				 	selectModelo.html('');
				 	var option = $('<option>',  {
						value : 0, 
						text  : 'Seleccione ' + 'modelo' 
					});
					selectModelo.append(option);
					var option = $('<option>',  {
						value : 'todos', 
						text  : 'Todos los modelos' 
					});
					selectModelo.append(option);
					for (var i = 0; i < data['modelo'].length; i++) {
						option = $('<option>',  {
							value : data['modelo'][i]['id' + firstUpper('modelo')], 
							text  : data['modelo'][i].nombre 
						});
						selectModelo.append(option);
						selectModelo.select2(select2Params);
					}
				}).error( err => {
					let option = $('<option>',  {
						value : 0, 
						text  : 'Error al cargar ' + 'modelo'
					});
					selectModelo.append(option);
					selectModelo.select2({placeholder: 'Error al cargar',allowClear: true});
				})

				selectModelo.slideDown();
				document.getElementById('modelo').removeAttribute('disabled');
			})
		})

		var btn_visibility = document.querySelectorAll('.btn-visibility');
		btn_visibility.forEach((item, i) => {
			item.addEventListener('click', () =>{
				btn_visibility.forEach((el, i) => {
					el.setAttribute('class', 'btn btn-secondary btn-visibility');
				})
				item.setAttribute('class', 'btn btn-primary btn-visibility');
				var visibility = item.getAttribute('data-visibility');
				document.getElementById('visibilidad').value = visibility;
			})
		})

		var btn_stock = document.querySelectorAll('.btn-stock');
		btn_stock.forEach((item, i) => {
			item.addEventListener('click', () =>{
				btn_stock.forEach((el, i) => {
					el.setAttribute('class', 'btn btn-secondary btn-stock');
				})
				item.setAttribute('class', 'btn btn-primary btn-stock');
				var stock = item.getAttribute('data-stock');
				document.getElementById('stock').value = stock;
			})
		})

		$("#imagen").change(function () {
			$('.preview').removeClass('hidden');
			$('#remove-images').removeClass('hidden');
		    filePreview(this);
		});

		$('#remove-images').click(function(){
			$('#remove-images').addClass('hidden');
			$('#images-preview img').remove();
			$('#images').val('');
		});

		$('#idFamilia').on("select2:select",  getAtributesByFamily);
		$('#idFamilia').on("select2:unselect",  getAtributesByFamily);
		// $('#atributoFamilia').on("select2:select",  getAtributesByFamily);
		// $('#atributoFamilia').on("select2:unselect",  getAtributesByFamily);


		$('#add-application').click(()=>{
			var moto      = $('#moto').val();
			var motoText  = $('#moto option:selected').text();
			var list_item = $('.list-app li.hidden').clone();
			console.log(list_item);
			list_item.removeClass('hidden');
			list_item.find('input.moto-input').val(moto);
			list_item.find('label').text(motoText);
			$('.list-app').append(list_item);

			$('#idMarca').val('').select2(select2Params);
			$('#modelo').val('').select2(select2Params);
			$('#year').val('').select2(select2Params);
		})
		
		$('#search-moto').click(() => {
			var marca  = $('#idMarca').val();
			var modelo = ($('#modelo').val() != 0) ? $('#modelo').val() : '';
			var year   = ($('#year').val() != 0 && $('#year').val()) ? $('#year').val() : '';

			var motoSelect = $('#moto');
			$.get( base_url + 'moto/get_moto_with_params/' + marca + '/' + modelo + '/' + year)
			 .done( data => {
			 	motoSelect.html('');
			 	$('#app-add').slideUp();
			 	var option = $('<option>',  {
					value : 0, 
					text  : 'Seleccione moto' 
				});
				motoSelect.append(option);
				if (data.motos.length > 0) {
					for (var i = 0; i < data.motos.length; i++) {
						var option = $('<option>',  {
							value : data['motos'][i].idMoto, 
							text  : data['motos'][i].nombre
						});
						motoSelect.append(option);
						document.getElementById('moto').removeAttribute('disabled');
						motoSelect.select2(select2Params);
					}
					$('#app-add').slideDown();
				}else{
					option = $('<option>',  {
						value : '0', 
						text  : 'No se encontraron coincidencias'
					});
					motoSelect.append(option);
					$('#moto-error').text('No hay coincidencias');
					$('#moto-error').slideDown(1000, ()=>{
						$('#moto-error').slideUp(2000);
					})
				}
			}).error( err => {
				let option = $('<option>',  {
					value : 0, 
					text  : 'Error al cargar ' + 'motos'
				});
				motoSelect.append(option);
				motoSelect.select2({placeholder: 'Error al cargar',allowClear: true});
			})
		})

		$('#modeloProducto').on('change', () => {
			exeValidName();
		})

		$('#marcaProducto').on('change', () => {
			exeValidName();
		})

		$('#idFamilia').on('change', () => {
			exeValidName();
			setAtributeFamily();
		})

		$('#idMarca').select2();
		$('#year').select2();

	})
</script>

<?php $this->load->view('producto/script') ?>


