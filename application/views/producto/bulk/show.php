<style>
	.errors {
		margin: 10px 0;
	}
</style>

<div class="row">
	<div class="col-md-12">
		<div class="box list">
			<div class="box-header">
				<h3 class="box-title">Producto registro de actualización masiva</h3>
				<div class="box-tools">
					<a href="<?php echo site_url('producto/index'); ?>" class="btn btn-success btn-sm">Productos</a>
					<a href="<?php echo site_url('producto/updateBulkPrice'); ?>" class="btn btn-primary btn-sm">Subir xls</a>
					<a href="<?php echo site_url('producto/bulk_index'); ?>" class="btn btn-info btn-sm">Masivas</a>
				</div>
			</div>
			<div class="box-body">
				<ul>
					<li><strong>Archivo:</strong> <?php echo $bulkUpdateLogs['filename'] ?></li>
					<li><strong>Actulizados:</strong> <?php echo $bulkUpdateLogs['updated'] ?></li>
					<li>
						<strong>Errores:</strong>
						<ul class="errors">
							<?php foreach (json_decode($bulkUpdateLogs['errors']) as $error): ?>
								<li>
									SKU: <?php echo $error->sku; ?>,
									Precio:<?php echo $error->precio; ?>
								</li>
							<?php endforeach; ?>
						</ul>
					</li>
					<li><strong>Fecha:</strong> <?php echo date('d/m/Y', strtotime($bulkUpdateLogs['fecha'])) ?></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<script>

</script>
