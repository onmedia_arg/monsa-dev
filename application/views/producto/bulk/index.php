<style>
	.thumbs {
		text-align: center;
	}

	.list-thumb {
		width: auto;
		height: auto;
		max-height: 50px;
		max-width: 50px;
		border: solid 1px white;
		border-radius: 3px;
	}

	.box-body {
		overflow-x: scroll;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="box list">
			<div class="box-header">
				<h3 class="box-title">Producto registros de actualización masiva</h3>
				<div class="box-tools">
					<a href="<?php echo site_url('producto/index'); ?>" class="btn btn-info btn-sm">Productos</a>
					<a href="<?php echo site_url('producto/updateBulkPrice'); ?>" class="btn btn-primary btn-sm">Subir xls</a>
				</div>
			</div>
			<div class="box-body">
				<table class="table" id="table-prod">
					<thead>
					<tr>
						<th>ARCHIVO</th>
						<th>ACTUALIZADOS</th>
						<th>FECHA</th>
						<th>ACCIONES</th>
					</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
    var table = null;

    $(function () {
        fn_build_table();
    });

    function fn_build_table() {
        table = $('#table-prod').DataTable({
            "serverSide": true,
            "processing": true,
            "responsive": true,
            "ajax": {
                url: "<?php echo base_url()?>Producto/get_bulk_list_dt", // json datasource
                type: "post",
            }
        })
    };
</script>
