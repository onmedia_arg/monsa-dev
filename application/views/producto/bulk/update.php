<style>
	
</style>

<div class="row">
    <div class="col-md-12">
        <div class="box list">
            <div class="box-header">
                <h3 class="box-title">Actualizar Precios</h3>
				<div class="box-tools">
					<a href="<?php echo site_url('producto/index'); ?>" class="btn btn-info btn-sm">Productos</a>
					<a href="<?php echo site_url('producto/bulk_index'); ?>" class="btn btn-primary btn-sm">Masivas</a>
				</div>
            </div>
            <div class="box-body">
				<form action="<?php echo site_url('producto/do_updateBulkPrice'); ?>" method="post" enctype="multipart/form-data">
					<div>
						<label for="excel" class="control-label">Excel</label>
						<div class="form-group">
							<input type="file" name="excel" value="<?php echo $this->input->post('excel'); ?>" class="form-control" id="excel" readonly/>
							<?php echo form_error('excel'); ?>
						</div>
					</div>
					<button type="submit" class="btn btn-success">
						<i class="fa fa-save"></i> Enviar
					</button>
				</form>
            </div>
        </div>
    </div>
</div>

<script>
	
</script>
