<style>
	/*
		TODO: mover los estilos al main css;
	*/
	h5{
		margin-top: 0px;
		margin-bottom: 5px;
	}
	div.btn{
		display: flex;
		justify-content: center;
		align-items: center;
		min-width: 120px;
	}
	.select2-container{
		max-width: 500px !important;
		width: 100% !important;
	}
	.select2-selection{
		min-height: 34px !important;
	}

	.fileContainer {
	    overflow: hidden;
	    position: relative;
	    height: 40px;
	    width: 100%;
	    max-width: 120px;
	}

	.fileContainer [type=file] {
	    cursor: pointer;
	    opacity: 0;
	    position: absolute;
	    bottom: : 0;
	    right: 0;
	    left: 0;
	    top: 0;
	}
	#images-preview{
		display: flex;
		justify-content: flex-start;
		flex-wrap: wrap;
	}
	#images-preview img{
		max-width: 200px;
		max-height: 200px;
		height: auto;
		width: auto;
		border: solid 3px white;
		box-shadow: 0 0 3px black;
		border-radius: 5px;
		margin: 10px;
	}
	.img-wrapper{
		position: relative;
	}
	.img-wrapper .btn{
		position: absolute;
		top: 5px;
		right: 5px;
		border-radius: 10px;
	}
	.select2-selection__choice {
	    color: #333 !important;
	}

	.list-group-item{
		display: flex;
		justify-content: space-between;
	}
	.atributos .attr-saved-list {
	    display: flex;
	    flex-wrap: wrap;
	    justify-content: center;
	    align-content: center;
	}
	.atributos .list-group-item {
	    display: flex;
	    justify-content: space-between;
	    flex: 1;
	    min-width: 200px;
	    min-height: 60px;
	}
	.btn-danger.btn-xs{
		max-height: 30px;
	}

 
	.var-empty:after {
	    content: 'Puedes añadir una variación haciendo click en "Agregar"';
	    position: absolute;
	    left: 0;
	    right: 0;
	    margin: auto;
	    width: 100%;
	    height: 100%;
	    text-align: center;
	    display: flex;
	    flex-direction: column;
	    justify-content: center;
	    color: white;
	    font-size: 1.4em;
	    letter-spacing: 2px; 
	    height: 100px;
	    background: #3c8dbc;
	    border-radius: 3px;
	    opacity: .6;
	    position: relative;
	    /* border: dashed lightgray; */
	    /* padding: 10px; */
	}

	h4.attr-header, h4.var-header {
	    padding: 10px;
	}
	
	.nav-tabs-custom{
		overflow: hidden;
	}

	.var-row:first-child, .attr-row:first-child {
	    margin-top: 25px;
	}

	.var-row, .attr-row {
	    border-bottom: dashed #ecf0f5 1px;
	}

	.var-row:first-child, .attr-row:first-child {
	    border-top: dashed #ecf0f5 1px;
	}

	a.attr-slide, a.var-slide {
		transition: ease .2s;
	    background: #3c8dbc;
	    width: 25px;
	    height: 25px;
	    border-radius: 50%;
	    text-align: center;
	    margin-top: -5px;
	    color: white;
	}

	a.attr-slide:hover, a.var-slide:hover {
	    background: #367fa9;
	}

	a.attr-slide .fa-sort-up, a.var-slide .fa-sort-up {
	    position: relative;
	    bottom: -5px;
	}

	/* table border fix */
	.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{
		border: none !important;
	}

	.var-row:last-child, .attr-row:last-child {
	    margin-bottom: 25px;
	}
</style>
<div class="row">
    <div class="col-md-12"> 

      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">
              		Editar Producto
              		<a style="margin-left: 10px" target="_blank" href='<?= base_url( 'frontController/detalle/' . $producto["slug"] ) ?>' class='btn btn-success btn-xs pull-right'>
              			<span class='fa fa-eye'></span> VER
              		</a>
              	</h3>
            </div>
			<?php echo form_open_multipart('producto/edit/'.$producto['idProducto']); ?>
			<div class="box-body">
				<div class="clearfix">

<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Datos Generales</a></li>
              <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Venta</a></li>
              <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Galería</a></li>
              <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Atributos y Variaciones</a></li> 
              <li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false">Aplicación</a></li> 
              <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
					<div class="row datos-generales">
          				<fieldset class="col-md-9">
          					<legend><h2>Datos Generales</h2></legend>
							<div class="col-md-6">
								<label for="nombre" class="control-label">Nombre</label>
								<div class="form-group">
									<input type="text" name="nombre" value="<?php echo ($this->input->post('nombre') ? $this->input->post('nombre') : $producto['nombre']); ?>" class="form-control" id="nombre" />
								</div>
							</div>
							<div class="col-md-6">
								<label for="slug" class="control-label">Slug</label>
								<div class="form-group">
									<input type="text" name="slug" value="<?php echo ($this->input->post('slug') ? $this->input->post('slug') : $producto['slug']); ?>" class="form-control" id="slug" />
								</div>
							</div>
							<div class="col-md-6">
								<label for="idFamilia" class="control-label">Familia</label>
								<div class="form-group">
									<select name="idFamilia" class="form-control" id="idFamilia"/>
										<option value="0">Seleccione Familia</option>
										<?php 
										foreach($all_familia as $family)
											{
												$selected = ($family['idFamilia'] == $this->input->post('idFamilia')) ? ' selected="selected"' : "";

												if ($family['slug'] != 'todas') {
													printf(
														'<option value="%s" %s data-slug="%s">%s</option>',
															$family['idFamilia'],
															$selected,
															$family['slug'],
															$family['nombre']
													);
												}
											} 
										?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<label for="cats" class="control-label">Categorias</label>
								<div class="form-group">
									<select class="tags form-control" name="cats[]" id="cats" multiple="multiple">
									  <option value="0">Seleccione Categorías</option>
									  <?php 
										foreach ($cats as $key => $cat) {
											printf(
													'<option value="%s" selected="selected">%s</option>',
														$cat['idCategoria'],
														$cat['categoria']
												);
										}
									  ?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<label for="marcaProducto" class="control-label">Marcas del Producto</label>
								<div class="form-group">
									<select name="marcaProducto" class="form-control" id="marcaProducto">
										<option value="">Seleccione Marca</option>
										<?php 
										foreach($all_marca as $marca)
											{
												$selected = ($marca['idMarca'] == $this->input->post('idMarca')) ? ' selected="selected"' : "";

												printf(
														'<option value="%s" %s  data-slug="%s">%s</option>',
															$marca['idMarca'],
															$selected,
															$marca['slug'],
															$marca['nombre']
													);
											} 
										?>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<label for="modeloProducto" class="control-label">Modelo del Producto</label>
								<div class="form-group">
									<input type="text" name="modeloProducto" value="<?php echo ($this->input->post('modelo') ? $this->input->post('modelo') : $producto['modelo']); ?>" class="form-control" id="modeloProducto" />
								</div>
							</div>
							<div class="col-md-12">
								<label for="descripcion" class="control-label">Descripción</label>
								<div class="form-group">
									<textarea name="descripcion" class="form-control" id="descripcion" ><?php echo ($this->input->post('descripcion') ? $this->input->post('descripcion') : $producto['descripcion']); ?></textarea>
								</div>
							</div>
          				</fieldset>
          				<fieldset class="col-md-3">
          					<legend><h2>Visibilidad</h2></legend>
							<div class="col-md-12">
								<?php 
									$visibilidad = ($this->input->post('visibilidad') ? $this->input->post('visibilidad') : $producto['visibilidad']);
								 ?>
								<div class="btn-group visibilidad" role="group">
									<button type="button" class="btn btn-<?php echo ($visibilidad == 'oculto') ? 'primary': 'secondary';  ?> btn-visibility" data-visibility="ocultar">Ocultar</button>
									<button type="button" class="btn btn-<?php echo ($visibilidad == 'publicar') ? 'primary': 'secondary';  ?> btn-visibility" data-visibility="publicar">Publicar</button>
									<button type="button" class="btn btn-<?php echo ($visibilidad == 'borrador') ? 'primary': 'secondary';  ?> btn-visibility" data-visibility="borrador">Borrador</button>
								</div>
								<input type="hidden" name="visibilidad" id="visibilidad" value="<?php echo ($this->input->post('visibilidad') ? $this->input->post('visibilidad') : $producto['visibilidad']); ?>" class="form-control">
							</div>
          				</fieldset>
          			</div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
          			<div class="row venta">	
						<fieldset class="col-md-9">
							<legend><h2>Venta</h2></legend>
							<div class="col-md-6">
								<label for="sku" class="control-label">Sku</label>
								<div class="form-group">
									<input type="text" name="sku" value="<?php echo ($this->input->post('sku') ? $this->input->post('sku') : $producto['sku']); ?>" class="form-control" id="sku" />
								</div>
							</div>
							<div class="col-md-6">
								<label for="precio" class="control-label">Precio (ARS)</label>
								<div class="form-group">
									<input type="text" name="precio" value="<?php echo ($this->input->post('precio') ? $this->input->post('precio') : $producto['precio']); ?>" class="form-control" id="precio" />
								</div>
							</div>
							<div class="col-md-6">
								<label for="peso" class="control-label">Peso (g)</label>
								<div class="form-group">
									<input type="text" name="peso" value="<?php echo ($this->input->post('peso') ? $this->input->post('peso') : $producto['peso']); ?>" class="form-control" id="peso" />
								</div>
							</div>
							<div class="col-md-6">
								<h5><label for="stock" class="control-label">Stock</label></h5>
								<div class="btn-group stock" role="group">
									<?php 
										$stock = ($this->input->post('stock') ? $this->input->post('stock') : $producto['stock']);
									 ?>
									<button type="button" class="btn btn-<?php echo ($stock == 'alto') ? 'primary' : 'secondary'  ?> btn-stock" data-stock="alto">Alto</button>
									<button type="button" class="btn btn-<?php echo ($stock == 'medio') ? 'primary' : 'secondary'  ?> btn-stock" data-stock="medio">Medio</button>
									<button type="button" class="btn btn-<?php echo ($stock == 'bajo') ? 'primary' : 'secondary'  ?> btn-stock" data-stock="bajo">Bajo</button>
								</div>
								<input type="hidden" name="stock" id="stock" value="<?php echo $stock; ?>" class="form-control">
							</div>
							<div class="col-md-11 col-md-offset-1">
								<fieldset class="row">

									<?php
										if($producto['dimensiones']!=""){
											$dimensiones = json_decode($producto['dimensiones']);
											$alto = (is_numeric($dimensiones->alto)?$dimensiones->alto:0);
											$largo = (is_numeric($dimensiones->largo)?$dimensiones->largo:0);
											$ancho = (is_numeric($dimensiones->ancho)?$dimensiones->ancho:0);
										}
										else {
											$alto = 0;
											$largo = 0;
											$ancho = 0;
										}
									?>
									<legend><h5><label for="stock" class="control-label">Dimensiones (cm)</label></h5></legend>
									<div class="col-md-4">
										<label for="alto" class="control-label">Alto</label>
										<div class="form-group">
											<input type="number" name="alto" value="<?php echo $alto; ?>" class="form-control" id="alto" placeholder="Alto"/>
										</div>
									</div>
									<div class="col-md-4">
										<label for="largo" class="control-label">Largo</label>
										<div class="form-group">
											<input type="number" name="largo" value="<?php echo $largo; ?>" class="form-control" id="largo" placeholder="Largo"/>
										</div>
									</div>
									<div class="col-md-4">
										<label for="ancho" class="control-label">Ancho</label>
										<div class="form-group">
											<input type="number" name="ancho" value="<?php echo $ancho; ?>" class="form-control" id="ancho" placeholder="ancho"/>
										</div>
									</div>
								</fieldset>
							</div>

						</fieldset>
          			</div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
          			<div class="row galeria">
          				<fieldset class="col-md-12">

          					<legend><h2>Galería</h2></legend>
          					<div class="col-md-2">
								<div class="fileContainer btn btn-primary">
									Subir Imágenes
									<input type="file" name="imagen[]" class="form-control-file" id="imagen" placeholder="Imagen" multiple/>
								</div>
								<div class="btn btn-warning fileContainer hidden" id="remove-images">Borrar Imágenes</div>
          					</div>
          					<div class="col-md-10">
								<fieldset class="preview hidden">
									<div id="images-preview">
										<?php
											$quitar   = ['\\'];
											$remplazar= '';
											$imagenes = str_replace($quitar, $remplazar, $producto['imagen']);
											$imagenes = json_decode($imagenes);
											if (count($imagenes) > 0) {
												for ($i=0; $i < count($imagenes) ; $i++) {
													printf('<div class="img-wrapper">
																<img src="%s" alt="%s" class="list-thumb">
																<button type="button" class="btn btn-danger btn-xs" data-id ="%s" onclick="borrar(this)">
																	<i class="fa fa-remove"></i>
																</button>
																%s
															</div>',
														base_url($imagenes[$i]), 
														$producto['slug'], 
														$i, 
														'<input type="hidden" value="'.$imagenes[$i].'" name="upload[]"/>'
													);
												}
											}
										 ?>
									</div>
								</fieldset>
          					</div>
          				</fieldset>
          			</div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_4">
			   
          			<div id="ko-attributes" class="row atributos">
          				<fieldset class="col-md-12">
          					<legend><h2>Detalle Atributos</h2></legend>
          					<div class="col-md-12">
								<label for="atributoFamilia" class="control-label">Atributos por Familia</label>
								<div class="form-row">
									<div class="col-sm-6">
										<select name="atributoFamilia" class="form-control" id="atributoFamilia"/>
											<option data-placeholder selected>Seleccione Familia</option> 
											<?php foreach($atributos_familia as $atributo):?>
												<option data-family="<?= $atributo['idfamilia'] ?>" value="<?= $atributo['idAtributo'] ?>"><?= $atributo['nombre'] ?></option>
											<?php endforeach; ?>
										</select>
									</div>
									<div class="col-sm-6">
										<button data-bind='click: addAttr' class="btn btn-primary">Nuevo Atributo</button>
									</div> 
								</div>
							</div>

							<!-- Atributos --> 
							<div class="row" data-bind='foreach: { data: attributes, afterRender: $root.attrAfterRender }'>  
								<div data-bind="attr: { id: attributesSection }" class="col-sm-12 attr-row">
									<h4 class="attr-header">
										<span data-bind='text: attributeName'></span>
										<a href="#" class="attr-slide pull-right text-dark">
											<i class="fa fa-sort-up"></i>
										</a>
										<small class="pull-right" style="margin-right: 15px;">
											<a href='#' class="text-danger" data-bind='click: $root.removeAttr'>Quitar</a>
										</small>
									</h4> 
									<div class="row attr-row-data">
										<div class="col-sm-12">
			                                <table class="table"> 
			                                	<tr> 
			                                		<td style="display: none; padding: 25px 10px;">
				                                        <div class="form-group"> 
			                                				<input type="hidden" class="form-control" name="atributo_id[]" data-bind='value: attributesSection' disabled="" required="">
			                                			</div>
			                                		</td>
				                                    <td style="width: 30%; padding: 25px 10px;">
				                                        <div class="form-group">
															<input type="text" data-bind='value: attributeName' name="atributo_nombre[]" class="form-control" placeholder="Nombre" required="" disabled="" />  
															
				                                        </div>
				                                        <!-- <div style="margin: 0" class="form-group" data-bind="if: isAttrVariable"> -->
				                                        <div style="margin: 0" class="form-group">
				                                        	<label for="">
				                                    			<input type="checkbox" data-bind="checked: attrVar, event: { change: $root.activeVariable }" name="atributo_variacion[]" value="1"> Usado para Variaciones
				                                    		</label>
				                                        </div>
				                                    </td> 
				                                    <td style="width: 40%; padding: 25px 10px;" class="text-center">
				                                        <div class="form-group">
				                                        	<select data-bind=" optionsCaption: 'Seleccione los Atributos',
				                                        						options: attributesOptions,
				                                        						selectedOptions: attributesSelectedOptions" 
				                                        			name="atributo_valores[]" class="form-control" multiple="" required=""> 
		                                                		<!-- Knockout -->
		                                            		</select> 
				                                        </div>
				                                        <div style="margin: 0" class="form-group">
				                                        	<button data-bind="attr: { 'data-attribute': attributesSection }" class="btn btn-primary pull-right new-attr-value">Agregar Nuevo</button>
				                                        </div>
				                                    </td> 
			                                    </tr>
			                                </table>
			                            </div>
			                        </div>
								</div>  
							</div> 

							<div class="col-sm-12" data-bind="visible: attributes().length >= 1">
								<button class="btn btn-success save-attributes pull-right">Guardar Atributos</button>
							</div>
          				</fieldset>
          			</div>

					<div id="ko-variations" class="row">
						<div class="col-md-12"> 
							<legend>
								<h2>
									Variaciones
									<button data-bind='click: addVar' class="btn btn-primary">Agregar</button> 
								</h2>
							</legend> 
						</div>

						<!-- Mensaje Variacion -->
						<div class="var-empty" data-bind='visible: variations().length == 0'></div> 

						<div class="col-md-12"  data-bind='foreach: { data: variations, afterRender: $root.varAfterRender }'>
							<!-- Fila de Variacion -->
							<div data-bind="attr: { 'data-variation-attribute': varAttr }" class="var-row col-sm-12">
								<h4 class="var-header">
									Variacion #<span data-bind="text: $index() + 1"></span>
									<a href="#" class="var-slide pull-right text-dark">
										<i class="fa fa-sort-up"></i>
									</a>
									<small class="pull-right" style="margin-right: 15px;">
										<a href='#' class="text-danger" data-bind='click: $root.removeVar'>Quitar</a>
									</small>
								</h4>
								<div class="row var-row-data">

										<div class="form-group col-sm-6">
										    <label>SKU de la variación</label>
										    <input type="text" data-bind='value: varSKU' name="variacion_sku[]" class="form-control" placeholder="Ingrese SKU" required="" />  
										</div>
										<div class="form-group col-sm-6">
										    <label>Precio de la variación</label>
										    <input type="number" min="1" data-bind='value: varPrice' name="variacion_precio[]" class="form-control" placeholder="Precio" required="" />
										</div>
										<div class="form-group col-sm-6">
										    <label>Stock de la variación</label>
										    <!-- <input type="number" min="1" data-bind='value: varStock' name="variacion_stock[]" class="form-control" required="" placeholder="Stock" />  -->
										    <select data-bind=" selectedOptions: varStock " name="variacion_stock[]" class="form-control">
										    	<option value="bajo">Bajo</option>
										    	<option value="medio">Medio</option>
										    	<option value="alto">Alto</option>
										    </select>
										</div>
										<!-- Variaciones KO -->
										<div data-bind="html: varValuesHTML">
										    <!-- knockout -->
										</div>  

								</div>
							</div>  
							<!-- Fila de Variacion -->
						</div>
						<!-- Guardar -->
						<div class="col-sm-12" data-bind="visible: variations().length >= 1" style="margin: 15px 0;">
							<button class="btn btn-success save-variations pull-right">Guardar Variaciones</button>
						</div>
					</div> 

			  </div> 

              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_5">

          			<div class="row aplicacion">
						<fieldset class="col-md-12">
							<legend><h2>Aplicación</h2></legend>
							<div class="col-md-6">
								<div class="col-md-12">
									<label for="idMarca" class="control-label">Marcas de Moto</label>
									<div class="form-group">
										<select name="idMarca" class="form-control" id="idMarca">
											<option value="">Seleccione Marca</option>
											<?php 
											foreach($all_marca as $marca)
												{
													$selected = ($marca['idMarca'] == $this->input->post('idMarca')) ? ' selected="selected"' : "";

													printf(
															'<option value="%s" %s>%s</option>',
																$marca['idMarca'],
																$selected,
																$marca['nombre']
														);
												} 
											?>
										</select>
									</div>
								</div>
								<div class="col-md-12">
									<label for="modelo" class="control-label">Modelos de Moto</label>
									<div class="form-group">
										<select class="modelo form-control" name="modelo[]" id="modelo" disabled="disabled">
										  <option value="">Seleccione Modelos</option>
										</select>
									</div>
								</div>
								<div class="col-md-12">
									<label for="year" class="control-label">Año</label>
									<div class="form-group">
										<select class="year form-control" name="year" id="year">
										  <option value="0">Seleccione Año</option>
										  <option value="todos">Todos</option>
										  <?php 
											for ($i = date('Y'); $i > (date('Y') - 100); $i--) { 
												printf('<option value="%s">%s</option>', $i, $i);
											}
										  ?>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="fileContainer btn btn-warning" id="search-moto">Buscar Motos</div>
									<br><div id="moto-error" class="alert alert-info" style="display: none;"></div>
								</div>
							</div>
							<div class="col-md-6">
								<div id="app-add" style="display: none;">
									<div class="col-md-12">
										<label for="year" class="control-label">Motos Encontradas</label>
										<div class="form-group">
											<select class="moto form-control" name="moto" id="moto" disabled="disabled">
											  <option value="0">Seleccione Moto</option>
											  <option value="todos">Todos</option>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="fileContainer btn btn-primary" id="add-application">Agregar Aplicación</div>
									</div>
								</div>
								<div class="col-md-12">
									<label class="control-label">App Seleccionadas</label>
									<div class="well">
										<ul class="list-app list-group">
											<li class="list-group-item hidden">
												<label for="year" class="control-label"></label>
									 			<input type="hidden" class="form-control moto-input" name="aplicacion[]" value="">
									 			<button type="button" class="btn btn-danger btn-xs" onclick="borrar(this)"><i class="fa fa-trash"></i></button>
									 		</li>
									 		<?php 
												foreach ($aplicacion['motos'] as $key => $moto) {
													printf('<li class="list-group-item">
																<label for="year" class="control-label">%s</label>
													 			<input type="hidden" class="form-control moto-input" name="aplicacion[]" value="%s">
													 			<button type="button" class="btn btn-danger btn-xs" onclick="borrar(this)"><i class="fa fa-trash"></i></button>
													 		</li>',
															$moto['name'], 
															$moto['idMoto']
													 	);
												}
									 		?>
										</ul>
									</div>
								</div>
							</div>
						</fieldset>

					</div>

			  </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
 
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>

<?php include_once(FCPATH."/application/controllers/js/monsa-back.php"); ?>

<!-- RD -->
<script type="text/javascript">
	var idProducto = '<?= $producto['idProducto'] ?>',
		base_url = '<?= base_url() ?>'
</script>
<script src="<?= base_url('resources/js/admin/edit-producto.js') ?>" type="text/javascript"></script>

<script>
	var base_url = '<?= base_url() ?>'
	var select2Params = {placeholder: 'Seleccione ...', allowClear: true};
	$(() =>{

		$('#idFamilia').select2(select2Params);
		$('#idFamilia').val(['<?php echo ($this->input->post('idFamilia') ? $this->input->post('idFamilia') : $producto['idFamilia']); ?>']);
		$('#idFamilia').trigger('change');
		var cats = [
						<?php 
							$i = 0;
							foreach ($cats as $key => $cat) {
								if ($i == 0) {
									echo '"' . $cat['idCategoria'] . '"';
								}else{
									echo ',"' . $cat['idCategoria'] . '"';
								}
								$i++;
							} 
						?>
					]
		$('#cats').select2(select2Params);
		$('#cats').val(cats).trigger('change');
		$('#marcaProducto').select2(select2Params);
		$('#marcaProducto').val(['<?php echo ($this->input->post('idMarca') ? $this->input->post('idMarca') : $producto['idMarca']); ?>']);
		$('#marcaProducto').trigger('change');
		$('.attr-select').select2(select2Params);
		// $('#atributoFamilia').select2(select2Params);
		$('#atributoFamilia').val(['<?php echo ($this->input->post('idFamilia') ? $this->input->post('idFamilia') : $producto['idFamilia']); ?>']);
		$('#atributoFamilia').trigger('change');

		
		$('#idFamilia').change(() => {
			var idFamilia = ($('#idFamilia').val() == 1 ? '' : $('#idFamilia').val());
			var selectCats = $('select#cats')
			selectCats.html('<option value="0">Cargando ...</option>').slideUp(()=>{
				fillSelect(
					'categoria', 
					selectCats,
					base_url + 'categorium/getCategoryJson/' + idFamilia 
				);

				$.get( base_url + 'categorium/getCategoryJson/' + idFamilia )
				 .done( data => {
				 	selectCats.html('');
				 	var option = $('<option>',  {
						value : 0, 
						text  : 'Seleccione ' + 'categoria' 
					});
					selectCats.append(option);
					for (var i = 0; i < data['categoria'].length; i++) {
						option = $('<option>',  {
							value : data['categoria'][i]['id' + firstUpper('categoria')], 
							text  : data['categoria'][i].nombre 
						});
						selectCats.append(option);
						selectCats.select2(select2Params);
					}
				}).error( err => {
					let option = $('<option>',  {
						value : 0, 
						text  : 'Error al cargar ' + 'categoria'
					});
					selectCats.append(option);
					selectCats.select2({placeholder: 'Error al cargar',allowClear: true});
				})

				selectCats.slideDown();
				document.getElementById('cats').removeAttribute('disabled');
				// $('select#cats').parentsUntil('.col-md-6').fadeIn();
			});
		})

		$('select#idMarca').change(() => {
			var idMarca = ($('#idMarca').val() == 1 ? '' : $('#idMarca').val());
			var selectModelo = $('select#modelo')
			selectModelo.html('<option value="0">Cargando ...</option>').slideUp(()=>{
				$.get( base_url + "modelo/getModeloJson/" + idMarca)
				 .done( data => {
				 	selectModelo.html('');
				 	var option = $('<option>',  {
						value : 0, 
						text  : 'Seleccione ' + 'modelo' 
					});
					selectModelo.append(option);
					var option = $('<option>',  {
						value : 'todos', 
						text  : 'Todos los modelos' 
					});
					selectModelo.append(option);
					for (var i = 0; i < data['modelo'].length; i++) {
						option = $('<option>',  {
							value : data['modelo'][i]['id' + firstUpper('modelo')], 
							text  : data['modelo'][i].nombre 
						});
						selectModelo.append(option);
						selectModelo.select2(select2Params);
					}
				}).error( err => {
					let option = $('<option>',  {
						value : 0, 
						text  : 'Error al cargar ' + 'modelo'
					});
					selectModelo.append(option);
					selectModelo.select2({placeholder: 'Error al cargar',allowClear: true});
				})

				selectModelo.slideDown();
				document.getElementById('modelo').removeAttribute('disabled');
			})
		})

		var btn_visibility = document.querySelectorAll('.btn-visibility');
		btn_visibility.forEach((item, i) => {
			item.addEventListener('click', () =>{
				btn_visibility.forEach((el, i) => {
					el.setAttribute('class', 'btn btn-secondary btn-visibility');
				})
				item.setAttribute('class', 'btn btn-primary btn-visibility');
				var visibility = item.getAttribute('data-visibility');
				document.getElementById('visibilidad').value = visibility;
			})
		})

		var btn_stock = document.querySelectorAll('.btn-stock');
		btn_stock.forEach((item, i) => {
			item.addEventListener('click', () =>{
				btn_stock.forEach((el, i) => {
					el.setAttribute('class', 'btn btn-secondary btn-stock');
				})
				item.setAttribute('class', 'btn btn-primary btn-stock');
				var stock = item.getAttribute('data-stock');
				document.getElementById('stock').value = stock;
			})
		})

		// function validarFile(all){
		//     //EXTENSIONES Y TAMANO PERMITIDO.
		//     var extensiones_permitidas = [".png", ".bmp", ".jpg", ".jpeg", ".pdf", ".doc", ".docx", ".gif"];
		//     var tamano = 8; // EXPRESADO EN MB.
		//     var rutayarchivo = all.value;
		//     var ultimo_punto = all.value.lastIndexOf(".");
		//     var extension = rutayarchivo.slice(ultimo_punto, rutayarchivo.length);
		//     if(extensiones_permitidas.indexOf(extension) == -1)
		//     {
		//         alert("Extensión de archivo no valida");
		//         document.getElementById(all.id).value = "";
		//         return; // Si la extension es no válida ya no chequeo lo de abajo.
		//     }
		//     if((all.files[0].size / 1048576) > tamano)
		//     {
		//         alert("El archivo no puede superar los "+tamano+"MB");
		//         document.getElementById(all.id).value = "";
		//         return;
		//     }
		// }

		function filePreview(input) {
			// TODO: eliminar imagenes
			// TODO: validar extensiones
			// TODO: validar tamaños
		    $('#images-preview img').remove();
		    if (input.files && input.files.length > 0) {
		    	for (var i = 0; i < input.files.length; i++) {
					var reader = new FileReader();
			        reader.onload = function (e) {
			            $('#images-preview').append('<img src="'+e.target.result+'" width="450" height="300"/>');
			        }
			        reader.readAsDataURL(input.files[i]);
		    	}
		    }
		}

		function fillSelect (attr, target, url){
	        $.get( url)
	         .done( data => {
	            target.html('');
	            var option = $('<option>',  {
	                value : 0, 
	                text  : 'Seleccione ' + attr 
	            });
	            target.append(option);
	            for (var i = 0; i < data[attr].length; i++) {
	                option = $('<option>',  {
	                    value : data[attr][i].idFamilia, 
	                    text  : data[attr][i].nombre 
	                });
	                target.append(option);
	                target.select2({placeholder: 'Seleccione ...', allowClear: true});
	            }
	        }).error( err => {
	            let option = $('<option>',  {
	                value : 0, 
	                text  : 'Error al cargar ' + attr
	            });
	            target.append(option);
	            target.select2({placeholder: 'Error al cargar',allowClear: true});
	        })
	    }

		$("#imagen").change(function () {
			$('.preview').removeClass('hidden');
			$('#remove-images').removeClass('hidden');
		    filePreview(this);
		});

		<?php 
			if (count($imagenes) > 0) {
				echo '$(".preview").removeClass("hidden");';
				echo '$("#remove-images").removeClass("hidden");';
			}
		?>

		$('#remove-images').click(function(){
			$('#remove-images').addClass('hidden');
			$('#images-preview img').remove();
			$('#images').val('');
		});
		
		var getAtributesForAllFamily = (e) => { 
			$('#attr-wrapper').html('');
			var attr = 'todas';
			var ul = $('<ul>', {"class" : "family-list-ajax"});
			$.get('<?php echo base_url() ?>atributo/get_attr_for_family_all/' + attr)
			.done(data => {
				if (data.atributo[0]) {
					for (var j = 0; j < data.atributo.length; j++) {
						if (data.atributo[j].valor) {
							var li = $('<li>');
							var label = $('<label>', {
								class : 'control-label', 
								text  : data.atributo[j].familia.toUpperCase() + '/' + data.atributo[j].nombre.toUpperCase()
							})
							var form_group = $('<div>', {
								class : 'form-group'
							});
							var select = $('<select>', {
								name : 'atributo['+ data.atributo[j].familiaSlug + '][' + data.atributo[j].idAtributo + '][]', 
								class: 'form-control attr-select'
								// multiple : 'multiple'
							})
							for (var i = 0; i < data.atributo[j].valor.length; i++) {
								let option = $('<option>',  {
				                    value : data.atributo[j].valor[i], 
				                    text  : data.atributo[j].valor[i] 
				                });
				                select.append(option);
							}
							form_group.append(select);
							li.append(label);
							li.append(form_group);
							ul.append(li);
						}
					}
					$('.attr-select').select2(select2Params);
				}else{
					$('#attr-error').slideDown(()=>{
						$('#attr-wrapper').fadeOut(()=>{
							$('#attr-wrapper').fadeIn().html('');
						});
						$('#atributoFamilia').val(null).trigger('change')
						$('#attr-error').text('No hay aun atributos para esta familia').fadeOut(2000);
					})
				}
			})
			.error(err => {
				console.log(err);
			});
			$('#attr-wrapper').append(ul);
			
		}

		var getAtributesByFamily = (e) => { 
			getAtributesForAllFamily();
			var attr = $('#atributoFamilia').val();
			if (attr) {
				var ul = $('<ul>', {"class" : "family-list-ajax"});
				for (var i = 0; i < attr.length; i++) {
					$.get('<?php echo base_url() ?>/atributo/get_attr_by_family/' + attr[i])
					.done(data => {
						console.log(data);
						if (data.atributo[0]) {
							for (var j = 0; j < data.atributo.length; j++) {
								if (data.atributo[j].valor && data.atributo[j].valor!='') {
									var li = $('<li>');
									var label = $('<label>', {
										class : 'control-label', 
										text  : data.atributo[j].familia.toUpperCase() + '/' + data.atributo[j].nombre.toUpperCase()
									})
									var form_group = $('<div>', {
										class : 'form-group'
									});
									// var select = $('<select>', {
									// 	name : 'atributo['+ data.atributo[j].familiaSlug + '][' + data.atributo[j].idAtributo + '][]', 
									// 	class: 'form-control attr-select', 
									// 	multiple : 'multiple'
									// })
									var select = $('<select>', {
										name : 'atributo['+ data.atributo[j].familiaSlug + '][' + data.atributo[j].idAtributo + '][]', 
										class: 'form-control attr-select'
									})
									for (var i = 0; i < data.atributo[j].valor.length; i++) {
										let option = $('<option>',  {
						                    value : data.atributo[j].valor[i], 
						                    text  : data.atributo[j].valor[i] 
						                });
						                select.append(option);
									}
									form_group.append(select);
									li.append(label);
									li.append(form_group);
									ul.append(li);
								}
							}
							$('.attr-select').select2(select2Params);
						}else{
							$('#attr-error').slideDown(()=>{
								$('#attr-wrapper').fadeOut(()=>{
									$('#attr-wrapper').fadeIn().html('');
								});
								$('#atributoFamilia').val(null).trigger('change')
								$('#attr-error').text('No hay aun atributos para esta familia').fadeOut(2000);
							})
						}
					})
					.error(err => {
						console.log(err);
					});
				}
				$('#attr-wrapper').append(ul);
			}else{
				console.log('cleared');
			}
		}
		var promiseAttr = new Promise((resolve, reject) => {
			try{
				getAtributesByFamily();
				resolve(true);
			}catch(err){
				reject(err);
			}
		})

		// promiseAttr
		// 	.then(()=>{
		// 		var familiaSlug = $('#idFamilia option:selected').data('slug');
		// 		var atributos = <?php echo json_encode($atributos); ?>;
		// 		atributos.forEach((item, index) => {

		// 			var values = item.valores.replace(/['"]+/g, '');
		// 			values = values.replace(/['\[\]]+/g, '');
		// 			values = values.split(',');
		// 			for (var i = 0; i < values.length; i++) {
		// 				var input = $('<input>', {
		// 						type: "hidden", 
		// 						class: "form-control", 
		// 						value: values[i], 
		// 						name : 'atributo[' + familiaSlug + "][" + item.idAtributo + "][]"
		// 					})

		// 				var btn = '<button type="button" class="btn btn-danger btn-xs pull-right" onclick="borrar(this)"><i class="fa fa-trash"></i></button>';
		// 				// var label = $('<label>', {html :familiaSlug.toUpperCase() +'/'+item.nombre.toLowerCase() +'/'+values[i]})
		// 				var label = $('<label>', {html :item.nombre.toLowerCase() +': '+values[i]})
		// 				var li = $('<li>', {
		// 					class: 'list-group-item'
		// 				});
		// 				li.append(label);
		// 				li.append(input);
		// 				li.append(btn);

		// 				$('.attr-saved-list').append(li);
		// 			}
		// 		})
		// 	})
		// 	.catch((e) => {
		// 		console.log('err', e);
		// 	})

		$('#atributoFamilia').on("select2:select",  getAtributesByFamily);
		$('#atributoFamilia').on("select2:unselect",  getAtributesByFamily);

		$('#add-application').click(()=>{
			var moto      = $('#moto').val();
			var motoText  = $('#moto option:selected').text();
			var list_item = $('.list-app li.hidden').clone();
			console.log(list_item);
			list_item.removeClass('hidden');
			list_item.find('input.moto-input').val(moto);
			list_item.find('label').text(motoText);
			$('.list-app').append(list_item);

			$('#idMarca').val('').select2(select2Params);
			$('#modelo').val('').select2(select2Params);
			$('#year').val('').select2(select2Params);
		})
		
		$('#search-moto').click(() => {
			var marca  = $('#idMarca').val();
			var modelo = ($('#modelo').val() != 0) ? $('#modelo').val() : '';
			var year   = ($('#year').val() != 0 && $('#year').val()) ? $('#year').val() : '';

			var motoSelect = $('#moto');
			$.get( base_url + 'moto/get_moto_with_params/' + marca + '/' + modelo + '/' + year)
			 .done( data => {
			 	motoSelect.html('');
			 	$('#app-add').slideUp();
			 	var option = $('<option>',  {
					value : 0, 
					text  : 'Seleccione moto' 
				});
				motoSelect.append(option);
				if (data.motos.length > 0) {
					for (var i = 0; i < data.motos.length; i++) {
						var option = $('<option>',  {
							value : data['motos'][i].idMoto, 
							text  : data['motos'][i].nombre
						});
						motoSelect.append(option);
						document.getElementById('moto').removeAttribute('disabled');
						motoSelect.select2(select2Params);
					}
					$('#app-add').slideDown();
				}else{
					option = $('<option>',  {
						value : '0', 
						text  : 'No se encontraron coincidencias'
					});
					motoSelect.append(option);
					$('#moto-error').text('No hay coincidencias');
					$('#moto-error').slideDown(1000, ()=>{
						$('#moto-error').slideUp(2000);
					})
				}
			}).error( err => {
				let option = $('<option>',  {
					value : 0, 
					text  : 'Error al cargar ' + 'motos'
				});
				motoSelect.append(option);
				motoSelect.select2({placeholder: 'Error al cargar',allowClear: true});
			})
		})

		getAtributesInputsList();

		function getAtributesInputsList(){
			// var atributos = <?php echo json_encode($atributos); ?>;
			// alert(atributos);
			var url = "<?php echo base_url() ?>/atributo/get_attr_by_family/" + $('#idFamilia').val();
			
			$.ajax({
				url: url,
				type: 'POST',
				success: function(data){
					console.log(data);
					var ul = $('<ul>', {"class" : "family-list-ajax"});
					if (data.atributo[0]) {
							for (var j = 0; j < data.atributo.length; j++) {
								if (data.atributo[j].valor && data.atributo[j].valor!='') {
									var li = $('<li>');
									var label = $('<label>', {
										class : 'control-label', 
										text  : data.atributo[j].familia.toUpperCase() + '/' + data.atributo[j].nombre.toUpperCase()
									})
									var form_group = $('<div>', {
										class : 'form-group'
									});
									var select = $('<select>', {
										name : 'atributo['+ data.atributo[j].familiaSlug + '][' + data.atributo[j].idAtributo + '][]', 
										class: 'form-control attr-select'
									})
									for (var i = 0; i < data.atributo[j].valor.length; i++) {
										let option = $('<option>',  {
						                    value : data.atributo[j].valor[i], 
						                    text  : data.atributo[j].valor[i] 
						                });
						                select.append(option);
									}
									form_group.append(select);
									li.append(label);
									li.append(form_group);
									ul.append(li);

									$('#form-variations').append(li);
								}
							}
					}		

				}

			})





		}

		


	})
</script>