<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Listado de Atributos</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('atributo/add'); ?>" class="btn btn-success btn-sm">Nuevo Atributo</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
    						<th>IdAtributo</th>
    						<th>Familia</th>
                            <th>Nombre</th>
    						<th>Slug</th>
    						<th>Valor</th>
    						<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($atributo as $a){ ?>
                        <tr>
    						<td><?php echo $a['idAtributo']; ?></td>
    						<td><?php echo $a['familia']; ?></td>
                            <td><?php echo $a['nombre']; ?></td>
    						<td><?php echo $a['slug']; ?></td>
    						<td><?php echo $a['valor']; ?></td>
    						<td>
                                <a href="<?php echo site_url('atributo/edit/'.$a['idAtributo']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Editar</a> 
                                <a href="<?php echo site_url('atributo/remove/'.$a['idAtributo']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Borrar</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                                
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('script/dataTable') ?>
