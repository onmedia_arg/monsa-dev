<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Order Item Edit</h3>
            </div>
			<?php echo form_open('order_item/edit/'.$order_item['idOrderItem']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="order_item_name" class="control-label">Order Item Name</label>
						<div class="form-group">
							<input type="text" name="order_item_name" value="<?php echo ($this->input->post('order_item_name') ? $this->input->post('order_item_name') : $order_item['order_item_name']); ?>" class="form-control" id="order_item_name" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="order_item_type" class="control-label">Order Item Type</label>
						<div class="form-group">
							<input type="text" name="order_item_type" value="<?php echo ($this->input->post('order_item_type') ? $this->input->post('order_item_type') : $order_item['order_item_type']); ?>" class="form-control" id="order_item_type" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="idOrder" class="control-label">IdOrder</label>
						<div class="form-group">
							<input type="text" name="idOrder" value="<?php echo ($this->input->post('idOrder') ? $this->input->post('idOrder') : $order_item['idOrder']); ?>" class="form-control" id="idOrder" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>