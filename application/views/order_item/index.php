<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Order Item Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('order_item/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>IdOrderItem</th>
						<th>Order Item Name</th>
						<th>Order Item Type</th>
						<th>IdOrder</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($order_item as $o){ ?>
                    <tr>
						<td><?php echo $o['idOrderItem']; ?></td>
						<td><?php echo $o['order_item_name']; ?></td>
						<td><?php echo $o['order_item_type']; ?></td>
						<td><?php echo $o['idOrder']; ?></td>
						<td>
                            <a href="<?php echo site_url('order_item/edit/'.$o['idOrderItem']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('order_item/remove/'.$o['idOrderItem']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
