<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Username Or Email On Hold Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('username_or_email_on_hold/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Ai</th>
						<th>Username Or Email</th>
						<th>Time</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($username_or_email_on_hold as $u){ ?>
                    <tr>
						<td><?php echo $u['ai']; ?></td>
						<td><?php echo $u['username_or_email']; ?></td>
						<td><?php echo $u['time']; ?></td>
						<td>
                            <a href="<?php echo site_url('username_or_email_on_hold/edit/'.$u['ai']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('username_or_email_on_hold/remove/'.$u['ai']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
