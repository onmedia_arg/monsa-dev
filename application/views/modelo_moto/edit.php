<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Modelo Moto Edit</h3>
            </div>
			<?php echo form_open('modelo_moto/edit/'.$modelo_moto['idModeloMoto']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="idMarcaMoto" class="control-label">Marca Moto</label>
						<div class="form-group">
							<select name="idMarcaMoto" class="form-control">
								<option value="">select marca_moto</option>
								<?php 
								foreach($all_marca_moto as $marca_moto)
								{
									$selected = ($marca_moto['idMarcaMoto'] == $modelo_moto['idMarcaMoto']) ? ' selected="selected"' : "";

									echo '<option value="'.$marca_moto['idMarcaMoto'].'" '.$selected.'>'.$marca_moto['idMarcaMoto'].'</option>';
								} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="nombre" class="control-label">Nombre</label>
						<div class="form-group">
							<input type="text" name="nombre" value="<?php echo ($this->input->post('nombre') ? $this->input->post('nombre') : $modelo_moto['nombre']); ?>" class="form-control" id="nombre" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="descripcion" class="control-label">Descripcion</label>
						<div class="form-group">
							<input type="text" name="descripcion" value="<?php echo ($this->input->post('descripcion') ? $this->input->post('descripcion') : $modelo_moto['descripcion']); ?>" class="form-control" id="descripcion" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="year" class="control-label">Year</label>
						<div class="form-group">
							<input type="text" name="year" value="<?php echo ($this->input->post('year') ? $this->input->post('year') : $modelo_moto['year']); ?>" class="form-control" id="year" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>