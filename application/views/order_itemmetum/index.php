<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Order Itemmeta Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('order_itemmetum/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>IdMeta</th>
						<th>IdOrderItem</th>
						<th>MetaKey</th>
						<th>MetaValue</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($order_itemmeta as $o){ ?>
                    <tr>
						<td><?php echo $o['idMeta']; ?></td>
						<td><?php echo $o['idOrderItem']; ?></td>
						<td><?php echo $o['metaKey']; ?></td>
						<td><?php echo $o['metaValue']; ?></td>
						<td>
                            <a href="<?php echo site_url('order_itemmetum/edit/'.$o['idMeta']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('order_itemmetum/remove/'.$o['idMeta']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
