<style>
    .slider0 {
        width: 100%;
        height: auto;
    }
</style>
<img id="popup" src="">

<!-- start banner Area -->
<section class="">
    <!-- desktop slider     -->
    <div id="carouselDesktop" class="carousel slide d-none d-md-block" data-ride="carousel">
        <div class="carousel-inner">
            <?php
            $first = true; // Variable para controlar el primer elemento
            $desktop = array_filter($slider, function($item){
                return $item->location == 'D';
            });

            foreach ($desktop as $key => $value) {
                echo '<div class="carousel-item' . ($first ? ' active' : '') . '">
                    <img class="d-block w-100" src="' . $value->publicfullpath . '" alt="Slide">
                    </div>';
                $first = false; // Cambiar a false después del primer elemento
            }
            ?>
        </div>
        <a class="carousel-control-prev" href="#carouselDesktop" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselDesktop" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <!-- mobile slider     -->
    <div id="carouselMobile" class="carousel slide d-md-none" data-ride="carousel">
        <div class="carousel-inner">
        <?php
            $first = true; // Variable para controlar el primer elemento
            $mobile = array_filter($slider, function($item){
                return $item->location == 'M';
            });

            foreach ($mobile as $key => $value) {
                echo '<div class="carousel-item' . ($first ? ' active' : '') . '">
                    <img class="d-block w-100" src="' . $value->publicfullpath . '" alt="Slide">
                    </div>';
                $first = false; // Cambiar a false después del primer elemento
            }
            ?>
        </div>
        <a class="carousel-control-prev" href="#carouselMobile" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselMobile" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>


    <!-- <div class="row align-items-center justify-content-start banner-fix"> -->

    <!-- </div> -->
</section>
<section>
    <div class="container-fluid">
        <div class="row"></div>
    </div>
</section>
<!-- End banner Area -->