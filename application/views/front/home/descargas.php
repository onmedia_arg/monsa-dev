<style type="text/css">
    .features-inner div {
        background-size: contain !important;
        background-repeat: no-repeat !important;
        min-height: 90px;
    }

    .file-card {
        display: contents;
    }

    a.file-card {
        transition: none;
    }

    .file-card:hover {
        color: #ffba00;
    }

    .msj-download {
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    .msj {
        width: 80%;
    }

    .single-file {
        /* text-align: center;
    font-size: 26px;
    font-family: "Exo 2";
    line-height: 36px;
    margin: auto; */
        padding: 0px;
    }

    .file-date-updated {
        text-align: center;
        color: #666;

    }

    .file-description {
        font-size: 18px;
        color: #333;
        margin-left: 30%;
    }

    .col-downloads {
        display: flex;
        flex-direction: column;
        align-items: center;
    }

    .download-btn {
        margin-top: 10px;
        background-color: #121212;
        border-radius: 5px;
        text-align: center;
        color: #fff;
        font-size: 1.2rem;
        font-weight: 600;
        width: 300px;
        display: flex;
        padding: 20px;
    }

    .download-btn:hover {
        cursor: pointer;
        background-color: #000;
        color: #A1F500;
    }

    .download-btn-description {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        width: 100%;
    }

    .container-msj {
        margin-bottom: 50px;

    }

    .h-500 {
        height: 400px;
    }

    .descargas-col-2 {
        margin: 40px;
    }
    .download-link {
        margin: 0px 30px;
    }

</style>

<!-- start features Area -->
<section class="features-area section_gap">
    <div class="container features-inner container-msj">
        <div class="row">
            <div class="col-12 msj-download">
                <div class="msj"></div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="d-flex h-500">
            <div style="background-image: url('<?php echo site_url('resources/img/moto.png'); ?>'); 
                        background-size: cover; 
                        background-repeat: no-repeat;
                        width: 250px;
                        border-radius: 20px;
                        overflow: hidden;
                        ">
            </div>
            <div class="d-flex flex-column descargas-col-2">
                <div class="section-title d-flex flex-column">
                    <h1 style="font-size: 2.2em; width:70%; font-family: 'Neue'; font-weight: 800;">Descargas</h1>
                    <p style="font-size: 1.5em; font-family: 'Neue';font-weight: normal;">Descarga nuestras listas de precios en los formatos que necesites.</p>
                </div>

                <div class="d-flex">
                    <a class="download-link" id="link-pdf" href="" target="_blank">
                        <div class="download-btn">
                            <div class="download-btn-description">
                                <span> Versión PDF</span>
                                <h6 class="file-date-updated"></h6>
                            </div>
                        </div>
                    </a>
                    <a class="download-link" id="link-csv" href="" target="_blank">
                        <div class="download-btn">
                            <div class="download-btn-description">
                                <span> Lista Código/Precio</span>
                                <h6 class="file-date-updated"></h6>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- end features Area -->