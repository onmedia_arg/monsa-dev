<style>
    /**
 * Owl Carousel v2.3.4
 * Copyright 2013-2018 David Deutsch
 * Licensed under: SEE LICENSE IN https://github.com/OwlCarousel2/OwlCarousel2/blob/master/LICENSE
 */
    .owl-theme .owl-dots,
    .owl-theme .owl-nav {
        text-align: center;
        -webkit-tap-highlight-color: transparent
    }

    .owl-theme .owl-nav {
        margin-top: 40px
    }

    .owl-theme .owl-nav [class*=owl-] {
        color: #FFF;
        font-size: 14px;
        margin: 5px;
        padding: 4px 7px;
        background: #D6D6D6;
        display: inline-block;
        cursor: pointer;
        border-radius: 3px
    }

    .owl-theme .owl-nav [class*=owl-]:hover {
        background: #869791;
        color: #FFF;
        text-decoration: none
    }

    .owl-theme .owl-nav .disabled {
        opacity: .5;
        cursor: default
    }

    .owl-theme .owl-nav.disabled+.owl-dots {
        margin-top: 10px
    }

    .owl-theme .owl-dots .owl-dot {
        display: inline-block;
        zoom: 1
    }

    .owl-theme .owl-dots .owl-dot span {
        width: 10px;
        height: 10px;
        margin: 5px 7px;
        background: #D6D6D6;
        display: block;
        -webkit-backface-visibility: visible;
        transition: opacity .2s ease;
        border-radius: 30px
    }

    .owl-theme .owl-dots .owl-dot.active span,
    .owl-theme .owl-dots .owl-dot:hover span {
        background: #869791
    }
    .owl-dots{
        display: none;
    }

    #marcas-carousel{
        margin: 40px 0px 50px 0px;
    }
</style>

<!-- Start brand Area -->
<section class="brand-area section_gap">
    <div class="container">
        <div class="row ">
            <div class="col-lg-6">
                <div class="section-title">
                    <h1>Nuestras marcas</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="marcas-carousel" class="owl-carousel owl-theme">
                <?php
                shuffle($brands); 
                foreach ($brands as $key => $value) {
                    echo '<div class="item">
                                <img src="' . $value->publicfullpath . '">
                            </div>';
                }
                ?>

            </div>
        </div>
        <div class="row">
            <div id="marcas-carousel-ltr" class="owl-carousel owl-theme">
                <?php

                shuffle($brands); 

                foreach ($brands as $key => $value) {
                    echo '<div class="item">
                                <img src="' . $value->publicfullpath . '">
                            </div>';
                }
                ?>

            </div>
        </div>
    </div>
</section>
<!-- End brand Area -->