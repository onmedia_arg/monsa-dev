<style>
.home-notas {
    padding: 60px 0px;
    background-color: #f5f5f5;
}

.nota-title{
    font-size:1.2em;
    font-weight: bold;
    color: #333;
    margin-bottom:10px;
}

.nota-image{
    background-position: left;
    background-size: cover;
    width: 100%;
    height: 200px;
    margin-bottom:20px;
}

.nota-img img {
    width: 100%;
    height: 160px;
    margin-bottom: 14px;
}

.nota-extract{
    color: #666;
}
.nota-extract a{
    color: #0471e3;
}

.nota-date {
    position: absolute;
    left: 20px;
    top: 10px;
    background-color: #ffc006;
    padding: 2px 9px;
    color: #fff;
    font-weight: bold;
}
</style>

<div class="home-notas">
    <div class="container">
        <div class="row home-notas-wrapper">
            <!-- <div class="col-4 home-notas-item">
                <div class="nota-img"><span class="nota-date">05 OCT</span><img src="https://lorempixel.com/300/180/transport/" alt=""></div>
                <div class="nota-title">Lorem ipsum dolor sit</div>
                <div class="nota-extract">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat laboriosamc onsectetur elit ....<a href="">Leer más</a></div>
            </div>
            <div class="col-4 home-notas-item">
                <div class="nota-img"><span class="nota-date">20 SEPT</span><img src="https://lorempixel.com/300/180/transport/" alt=""></div>
                <div class="nota-title">Lorem ipsum dolor sit</div>
                <div class="nota-extract">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat laboriosamc onsectetur elit ....<a href="">Leer más</a></div>
            </div>
            <div class="col-4 home-notas-item">
                <div class="nota-img"><span class="nota-date">11 JUN</span><img src="https://lorempixel.com/300/180/transport/" alt=""></div>
                <div class="nota-title">Lorem ipsum dolor sit</div>
                <div class="nota-extract">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat laboriosamc onsectetur elit ....<a href="">Leer más</a></div>
            </div> -->

        </div>
    </div>



</div>