<!-- start product Area -->
<section class=" section_gap">

    <!-- single product slide -->
    <div class="single-product-slider">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title">
                        <h1>DESTACADOS DEL MES</h1>
                       </div>
                </div>
            </div>
            <section class="lattest-product-area pb-40 category-list">
                <div class="row" id="productos-home"></div>                    
            </section>

        </div>
    </div>
</section>
<!-- end product Area -->

<style>
    .carrousel-item-title{
        text-wrap: initial;
    }


    .home-product-item{
        display:flex;
        flex-direction: column;
        justify-content: space-between
    }

    .home-product-box{
        /* background-color: #f7f7f7; */
        display:flex;
        flex-direction: column;        
        white-space: nowrap;
    overflow: hidden;
        flex-grow: 1;
    }
    .home-product-box-img{
      height: 180px;

      background-size: cover !important;
      background-repeat: no-repeat !important;
      background-position: center !important;
      margin-bottom: 10px;
    }
    .home-product-box-details {
        flex-grow: 1;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
    }
    
    .home-product-price {
        margin-top: 10px;
        text-align: center;
    }    

    .home-product-add-bag{

    }
    .home-product-box-btn {
        width: 100%;
        background-color: #121212;
        border: none;
        color: #fff;
        font-weight: bold;
        border-radius: 5px;
        /* align-self: end !important; */
    }
    .home-product-box-btn i{
        margin-right: 3px;
    }
    .home-product-box-btn:hover{
        background-color: #000;
        color: #A1F500;
    }

    /* .home-product-promo {
        background-color: #e32026;
        color: #fff;
        padding: 2px 12px;
        font-weight: 800;
        letter-spacing: 0.5px;
    }     */

    .home-product-promo {
        width: 0;
        height: 0;
        border-right: 80px solid #fec101;
        border-bottom: 80px solid transparent;
        z-index: 12;
        position: relative;
        top: -2px;
    }

    .promo-text {
        font-size: 13px;
        font-weight: bold;
        line-height: 13px;
        position: absolute;
        z-index: 14;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(49deg);
        top: 28px;
        left: 1px;
        width: 99px;
        text-align: center;
        color: #333;
    } 

    .discount-label {
    position: absolute;
    top: 8px;
    left: 10px;
    background-color: rgba(0, 0, 0, 0.8);
    color: #fff;
    padding: 5px 10px;
    font-size: 14px;
    font-weight: bold;
    border-radius: 3px;
    z-index: 1;
}

</style>


<script defer>
$(document).ready(function(){
    render_productos_home()
})

function render_productos_home(){

    var url = window.base + 'home/getProductosHome';

    $.ajax({
        url     : url,
        method  : 'POST',
        success : function(data){
                data = JSON.parse(data);
                var output = "";
                for( var i = 0; i < data.length ; i++ ) {
                    

                    output += `<div id="product-${data[i]['idProducto']}" class="home-product-item col-xl-2 col-lg-4 col-md-6">
                                    <div class="home-product-box">
                                        <a href="${window.base}frontController/detalle/${data[i]['slug']}">`
                    if(data[i]['is_promo_active'] != null && data[i]['is_promo_active'] != 0){
                        output += `<span class="discount-label">${data[i]['tag']}</span>`
                    }
                    output += `<div class="home-product-box-img" style="background:url('${data[i]['imagen']}')">`
                                                // <span class="home-product-promo float-right "><span class="promo-text">INGRESO<span></span>
                    output +=               `</div>
                                        </a>
                                        <div class="home-product-box-details">
                                            <a class="carrousel-item-title" href="${window.base}frontController/detalle/${data[i]['slug']}">
                                             ${data[i]["familia"]} ${data[i]["marca"]}<br>
                                             ${data[i]["modelo"]}
                                             </a>`

                if(data[i]['is_promo_active'] == null || data[i]['is_promo_active'] == 0){
                    output += `<div class="price home-product-price"><p class="p-price">$ ${formatCurrency(data[i]['precio'])}</p></div>`
                }else{
                    output += `<div class="price home-product-price d-flex flex-column">
                                    <span class="" 
                                    style="text-decoration: line-through; 
                                            margin-right: 5px;
                                            font-size:1.2em">$ ${formatCurrency(data[i]['precio'])}</span>
                                    <span class="p-price"
                                    style="font-size: 1.8em; margin-bottom: 10px">$ ${formatCurrency(data[i]['price_promo'])}</span>
                                </div>`
                }
                                            
                                            
                                            
                    output +=              `<div class="home-product-add-bag">
                                                <input type="number" name="quantity" class="form-control quantity d-none" value="${data[i]["cantidad_minima"]}" id="${data[i]["idProducto"]}"/>
                                                <button type="button" id="add_cart" name="add_cart" class="home-product-box-btn"
                                                    data-productname = "${data[i]["nombre"]}"
                                                    data-price       = "${data[i]["precio"]}"
                                                    data-productid   = "${data[i]["idProducto"]}"
                                                    data-sku         = "${data[i]["sku"]}"
                                                    data-price_promo = "${data[i]["price_promo"]}"
                                                    data-is_promo_active = "${data[i]["is_promo_active"]}"
                                                    data-cantidadminima = "${data[i]["cantidad_minima"]}"
                                                    >
                                                    </i>AGREGAR</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>`


                    // output = render_item(output, data[i]);
                }
                $('#productos-home').html(output);
        }
    })
}

function formatCurrency(amount) {
    // Formatea el número a una cadena con el formato "$ XX.XXX,XX"
    return parseFloat(amount).toLocaleString('es-ES', {
        style: 'currency',
        currency: 'ARS',
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
        useGrouping: true
    }).replace('ARS', ''); 
}

</script>