            <!-- start features Area -->
            <section class="features-area section_gap">
                <div class="container">
                    <div class="row features-inner">
                        <!-- single features -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="single-features">
                                <div class="f-icon">
                                    <i class="fas fa-truck fa-2x"></i>
                                </div>
                                <h6>Envios a domicilio</h6>
                                
                            </div>
                        </div>
                        <!-- single features -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="single-features">
                                <div class="f-icon">
                                    <i class="fas fa-undo fa-2x"></i>
                                </div>
                                <h6>Políticas de reembolso</h6>
                                
                            </div>
                        </div>
                        <!-- single features -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="single-features">
                                <div class="f-icon">
                                    <i class="fas fa-headset fa-2x"></i>
                                </div>
                                <h6>Atención al cliente</h6>
                                
                            </div>
                        </div>
                        <!-- single features -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="single-features">
                                <div class="f-icon">
                                    <i class="fas fa-database fa-2x"></i>
                                </div>
                                <h6>Formas de pago  online</h6>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end features Area -->

<style type="text/css">

.features-inner div{
    background-size: contain !important;
    background-repeat: no-repeat !important;
    min-height: 90px;
}

.file-card {
    display: contents;
}

a.file-card {
    transition: none;
}

.file-card:hover {
    color: #ffba00;
}

.msj-download{
    display: flex;
    flex-direction:column;
    align-items: center;
}
.msj{
    width: 80%;
}

.single-file{
    /* text-align: center;
    font-size: 26px;
    font-family: "Exo 2";
    line-height: 36px;
    margin: auto; */
    padding: 0px;
}

.file-date-updated{
    text-align: center;
    color: #666;

}

.file-description {
    font-size: 18px;
    color: #333;
    margin-left: 30%;
}

.col-downloads {
    display: flex;
    flex-direction: column;
    align-items: center;
}

.download-btn {
    margin-top: 10px;
    background-color: #121212;
    border-radius: 5px;
    text-align: center;
    color: #fff;
    font-size: 1.2rem;
    font-weight: 600;
    width: 70%;
    display: flex;
}

.download-btn:hover {
    cursor: pointer;
    background-color: #000;
    color: #A1F500;    
}

.download-btn-description {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;    
}

</style>

            <!-- start features Area -->
            <section class="features-area section_gap">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="section-title">
                                <h1 style="width:70%;">Descargas</h1>
                            </div>
                        </div>
                    </div> 
                </div>                
                <div class="container features-inner">
                    <div class="row">
                        <div class="col-8 msj-download">
                            <div class="msj"></div>
                        </div>
                        <div class="col-4 col-downloads" >
                            <a class="download-link" id="link-xls" href="">
                                <div class="download-btn">
                                    <div class="download-btn-description">
                                        <span> Versión XLS</span>
                                        <h6 class="file-date-updated"></h6>  
                                    </div>
                                </div>
                            </a>                                
                            <a class="download-link" id="link-pdf" href="">
                                <div class="download-btn">
                                    <div class="download-btn-description">
                                        <span> Versión PDF</span>
                                        <h6 class="file-date-updated"></h6>  
                                    </div>
                                </div>
                            </a>
                            <a class="download-link" id="link-csv" href="">
                                <div class="download-btn">
                                    <div class="download-btn-description">
                                        <span> Lista Código/Precio</span>
                                        <h6 class="file-date-updated"></h6>  
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
<!-- 
                <div class="container">
                    <div id="home-files" class="row features-inner"> -->
                        <!-- single features -->
                        <!-- <a class="file-card" target="_blank" href="https://monsa-srl.com.ar/wp-content/uploads/descargas/l_general.xls">
                        <div class="col-lg-6 col-md-6 col-sm-6" style="background:url('<?php echo base_url()?>/resources/img/xls.png')">
                            <div class="single-file">
                                <p>LISTA PRECIOS<br>ACTUALIZADA!</p>
                            </div>
                        </div></a> -->
                        <!-- single features -->
                        <!-- <a class="file-card" target="_blank" href="https://monsa-srl.com.ar/wp-content/uploads/descargas/c_general.pdf">
                        <div class="col-lg-6 col-md-6 col-sm-6" style="background:url('<?php echo base_url()?>/resources/img/pdf.png')">
                            <div class="single-file">
                                <p>LISTA PRECIOS<br>ACTUALIZADA!<br>PDF</p>
                            </div>
                        </div></a> -->
                    <!-- </div>
                </div> -->
            </section>
            <!-- end features Area -->


