<!-- Detalle Producto Variable -->
<div class="col-lg-8 offset-lg-1">
    <div class="s_product_text">
        <h3><?php echo $product['nombre']; ?></h3>
        <ul class="list">
            <li><span>Categoría:</span> <?php if (count($product['cats'])) { ?>
                    <?php
                    printf(
                        '<a href="%s" class="btn btn-info btn-xs">%s</a>',
                        base_url('categoria/buscar/' . $product['cats'][0]['familiaSlug']),
                        strtolower($product['cats'][0]['familia'])
                    );
                    foreach ($product['cats'] as $key => $cat) {
                        printf('
                                                <a href="%s" class="btn btn-info btn-xs">%s</a>
                                            ',
                            base_url('categoria/buscar/' . $product['cats'][0]['familiaSlug'] . '/' . $cat['slug']),
                            strtolower($cat['categoria'])
                        );
                    }
            }
            ?>
            </li>
            <!-- <li><a href="#"><span>Modelo:</span> <?php echo $modelo = $product['modelo']; ?></a></li> -->
            <?php
            if ($product['atributos']) {
                foreach ($product['atributos'] as $key => $attr) {
                    if (!$attr['is_variacion']) {
                        ?>
                        <li><a href="#"><span><?php echo $attr['nombre']; ?>: </span> <?php echo $attr['valores']; ?></a></li>
                        <?php
                    }
                }
            }
            ?>
        </ul>
        <hr>
        <!--                         <p><?php echo $product['descripcion']; ?></p> -->
    </div>
    <h2 class="pb-3">Seleccione su producto:</h2>
    <div class="row">
        <input name="idProducto" value="<?php echo $product['idProducto'] ?>" hidden>
        <?php
           if (isset($product["test_atributos"]) && is_array($product["test_atributos"]) && !empty($product["test_atributos"])) {
            $current_attr = $product["test_atributos"][0]["idAtributo"];

            $row = current($product["test_atributos"]);
            if ($row !== false) {
                echo ("<ul class='attr-list'>");
                while (!empty($row)) {

                    $current_attr = $row["idAtributo"];
                    echo ("<li class='attr-line'><div class='row'><div class='col-md-3'><span class='attr-nombre'>" . $row["nombre"] . "</span></div><div class='col-md-9'>");
                   
                    while ($row !== false && $row['idAtributo'] == $current_attr) {

                        echo ("<span data-idattr='" . $row["idAtributo"] . "' data-slug='" . $row["slug"] . "'class='attr-value'>" . $row["valores"] . "</span>");

                        $row = next($product["test_atributos"]);
                    }
                    echo ("</div></li>");
                }
                echo ("</ul>");
            }else{
                echo "<p>No hay atributos disponibles para este producto.</p>";
            }    
        } else {
            echo "<p>No hay atributos disponibles para este producto.</p>";
        }
        ?>
    </div>
</div>
<!-- Fin Detalle Producto Variable -->