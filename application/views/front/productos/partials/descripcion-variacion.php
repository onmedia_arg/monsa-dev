    <!--================Product Simple Description Area =================-->
    <section class="product_description_area">
        <div class="container">
            <div class="row">
                <?php  
                    if($product['atributos']){
                ?>
                    <div class="table-responsive col-md-6">
                        <h2 class="pb-3">Atributos</h2>
                        <table class="table">
                            <tbody>
                        <?php
                            
                            foreach ($product['atributos'] as $key => $attr) {
                                    printf('<tr class="list-group-item">
                                                <td><h5>%s</h5></td>
                                                <td>%s</td>
                                            </tr>',
                                            $attr['nombre'], 
                                            $attr['valores']
                                        );                                
                            }
                        ?>    </tbody></table>                
                        
                    </div>
                    <?php } if(count($product['aplicacion']['motos'] )) { ?>
                        <div class="table-responsive col-md-6">
                            <h2 class="pb-3">Aplicaciones</h2>
                            <table class="table">
                                <tbody>
                                    <?php
                                        foreach ($product['aplicacion']['motos'] as $key => $moto) {
                                            printf('<tr>
                                                        <td>%s</td>
                                                    </tr>',
                                                    $moto['name']
                                                );
                                        } 
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
            </div>
        </div>
    </section>
    
    <div class="container"><hr></div>
    
    <!--================End Product Description Area =================-->