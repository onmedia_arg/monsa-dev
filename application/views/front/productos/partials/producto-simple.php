                <!-- Detalle Producto Simple -->                
                <!-- offset-lg-1 -->
                <div class="col-lg-8 ">

                    <div class="s_product_text mt-10">
                        <?php if($product['is_promo_active'] == 1 && $product['tag'] != ''){ ?>
                            <span class="discount-label"><?php echo $product['tag']; ?></span>
                        <?php } ?>
                        <h3><?php echo $product['nombre']; ?></h3>

                        <dl class="row mb-0">
                            <dt class="col-sm-3">SKU:</dt>
                            <dd class="col-sm-9"><?php echo $product['sku']; ?></dd>

                            <dt class="col-sm-3">Precio:</dt>
                                
                                <?php if($product['is_promo_active'] == null || $product['is_promo_active'] == 0){ ?>
                                    <dd class="col-sm-9">$ <?php echo $product['precio']; ?></dd>
                                <?php }else{ ?>
                                    <dd class="col-sm-9">
                                        <span class="p-price-promo" style="font-size: 18px;">$  <?php echo number_format($product['precio'], 0, ",", "."); ?></span>
                                        <span class="fw-bold" style="font-size: 1.2em; font-weight: bold;">$  <?php echo number_format($product['price_promo'], 0, ",", "."); ?></span>
                                    </dd>
                                <?php } ?>
                                

                                <dt class="col-sm-3">Categoría:</dt>
                                <dd class="col-sm-9">No Aplica </dd>

                                <dt class="col-sm-3">Modelo:</dt>
                                <dd class="col-sm-9"><?php echo $modelo = $product['modelo']; ?></dd>

                                <dt class="col-sm-3">Stock:</dt>
                                <dd class="col-sm-9"><?php echo $product['stock']; ?></dd>

                                <dt class="col-sm-3">Descripción:</dt>
                                <dd class="col-sm-9"><?php echo $product['descripcion']; ?></dd>
                                
                                <?php 
                                    if($product['atributos']){
                                        foreach ($product['atributos'] as $key => $attr) {
                                                echo ( '<dt class="col-sm-3">' . $attr['nombre']  . ':</dt>' .
                                                        '<dd class="col-sm-9">' . $attr['valores'] . '</dd>' );                                                                        
                                            }
                                    }
                                ?>

                            <dt class="dt-separador"></dt>
                            <hr>
                        
                            <dt class="col-sm-3">Cantidad:</dt>
                            <dd class="col-sm-4">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-add-cart" type="button"
                                                onclick="decreaseQuantity('<?php echo $product['idProducto'] ?>')"
                                        ><i class="fas fa-minus"></i></button>
                                    </div>
                                    <!-- <input type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1"> -->
                                    <input type="number" name="qty" maxlength="12" 
                                        min="<?php echo $product['cantidad_minima'] ?>" 
                                        value="<?php echo $product['cantidad_minima'] ?>" 
                                        step="<?php echo $product['rango'] ?>"  
                                        title="Quantity:" 
                                        class="form-control qty" 
                                        id="<?php echo $product['idProducto'] ?>" 
                                        onkeydown="return false;">
                                    <div class="input-group-append">
                                        <button class="btn btn-add-cart" type="button"
                                                onclick="increaseQuantity('<?php echo $product['idProducto'] ?>')"
                                        ><i class="fas fa-plus"></i></button>
                                    </div>
                                </div>                                
                                <!-- <input type="number" name="qty" maxlength="12" 
                                       min="<?php echo $product['cantidad_minima'] ?>" 
                                       value="<?php echo $product['cantidad_minima'] ?>" 
                                       step="<?php echo $product['rango'] ?>"  
                                       title="Quantity:" 
                                       class="form-control qty" 
                                       id="<?php echo $product['idProducto'] ?>" 
                                       onkeydown="return false;"> -->
                            </dd>
                            <dd class="col-sm-4">
                                <?php 
                                    $attrsArray = array_map(function($value){
                                        return $value["valores"];
                                    }, $product["atributos"]);

                                    $attrString = '';
                                    if (isset($attrsArray)){
                                        $separador = '';
                                        for ($i=0; $i < count($attrsArray) ; $i++){
                                            $separador = ' - ';
                                            if (($i+1) == count($attrsArray)){
                                                $separador = '';
                                            }
                                            $attrString .= $attrsArray[$i] . $separador ;
                                        }
                                    }                            
                                ?>
                                <button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart" 
                                        data-productname = "<?php echo $product['nombre'] ?>"
                                        data-price       = "<?php echo $product['precio'] ?>"
                                        data-price_promo = "<?php echo $product['price_promo'] ?>"
                                        data-is_promo_active = "<?php echo $product['is_promo_active'] ?>"
                                        data-productid   = "<?php echo $product['idProducto'] ?>"
                                        data-sku = "<?php echo $product['sku'] ?>"
                                        data-cantidadminima = "<?php echo $product['cantidad_minima'] ?>"
                                        data-attributes = "<?php echo $attrString ?>"
                                        > Agregar al Pedido</button>                                 





                            </dd>                            

                        </dl>   

                                    
                                    




                        <!-- <li><span></span> <?php //if(count($product['cats'])){ ?>
                                <?php
                                    // printf('
                                    //             <a href="%s" class="btn btn-info btn-xs">%s</a>
                                    //          ',
                                    //         base_url('categoria/buscar/' . $product['cats'][0]['familiaSlug']), 
                                    //         strtolower($product['cats'][0]['familia'])
                                    //     );
                                //     foreach ($product['cats'] as $key => $cat) {
                                //         printf('
                                //                 <a href="%s" class="btn btn-info btn-xs">%s</a>
                                //             ',
                                //             base_url('categoria/buscar/'.$product['cats'][0]['familiaSlug'].'/'.$cat['slug']), 
                                //             strtolower($cat['categoria'])
                                //         );
                                //     }
                                // }
                                ?>
                            </li> -->

                        <!-- <div class="product_count">
                            <label for="qty">Cantidad:</label>
                            <input type="number" name="qty" maxlength="12" value="1" title="Quantity:" class="input-text qty" id="<?php echo $product['idProducto'] ?>">
                        </div> -->
                        <div class="card_area d-flex align-items-center">

                        </div>
                    </div>
                </div>
                <!-- Fin Detalle Producto Simple -->