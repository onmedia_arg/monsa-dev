                <!-- Detalle Producto Simple -->                
                <div class="col-lg-8 offset-lg-1">
                    <div class="s_product_text">
                        <h3><?php echo $product['nombre']; ?></h3>
                        <h2>$<?php echo $product['precio']; ?></h2>
                        <ul class="list">
                            <li><span>Categoría:</span> <?php if(count($product['cats'])){ ?>
                                <?php
                                    printf('
                                                <a href="%s" class="btn btn-info btn-xs">%s</a>
                                             ',
                                            base_url('categoria/buscar/' . $product['cats'][0]['familiaSlug']), 
                                            strtolower($product['cats'][0]['familia'])
                                        );
                                    foreach ($product['cats'] as $key => $cat) {
                                        printf('
                                                <a href="%s" class="btn btn-info btn-xs">%s</a>
                                            ',
                                            base_url('categoria/buscar/'.$product['cats'][0]['familiaSlug'].'/'.$cat['slug']), 
                                            strtolower($cat['categoria'])
                                        );
                                    }
                                }
                                ?>
                            </li>
                            <li><a href="#"><span>Modelo:</span> <?php echo $modelo = $product['modelo']; ?></a></li>
                            <li><a href="#"><span>Stock:</span> <?php echo $product['stock']; ?></a></li>
                        </ul>
                        <p><?php echo $product['descripcion']; ?></p>
                        <div class="product_count">
                            <label for="qty">Cantidad:</label>
                            <input type="number" name="qty" maxlength="12" value="1" title="Quantity:" class="input-text qty" id="<?php echo $product['idProducto'] ?>">
                        </div>
                        <div class="card_area d-flex align-items-center">
                            

                        <?php 

                            $attrsArray = array_map(function($value){
                                return $value["valores"];
                            }, $product["atributos"]);

                            $attrString = '';
                            if (isset($attrsArray))
                            {
                                $separador = '';
                                for ($i=0; $i < count($attrsArray) ; $i++)
                                {
                                    $separador = ' - ';
                                    if (($i+1) == count($attrsArray)) 
                                    {
                                        $separador = '';
                                    }
                                    $attrString .= $attrsArray[$i] . $separador ;
                                }
                            }                            
                            ?>

                            <button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart" 
                                    data-productname = "<?php echo $product['nombre'] ?>"
                                    data-price       = "<?php echo $product['precio'] ?>"
                                    data-sku = "<?php echo $product['sku'] ?>"
                                    data-attributes = "<?php echo $attrString ?>"
                                    data-productid   = "<?php echo $product['idProducto'] ?>"> Agregar al Pedido</button>
                        </div>
                    </div>
                </div>
                <!-- Fin Detalle Producto Simple -->