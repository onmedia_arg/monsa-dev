<?php 

    if($product['dimensiones']!=""){
        $dimen  = json_decode($product['dimensiones']);
        $alto   = $dimen->alto;
        $largo  = $dimen->largo;
        $ancho  = $dimen->ancho;
    }
    else{
        $alto   = 0;
        $largo  = 0;
        $ancho  = 0;
    }
    // var_dump($product['imagen']);

    $product['imagen'] = build_img_url_new($product['imagen'], 5);
    $images = $product['imagen'];

?>
    <!--================Single Product Area =================-->
    <div class="product_image_area">
        <div class="container">
            <div class="row s_product_inner">
                <div class="col-lg-4">
                    <?php 
                    if (isset($images)&& !empty($images)) {
                       ?>
                        <ul class="product-slider">
                            <div class="product-image-carousel owl-carousel">
                                <!-- Imágenes del producto -->
                                <?php
                                    foreach ($images as $image) {
                                        echo '<div class="item"><a href="' . $image . '" data-fancybox="gallery"><img src="' . $image . '" alt="Imagen 1"></a></div>';
                                    }
                                ?>

                            </div> 
                       <?php
                    } 
                        else
                            echo '<img src="'.site_url('').'resources/img/default.jpeg" alt="img" class="img-fluid" style="width:100%">'; 
                    ?>
                     
                </div>
             

            <?php 

                // Selecciona la plantilla segun el tipo de producto
                switch ( $product['idTipo'] ) {
                    // Simple
                    case '0':
                    case 0:
                        $this->load->view('front/productos/partials/producto-simple');
                        break;
                    
                    // Variable
                    case '1':
                    case 1: 
                        $this->load->view('front/productos/partials/producto-variable');
                        break;

                    // Variacion
                    case '2':
                    case 2:
                        $this->load->view('front/productos/partials/producto-simple');
                        break;

                    default:
                        show_error( 'No hay un tipo de producto seleccionado', 404 );
                        break;
                }

            ?>   

            </div>
        </div>
    </div>
    <!--================ Description =================--> 
    <?php 
        if($product['idTipo'] == 1){
            $this->load->view('front/productos/partials/descripcion-variable');
        }
    ?>  

<script>
   $(document).ready(function () {
      $(".product-image-carousel").owlCarousel({
         items: 1,
         loop: true,
         nav: true,
         dots: true,
         navText: ["<i class='product-single-carrousel-arrow fa fa-chevron-left'></i>", 
                   "<i class='product-single-carrousel-arrow fa fa-chevron-right'></i>"],
         // Otras opciones según sea necesario
      });
   });
</script>


