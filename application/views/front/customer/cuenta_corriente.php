
<div class="container section_gap">
	<div class="row">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Cuenta Corriente</h1>
				</div>
				<div class="col-sm-6 detail-cuenta-corriente d-none">
					<h2 class="float-sm-right">Saldo: <span id="saldo">$</span></h2>
				</div>
			</div>
		</div>	
		
		<div class="col-md-12 mt-3 detail-cuenta-corriente d-none">
			<h2>Ultimos Movimientos<span id="nro-pedido"></span></h2>
			<table class="table" id="details_cuenta">
				<thead>
					<th class="text-center">FECHA</th>
					<th class="text-center">TIPO</th>
					<th class="text-center">NRO DOCUMENTO</th>
					<th class="text-center">ESTADO</th>
					<th class="text-center">REFERENCIA</th>
					<th class="text-right">IMPORTE</th>
				</thead>
				<tbody id="detail_cuenta">
					
				</tbody>		
			</table>
		</div>	
	</div>	
	<div class="fa-3x text-center" id="spinner">
        <i class="fas fa-spinner fa-spin" style="color:#ffc006"></i>
	</div> 
	<div class="fa-3x text-center d-none" id="cliente_not_found">
        <h3 class="text-danger">Cliente no identificado, por favor comunicarse con su Oficial de Cuenta.</h3>
	</div> 
</div>			