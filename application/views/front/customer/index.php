<?php 
    $userstring = (isset($user['email'])) ? $user['email'] : $this->session->userdata('email');
    $role       = (isset($user['role'])) ? $user['role'] : '';

    $this->load->view('front/layouts/header', $nav);
    $this->load->view('front/layouts/seccion',$title);
    $this->load->view('front/customer/menu');
    $this->load->view('front/customer/main');
    $this->load->view('front/layouts/footer');

?>