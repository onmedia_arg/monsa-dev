<div class="container section_gap">
	<div class="row">
		<div class="col-md-3">
			<div class="head">PEDIDOS</div>
			<div class="box-shadow">
				<table class="table" id="orders_list">
					<thead>
						<th class="w-20 text-center">Nº</th>
						<th class="text-center">CREADO</th>
						<th class="text-center">ESTADO</th>
					</thead>		
				</table>				
			</div>

		</div>

		<div class="col-md-9">
			<h2>Detalle Pedido<span id="nro-pedido"></span></h2>
			<div class="action-list">
				<button id="btn-repeat-order" class="cust-btn gray_btn">Repetir Pedido</button>
				<!-- <button id="btn-load-draft" class="cust-btn gray_btn">Continuar Pedido</button> -->
			</div>
			<ul class="pedido-detail">
				<li id="detail-created" class="detail-item">Fecha de Creación: <span></span></li>
				<li id="detail-nota"    class="detail-item">Notas:  <span></span></li>
				<li id="detail-total"   class="detail-item">Total:  <span></span></li>
				<li id="detail-estado"  class="detail-item">Estado: <span></span></li>
			</ul>
			<table class="table" id="order_items">
				<thead>
					<th>#</th>
					<th>SKU</th>
					<th>PRODUCTO</th>
					<th>PRECIO</th>
					<th>CANT</th>
					<th>TOTAL</th>
				</thead>		
			</table>
		</div>	
	</div>	
</div>			