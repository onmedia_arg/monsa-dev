<!-- start footer Area -->

<style>
    .mb-50{
        margin-bottom: 50px;
    }

    .footer-section {
        background-color: #000;
        color: #fff;
    }
    .footer-section h6{
        color: #fff;
        font-weight: bold;
        font-size: 18px;
        margin: 20px 0px; 

    }

    .footer-section a.nav-link {
        color: #FFFFFF;
    }    

    .footer-section a.nav-link:hover {
        color: #A1F500;
    }    

    .footer-section .footer-logo img{
        width: 350px;
    }

    .footer-section .footer-social a,
    .footer-social i {
        color: #A1F500;
        font-size: 40px;
        margin-left: 10px;
    }
    .footer-social i:hover,
    .footer-social a:hover i {
        color: #fff;
    }
    .footer-text a{
        color: #A1F500;
    }

    .decorator {
    background: #000;
    position: relative;
}

.background-v {
    background: url('<?= site_url('resources/img/v.png') ?>');
    position: relative;
    background-size: 50px;
    z-index: 1; 
}

.background-gradient {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background:linear-gradient(180deg, rgba(0, 0, 0, 60%) 0%, rgb(0, 0, 0,100%) 100%);
    z-index: 2;
    pointer-events: none;
}

</style>

<section class="decorator">
    <div class="background-gradient"></div>
    <div class="container-fluid background-v" style="padding: 80px 0px;">
        </div>
</section>


<footer class="footer-section section_gap">
    <div class="container mb-50">
        <div class="row">
            <div class="col-6">
                <div class="footer-logo">
                    <img class="logo" src="<?php echo site_url('resources/img/logo-verde.png'); ?>" alt="">
                </div>
            </div>
            <div class="col-6 d-flex justify-content-end">                
                <div class="footer-social">
                    <a href="https://www.instagram.com/monsamayorista/"><i class="fab fa-instagram"></i></a>
                    <a href="https://www.facebook.com/monsasrl"><i class="fab fa-facebook"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-3">
                <h6>Navegación</h6>
                <div class="footer-menu d-flex align-items-center">
                    <ul class="">
                        <li class="nav-item active"><a class="nav-link" href="<?php echo site_url('/'); ?>">HOME</a>
                        </li>

                        <li class="nav-item submenu dropdown">
                            <a href="<?php echo site_url('frontController/shop');?>" class="nav-link ">PRODUCTOS</a>
                        </li>
                        <li class="nav-item submenu dropdown">
                            <a href="<?php echo site_url('frontController/shop');?>" class="nav-link ">MARCAS</a>
                        </li>
                        <li class="nav-item submenu dropdown">
                            <a href="<?php echo site_url('frontController/promociones');?>" class="nav-link ">PROMOCIONES</a>
                        </li>
                        <!-- <li class="nav-item"><a class="nav-link" href="">CONTACTO</a></li> -->
                    </ul>
                </div>
            </div>
            <div class="col-3">
                <h6>Accesos directos</h6>
                <div class="footer-menu d-flex align-items-center">
                    <ul class="">
                        <li class="nav-item submenu dropdown">
                            <a href="<?php echo site_url('customer');?>" class="nav-link ">MIS PEDIDOS</a>
                        </li>
                        <li class="nav-item submenu dropdown">
                            <a href="<?php echo site_url("frontController/carrito")?>" class="nav-link ">CARRITO</a>
                        </li>
                        <li class="nav-item submenu dropdown">
                            <a href="<?php echo site_url('customer');?>" class="nav-link ">CUENTA CORRIENTE</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-3">
                <div class="single-footer-widget mail-chimp">
                    <h6 class="mb-20">Seguínos en Instagram</h6>
                    <ul class="instafeed d-flex flex-wrap">
                        <li><img src="<?php echo site_url('resources/img/ig.jpg'); ?>" alt=""></li>
                        <li><img src="<?php echo site_url('resources/img/ig.jpg'); ?>" alt=""></li>
                        <li><img src="<?php echo site_url('resources/img/ig.jpg'); ?>" alt=""></li>
                        <li><img src="<?php echo site_url('resources/img/ig.jpg'); ?>" alt=""></li>
                    </ul>
                </div>                
            </div>
            <div class="col-3">
                <div class="single-footer-column">
                    <h6>Suscribite a nuestro newsletter</h6>
                    <p>Recibí las últimas novedades y promociones que tenemos para vos.</p>
                    <div class="" id="mc_embed_signup">

                        <form target="_blank" novalidate="true"
                            action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                            method="get" class="form-inline">

                            <div class="d-flex flex-row">

                                <input class="form-control" name="EMAIL" placeholder="Ingresá tu email"
                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ingresá tu email '"
                                    required="" type="email">


                                <button class="click-btn btn btn-default"><i class="fas fa-angle-right"></i></button>
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value=""
                                        type="text">
                                </div>
                            </div>
                            <div class="info"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
        <p class="footer-text m-0">
            &copy;
            <script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Desarrollado
            por <a href="https://onmedia.com.ar" target="_blank">onMedia</a>
        </p>
    </div>    
    <!-- <div class="container">
        <div class="row">
            <div class="col-lg-3  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <img class="logo" src="<?php echo site_url('resources/img/monsa-srl-logo.png'); ?>" alt="">
                </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Menu</h6>
                    <div class="footer-social d-flex align-items-center">
                        <ul class="">
                            <li class="nav-item active"><a class="nav-link" href="<?php echo site_url('/'); ?>">HOME</a>
                            </li>
                            <li class="nav-item submenu dropdown">
                                <a href="#" class="nav-link ">CATEGORÍAS</a>
                            </li>
                            <li class="nav-item submenu dropdown">
                                <a href="#" class="nav-link ">PRODUCTOS</a>
                            </li>
                            <li class="nav-item submenu dropdown">
                                <a href="#" class="nav-link ">MARCAS</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="contact.html">CONTACTO</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Newsletter</h6>
                    <p>Subscribite para recibir novedades y descuentos</p>
                    <div class="" id="mc_embed_signup">

                        <form target="_blank" novalidate="true"
                            action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                            method="get" class="form-inline">

                            <div class="d-flex flex-row">

                                <input class="form-control" name="EMAIL" placeholder="Ingresá tu email"
                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ingresá tu email '"
                                    required="" type="email">


                                <button class="click-btn btn btn-default"><i class="fas fa-angle-right"></i></button>
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value=""
                                        type="text">
                                </div>
                            </div>
                            <div class="info"></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-3  col-md-6 col-sm-6">
                <div class="single-footer-widget mail-chimp">
                    <h6 class="mb-20">Seguínos en Instagram</h6>
                    <ul class="instafeed d-flex flex-wrap">
                        <li><img src="<?php echo site_url('resources/img/ig.jpg'); ?>" alt=""></li>
                        <li><img src="<?php echo site_url('resources/img/ig.jpg'); ?>" alt=""></li>
                        <li><img src="<?php echo site_url('resources/img/ig.jpg'); ?>" alt=""></li>
                        <li><img src="<?php echo site_url('resources/img/ig.jpg'); ?>" alt=""></li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
            <p class="footer-text m-0">
                &copy;
                <script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Desarrollado
                por <a href="https://onmedia.com.ar" target="_blank">onMedia</a>
            </p>
        </div>
    </div> -->
</footer>
<!-- End footer Area -->

</div>
<!-- ./wrapper -->

<!-- <script defer src="<?= site_url('resources/js/front/jquery-3.4.0.min.js'); ?>"></script> -->
<script defer src="<?= site_url('resources/js/front/bootstrap.bundle.min.js'); ?>"></script>
<!-- <script defer src="<?= site_url('resources/js/front/owl.carousel.min.js'); ?>"></script> -->
<!-- Owl Carousel CSS y JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

<script defer src="<?= site_url('resources/js/front/jquery.nice-select.min.js'); ?>"></script>
<script defer src="<?= site_url('resources/js/front/jquery.sticky.js'); ?>"></script>
<script defer src="<?= site_url('resources/js/front/nouislider.min.js'); ?>"></script>
<script defer src="<?= site_url('resources/js/front/bootstrap-notify.min.js'); ?>"></script>
<script defer src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<!-- iziToast -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"
    integrity="sha256-321PxS+POvbvWcIVoRZeRmf32q7fTFQJ21bXwTNWREY=" crossorigin="anonymous"></script>

<script src="<?php echo site_url('resources/js/moment.js'); ?>"></script>
<script defer src="<?= site_url('resources/js/plugins/loaders/blockui.min.js'); ?>"></script>
<script defer src="<?php echo site_url('assets/plugins/fancybox/jquery.fancybox.js'); ?>"></script>
<script defer src="<?php echo site_url('resources/js/front/downloads.js'); ?>"></script>
<script defer src="<?php echo site_url('assets/front/js/cart.js'); ?>"></script>
<script defer src="<?php echo site_url('resources/js/front/scripts.js'); ?>"></script>
<!-- Scripts dinamicos que llegan desde el controlador -->
<?php
if (isset($scripts)) {
    if (is_array($scripts)) {
        foreach ($scripts as $script) {
            echo '<script defer src="' . base_url() . $script . '"></script>' . PHP_EOL;
        }
    }
}
?>
<!-- fin Scripts dinámicos desde controlador -->
</body>
<!-- Modal -->
<div class="modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel2">Right Sidebar</h4>
            </div>

            <div class="modal-body">
                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3
                    wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum
                    eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla
                    assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt
                    sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                    farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                    labore sustainable VHS.
                </p>
            </div>

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->

</html>