<!-- Start related-product Area -->
<style>

    .single-related-product{
        display: flex;
        flex-direction: row;
    }

    @media (max-width: 768px) {
        .single-related-product{
            flex-direction: column;
        }
    }

    .single-related-product .desc {
        margin: 0px;
        padding: 0px 15px;
        overflow: hidden;
        overflow-wrap: break-word;
    }

    .related-img{
        width:  230px;
        height: 230px;
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;        
    }
    
    .desc a.title {
        font-size: 1.8em;
        line-height: 1.3em;
    }    

    .desc .price h6 {
    font-size: 1.5em;
    }    

    .desc .price {
        margin-top: 16px;
    }
</style>

<section class="related-product-area productos-small section_gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="section-title">
                    <h1>MAS VENDIDOS</h1>
                </div>
            </div>
        </div>

        <div class="row" id="mas-vendidos">
            
        </div>

    </div>
</section>
<!-- End related-product Area -->

<script defer>
$(document).ready(function(){
    render_productos_bottom()
})

function render_productos_bottom(){

    var url = window.base + 'home/getProductosMasVendidos';

    $.ajax({
        url     : url,
        method  : 'POST',
        success : function(data){
                data = JSON.parse(data);
                var output = "";
                for( var i = 0; i < data.length ; i++ ) {

                    output += ` <div class="col-xs-12 col-lg-6 col-md-4 col-sm-6 m-0">
                                    <div class="single-related-product featured-box" id="${data[i]["idProducto"]}">
                                    <a href="${window.base}frontController/detalle/${data[i]['slug']}">
                                        <div class="related-img" style="background-image:url('${data[i]['imagen']}')"></div>
                                    </a>
                                    <div class="desc d-flex flex-column justify-content-center">
                                        <span style="font-size: 0.9em;color: #6f6f6f;">SKU: ${data[i]['sku']}</span>
                                        <a href="${window.base}frontController/detalle/${data[i]['slug']}" class="title">${data[i]["modelo"]}</a>
                                        
                                        <h4>${data[i]["familia"]} - ${data[i]["marca"]}</h4>
                                        <div class="price">`
                    
                    if(data[i]['is_promo_active'] == 1){
                        output += `<div style="margin-bottom:10px">
                                            <span style="font-size: 1.2em; 
                                                   color:#A1F500; 
                                                   font-weight: 600;
                                                   background-color: #121212;
                                                   padding: 5px 10px;
                                                   border-radius: 5px;">${data[i]['tag'] ?? ''}</span></div>
                        <div class="d-flex justify-content-start">
                                    <span style="font-size: 1.2em; text-decoration: line-through;margin-right:15px">$ ${formatCurrency(data[i]['precio'])}</span>
                                    <span style="font-size: 1.5em; color:#333 ;">$ ${formatCurrency(data[i]['price_promo'])}</span>
                                    </div>`
                    }else{
                        output += `<h6>$ ${formatCurrency(data[i]['precio'])}</h6>`
                    }
                                        
                    output +=       `</div>
                                    <span style="font-size: 0.9em;color: #6f6f6f;">No incluye IVA</span>
                                    </div>
                                    </div>
                                </div>` 
                    // output = render_item(output, data[i]);
                }
                $('#mas-vendidos').html(output);
        }
    })
}



function formatCurrency(amount) {
    // Formatea el número a una cadena con el formato "$ XX.XXX,XX"
    return parseFloat(amount).toLocaleString('es-ES', {
        style: 'currency',
        currency: 'ARS',
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
        useGrouping: true
    }).replace('ARS', ''); 
}

</script>