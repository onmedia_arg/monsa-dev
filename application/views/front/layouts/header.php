<?php

foreach ($nav['nav_cats'] as $key => $categoria) {
    $categorias[$key]['id'] = $categoria['idCategoria'];
    $categorias[$key]['nombre'] = $categoria['nombre'];
    $categorias[$key]['slug'] = $categoria['slug'];
}

foreach ($nav['nav_family'] as $key => $familia) {
    $familias[$key]['id'] = $familia['idFamilia'];
    $familias[$key]['nombre'] = $familia['nombre'];
    $familias[$key]['slug'] = $familia['slug'];
}


foreach ($nav['nav_marcas'] as $key => $marca) {
    $marcasmenu[$key]['id'] = $marca['idMarca'];
    $marcasmenu[$key]['nombre'] = $marca['nombre'];
    $marcasmenu[$key]['slug'] = $marca['slug'];
}

?>

<!DOCTYPE html>
<script> var base = "<?php echo base_url(); ?>"; </script>
<html lang="es" class="no-js">

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="<?php echo site_url('resources/img/fav.png');?>">
        <meta name="author" content="onMedia">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="UTF-8">
        <title>Monsa SRL</title>


        <link rel="stylesheet" href="<?php echo site_url('resources/css/front/bootstrap.min.css');?>">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">

        <!-- <link rel="stylesheet" href="<?php echo site_url('resources/css/front/owl.carousel.min.css');?>"> -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo site_url('resources/css/front/nouislider.min.css');?>">
        <link rel="stylesheet" href="<?php echo site_url('resources/css/front/nice-select.css');?>">
        <link rel="stylesheet" href="<?php echo site_url('resources/css/front/animate.css');?>">
        <link rel="stylesheet" href="<?php echo site_url('resources/css/front/linearicons.css');?>">
        <!-- <style>
            @font-face {
                font-family: 'Neue';
                src:<?php echo site_url('resources/fonts/neue/NeuePowerTrial-Regular.woff2')?> ;
                font-weight: 400;
                font-style: normal;
            }
            @font-face {
                font-family: 'Neue';
                src:<?php echo site_url('/resources/fonts/neue/NeuePowerTrial-Bold.woff2')?>;
                font-weight: 600;
                font-style: bold;
            }
            @font-face {
                font-family: 'Neue';
                src:<?php echo site_url('/resources/fonts/neue/NeuePowerTrial-Heavy.woff2')?>;
                font-weight: 800;
                font-style: heavy;
            }
            
        </style> -->
        
        
        <link rel="stylesheet" href="<?php echo site_url('resources/css/front/neue.css');?>">
        <link rel="stylesheet" href="<?php echo site_url('resources/css/front/theme.css');?>">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('resources/css/front/custom.css');?>">        
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css" integrity="sha256-f6fW47QDm1m01HIep+UjpCpNwLVkBYKd+fhpb4VQ+gE=" crossorigin="anonymous" /> 
        <link rel="stylesheet" href="<?php echo site_url('assets/plugins/fancybox/jquery.fancybox.min.css');?>"/>

        <!-- CSS dinamicos que llegan desde el controlador -->
        <?php
        if (isset($csss)) {
            if (is_array($csss)) {
                foreach ($csss as $css) {
                    echo '<link href="' . base_url() . $css . ' "rel="stylesheet" type="text/css">' . PHP_EOL;
                }
            }
        }
        ?>
        <!-- fin CSS dinámicos desde controlador -->

        <script src="<?= site_url('resources/js/front/jquery-3.4.0.min.js');?>"></script>
        
    </head>
    <body class="hold-transition skin-blue sidebar-mini" data-url="<?php echo base_url(); ?>">

        <div class="wrapper" id="wrapper">
            
        <!-- Start Header Area -->
            <div class="secondary_menu">
                <div class="redes">
                    <ul>
                        <li><a title="Facebook" href="https://www.facebook.com/monsasrl/" target="_blank" ><i class="fab fa-facebook-f"></i></a></li>
                        <!-- <li><a title="Twitter" href="https://twitter.com/MONSASRL" target="_blank" ><i class="fab fa-twitter"></i></a></li> -->
                        <li><a title="Instagram" href="https://www.instagram.com/monsamayorista/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
                <div class="header-contacto">
                    <ul>
                        <li><i class="fas fa-phone"></i>+54 (011) 4601-1555</li>
                        <li><i class="far fa-clock"></i>Lunes a Viernes: 08:00 a 17:00 Hs</li>
                    </ul>
                </div>
            </div>        
            <header class="header_area sticky-header">
                <div class="main_menu">
                    <nav class="navbar navbar-expand-lg navbar-light main_box">
                        <div class="container">
                            <a class="navbar-brand logo_h" href="<?php echo site_url('frontController');?>"><img src="<?php echo site_url('resources/img/logo-verde.png');?>" alt=""></a>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                             aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                                <ul class="nav navbar-nav menu_nav ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo site_url('frontController');?>">INICIO</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo site_url('frontController/shop');?>">CATÁLOGO</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo site_url('frontController/promociones');?>">PROMOCIONES</a>
                                    </li>                                    
                                </ul>
                                <ul class="nav-actions justify-content-end">
                                    <button type="button" class="btn btn-outline-warning btn-sm btn-br-0 nav-item" data-toggle="modal" id="show-side-cart" data-target="#side-cart">
                                      <i style="margin-right:8px;" class="fas fa-eye"></i>Vista Rápida 
                                    </button>
                                    <a href="<?php echo site_url("frontController/carrito")?>" class="btn btn-outline-warning btn-sm btn-br-0 nav-item">
                                      <!-- <i style="margin-right:8px;" class="fas fa-shopping-cart"></i><span class="cart-count">10</span> Pedido Actual -->
                                      <i class="fas fa-shopping-cart"></i><span id="count-items" class="cart-count">0</span> Pedido Actual
                                    </a>
                                    
                                    <?php
                                    if($user['level'] == 9):?>
                                        <a href="<?php echo site_url('Start/index')?>" class="btn btn-nav-orders btn-sm btn-br-0 nav-item ">
                                        Administrar  
                                        </a>                                    
                                    <?php else:?>
                                        <a href="<?php echo site_url('customer')?>" class="btn btn-nav-orders btn-sm btn-br-0 nav-item ">
                                        Mis Pedidos
                                        </a>                                    
                                    <?php endif;?>

                                </ul>
                                    <div class="nav-item dropdown submenu nav-user">
                                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="far fa-user"></i><?echo $user['username']?></a>
                                         <ul class="dropdown-menu">
                                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url('customer')?>">
                                                Mis Pedidos</a></li>
                                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url('start/logout')?>">Cerrar Sesión</a></li>
                                        </ul>
                                    </div>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="search_input dnone" id="search_input_box">
                    <div class="container">
                        <form class="d-flex justify-content-between">
                            <input type="text" class="form-control" id="search_input" placeholder="Ingrese aquí su búsqueda">
                            <button type="submit" class="btn"></button>
                            <span id="close_search" title="Cerrar"><i class="fas fa-times"></i></span>
                        </form>
                    </div>
                </div>
            </header>
            <!-- End Header Area -->
            <!-- Modal -->
            <div class="modal right fade" id="side-cart" tabindex="-1" role="dialog" aria-labelledby="side-cart-Title" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h3 class="modal-title" id="side-cart-Title">Nuevo Pedido</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <style>
                    .cart-item-name{
                        width: 300px;
                    }
                    .cart-item-actions{
                        width: 80px;
                    }
                  </style>
                  <div id="side-cart-content" class="modal-body">
                    <table class="table side-cart-table car-table">

                    </table>
                  </div>
                  <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-chevron-left"></i> Volver</button>
                    <button type="button" class="btn btn-primary"></button> -->
                        <a class="cust-btn gray_btn" data-dismiss="modal" href="<?php echo site_url('frontController/Shop')?>"><i class="fas fa-chevron-left"></i> AGREGAR MAS PRODUCTOS</a>
                        <a class="cust-btn primary-btn ver_carrito" href="<?php echo site_url('frontController/carrito')?>">VER PEDIDO  <i class="fas fa-angle-right"></i></a>                    
                  </div>
                </div>
              </div>
            </div>