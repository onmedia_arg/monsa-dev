<!-- Start related-product Area -->
<section class="related-product-area productos-small section_gap">
    <div class="container">
        <div class="row ">
            <div class="col-lg-6">
                <div class="section-title">
                    <h1>Destacados</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">

                    <?php 

                    foreach ($featured_prod as $producto) { 

                        $quitar                  = ['\\'];
                        $remplazar               = '';
                        $imagenes = str_replace($quitar, $remplazar, $producto['imagen']);
                        $imagenes = json_decode($imagenes);
                        $max_images = 1;
                        if($imagenes != NULL){
                            if (count($imagenes) > 0) {
                                for ($i=0; $i < count($imagenes) ; $i++) {
                                    if ($i < $max_images) {
                                        $url_image = base_url($imagenes[$i]);
                                    }
                                }
                            }
                        }                                    
                    ?>

                        <div class="col-lg-3 col-md-4 col-sm-6 mb-20">
                            <div class="single-related-product d-flex featured-box" id="<?php echo $producto["idProducto"] ?>">
                                <a href="<?php echo site_url('frontController/detalle/'). $producto['slug']; ?>">
                                    <img class="img-fluid" src="<?php echo $url_image?>" alt=""></a>
                                <div class="desc">
                                    <a href="<?php echo site_url('frontController/detalle/'). $producto['slug']; ?>" class="title"><?php echo $producto["modelo"] ?></a>
                                    <div class="price">
                                        <h6>$ <?php echo $producto["precio"] ?></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        

                    <?php
                    }
                    ?>


                    <?php for ($x = 0; $x <= 7; $x++) { ?>

                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End related-product Area -->