<style>
    .sidebar-categories .head{
        margin-bottom: 5PX;
    }

    .list-group {
        /* box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24); */
        border-radius: 4px;
    }
    .list-group-item {
        border: none;
        padding: .25rem;
    }
    .list-group-item.checkbox label{
        padding-left: 5px;
    }

    .list-group-item.checkbox label:hover {
    background-color: inherit;
    }

    .list-group-item.checkbox:hover {
            background-color: #f7f7f7;
            color: #000;
    }
    
    input[type="checkbox"] {
        margin-right: 10px; /* Espacio a la derecha del checkbox */
    }
    .mt-50{
        margin-top: 45px;
    }
    
    /* Children*/
    .filter-value.children {
        padding-left: 5px;
    }

    .a-parent[aria-expanded="false"]:after {
        font-family:"Font Awesome 5 Free";
        font-style:normal;
        font-weight:900;
        content: '\f0d7';
        float: right;
        color: black;
    } 

    .a-parent[aria-expanded="true"]:after {
        font-family:"Font Awesome 5 Free";
        font-style:normal;
        font-weight:900;
        content: '\f0d8';
        float: right;
        color: black;
    } 

    label.active{

        border-bottom: solid lightgrey 1px;
        padding-bottom: 5px;

    }

</style>
<div class="container section_gap">
        <div class="row">
            <div class="d-none d-md-block col-xl-3 col-lg-4 col-md-5">

                <div id="brands-wrap" class="sidebar-categories mt-50">

                     
                    <div class="filter-h"
                         style="margin-right: 10px;
                                display: flex; 
                                justify-content: space-between;
                                align-items: center;">
                        
                        <div>MARCAS</div> 
                        
                        <a class="btn btn-dark btn-sm" 
                            data-toggle="collapse" 
                            href="#multiCollapseExample1" 
                            role="button" 
                            aria-expanded="false" 
                            aria-controls="multiCollapseExample1">
                            <i class="fas fa-chevron-down"></i>
                        </a>
                    </div> 
                    <div class="sep"></div>
                    <div class="collapse multi-collapse" id="multiCollapseExample1">
                        <div class="list-group">
                            <?php foreach($marca_data as $marca) { 

                                    $hasProducts = $this->Marca_model->marca_has_products( $marca['idMarca'] );

                                    if ( ! $hasProducts ) {
                                        continue;
                                    } 

                                    // si hay un parametro en el url
                                    if ( isset($_GET['marca']) ) {

                                        // Revisa si son varios parametros
                                        $getArray = ( strpos($_GET['marca'], ',') !== false ) ? explode(',', $_GET['marca']) : $_GET['marca'] ;

                                        // Si son varios revisa si esta en el array
                                        if ( is_array($getArray) ) {
                                            $checked = ( in_array( $marca['idMarca'], $getArray ) )  ? 'checked' : null ;
                                        }else{
                                            $checked = ( $getArray == $marca['idMarca'] ) ? 'checked' : null ;
                                        }   

                                    }else{
                                        $checked = null;
                                    }

                                ?>
                                <div class="list-group-item checkbox">
                                    <label>
                                        <input type="checkbox" class="common_selector marca" name="marca" data-name="<?= $marca["nombre"] ?>" data-marca="<?= $marca['idMarca'] ?>" value="<?php echo $marca['idMarca']; ?>" 
                                        <?= $checked ?> > <?php echo $marca["nombre"]; ?>
                                    </label>
                                </div>                            
                            <?php } ?>                          
                        </div>                        
                    </div>
                    <!-- <div class="list-group">
                        <?php foreach($marca_data as $marca) { 

                                $hasProducts = $this->Marca_model->marca_has_products( $marca['idMarca'] );

                                if ( ! $hasProducts ) {
                                    continue;
                                } 

                                // si hay un parametro en el url
                                if ( isset($_GET['marca']) ) {

                                    // Revisa si son varios parametros
                                    $getArray = ( strpos($_GET['marca'], ',') !== false ) ? explode(',', $_GET['marca']) : $_GET['marca'] ;

                                    // Si son varios revisa si esta en el array
                                    if ( is_array($getArray) ) {
                                        $checked = ( in_array( $marca['idMarca'], $getArray ) )  ? 'checked' : null ;
                                    }else{
                                        $checked = ( $getArray == $marca['idMarca'] ) ? 'checked' : null ;
                                    }   

                                }else{
                                    $checked = null;
                                }

                            ?>
                            <div class="list-group-item checkbox">
                                <label>
                                    <input type="checkbox" class="common_selector marca" name="marca" data-name="<?= $marca["nombre"] ?>" data-marca="<?= $marca['idMarca'] ?>" value="<?php echo $marca['idMarca']; ?>" 
                                    <?= $checked ?> > <?php echo $marca["nombre"]; ?>
                                </label>
                            </div>                            
                        <?php } ?>                          
                    </div> -->
                </div>

                <divd id="family-wrap" class="sidebar-categories">
                    <div class="filter-h"
                         style="margin-right: 10px;
                                display: flex; 
                                justify-content: space-between;
                                align-items: center;">
                        
                        <div>TIPO PRODUCTO</div> 
                        
                        <a class="btn btn-dark btn-sm" 
                            data-toggle="collapse" 
                            href="#multiCollapseExample2"
                            role="button" 
                            aria-expanded="false" 
                            aria-controls="multiCollapseExample2">
                            <i class="fas fa-chevron-down"></i>
                        </a>
                    </div>                     
                    
                    <div class="sep"></div>
                    <div class="collapse multi-collapse" id="multiCollapseExample2">
                        <div class="list-group">
                    
                            <?php foreach($family_data as $familia) { 

                                        $hasProducts = $this->Familium_model->familium_has_products( $familia['idFamilia'] );

                                        if ( ! $hasProducts ) {
                                            continue;
                                        }

                                        // si hay un parametro en el url
                                        if ( isset($_GET['familia']) ) {

                                            // Revisa si son varios parametros
                                            $getArray = ( strpos($_GET['familia'], ',') !== false ) ? explode(',', $_GET['familia']) : $_GET['familia'] ;

                                            // Si son varios revisa si esta en el array
                                            if ( is_array($getArray) ) {
                                                $checked = ( in_array( $familia['idFamilia'], $getArray ) )  ? 'checked' : null ;
                                            }else{
                                                $checked = ( $getArray == $familia['idFamilia'] ) ? 'checked' : null ;
                                            }   

                                        }else{
                                            $checked = null;
                                        }

                                ?>
                                <div class="list-group-item checkbox">
                                    <label class="w-100">
                                        <input  type="checkbox" 
                                                class="common_selector familia" 
                                                name="familia" 
                                                data-name="<?= $familia["nombre"] ?>" 
                                                data-familia="<?= $familia['idFamilia'] ?>" 
                                                value="<?php echo $familia['idFamilia']; ?>" <?= $checked ?> > 
                                        <?php echo $familia["nombre"]; ?>
                                    </label>
                                        <?php   

                                            $f = $familia['idFamilia']; 

                                            if ( is_array( $children_atributos[$f] ) && ! empty( $children_atributos[$f] ) ):

                                                $current_attr = $children_atributos[$f][0]["idAtributo"];

                                                $row = current( $children_atributos[$f] );  

                                                while( !empty( $row ) ):
                                                    
                                                    $current_attr = $row["idAtributo"];

                                                    while( $row != FALSE && $row["idAtributo"] == $current_attr ):

                                                        if ( ! empty( $row["valor"] ) ):

                                        ?>
                                                            <ul class="attribute-parent-list pl-3 mt-1 collapse parent-<?= $familia['idFamilia'] ?>">
                                                            
                                                            <li class="p-1">

                                                                <a class="text-primary a-parent" data-toggle="collapse" href="#children-<?= $row["idAtributo"] ?>" role="button" aria-expanded="false">
                                                                    <?= $row["nombre"] ?>
                                                                </a> 
                                                                
                                                                <ul id="children-<?= $row["idAtributo"] ?>" class="filter-value children mt-1 collapse">

                                        <?php 
                                                                foreach ( json_decode( $row["valor"] ) as $atributo ):  
                                                                    
                                                                    $checked = null;

                                                                    // Revisa si el atributo esta en el url
                                                                    if ( isset($_GET['atributo']) ) {
    
                                                                        $getAttr = json_decode( urldecode( $_GET['atributo'] ), true ); 

                                                                        // print_r( $getAttr );

                                                                        foreach ( $getAttr as $attr) {

                                                                            // Si esta, tilda el input
                                                                            if( in_array( $atributo, $attr ) && in_array( $row["idAtributo"], $attr ) )
                                                                                $checked = 'checked';  
                                                                                
                                                                        }

                                                                    } 

                                        ?>              
                                                                    <li class='shop-attr-line'> 
                                                                    
                                                                        <label>
                                                                            <input  type="checkbox" 
                                                                                    data-idattr='<?= $row["idAtributo"] ?>' 
                                                                                    data-slug='<?= $row["slug"] ?>' 
                                                                                    data-idfamilia='<?= $familia['idFamilia'] ?>' 
                                                                                    data-name="<?= $row["nombre"] ?>: <?= $atributo ?>"
                                                                                    name="atributo"
                                                                                    class='common_selector atributo' value="<?= $atributo ?>" 
                                                                                    <?= $checked ?> >
                                                                            <?= $atributo ?>
                                                                        </label>  

                                                                    </li>                                       
                                        <?php 
                                                                endforeach;
                                        ?>
                                                                </ul> 

                                                            </li> 

                                                            </ul> 
                                        <?php
                                                        endif;

                                                        $row = next( $children_atributos[$f] ); 
                                                    endwhile;

                                                endwhile; 

                                            endif;

                                        ?>  
                                </div>                            
                            <?php } ?>                          
                        </div>
                    </divd>
                </div>


            </div>
            
          