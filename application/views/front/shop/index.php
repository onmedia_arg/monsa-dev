<?php 
    $userstring = (isset($user['email'])) ? $user['email'] : $this->session->userdata('email');
    $role       = (isset($user['role'])) ? $user['role'] : '';

    $this->load->view('front/layouts/header', $nav, $user);
    $this->load->view('front/layouts/seccion',$title);
    $this->load->view('front/shop/filtros');
    $this->load->view('front/shop/productos', $flash_message);
    $this->load->view('front/layouts/destacados');
    $this->load->view('front/layouts/footer');

?>