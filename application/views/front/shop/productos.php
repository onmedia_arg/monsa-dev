        <div class="col-xl-9 col-lg-8 col-md-7">
            
<style>
.home-msj {
    background-color: #000;
    padding: 1vh 1vw;
    border-left: 4px solid #A1F500;
    border-radius: 3px 0px 0px 3px;
    color: #fff;
}
.home-msj-close {
    float: right;
    font-size: 20px;
}
.home-msj-close:hover {
    color: #A1F500;
    cursor: pointer;
}


.home-msj-title {
    font-size: 16px;
    
    margin-bottom: 10px;
}

.row-price {
    display: flex;
    justify-content: space-between;
    margin-top:15px;
}

.circle-stock {
    width: 1.5rem;
    height: 1.5rem;
    border-radius: 50%;
}

.product-list-row-top{
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 5px;
}
</style>            
            <?php
                if(!empty($flash_message['titulo']) ||
                   !empty($flash_message['detail'])) {?>

                <section class="home-msj mb-25">
                    <div id="home-msj-close" class="home-msj-close"><i class="far fa-times-circle"></i></div>
                    <div class="home-msj-title"><?= $flash_message['titulo']?></div>
                    <div class="home-msj-detail"><?= $flash_message['detail']?></div>
                </section>

            <?php } ?>

            <section class="mb-25">
                <label style="display:block;margin-bottom:5px" for="search">Buscar productos</label>
                <div style="display:flex;justify-content: space-between">
                    <input id="search" type="text" name="search" placeholder="Marca, Familia, Descripción, Código ...">
                    <button id="btn-search"  class=""><i class="fas fa-search"></i> BUSCAR</button>                
                </div>

            </section>

            <ul id="active-filters">
                <!-- Filter -->
            </ul>

            <section class="shop-buttons">
                <!-- <div class="float-left">
                    <input id="search" type="text" name="search">
                    <button id="btn-search"  class="cust-btn gray_btn"><i class="fas fa-search"></i>BUSCAR</button>
                </div> -->
                
                <button id="btn-grid-layout" class="cust-btn gray_btn"><i class="fas fa-th"></i>VER CUADRICULA</button>
                <button id="btn-list-layout"  class="cust-btn gray_btn"><i class="fas fa-list"></i>VER LISTA</button>

                <select name="item-number" id="item-number" class="col-1">
                    <option value="12">12</option>
                    <option value="15">15</option>  
                    <option value="30">30</option>
                    <option value="45">45</option>
                </select>
            </section>

            <!-- <div class="spinner"><i class="spinner-grow text-warning"></i></div> -->

            <section class="lattest-product-area pb-40 category-list">
                <div class="row filter_data equal" id="catalogo"></div>                    
                <div align="center" id="pagination_link"></div>
            </section>
        </div>
    </div>
</div> 