<?php 
    $userstring = (isset($user['email'])) ? $user['email'] : $this->session->userdata('email');
    $role       = (isset($user['role'])) ? $user['role'] : '';
        
    $this->load->view('front/layouts/header',$nav);
    $this->load->view('front/home/banner', $slider);
    $this->load->view('front/home/productos');

        // echo "<pre>";
        // print_r($carrouselData);
        // echo "</pre>";

        if(!empty($carrouselData)){
            foreach ($carrouselData as $carrousel) {
                if(empty($carrousel)){
                    continue;
                }
                $data['carrousel'] = $carrousel;
                $this->load->view('front/home/carrousel', $data);
            }
        }
   
    // $this->load->view('front/home/badges');
    $this->load->view('front/home/descargas');    
    // $this->load->view('front/home/families', $nav);    
    $this->load->view('front/home/marcas', $brands);
    // $this->load->view('front/layouts/old-destacados');
    $this->load->view('front/layouts/destacados');
    $this->load->view('front/home/notas');    
    $this->load->view('front/layouts/footer');

?>