<style>
    .page-title {}

    .file-box {
        box-shadow: 0 10px 30px rgb(0 0 0 / 10%);
        padding: 30px 10px;
        border-radius: 10px;
        margin-top: 30px;
        display: flex;
        flex-direction: column;
        align-items: center;

    }

    .file-box h6 {
        color: #6f6f6f;
        margin-top: 10px;
    }

    .download-btn {
        margin-top: 20px;
        background-color: #121212;
        border-radius: 5px;
        text-align: center;
        color: #fff;
        font-size: 1rem;
        font-weight: 600;
        border: none;
        margin-inline: auto;
        padding: 7px 25px;
    }

    .download-btn:hover {
        background-color: #000;
        color: #A1F500;
        cursor: pointer;
    }

    .download-btn-description {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        width: 100%;
        font-size: 1.5em;
        font-weight: 600;
        padding: 3px 40px;
        text-align: center;
        color: #636363;
        font-family: 'EXO 2';
    }

    .custom-message {
        background-color: #F7F7F7;
        border-radius: 10px;
        border-left: solid 2px #ffc006;
        padding: 20px;
    }

    .section_gap_2 {
        margin-bottom: 100px;
    }

.home-product-item{
    display:flex;
    flex-direction: column;
    justify-content: space-between
}

.home-product-box{
    /* background-color: #f7f7f7; */
    display:flex;
    flex-direction: column;        
    white-space: nowrap;
overflow: hidden;
    flex-grow: 1;
}
.home-product-box-img{
  height: 180px;

  background-size: cover !important;
  background-repeat: no-repeat !important;
  background-position: center !important;
  margin-bottom: 10px;
}
.home-product-box-details {
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
}

.home-product-price {
    margin-top: 10px;
}    

.home-product-add-bag{
    margin-top: 20px;
}
.home-product-box-btn {
    width: 100%;
    background-color: #121212;
    border: none;
    color: #fff;
    font-weight: bold;
    border-radius: 5px;
    /* align-self: end !important; */
}
.home-product-box-btn i{
    margin-right: 3px;
}
.home-product-box-btn:hover{
    background-color: #000;
    color: #A1F500;
}

/* .home-product-promo {
    background-color: #e32026;
    color: #fff;
    padding: 2px 12px;
    font-weight: 800;
    letter-spacing: 0.5px;
}     */

.home-product-promo {
    width: 0;
    height: 0;
    border-right: 80px solid #fec101;
    border-bottom: 80px solid transparent;
    z-index: 12;
    position: relative;
    top: -2px;
}

.promo-text {
    font-size: 13px;
    font-weight: bold;
    line-height: 13px;
    position: absolute;
    z-index: 14;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(49deg);
    top: 28px;
    left: 1px;
    width: 99px;
    text-align: center;
    color: #333;
} 

</style>


<section class="section_gap">
    <div class="container">
        <h1 style="margin-bottom: 40px;"> PROMOCIONES VIGENTES </h1>
        <div id="thumbs-promotions" class="owl-carousel owl-theme"></div>
    </div>
</section>

<section class="section_gap">
    <div class="container">
        <h1 style="margin-bottom: 40px;"> PROMOCIONES DESTACADAS </h1>
        <div id="productos-promociones" class="row"></div>
    </div>
</section>

<section class="section_gap">
    <div class="container">

    </div>
</section>

<section class="section_gap">
    <div class="container">
        <div class="page-title">
            <h1> DESCARGAS </h1>
            <!-- <div id="msj" class="custom-message">
                <div class="fa-3x">
                    <i class="fas fa-spinner fa-spin" style="color:#ffc006"></i>
                </div>
            </div> -->
        </div>
    </div>
</section>

<section class="section_gap_2">
    <div class="container">
        <div id="files-list" class="row">
            <div class="fa-3x">
                <i class="fas fa-spinner fa-spin" style="color:#ffc006"></i>
            </div>
        </div>
    </div>
</section>

<script defer>
    $(document).ready(function () {
        render_download_button()
    })

    function render_download_button() {

        var post = base + 'front/descargas/get_content'

        fetch(post, {
            method: 'POST'
        })
            .then(data => data.json())
            .then(data => {
                if (data.status === 200) {

                    var output = ""

                    data.files.forEach(file => {

                        if (file.type == 'PDF') {
                            filename = 'Versión PDF';
                        } else if (file.type == 'XLS') {
                            filename = 'Versión XLS';
                        } else if (file.type == 'CSV') {
                            filename = 'Lista Código/Precio';
                        } else {
                            filename = file.name + '.' + file.type.toLowerCase();
                        }

                        output += `<div class="col-12 col-md-6 col-lg-4 ">
                                    <div class="file-box">
                                        <div class="download-btn-description">${filename}</div>
                                        <a class="download-btn" href="${file.publicfullpath}">DESCARGAR</a>
                                        <h6>Actualizado: ${moment(file.updated_at).format('DD/MM/YYYY')}</h6>    
                                    </div>
                                </div>`
                    })

                    $('#files-list').empty().html(output)
                    // $('#msj').empty().html(data.msj.opt_value)
                    setPromotionFiles(data.promotions)
                    render_productos(data.products)
                } else {
                    /*Armamos el mensaje toast*/
                    dangerToast(data.title, data.message)

                }
            })

    }

    function setPromotionFiles(promotions) {

        var output = ""

        promotions.forEach(promotion => {
            output += `<div class="item"><a href="${promotion.publicfullpath}" data-fancybox="gallery"><img src="${promotion.publicfullpath}" alt="${promotion.name}" style="width: 500px;"></a></div>`
        })

        $('#thumbs-promotions').empty().html(output)
        
        
        $('#thumbs-promotions').owlCarousel({
            loop: true,
            margin: 10,
            nav: false,
            dots: true,
            // items: 2,
            // itemsMobile: 1
            responsive:{
                0:{
                    items: 1
                },
                500:{
                    items: 2
                },
            }
        });
    }

    function render_productos(data) {

        var output = "";

        data.forEach(product => {
            output += `<div id="product-${product['idProducto']}" class="col-3">
                        <div class="home-product-box">
                            <a href="${window.base}frontController/detalle/${product['slug']}">
                                <div class="home-product-box-img" style="background:url('${product['imagen']}')"></div>
                            </a>
                            <div class="home-product-box-details">
                                <a href="${window.base}frontController/detalle/${product['slug']}">
                                    ${product["familia"]} ${product["marca"]}<br>
                                    ${product["modelo"]}
                                    </a>
                                <div class="price home-product-price">
                                    <p class="d-flex justify-content-start align-items-center gap-4">
                                        <span style="font-size: 1.4em;text-decoration: line-through;margin-right: 10px;">$ ${product['precio']}</span>
                                        <span class="p-price">$ ${product['price_promo']}</span>
                                    </p>
                                </div>
                                <div class="home-product-add-bag">
                                    <input type="number" name="quantity" class="form-control quantity d-none" value="1" id="${product["idProducto"]}"/>
                                    <button type="button" id="add_cart" name="add_cart" class="home-product-box-btn"
                                        data-productname = "${product["nombre"]}"
                                        data-price       = "${product["price_promo"]}"
                                        data-productid   = "${product["idProducto"]}"
                                        data-sku         = "${product["sku"]}">
                                        </i>AGREGAR</button>
                                </div>
                            </div>
                        </div>
                    </div>`            
        })
        $('#productos-promociones').html(output);
    }


</script>