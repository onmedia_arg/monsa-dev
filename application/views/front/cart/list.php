
<section class="cart-list-area pb-40">
    <div class="container section_gap">
        <div class="section-title">
            <h1 id="title">Pedido Nro: <?= $idOrderHdr ?> </h1>
            <div class="section-title-buttons">
                <a id="btn-new-order"   href="#" class="cust-btn primary-btn" >Vaciar Pedido</a>
                <!-- <a id="btn-clear-order" href="#" class="cust-btn primary-btn">Borrar Items</a> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box-shadow">
                    <div class="spinner"><i class="spinner-grow text-warning"></i></div>
                    <div class="form-cart">
                        <div class="col-md-12">
                            <table class="table cart-list-table">
                            </table>
                        </div> 

                        <div class="col-md-12" style="text-align:right">
                            <!-- <small>Los precios no incluyen IVA<br>Los precios pueden sufrir aumentos al momento de su facturación</small> -->
                            <small>Los precios pueden sufrir aumentos al momento de su facturación</small>
                        </div>

                        <div id="cart-notes" class="col-md-12">
                            <div class="form-group">
                                <label for="nota-cart">Notas</label>
                                <textarea class="form-control" id="nota-cart" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <!-- <div class="checkout_btn_inner d-flex align-items-center"> -->
                            <div class="checkout_btn_inner">                            
                                <a class="cust-btn gray_btn" href="<?php echo site_url('frontController/Shop')?>"><i class="fas fa-chevron-left"></i> AGREGAR MAS PRODUCTOS</a>
                                <a class="cust-btn primary-btn enviar_pedido float-right" href="#">ENVIAR <i class="fas fa-angle-right"></i></a>
                                <!-- <a class="cust-btn primary-btn guardar_borrador float-right" href="#">GUARDAR <i class="fas fa-save"></i></a> -->

                            </div>            
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
</section>
<div class="container"><hr></div>


<!-- End Best Seller -->
