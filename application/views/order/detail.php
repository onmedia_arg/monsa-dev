<style>
	.thumbs{
		text-align: center;
	}
	.list-thumb{
		width: auto;
		height: auto;
		max-height: 50px;
		max-width: 50px;
		border: solid 1px white;
		border-radius: 3px;
	}
	.box-body{
		overflow-x: scroll;
	}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box list">
            <div class="box-header">
                <h3 class="box-title">Detalle de Pedidos</h3>
            	<div class="box-tools">
                    <!-- <a href="" class="btn btn-success btn-sm">Agregar</a>  -->
                </div>
            </div>
            <div class="box-body">
				<table class="table" id="table-order-detail">
					<thead>
						<tr>
							<!-- <th>idOrderHdr</th> -->
							<th>#</th>
							<th>Producto</th>
							<th>Precio</th>
							<th>Cantidad</th>
							<th>Total</th>
							<th>options</th>
							<th>Status</th>
							<th>createdBy</th>
							<th>createdDate</th>
							<th>updatedBy</th>
							<th>updatedDate</th>
							<th>Acciones</th>
			            </tr>
					</thead>
				</table>                
                                
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="edit-item">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Usuarios Asociados</h4>
            </div>
            <div class="modal-body"> 
                
                <div class="table-responsive">
                    <table id="client-users-table" class="table">
                        <thead>
                            <tr> 
                                <th>User Id</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Auth Level</th>
                                <th>Banned</th> 
                                <th>Created At</th>
                                <th>Modified At</th>  
                                <th>Acciones</th>  
                            </tr>
                        </thead>
                        <tbody> 

                            <!-- Ajax -->

                        </tbody>
                    </table>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button> 
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>

<script>
	var table = null,
		id_order = '<?= $order ?>',
		base_url = '<?= base_url() ?>';

    $(function () {
        
        fn_build_table();

    });	

	function fn_build_table(){

		table = $('#table-order-detail').DataTable({
			"serverSide": true,
			"processing": true,
			"responsive": true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            },
			"ajax":{
				url : base_url + 'Order/getOrderDetail/' + id_order,
				type: "post", 
			},
	        "columnDefs":[
	                    {
	                    "targets": [ 2 ],
	                    render: $.fn.dataTable.render.number( '.', ',', 0, '$ ' )
	                     },
	            ],			
		})

	};


    $(document).ready(function(){ 

        $(document).on('click', '.delete-item', function(){  

        	var item_id = $(this).data('id');

            $.getJSON( base_url + 'Order/delete_order_item/' + id_order + '/' + item_id, function(data){

                if ( data.code == 1 ) {

                    iziToast.success({
                        title: 'OK!',
                        message: data.message,
                    });

                    table.ajax.reload( null, false );

                }else{

                    iziToast.error({
                        title: 'Error!',
                        message: data.message,
                    });

                }
 
            })

        })

    })

</script> 