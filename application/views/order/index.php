<style>
	.thumbs{
		text-align: center;
	}
	.list-thumb{
		width: auto;
		height: auto;
		max-height: 50px;
		max-width: 50px;
		border: solid 1px white;
		border-radius: 3px;
	}
	.box-body{
		overflow-x: scroll;
	}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box list">
            <div class="box-header">
                <h3 class="box-title">Pedidos</h3>
            	<div class="box-tools">
                    <!-- <a href="" class="btn btn-success btn-sm">Agregar</a>  -->
                </div>
            </div>
            <div class="box-body">
				<table class="table" id="table-order">
					<thead>
						<tr>
							<th>Pedido Nro</th>
							<th>idUser</th>
							<th>total</th>
							<th>idOrderStatus</th>
							<th>nota</th>
							<th>createdBy</th>
							<th>created</th>
							<th>updatedBy</th>
							<th>updated</th>
							<th>acciones</th>
			            </tr>
					</thead>
				</table>                
                                
            </div>
        </div>
    </div>
</div>


<script>
	var table = null;

    $(function () {
        
        fn_build_table();

    });	

	function fn_build_table(){

		table = $('#table-order').DataTable({
			"serverSide": true,
			"processing": true,
			"responsive": true,
			"ajax":{
				url :"<?php echo base_url()?>Order/get_list_dt", // json datasource
				type: "post", 
			},
	        "columnDefs":[
	                    {
	                    "targets": [ 4 ],
	                    render: $.fn.dataTable.render.number( '.', ',', 0, '$ ' )
	                     },
	            ],			
		})

	};

</script>
