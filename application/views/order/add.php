<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Order Add</h3>
            </div>
            <?php echo form_open('order/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="items_count" class="control-label">Items Count</label>
						<div class="form-group">
							<input type="text" name="items_count" value="<?php echo $this->input->post('items_count'); ?>" class="form-control" id="items_count" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="sub_total" class="control-label">Sub Total</label>
						<div class="form-group">
							<input type="text" name="sub_total" value="<?php echo $this->input->post('sub_total'); ?>" class="form-control" id="sub_total" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="total" class="control-label">Total</label>
						<div class="form-group">
							<input type="text" name="total" value="<?php echo $this->input->post('total'); ?>" class="form-control" id="total" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="idUser" class="control-label">IdUser</label>
						<div class="form-group">
							<input type="text" name="idUser" value="<?php echo $this->input->post('idUser'); ?>" class="form-control" id="idUser" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="createdBy" class="control-label">CreatedBy</label>
						<div class="form-group">
							<input type="text" name="createdBy" value="<?php echo $this->input->post('createdBy'); ?>" class="form-control" id="createdBy" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="updatedBy" class="control-label">UpdatedBy</label>
						<div class="form-group">
							<input type="text" name="updatedBy" value="<?php echo $this->input->post('updatedBy'); ?>" class="form-control" id="updatedBy" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="created" class="control-label">Created</label>
						<div class="form-group">
							<input type="text" name="created" value="<?php echo $this->input->post('created'); ?>" class="form-control" id="created" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="updated" class="control-label">Updated</label>
						<div class="form-group">
							<input type="text" name="updated" value="<?php echo $this->input->post('updated'); ?>" class="form-control" id="updated" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="state" class="control-label">State</label>
						<div class="form-group">
							<input type="text" name="state" value="<?php echo $this->input->post('state'); ?>" class="form-control" id="state" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="items" class="control-label">Items</label>
						<div class="form-group">
							<textarea name="items" class="form-control" id="items"><?php echo $this->input->post('items'); ?></textarea>
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>