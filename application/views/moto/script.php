<script>
	var select2Params = {placeholder: 'Seleccione ...', allowClear: true};
	var base_url = '<?php echo base_url() ?>';

	function limpiarSlug(slug){
		slug = slug.toLowerCase();
		slug = slug.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
		slug = slug.replace(/ /g,"-");
		slug = slug.replace((/[^A-Za-z0-9]+/g),"_");
		return slug;
	}

	function formOnSubmit(){
		var valid = validForm();
		if (valid.success) {
			return true;
		}else{
			showErrors(valid.error);
			return false;
		}
	}

	function showErrors(arr){
		$('#form-error').parent().slideUp();
		$('#form-error').html('');
		arr.forEach((item, index) => {
			console.log(item);
			var li = $('<li>', {
				class : 'list-group-item', 
				html : item
			})
			$('#form-error').append(li);
		})
		$('#form-error').parent().slideDown();
	}

	function validForm(){
		var success = {
			error : [], 
			success : true
		};
		
		if ($('#sku').val() == 0) {
			success.error.push('SKU: Escribe un sku'); 
			success.success = false; 
		}

		if ($('#idMarca').val() == 0) {
			success.error.push('Marca: selecciona una marca'); 
			success.success = false; 
		}

		if ($('#idModelo').val() == 0) {
			success.error.push('Modelo: selecciona un modelo'); 
			success.success = false; 
		}

		if ($('#year').val() == 0) {
			success.error.push('Año: Escribe el año del modelo moto'); 
			success.success = false; 
		}

		if ($('#nombre').val() == '') {
			success.error.push('Nombre: Escribe un nombre'); 
			success.success = false; 
		}

		if ($('#descripcion').val() == '') {
			success.error.push('Descripcion: escribe algo para la descripción'); 
			success.success = false; 
		}

		return success;
	}
	$(() =>{
		$('#idMarca').change(() => {
			var idMarca = ($('#idMarca').val() == 1 ? '' : $('#idMarca').val());
			var selectModelo = $('select#modelo')
			selectModelo.html('<option value="0">Cargando ...</option>').slideUp(()=>{
				$.get( base_url + "modelo/getModeloJson/" + idMarca)
				 .done( data => {
				 	selectModelo.html('');
				 	var option = $('<option>',  {
						value : 0, 
						text  : 'Seleccione ' + 'modelo' 
					});
					selectModelo.append(option);
					for (var i = 0; i < data['modelo'].length; i++) {
						option = $('<option>',  {
							value : data['modelo'][i].idModelo, 
							text  : data['modelo'][i].nombre 
						});
						selectModelo.append(option);
					}
					selectModelo.select2(select2Params);
				}).error( err => {
					let option = $('<option>',  {
						value : 0, 
						text  : 'Error al cargar ' + 'modelo'
					});
					selectModelo.append(option);
					selectModelo.select2({placeholder: 'Error al cargar',allowClear: true});
				})

				selectModelo.slideDown();
				document.getElementById('modelo').removeAttribute('disabled');
			})
		})

		$('#nombre').on('change', () => {
			var name = $('#nombre').val();
			name = limpiarSlug(name);
			$('#slug').val(name);
		})

		$('#idMarca').select2(select2Params);
		$('#year').select2(select2Params);
	})
</script>