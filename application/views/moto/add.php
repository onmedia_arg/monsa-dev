<style>
	.valor-input{
		margin-bottom: 3px;
	}
</style>
<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Nueva Moto</h3>
            </div>
            <?php 
            	$attributes = array('id' => 'form', 'onsubmit' => 'return formOnSubmit()');
            	echo form_open('moto/add', $attributes);
            ?>
          	<div class="box-body">
          		<div class="row clearfix">
          			<div class="col-md-6">
						<label for="sku" class="control-label">sku</label>
						<div class="form-group">
							<input type="text" name="sku" value="<?php echo $this->input->post('sku'); ?>" class="form-control" id="sku" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="idMarca" class="control-label">Marca</label>
						<div class="form-group">
							<select name="idMarca" class="form-control" id="idMarca"/>
								<option value="0">Seleccione marca</option>
								<?php 
								foreach($all_marca as $marca)
									{
										$selected = ($marca['idMarca'] == $this->input->post('idMarca')) ? ' selected="selected"' : "";

										printf(
											'<option value="%s" %s>%s</option>',
												$marca['idMarca'],
												$selected,
												$marca['nombre']
										);
									} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="modelo" class="control-label">Modelo</label>
						<div class="form-group">
							<select name="modelo" class="form-control" id="modelo"  disabled="disabled"/>
								<option value="0">Seleccione Modelo</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="year" class="control-label">Año</label>
						<div class="form-group">
							<select class="year form-control" name="year" id="year">
							  <option value="0">Seleccione Año</option>
							  <option value="todos">Todos</option>
							  <?php 
								for ($i = date('Y'); $i > (date('Y') - 100); $i--) { 
									printf('<option value="%s">%s</option>', $i, $i);
								}
							  ?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="nombre" class="control-label">Nombre</label>
						<div class="form-group">
							<input type="text" name="nombre" value="<?php echo $this->input->post('nombre'); ?>" class="form-control" id="nombre" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="descripcion" class="control-label">Descripcion</label>
						<div class="form-group">
							<input type="text" name="descripcion" value="<?php echo $this->input->post('descripcion'); ?>" class="form-control" id="descripcion" />
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Guardar
            	</button>
            	<div style="display: none;">
	                <h4 class="text text-danger">Ocurrió un error</h4>
	                <ul class="text text-warning list-group" id="form-error"></ul>
	            </div>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>

<?php $this->load->view('moto/script') ?>