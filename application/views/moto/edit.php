<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Actualizar Moto</h3>
            </div>
			<?php 
            	$attributes = array('id' => 'form', 'onsubmit' => 'return formOnSubmit()'); 
				echo form_open('moto/edit/'.$moto['idMoto'], $attributes);
			?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="sku" class="control-label">sku</label>
						<div class="form-group">
							<input type="text" name="sku" value="<?php echo ($this->input->post('sku')?$this->input->post('sku'):$moto['sku']); ?>" class="form-control" id="sku" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="idMarca" class="control-label">Marca</label>
						<div class="form-group">
							<select name="idMarca" class="form-control" id="idMarca"/>
								<option value="0">Seleccione marca</option>
								<?php 
								foreach($all_marca as $marca)
									{
										$selected = ($marca['idMarca'] == $this->input->post('idMarca') || $marca['idMarca'] == $moto['idMarca']) ? ' selected="selected"' : "";

										printf(
											'<option value="%s" %s>%s</option>',
												$marca['idMarca'],
												$selected,
												$marca['nombre']
										);
									} 
								?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="modelo" class="control-label">Modelo</label>
						<div class="form-group">
							<select name="modelo" class="form-control" id="modelo"  disabled="disabled"/>
								<option value="0">Seleccione Modelo</option>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="year" class="control-label">Año</label>
						<div class="form-group">
							<select class="year form-control" name="year" id="year">
							  <option value="0">Seleccione Año</option>
							  <option value="todos">Todos</option>
							  <?php 
								for ($i = date('Y'); $i > (date('Y') - 100); $i--) { 
									$selected = ($i == $moto['year'])?'selected="selected"':'';
									printf('<option value="%s" %s>%s</option>', $i, $selected, $i);
								}
							  ?>
							</select>
						</div>
					</div>
					<div class="col-md-6">
						<label for="nombre" class="control-label">Nombre</label>
						<div class="form-group">
							<input type="text" name="nombre" value="<?php echo ($this->input->post('name') ? $this->input->post('name') : $moto['name']); ?>" class="form-control" id="nombre" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="descripcion" class="control-label">Descripción</label>
						<div class="form-group">
							<input type="text" name="descripcion" value="<?php echo ($this->input->post('descripcion') ? $this->input->post('descripcion') : $moto['descripcion']); ?>" class="form-control" id="descripcion" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Guardar
				</button>
				<div style="display: none;">
	                <h4 class="text text-danger">Ocurrió un error</h4>
	                <ul class="text text-warning list-group" id="form-error"></ul>
	            </div>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>

<script>
	var select2Params = {placeholder: 'Seleccione ...', allowClear: true};
	var base_url = '<?php echo base_url() ?>';

	function limpiarSlug(slug){
		slug = slug.toLowerCase();
		slug = slug.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
		slug = slug.replace(/ /g,"-");
		slug = slug.replace((/[^A-Za-z0-9]+/g),"_");
		return slug;
	}

	function formOnSubmit(){
		var valid = validForm();
		if (valid.success) {
			return true;
		}else{
			showErrors(valid.error);
			return false;
		}
		return false;
	}

	function showErrors(arr){
		$('#form-error').parent().slideUp();
		$('#form-error').html('');
		arr.forEach((item, index) => {
			console.log(item);
			var li = $('<li>', {
				class : 'list-group-item', 
				html : item
			})
			$('#form-error').append(li);
		})
		$('#form-error').parent().slideDown();
	}

	function validForm(){
		var success = {
			error : [], 
			success : true
		};
		
		if ($('#sku').val() == 0) {
			success.error.push('SKU: Escribe un sku'); 
			success.success = false; 
		}

		if ($('#idMarca').val() == 0) {
			success.error.push('Marca: selecciona una marca'); 
			success.success = false; 
		}

		if ($('#modelo').val() == 0) {
			success.error.push('Modelo: selecciona un modelo'); 
			success.success = false; 
		}

		if ($('#year').val() == 0) {
			success.error.push('Año: Escribe el año del modelo moto'); 
			success.success = false; 
		}

		if ($('#nombre').val() == '') {
			success.error.push('Nombre: Escribe un nombre'); 
			success.success = false; 
		}

		if ($('#descripcion').val() == '') {
			success.error.push('Descripcion: escribe algo para la descripción'); 
			success.success = false; 
		}
		
		return success;
	}
	$(() =>{
		$('#idMarca').change(() => {
			var idMarca = ($('#idMarca').val() == 1 ? '' : $('#idMarca').val());
			var selectModelo = $('select#modelo')
			selectModelo.html('<option value="0">Cargando ...</option>').slideUp(()=>{
				$.get( base_url + "modelo/getModeloJson/" + idMarca)
				 .done( data => {
				 	selectModelo.html('');
				 	var option = $('<option>',  {
						value : 0, 
						text  : 'Seleccione ' + 'modelo' 
					});
					selectModelo.append(option);
					for (var i = 0; i < data['modelo'].length; i++) {
						option = $('<option>',  {
							value : data['modelo'][i].idModelo, 
							text  : data['modelo'][i].nombre 
						});
						selectModelo.append(option);
					}
					selectModelo.select2(select2Params);
					<?php if ($moto["idModelo"]) { ?>
							$('#modelo').val('<?php echo  $moto["idModelo"]?>');
							$('#modelo').trigger('change');
					<?php } ?>
				}).error( err => {
					let option = $('<option>',  {
						value : 0, 
						text  : 'Error al cargar ' + 'modelo'
					});
					selectModelo.append(option);
					selectModelo.select2({placeholder: 'Error al cargar',allowClear: true});

					
				})

				selectModelo.slideDown();
				document.getElementById('modelo').removeAttribute('disabled');
			})
		})

		$('#nombre').on('change', () => {
			var name = $('#nombre').val();
			name = limpiarSlug(name);
			$('#slug').val(name);
		})

		$('#idMarca').select2(select2Params);
		$('#year').select2(select2Params);
		$('#idMarca').trigger('change');
		$('#idModelo').trigger('change');
	})
</script>