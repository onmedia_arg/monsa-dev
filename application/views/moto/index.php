<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Listado de Motos</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('moto/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
    						<th>IdMoto</th>
    						<th>sku</th>
    						<th>Marca</th>
                            <th>Modelo</th>
    						<th>Año</th>
                            <th>Nombre</th>
                            <th>Descipción</th>
    						<th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($moto as $a){ ?>
                        <tr>
                            <td><?php echo $a['idMoto']; ?></td>
                            <td><?php echo $a['sku']; ?></td>
                            <td><?php echo $a['marca']; ?></td>
                            <td><?php echo $a['modelo']; ?></td>
                            <td><?php echo $a['year']; ?></td>
                            <td><?php echo $a['nombre']; ?></td>
    						<td><?php echo $a['descripcion']; ?></td>
    						<td>
                                <a href="<?php echo site_url('moto/edit/'.$a['idMoto']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                                <a href="<?php echo site_url('moto/remove/'.$a['idMoto']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                                
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('script/dataTable') ?>