<style type="text/css">
	pre {
	    background: lightgray;
	    text-align: left;
	    border: solid grey 1px;
	    padding: 5px;
	}
</style>

<div class="row">
    <div class="col-md-12"> 
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Cook</h3> 
            </div>
            <div class="box-body table-responsive">

			<div class="nav-tabs-custom">

	            <ul class="nav nav-tabs">
	              <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Sku</a></li>
	              <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Slug</a></li> 
	            </ul>
	            <div class="tab-content">
	              	<div class="tab-pane active" id="tab_1">
						<form style="margin-bottom: 25px;" method="post" action="">

						  	<div class="form-group">
							    <label for="exampleInputEmail1">Carpetas <i class="fa fa-info" data-toggle="tooltip" title="Podés seleccionar multiples carpetas para realizar la subida"></i> </label>
								<select name="folders[]" class="form-control" multiple="" required="">
									<?php foreach ( $folders as $folder ): ?>
										<option value="<?= $folder ?>"><?= $folder ?></option>
									<?php endforeach; ?>
								</select>
						  	</div>

						  	<div class="form-group">
						    	<label for="exampleInputEmail1">Longitud de SKU <i class="fa fa-info" data-toggle="tooltip" title="Se refiere a la longitud de caracteres que tiene SKU en el nombre del archivo. EJ: 1234-X.jpg donde 1234 es el SKU de 4 caracteres"></i> </label>
								<input name="sku_lenght" type="number" class="form-control" placeholder="Longitud de SKU" required="">
						  	</div>

							<input type="radio" name="sku" value="1" checked="" style="display: none;">

							<button class="btn btn-success">Enviar</button>

						</form>
					</div>

	              	<div class="tab-pane" id="tab_2">
	              		<div class="alert alert-warning">
	              			La subida masiva por slug, toma el nombre del archivo como slug y lo compara contra la base de datos. EJ: <pre style="display: inline;">cubierta_metzeler_me_1_1980.jpg</pre> donde el nombre "cubierta_metzeler_me_1_1980" es el slug a buscar.
	              		</div>
						<form style="margin-bottom: 25px;" method="post" action="">

						  	<div class="form-group">
							    <label for="exampleInputEmail1">Carpetas</label>
								<select name="folders[]" class="form-control" multiple="" required="">
									<?php foreach ( $folders as $folder ): ?>
										<option value="<?= $folder ?>"><?= $folder ?></option>
									<?php endforeach; ?>
								</select>
						  	</div> 

							<input type="radio" name="slug" value="1" checked="" style="display: none;">

							<button class="btn btn-success">Enviar</button>

						</form>
					</div>
				</div>
	
			</div>

			<script type="text/javascript">
				
				$(document).ready(function(){
					$('select').select2()
				})

				$(document).on('shown.bs.tab', function (e) { 
					$('select').select2()
				});

			</script>
	
				<!-- LOG -->
				<?php if ( isset( $output ) ): ?>
				<h3>Log</h3>
				<div style="max-height: 500px; overflow: auto; border: solid grey 1px; padding: 15px;">
					<?= $output ?>
				<?php endif; ?>
				</div>

			</div>
        </div>
    </div>
</div>