<div class="row">
    <div class="col-md-12">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Editar Cliente</a></li>
                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Usuarios Asociados</a></li>
                <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Ordenes</a></li> 
                <!-- <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li> -->
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <?php 
                        $attributes = array('id' => 'form', 'onsubmit' => 'return formOnSubmit()');
                        echo form_open('clientes/edit/'.$cliente['idCliente'], $attributes); 
                    ?>
                    <div class="box-body">

                        <div class="form-group col-md-6">
                            <label for="razon_social">Nombre / Razón Social</label>
                            <input type="text" name="razon_social" id="razon_social" class="form-control mb-1" placeholder="Nombre / Razón Social" value="<?php echo ($this->input->post('razon_social') ? $this->input->post('razon_social') : $cliente['razon_social']); ?>" required autofocus>
                        </div>  

                        <div class="form-group col-md-6">
                            <label for="tipo_cliente">Tipo de Usuario</label>
                            <select type="text" name="tipo_cliente" id="tipo_cliente" class="form-control mb-1">

                                <option value="" disabled="" hidden="" selected="">Tipo de Usuario</option>
                                <?php foreach ($tipos_usuarios as $tu => $info): ?>
                                    <option value="<?= $info['idTipoCliente'] ?>" <?php if( (int) $cliente['idTipoCliente'] == (int) $info['idTipoCliente'] ){ echo 'selected'; } ?> >
                                        <?= $info['texto'] ?>
                                    </option>
                                <?php endforeach; ?>

                            </select>
                        </div>  

                        <div class="form-group col-md-6">
                            <label for="cuit">CUIT</label>
                            <input type="text" name="cuit" id="cuit" class="form-control mb-1" placeholder="CUIT" value="<?php echo ($this->input->post('cuit') ? $this->input->post('cuit') : $cliente['cuit']); ?>" required>
                        </div> 

                        <div class="form-group col-md-6">
                            <label for="telefono">Telefono</label>
                            <input type="text" name="telefono" id="telefono" class="form-control mb-1" placeholder="Telefono" value="<?php echo ($this->input->post('telefono') ? $this->input->post('telefono') : $cliente['telefono']); ?>" required>
                        </div> 

                        <div class="form-group col-md-6">
                            <label for="mail">Mail</label>
                            <input type="text" name="mail" id="mail" class="form-control mb-1" placeholder="Mail" value="<?php echo ($this->input->post('mail') ? $this->input->post('mail') : $cliente['mail']); ?>" required>
                        </div> 

                        <div class="form-group col-md-6">
                            <label for="direccion">Dirección</label>
                            <input type="text" name="direccion" id="direccion" class="form-control mb-1" placeholder="Dirección" value="<?php echo ($this->input->post('direccion') ? $this->input->post('direccion') : $cliente['direccion']); ?>" required>
                        </div>   

                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success pull-right">
                            <i class="fa fa-check"></i> Guardar
                        </button>
                        <div style="display: none;">
                            <h4 class="text text-danger">Ocurrió un error</h4>
                            <ul class="text text-warning list-group" id="form-error"></ul>
                        </div>
                    </div>              
                    <?php echo form_close(); ?>

                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <table class="table">
                        <thead>
                            <tr> 
                                <th>User Id</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Auth Level</th>
                                <th>Banned</th> 
                                <th>Created At</th>
                                <th>Modified At</th>
                                <?php if ($user['level'] == '9'): ?>
                                    <th>Acciones</th>
                                <?php endif ?> 
                            </tr>
                        </thead>
                        <tbody>
                            
                        <?php foreach($usuarios as $u): ?>
                            <tr>
                                <td><?php echo $u['user_id']; ?></td>
                                <td><?php echo $u['username']; ?></td>
                                <td><?php echo $u['email']; ?></td>
                                <td><?php echo $u['auth_level']; ?></td>
                                <td><?php echo $u['banned']; ?></td> 
                                <td><?php echo $u['created_at']; ?></td>
                                <td><?php echo $u['modified_at']; ?></td>
                                <?php if ($user['level'] == 9): ?>
                                    <td>
                                        <a href="<?php echo site_url('user/edit/'.$u['user_id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Editar</a> 
                                        <a href="<?php echo site_url('user/remove/'.$u['user_id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Borrar</a>
                                    </td>
                                <?php endif ?>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">
                    <table class="table">
                        <thead>
                            <tr> 
                                <th>ID</th>
                                <th>Usuario</th>
                                <th>Cliente</th>
                                <th>Total</th>
                                <th>Status</th> 
                                <th>Nota</th>
                                <th>Fecha Creacion</th>
                                <th>Fecha Actualizacion</th>
                                <?php if ($user['level'] == '9'): ?>
                                    <th>Acciones</th>
                                <?php endif ?> 
                            </tr>
                        </thead>
                        <tbody>
                            
                        <?php foreach($ordenes as $o): ?>
                            <tr>
                                <td><?php echo $o['idOrderHdr']; ?></td>
                                <td><?php echo $o['idUser']; ?></td>
                                <td><?php echo $o['idCliente']; ?></td>
                                <td><?php echo $o['total']; ?></td>
                                <td><?php echo $o['idOrderStatus']; ?></td> 
                                <td><?php echo $o['nota']; ?></td>
                                <!-- <td><?php echo $o['createdBy']; ?></td> -->
                                <td><?php echo $o['created']; ?></td>
                                <!-- <td><?php echo $o['updatedBy']; ?></td> -->
                                <td><?php echo $o['updated']; ?></td>


                                <?php if ($user['level'] == 9): ?>
                                    <td>
                                        <a href="#" data-id="<?= $o['idOrderHdr'] ?>" data-toggle="modal" data-target="#order-details" class="btn btn-info btn-xs">
                                            <span class="fa fa-eye"></span> Detalle
                                        </a>
                                    </td>
                                <?php endif ?>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                    </table>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </div>
</div>

<div class="modal fade in" id="order-details">
    <div class="modal-dialog modal-lg">

        <section class="invoice modal-content">
              <!-- title row -->
              <div class="row">
                <div class="col-xs-12">
                  <h2 class="page-header">
                    <i class="fa fa-globe"></i> Detalle de Orden
                    <small class="pull-right">Fecha: <span id="order-date"></span></small>
                  </h2>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div style="margin-bottom: 15px;" class="row invoice-info">
 
                    <div class="col-sm-4 invoice-col"> 
                      <b>Orden:</b> #<span id="order-id"></span></b><br> 
                      <b>Razon Social:</b> <span id="order-rs"></span></b><br> 
                      <b>CUIT:</b> <span id="order-cuit"></span>
                    </div> 

                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col"> 

                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 invoice-col"> 
                      <b>Email:</b> <span id="order-email"></span><br> 
                      <b>Telefono:</b> <span id="order-tel"></span><br> 
                      <b>Dirección:</b> <span id="order-address"></span>

                    </div>
                    <!-- /.col -->

              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-xs-12 table-responsive">
                    <table id="order-details-table" class="table table-striped no-dt">
                        <thead>
                            <tr>  
<!--                                 <th>idOrderItm</th>
                                <th>idOrderHdr</th> -->
                                <th>posnr</th>
                                <th>idProducto</th>
                                <th>precio</th>
                                <th>qty</th>
                                <th>total</th> 
                                <!-- <th>options</th>  -->
                                <!-- <th>idItemStatus</th>  -->
<!--                                 <th>createdBy</th> 
                                <th>createdDate</th> 
                                <th>updatedBy</th>  -->
                                <!-- <th>updatedDate</th>  -->
                            </tr>
                        </thead>
                        <tbody>  

                            <!-- Ajax -->

                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-xs-6">
 
                </div>
                <!-- /.col -->
                <div class="col-xs-6"> 
                  <div class="table-responsive">
                    <table class="table no-dt">
                        <tbody> 
                          <tr>
                            <th>Total:</th>
                            <td><span id="order-total"></span></td>
                          </tr>
                        </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row --> 
            </section>
 
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>


<script type="text/javascript">
    
    var base_url = '<?= base_url() ?>';
    var unblockConfig = {
                message: null,
            css:{
                backgroundColor: 'transparent',
                border: 'none', 
            },
            overlayCSS: {
                backgroundColor: '#FFF',
            }
        }; 

    $(document).ready(function(){ 
 
        $('.tab-pane .table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });

        $('.is_activo').bootstrapSwitch();

        $(document).on('switchChange.bootstrapSwitch', '[name="is_activo"]', function(){ 

            if( $(this).is(':checked') ){
                var active = 1;
            }else{
                var active = 0;
            }

            $.getJSON( base_url + 'Clientes/update_client_status/' + $(this).val() + '/' + active, function(data){

                if ( data.code == 1 ) {

                    iziToast.success({
                        title: 'OK!',
                        message: data.message,
                    });

                }else{

                    iziToast.error({
                        title: 'Error!',
                        message: data.message,
                    });

                }
 
            })

        })

        $(document).on('click', '[data-target="#order-details"]', function(){ 

            var id_order = $(this).data('id')

            $('#order-details-table').block( unblockConfig );
            $('#order-details-table tbody').empty()

            $.getJSON( base_url + 'Order/get_order_json/' + id_order, function(data){

                var html = '';
                data['details'].forEach(function( item, index ){
                    
                    console.log( item.producto )

                    html+= '<tr>'
                        // html+= '<td>'+ item.idOrderItm +'</td>'
                        // html+= '<td>'+ item.idOrderHdr +'</td>'
                        html+= '<td>'+ item.posnr +'</td>'
                        html+= '<td>'+ item.producto.nombre +'</td>'
                        html+= '<td>'+ item.producto.precio +'$</td>'
                        html+= '<td>'+ item.qty +'</td>'
                        html+= '<td>'+ item.total +'$</td>'
                        // html+= '<td>'+ item.options +'</td>'
                        // html+= '<td>'+ item.idItemStatus +'</td>'
                        // html+= '<td>'+ item.createdBy +'</td>'
                        // html+= '<td>'+ item.createdDate +'</td>'
                        // html+= '<td>'+ item.updatedBy +'</td>'
                        // html+= '<td>'+ item.updatedDate +'</td>'  
                    html+= '</tr>'

                }) 

                $('#order-details-table').unblock();
                $('#order-details-table tbody').html( html )
                $('#order-date').text( data['order'].created )
                $('#order-id').text( data['order'].idOrderHdr ) 
                $('#order-total').text( data['order'].total + '$' ) 
                $('#order-email').text( data['client'].mail )
                $('#order-address').text( data['client'].direccion )
                $('#order-cuit').text( data['client'].cuit )
                $('#order-rs').text( data['client'].razon_social )
                $('#order-tel').text( data['client'].telefono )

            }) 

        })

    })

</script> 
 
<?php $this->load->view('clientes/script') ?>