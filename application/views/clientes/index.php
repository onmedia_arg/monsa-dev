<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Mostrar Clientes</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('clientes/add'); ?>" class="btn btn-success btn-sm">Nuevo Cliente</a> 
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="sortable" class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Razon Social</th>
                                <th>Tipo</th>
                                <th>CUIT</th>
                                <th>Dirección</th>
                                <th>Teléfono</th>
                                <th>Email</th>
                                <th>Activo</th>
                                <th>Fecha Creación</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($cliente as $c){ ?>
                            <tr data-id="<?= $c['idCliente']; ?>">
                                <td><?= $c['idCliente']; ?></td>
                                <td><?= $c['razon_social']; ?></td>
                                <td><?= $this->TipoCliente_model->get_tc( $c['idTipoCliente'] )['texto'] ?></td>
                                <td><?= $c['cuit']; ?></td>
                                <td><?= $c['direccion']; ?></td>
                                <td><?= $c['telefono']; ?></td>
                                <td><?= $c['mail']; ?></td>
                                <td> 
                                    <input type="checkbox" data-on-text="Si" data-off-text="No" class="is_activo" name="is_activo" value="<?= $c['idCliente']; ?>" <?php if ( $c['is_activo'] == 1 ) { echo 'checked'; } ?>>
                                </td>
                                <td><?= $c['fechaCreacion']; ?></td> 
                                <td>
                                    <a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#client-users" data-id="<?= $c['idCliente']; ?>"><span class="fa fa-users"></span> Usuarios</a>

                                    <a href="<?php echo site_url('clientes/edit/'.$c['idCliente']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                                    
                                    <a href="<?php echo site_url('clientes/remove/'.$c['idCliente']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a> 

                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>                                
            </div>
        </div>
    </div>
</div> 


<div class="modal fade in" id="client-users">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Usuarios Asociados</h4>
            </div>
            <div class="modal-body"> 
                
                <div class="table-responsive">
                    <table id="client-users-table" class="table">
                        <thead>
                            <tr> 
                                <th>User Id</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Auth Level</th>
                                <th>Banned</th> 
                                <th>Created At</th>
                                <th>Modified At</th>  
                                <th>Acciones</th>  
                            </tr>
                        </thead>
                        <tbody> 

                            <!-- Ajax -->

                        </tbody>
                    </table>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button> 
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
  <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    
    var base_url = '<?= base_url() ?>';
    var unblockConfig = {
                message: null,
            css:{
                backgroundColor: 'transparent',
                border: 'none', 
            },
            overlayCSS: {
                backgroundColor: '#FFF',
            }
        }; 

    $(document).ready(function(){

        $('.is_activo').bootstrapSwitch();

        $(document).on('switchChange.bootstrapSwitch', '[name="is_activo"]', function(){ 

            if( $(this).is(':checked') ){
                var active = 1;
            }else{
                var active = 0;
            }

            $.getJSON( base_url + 'Clientes/update_client_status/' + $(this).val() + '/' + active, function(data){

                if ( data.code == 1 ) {

                    iziToast.success({
                        title: 'OK!',
                        message: data.message,
                    });

                }else{

                    iziToast.error({
                        title: 'Error!',
                        message: data.message,
                    });

                }
 
            })

        })

        $(document).on('click', '[data-target="#client-users"]', function(){ 

            var id_client = $(this).data('id')

            $('#client-users-table').block( unblockConfig );
            $('#client-users-table tbody').empty()

            $.getJSON( base_url + 'Clientes/get_client_users_json/' + id_client, function(data){

                var html = '';

                data.forEach(function( item, index ){

                    html+= '<tr>'
                        html+= '<td>'+ item.user_id +'</td>'
                        html+= '<td>'+ item.username +'</td>'
                        html+= '<td>'+ item.email +'</td>'
                        html+= '<td>'+ item.auth_level +'</td>'
                        html+= '<td>'+ item.banned +'</td>'
                        html+= '<td>'+ item.created_at +'</td>'
                        html+= '<td>'+ item.modified_at +'</td>'
                        html+= '<td>' 
                            html+= '<a href="' + base_url + 'user/edit/' + item.user_id + '" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a>';
                            html+= '<a href="' + base_url + 'user/remove/' + item.user_id + '" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>';
                        html+= '</td>'
                    html+= '</tr>'

                })

                $('#client-users-table').unblock();
                $('#client-users-table tbody').append( html )

            })

        })

    })

</script>
<?php $this->load->view('script/dataTable') ?>
