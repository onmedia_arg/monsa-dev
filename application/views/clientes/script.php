<script> 

	function formOnSubmit(){
		var valid = validForm();
		if (valid.success) {
			return true;
		}else{
			showErrors(valid.error);
			return false;
		}
	}

	function showErrors(arr){
		$('#form-error').parent().slideUp();
		$('#form-error').html('');

		arr.forEach((item, index) => {
			console.log(item);
			var li = $('<li>', {
				class : 'list-group-item', 
				html : item
			})
			$('#form-error').append(li);
		})
		
		$('#form-error').parent().slideDown();

	}

	function validForm(){
		var success = {
			error : [], 
			success : true
		};

		if ($('#idMarca').val() == '') {
			success.error.push('Id: Ingresa un id para poder identificar el registro'); 
			success.success = false; 
		}

		if ($('#nombre').val() == '') {
			success.error.push('Nombre: Escribe un nombre'); 
			success.success = false; 
		}

		if ($('#slug').val() == '') {
			success.error.push('Slug: Slug no valido'); 
			success.success = false; 
		}

		return success;
	}  
</script>