<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Crear Cliente</h3>
            </div>
              <?php 
                  $attributes = array('id' => 'form', 'onsubmit' => 'return formOnSubmit()');
                  echo form_open('clientes/add', $attributes);
              ?>

              <div class="box-body"> 

                <div class="form-group col-md-6">
                    <label for="razon_social">Nombre / Razón Social</label>
                    <input type="text" name="razon_social" id="razon_social" class="form-control mb-1" placeholder="Nombre / Razón Social" required autofocus>
                </div>  

                <div class="form-group col-md-6">
                    <label for="tipo_cliente">Tipo de Usuario</label>
                    <select type="text" name="tipo_cliente" id="tipo_cliente" class="form-control mb-1">

                        <option value="" disabled="" hidden="" selected="">Tipo de Usuario</option>
                        <?php foreach ($tipos_usuarios as $tu => $info): ?>
                            <option value="<?= $info['idTipoCliente'] ?>" <?php if( isset( $_POST['tipo_cliente'] ) && $_POST['tipo_cliente'] == $info['idTipoCliente'] ){ echo 'selected'; } ?> >
                                <?= $info['texto'] ?>
                            </option>
                        <?php endforeach; ?>

                    </select>
                </div> 

                <div class="form-group col-md-6">
                    <label for="cuit">CUIT</label>
                    <input type="text" name="cuit" id="cuit" class="form-control mb-1" placeholder="CUIT" required>
                </div> 

                <div class="form-group col-md-6">
                    <label for="telefono">Telefono</label>
                    <input type="text" name="telefono" id="telefono" class="form-control mb-1" placeholder="Telefono" required>
                </div> 

                <div class="form-group col-md-6">
                    <label for="mail">Mail</label>
                    <input type="text" name="mail" id="mail" class="form-control mb-1" placeholder="Mail" required>
                </div> 

                <div class="form-group col-md-6">
                    <label for="direccion">Dirección</label>
                    <input type="text" name="direccion" id="direccion" class="form-control mb-1" placeholder="Dirección" required>
                </div> 

                <div class="form-group col-md-6">
                    <label for="usuario">Usuario</label>
                    <input type="text" name="usuario" id="usuario" class="form-control mb-1" placeholder="Usuario" required>
                </div> 

                <div class="form-group col-md-6">
                    <label for="nombre_usuario">Nombre y Apellido del Usuario</label>
                    <input type="text" name="nombre_usuario" id="nombre_usuario" class="form-control mb-1" placeholder="Nombre y Apellido del Usuario" required>
                </div> 

                <div class="form-group col-md-6">
                    <label for="password_usuario">Password</label>
                    <input type="text" name="password_usuario" id="password_usuario" class="form-control mb-1" placeholder="Password" required>
                </div> 

      				</div>

              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">
                  <i class="fa fa-check"></i> Guardar Cliente
                </button>
                <div style="display: none;">
                  <h4 class="text text-danger">Ocurrió un error</h4>
                  <ul class="text text-warning list-group" id="form-error"></ul>
                </div>
              </div>

      			</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>

<?php $this->load->view('clientes/script') ?>