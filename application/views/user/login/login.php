	<style>
		.decorator {
			background: #000;
			position: relative;
			width: 100%;
		}

		.background-v {
			background: url('<?= site_url('resources/img/v.png') ?>');
			position: relative;
			background-size: 50px;
			z-index: 1; 
		}

		.background-gradient {
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background:linear-gradient(180deg, rgba(0, 0, 0, 100%) 0%, rgb(0, 0, 0,60%) 100%);
			z-index: 2;
			pointer-events: none;
		}	
		.section-login{
			display: flex;
			margin: 0;
			flex-direction: column;
			justify-content: space-between;
			align-items: center;
			height: 100vh;
			width: 100%;
		}
		.form-wrapper{
			margin-top: 100px;
			padding: 20px;
			display: flex;
			border-radius: 10px;
			justify-content: center;
			flex-direction: column;
		}
	</style>			
	
	<body>
		<div class="section-login">
			<div class="container" style="height: 100%;">
				<div class="form-wrapper">
					<div class="row text-center"> 
					<img class="mb-4" src="<?php echo site_url('resources/img/logo-verde.png');?>" alt="" width="300" >
		
						<form class="form-signin" action="<?php echo base_url('start/login')?>" method="post" accept-charset="utf-8">
							
							<!-- Messages -->
							<?php if ( $this->session->flashdata('error_message') !== null ): ?>
								<div class="alert alert-danger text-white"><i class="fa fa-times mr-2"></i><?= $this->session->flashdata('error_message') ?></div>
							<?php endif; ?>
		
							<?php if ( $this->session->flashdata('alert_message') !== null ): ?>
								<div class="alert alert-warning text-white"><i class="fa fa-exclamation mr-2"></i><?= $this->session->flashdata('alert_message') ?></div>
							<?php endif; ?>
		
							<?php if ( $this->session->flashdata('success_message') !== null ): ?>
								<div class="alert alert-success text-white"><i class="fa fa-check mr-2"></i><?= $this->session->flashdata('success_message') ?></div>
							<?php endif; ?>
							
							<label for="login_string" class="sr-only" >Usuario</label>
							<input type="text" name="login_string" id="login_string" class="form-control mb-1" placeholder="Usuario" required autofocus>
							
							<label for="login_pass" class="sr-only">Password</label>
							<input type="password" id="login_pass" name="login_pass" class="form-control" placeholder="Password" 
							<?php 
								if( config_item('max_chars_for_password') > 0 )
									echo 'maxlength="' . config_item('max_chars_for_password') . '"'; 
							?>
							required>
							
							<button class="btn btn-lg btn-block mb-1" style="background-color: #A1F500; color: #333;" name="submit"  type="submit" id="submit_button"  >Ingresar</button>
							<p class="mt-5 mb-3 text-muted">¿No tenés cuenta? <a href="<?= base_url('Start/showRegisterForm') ?>">Regístrate</a> - <a href="<?= base_url('Start/recover') ?>">Olvide mi clave</a></p>
		
						</form>
						<p class="mt-5 mb-3 text-muted">&copy; 2024 - Powered by onMedia</p>
					</div>
				</div>
			</div>
			
			<div class="decorator">
				<div class="background-gradient"></div>
				<div class="background-v" style="padding: 80px 0px;"></div>
			</div>			
		</div>
	</body>


<?php 
	// $link_protocol = USE_SSL ? 'https' : NULL;
	// echo site_url('examples/recover', $link_protocol);
	// echo base_url('start/registerUser'); 
?>
