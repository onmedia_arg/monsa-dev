  <!-- .box-header -->

<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Recuperacion de clave</h3>
            </div>
            <?php 
                echo form_open( base_url('start/registerUser'), ['class' => 'form-horizontal'] ); 
                ?>
                <div  class="box-body">
                    <div class="form-group">
                        <label for="login_string" class="col-sm-2 control-label">Usuario</label>
                        <div class="col-sm-10">
                            <input type="text" name="login_string" id="login_string" class="form-control" autocomplete="off" maxlength="255" placeholder="Ingresa tu usuario"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" name="email" id="email" class="form-control" autocomplete="off" maxlength="255" placeholder="Ingresa tu usuario"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="login_pass" class="col-sm-2 control-label">Contraseña</label>
                        <div class="col-sm-10">
                            <input 
                                type="password" 
                                place-holder="********"
                                name="login_pass" 
                                id="login_pass" 
                                class="form-control" 
                                <?php 
                                    if( config_item('max_chars_for_password') > 0 )
                                        echo 'maxlength="' . config_item('max_chars_for_password') . '"'; 
                                ?> 
                                autocomplete="off" readonly="readonly" onfocus="this.removeAttribute('readonly');" 
                            />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="r_login_pass" class="col-sm-2 control-label">Repetir Contraseña</label>
                        <div class="col-sm-10">
                            <input 
                                type="password" 
                                place-holder="********"
                                name="r_login_pass" 
                                id="r_login_pass" 
                                class="form-control" 
                                <?php 
                                    if( config_item('max_chars_for_password') > 0 )
                                        echo 'maxlength="' . config_item('max_chars_for_password') . '"'; 
                                ?> 
                                autocomplete="off" readonly="readonly" onfocus="this.removeAttribute('readonly');" 
                            />
                        </div>
                    </div>
                    <!-- <p>
                        <?php $link_protocol = USE_SSL ? 'https' : NULL; ?>
                        <a href="<?php echo site_url('examples/recover', $link_protocol); ?>">
                            Recuperar clave
                        </a>
                    </p> -->
                        <input class="btn btn-success" type="submit" name="submit" value="Registrar" id="submit_button"  />
                </div>
            </form>
            <div class="well">
                <pre>
                    <!-- <?php var_dump($_SESSION); var_dump($_POST); (isset($message))?var_dump($message):false;?> -->
                </pre>
            </div>
        </div>
    </div>
</div>
