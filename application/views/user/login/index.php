<!DOCTYPE html>
<html lang="en">
	<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Monsa SRL</title>

    <!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="<?php echo site_url('resources/css/bootstrap.min.css');?>">

	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
			  font-size: 3.5rem;
			}
		}

		html,
		body {
		  height: 100%;
		}

		body {
		  display: -ms-flexbox;
		  display: flex;
		  -ms-flex-align: center;
		  align-items: center;
		  padding-top: 40px;
		  padding-bottom: 40px;
		  background-color: #000;
		}

		.form-signin {
		  width: 100%;
		  max-width: 400px;
		  padding: 15px;
		  margin: auto;
			/* margin-top: 100px;	   */
		}
		.form-signin .checkbox {
		  font-weight: 400;
		}
		.form-signin .form-control {
		  position: relative;
		  box-sizing: border-box;
		  height: auto;
		  padding: 10px;
		  font-size: 16px;
		}
		.form-signin .form-control:focus {
		  z-index: 2;
		}
		.form-signin input[type="email"] {
		  margin-bottom: -1px;
		  border-bottom-right-radius: 0;
		  border-bottom-left-radius: 0;
		}
		.form-signin input[type="password"] {
		  margin-bottom: 10px;
		  border-top-left-radius: 0;
		  border-top-right-radius: 0;
		}   
		.mb-1{
			margin-bottom: 10px !important;
		} 

		.mb-4{
			margin-bottom: 40px;
		}

		.form-signin input {
		    border-radius: 4px !important;
		}
		legend{
			color: #d1d1d1;
    		text-align: left;
    		font-family: "Exo 2" !important;
    		margin-top:20px;
		}
		.btn{
			margin-top: 40px;
		}
		#usuario{
			text-transform: lowercase;
		}
		.h4-msg{
			color: #fff;
		}

		.form-user-login {
			color: #fff;
			font-size: 16px;
			margin-bottom: 2em;
			line-height: 26px;
		}
	
	</style>

	</head>
	    <?php                    
	    if(isset($_view) && $_view)
	        $this->load->view($_view);
	    ?> 

	
	   <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
	   <script src="<?= base_url()?>/resources/js/admin/auth/main.js"></script>
</html>

