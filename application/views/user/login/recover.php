	<body>
		<div class="container">
			<div class="row text-center"> 
				<img class="mb-4" src="<?php echo site_url('resources/img/monsa-srl-logo.png');?>" alt="" width="300" >
				<?php
					if(isset( $confirmation )){ ?>
						<h4 class="h4-msg">Enviamos a su mail los pasos para recuperar su clave</h4>
						
				<?php
						// echo $special_link;
						}else{
				?>
					<form class="form-signin" action="<?php echo base_url('Start/recover')?>" method="post" accept-charset="utf-8">
						<!-- Messages -->
						<?php if ( $this->session->flashdata('error_message') !== null ): ?>
							<div class="alert alert-danger text-white"><i class="fa fa-times mr-2"></i><?= $this->session->flashdata('error_message') ?></div>
						<?php endif; ?>

						<?php if ( $this->session->flashdata('alert_message') !== null ): ?>
							<div class="alert alert-warning text-white"><i class="fa fa-exclamation mr-2"></i><?= $this->session->flashdata('alert_message') ?></div>
						<?php endif; ?>

						<?php if ( $this->session->flashdata('success_message') !== null ): ?>
							<div class="alert alert-success text-white"><i class="fa fa-check mr-2"></i><?= $this->session->flashdata('success_message') ?></div>
						<?php endif; ?>
						
						<label for="email" class="sr-only" >Mail</label>
						<input type="text" name="email" id="email" class="form-control mb-1" placeholder="Ingrese su e-mail" required autofocus>
						
						<button class="btn btn-lg btn-warning btn-block mb-1" name="submit"  type="submit" id="submit_button"  >Enviar</button>
						
					</form>
				<?php } ?>
			
				<p class="mt-5 mb-3 text-muted">&copy; 2021 - Powered by onMedia</p>
			</div>
		</div>
	</body>


<?php 
	// $link_protocol = USE_SSL ? 'https' : NULL;
	// echo site_url('examples/recover', $link_protocol);
	// echo base_url('start/registerUser'); 
?>
