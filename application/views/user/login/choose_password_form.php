<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Community Auth - Choose Password Form View
 *
 * Community Auth is an open source authentication application for CodeIgniter 3
 *
 * @package     Community Auth
 * @author      Robert B Gottier
 * @copyright   Copyright (c) 2011 - 2018, Robert B Gottier. (http://brianswebdesign.com/)
 * @license     BSD - http://www.opensource.org/licenses/BSD-3-Clause
 * @link        http://community-auth.com
 */
?>
	<body>
		<div class="container">
			<div class="row text-center"> 
				<img class="mb-4" src="<?php echo site_url('resources/img/monsa-srl-logo.png');?>" alt="" width="300" >
<?php

$showform = 1;

if( isset( $validation_errors ) )
{
	echo '
		<div class="alert alert-danger text-white">
			<p> Ocurrió el siguiente error al cambiar su contraseña:<br>
				La contraseña debe contener:<br>
				Al menos 8 carácteres<br>
					1 número<br>
					1 letra minúscula<br>
					1 letra mayúscula<br>
			
				El campo CONFIRMAR NUEVA CONTRASEÑA es obligatorio.<br>
				CONTRASEÑA NO ACTUALIZADA
			</p>
		</div>
	';
}
else
{
	$display_instructions = 1;
}

if( isset( $validation_passed ) )
{ ?>
	<h4 class="h4-msg">Su clave fue modificada con éxito</h4>
	<a style="max-width: 400px;margin:auto;" href="<?= base_url()?>" class="btn btn-lg btn-warning btn-block mb-1" >Volver a Ingresar</a>

<?php
	$showform = 0;
}
if( isset( $recovery_error ) )
{
	echo '
		<div class="alert alert-danger text-white"">
			<p class="form-user-login">
				No usable data for account recovery.
			</p>
			<p class="form-user-login">
				Account recovery links expire after 
				' . ( (int) config_item('recovery_code_expiration') / ( 60 * 60 ) ) . ' 
				hours.<br />You will need to use the 
				<a href="/examples/recover">Account Recovery</a> form 
				to send yourself a new link.
			</p>
		</div>
	';

	$showform = 0;
}
if( isset( $disabled ) )
{
	echo '
		<div class="alert alert-danger text-white">
			<p class="form-user-login">
				Account recovery is disabled.
			</p>
			<p class="form-user-login">
				You have exceeded the maximum login attempts or exceeded the 
				allowed number of password recovery attempts. 
				Please wait ' . ( (int) config_item('seconds_on_hold') / 60 ) . ' 
				minutes, or contact us if you require assistance gaining access to your account.
			</p>
		</div>
	';

	$showform = 0;
}
if( $showform == 1 )
{
	if( isset( $recovery_code, $user_id ) )
	{
		if( isset( $display_instructions ) )
		{
			if( isset( $username ) )
			{
				echo '<p class="form-user-login">
					Cambiar clave para el usuario <i>' . $username . '</i><br />
					Ingrese la nueva clave:
				</p>';
			}
			else
			{
				echo '<p>Por favor, ingrese su nueva clave:</p>';
			}
		}

		?>
			<h4 style="color:#fff">Ingrese su nueva clave</h4>
			<div id="form" class="form-signin">
				<?php echo form_open(); ?>
					<fieldset>
						<div>

							<?php
								// PASSWORD LABEL AND INPUT ********************************
								echo form_label('Password','passwd', ['class'=>'sr-only']);

								$input_data = [
									'name'       => 'passwd',
									'id'         => 'passwd',
									'class'      => 'form-control mb-1',
									'placeholder' => 'Ingrese la nueva clave',
									'max_length' => config_item('max_chars_for_password'),

								];
								echo form_password($input_data);
							?>

						</div>
						<div>

							<?php
								// CONFIRM PASSWORD LABEL AND INPUT ******************************
								echo form_label('Confirm Password','passwd_confirm', ['class'=>'sr-only']);

								$input_data = [
									'name'       => 'passwd_confirm',
									'id'         => 'passwd_confirm',
									'class'      => 'form-control',
									'placeholder' => 'Repita la nueva clave',
									'max_length' => config_item('max_chars_for_password')
								];
								echo form_password($input_data);
							?>

						</div>
					</fieldset>
					<div>
						<div>

							<?php
								// RECOVERY CODE *****************************************************************
								echo form_hidden('recovery_code',$recovery_code);

								// USER ID *****************************************************************
								echo form_hidden('user_identification',$user_id);

								// SUBMIT BUTTON **************************************************************
								$input_data = [
									'name'  => 'form_submit',
									'id'    => 'submit_button',
									'value' => 'Confirmar',
									'class' => 'btn btn-lg btn-warning btn-block mb-1'
								];
								echo form_submit($input_data);
							?>

						</div>
					</div>
				</form>
			</div>

			<p class="mt-5 mb-3 text-muted">&copy; 2021 - Powered by onMedia</p>
			</div>
		</div>
	</body>

		<?php
	}
}
/* End of file choose_password_form.php */
/* Location: /community_auth/views/examples/choose_password_form.php */