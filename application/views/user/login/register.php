<style type="text/css">
    
    .container-login{
        position: fixed;
        width: 100%;
        height: 100%;
        overflow: auto;
    }

</style>

<body> 
    <div class="container container-login">
        <div class="row text-center">

            <form class="form-signin" action="<?= base_url('start/registerUser') ?>" method="post" accept-charset="utf-8">
                <img class="mb-4" src="<?php echo site_url('resources/img/monsa-srl-logo.png');?>" alt="" width="300" >
                            
                <!-- Messages -->
                <?php if ( $this->session->flashdata('error_message') !== null ): ?>
                    <div class="alert alert-danger text-white"><i class="fa fa-times mr-2"></i><?= $this->session->flashdata('error_message') ?></div>
                <?php endif; ?>

                <?php if ( $this->session->flashdata('alert_message') !== null ): ?>
                    <div class="alert alert-warning text-white"><i class="fa fa-exclamation mr-2"></i><?= $this->session->flashdata('alert_message') ?></div>
                <?php endif; ?>

                <?php if ( $this->session->flashdata('success_message') !== null ): ?>
                    <div class="alert alert-success text-white"><i class="fa fa-check mr-2"></i><?= $this->session->flashdata('success_message') ?></div>
                <?php endif; ?>

                <legend>Datos del Cliente</legend>
                <div class="form-group">
                    <label for="razon_social" class="sr-only" >Razón Social</label>
                    <input type="text" name="razon_social" id="razon_social" class="form-control mb-1" placeholder="Razón Social" required value="<?= set_value('razon_social') ?>" autofocus>
                </div>  

                <div class="form-group">
                    <!-- <label for="tipo_cliente" class="sr-only" >Tipo de Usuario</label> -->
                    <input style="display: none;"type="text" name="tipo_cliente" id="tipo_cliente" class="form-control mb-1" value="1" >
<!--                     <select type="text" name="tipo_cliente" id="tipo_cliente" class="form-control mb-1">
                        <option value="" disabled="" hidden="" selected="">Tipo de Usuario</option>
                        <?php foreach ($tipos_usuarios as $tu => $info): ?>
                            <option value="<?= $info['idTipoCliente'] ?>" <?php if( isset( $_POST['tipo_cliente'] ) && $_POST['tipo_cliente'] == $info['idTipoCliente'] ){ echo 'selected'; } ?> >
                                <?= $info['texto'] ?>
                            </option>
                        <?php endforeach; ?>
                    </select> -->
                </div>  

                <div class="form-group">
                    <label for="cuit" class="sr-only" >CUIT</label>
                    <input type="text" name="cuit" id="cuit" class="form-control mb-1" placeholder="CUIT" value="<?= set_value('cuit') ?>" required>
                </div> 

                <div class="form-group">
                    <label for="telefono" class="sr-only" >Telefono</label>
                    <input type="text" name="telefono" id="telefono" class="form-control mb-1" placeholder="Telefono" value="<?= set_value('telefono') ?>" required>
                </div> 

                <div class="form-group">
                    <label for="mail" class="sr-only" >Mail</label>
                    <input type="text" name="mail" id="mail" class="form-control mb-1" placeholder="Mail" value="<?= set_value('mail') ?>" required>
                </div> 

                <div class="form-group">
                    <label for="direccion" class="sr-only" >Dirección</label>
                    <input type="text" name="direccion" id="direccion" class="form-control mb-1" placeholder="Dirección" value="<?= set_value('direccion') ?>" required>
                </div> 

                <legend>Datos de Conexión</legend>
                <div class="form-group">
                    <label for="usuario" class="sr-only" >Usuario</label>
                    <input type="text" name="usuario" id="usuario" class="form-control mb-1" placeholder="Usuario" value="<?= set_value('usuario') ?>" required>
                </div> 

                <div class="form-group">
                    <label for="nombre_usuario" class="sr-only" >Nombre y Apellido del Usuario</label>
                    <input type="text" name="nombre_usuario" id="nombre_usuario" class="form-control mb-1" placeholder="Nombre y Apellido del Usuario" value="<?= set_value('nombre_usuario') ?>" required>
                </div> 

                <a class="btn btn-sm btn-primary btn-block mb-1" name="generate_password" id="generate_password" onClick="generate_password()" >Generar Contraseña</a>
                
                <div class="form-group">
                    <label for="password_usuario" class="sr-only" >Password</label>
                    <input type="password" name="password_usuario" id="password_usuario" class="form-control mb-1" placeholder="Password" required>
                </div> 

                <div class="form-group">
                    <label for="r_password_usuario" class="sr-only" >Confirmar Password</label>
                    <input type="password" name="r_password_usuario" id="r_password_usuario" class="form-control mb-1" placeholder="Password" required>
                </div> 

                <div class="form-group">
                    <input type="checkbox" onClick="view_password()"><span class="text-primary"> Ver Password</span>
                </div> 
                


                <button class="btn btn-lg btn-warning btn-block mb-1" name="submit"  type="submit" id="submit_button"  >Enviar</button>
                <p class="mt-5 mb-3 text-muted">¿Ya tenés cuenta? <a href="<?= base_url() ?>">Inicia Sesión</a></p>
                <p class="mt-5 mb-3 text-muted">&copy; 2019 - Powered by onMedia</p>
            </form>
        </div>
    </div>
</body>   

<script>
    function generate_password() {
        var base_url = '<?= base_url() ?>'
        $.get( base_url + "Start/generate_password")
                .done( data => {
                    $('#password_usuario').val(data)
                    $('#r_password_usuario').val(data)
            }).error( err => {
                console.log("Errpr al generar la contraseña")
            })
    }

    function view_password() {
        if($('#password_usuario').get(0).type === "password"){
            $('#password_usuario').get(0).type = "text"
            $('#r_password_usuario').get(0).type = "text"
        }else{
            $('#password_usuario').get(0).type = "password"
            $('#r_password_usuario').get(0).type = "password"
        }
    }
</script>