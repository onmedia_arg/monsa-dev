<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">User Add</h3>
            </div>
            <?php echo form_open('user/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="username" class="control-label">Username</label>
						<div class="form-group">
							<input type="text" name="username" value="<?php echo $this->input->post('username'); ?>" class="form-control" id="username" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="email" class="control-label">Email</label>
						<div class="form-group">
							<input type="text" name="email" value="<?php echo $this->input->post('email'); ?>" class="form-control" id="email" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="passwd" class="control-label">Passwd</label>
						<div class="form-group">
							<input type="password" name="passwd" value="<?php echo $this->input->post('passwd'); ?>" class="form-control" id="passwd" />
						</div>
					</div>

	                <div class="form-group col-md-6">
	                    <label for="cliente">Cliente Asociado</label>
	                    <select type="text" name="cliente" id="cliente" class="form-control mb-1">

	                        <option value="0" disabled="" hidden="" selected="">No Asignar</option>
	                        <?php foreach ($clientes as $c => $info): ?>
	                            <option value="<?= $info['idCliente'] ?>" <?php if( isset( $_POST['cliente'] ) && $_POST['cliente'] == $info['idCliente'] ){ echo 'selected'; } ?>>
	                                <?= $info['razon_social'] ?>
	                            </option>
	                        <?php endforeach; ?>

	                    </select>
	                </div> 

				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>