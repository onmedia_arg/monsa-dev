<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Listado de Usuarios</h3>
            	<div class="box-tools">
            		<?php if ($user['level'] == '9'): ?>
                    	<a href="<?php echo site_url('user/add'); ?>" class="btn btn-success btn-sm">Add</a> 
					<?php endif ?>
                </div>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-striped">
                	<thead>
                    <tr>
							<th>User Id</th>
							<th>Username</th>
							<th>Email</th>
							<th>Auth Level</th>
							<th>Banned</th>
							<th>Activo</th>
							<th>Cliente Activo</th>
							<th>Cliente <i class="fa fa-info" title="Tiene Cliente Asociado" data-toggle="tooltip"></i></th>
							<th>Created At</th>
							<th>Modified At</th>
							<?php if ($user['level'] == '9'): ?>
								<th>Acciones</th>
							<?php endif ?>
	                    </tr>
                	</thead>
                    <tbody>
                    	
	                    <?php foreach($users as $u){ 

	                    	if ( $u['auth_level'] == 1 ) {

	                    		// Cliente esta activo?
	                    		$activeClient = ( $this->Clientes_model->client_is_active_by_user( $u['user_id'] ) ) ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-times text-danger"></i>' ;
		                    	
		                    	// Usuario es cliente?
		                    	if ( $this->Clientes_model->user_has_client( $u['user_id'] ) ){
		                    		$client = $this->Clientes_model->get_client_by_user( $u['user_id'] );
		                    		$client= '<a href="'. base_url('clientes/edit/' . $client['idCliente'] ) .'">' . $client['razon_social'] . '</a>';
		                    	}else{
		                    		$client = '<i class="fa fa-times text-danger"></i>';
		                    	}

		                    	// Esta activo
		                    	$checked = ( $u['is_active'] == 1 ) ? 'checked' : null ;
		                    	$active = '<input type="checkbox" data-on-text="Si" data-off-text="No" class="is_active" name="is_active" value="' . $u['user_id'] . '" ' . $checked . '/>';

	                    	}else{
	                    		$activeClient = 'n/a';
	                    		$client = 'n/a';
	                    		$active = 'n/a';
	                    	}

	                    	?>
	                    <tr>
							<td><?php echo $u['user_id']; ?></td>
							<td><?php echo $u['username']; ?></td>
							<td><?php echo $u['email']; ?></td>
							<td><?php echo $u['auth_level']; ?></td>
							<td><?php echo $u['banned']; ?></td>
							<td align="center">
								<?= $active ?>
							</td>
							<td align="center"><?php echo $activeClient ?></td>
							<td> 
								<?= $client ?>
							</td>
							<td><?php echo $u['created_at']; ?></td>
							<td><?php echo $u['modified_at']; ?></td>
							<?php if ($user['level'] == 9): ?>
								<td>
		                            <a href="<?php echo site_url('user/edit/'.$u['user_id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Editar</a> 
		                            <a href="<?php echo site_url('user/remove/'.$u['user_id']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Borrar</a>
		                        </td>
							<?php endif ?>
	                    </tr>
	                    <?php } ?>

                    </tbody>
                </table>
                                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    var base_url = '<?= base_url() ?>';
    var unblockConfig = {
                message: null,
            css:{
                backgroundColor: 'transparent',
                border: 'none', 
            },
            overlayCSS: {
                backgroundColor: '#FFF',
            }
        }; 

    $(document).ready(function(){

        $('.is_active').bootstrapSwitch();

        $(document).on('switchChange.bootstrapSwitch', '[name="is_active"]', function(){ 

            if( $(this).is(':checked') ){
                var active = 1;
            }else{
                var active = 0;
            }

            $.getJSON( base_url + 'user/update_user_status/' + $(this).val() + '/' + active, function(data){

                if ( data.code == 1 ) {

                    iziToast.success({
                        title: 'OK!',
                        message: data.message,
                    });

                }else{

                    iziToast.error({
                        title: 'Error!',
                        message: data.message,
                    });

                }
 
            })

        })

    })

</script>

<?php $this->load->view('script/dataTable') ?>
