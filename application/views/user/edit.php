<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Editar Usuario</h3>
            </div>
			<?php echo form_open('user/edit/'.$user['user_id']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="form-group col-md-6">
						<label for="username" class="control-label">Username</label>
						<div class="form-group">
							<input type="text" name="username" value="<?php echo ($this->input->post('username') ? $this->input->post('username') : $user['username']); ?>" class="form-control" id="username" />
						</div>
					</div>
					<div class="form-group col-md-6">
						<label for="email" class="control-label">Email</label>
						<div class="form-group">
							<input type="text" name="email" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $user['email']); ?>" class="form-control" id="email" />
						</div>
					</div>
					<div class="form-group col-md-6">
						<label for="auth_level" class="control-label">Auth Level</label>
						<div class="form-group">
							<input type="text" name="auth_level" value="<?php echo ($this->input->post('auth_level') ? $this->input->post('auth_level') : $user['auth_level']); ?>" class="form-control" id="auth_level" />
						</div>
					</div>
					<div class="form-group col-md-6">
						<label for="banned" class="control-label">Banned</label>
						<div class="form-group">
							<input type="text" name="banned" value="<?php echo ($this->input->post('banned') ? $this->input->post('banned') : $user['banned']); ?>" class="form-control" id="banned" />
						</div>
					</div>

	                <div class="form-group col-md-6">
	                    <label for="tipo_cliente">Tipo de Usuario</label>
	                    <select type="text" name="tipo_cliente" id="tipo_cliente" class="form-control mb-1">

	                        <option value="" disabled="" hidden="" selected="">Tipo de Usuario</option>
	                        <?php foreach ($clientes as $c => $info): ?>
	                            <option value="<?= $info['idCliente'] ?>" <?php if( (int) $cliente['idCliente'] == (int) $info['idCliente'] ){ echo 'selected'; } ?> >
	                                <?= $info['razon_social'] ?>
	                            </option>
	                        <?php endforeach; ?>

	                    </select>
	                </div> 

				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
				<a href="<?php echo base_url('start/reset_key/').$user['user_id']; ?>" class="btn btn-warning pull-right">
					<i class="fa fa-check"></i> Recuperar Clave
				</a>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>