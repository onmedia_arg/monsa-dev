<?php 

    function insertLog($data){
    	// Get a reference to the controller object
	    $CI = get_instance();

	    // Cargo modelo de LOGS
	    $CI->load->model('Logs_model');

        $result = $CI->Logs_model->insertLog($data);
	    
	    return $result;
    }

?>