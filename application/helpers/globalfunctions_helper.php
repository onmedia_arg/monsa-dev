<?php 

    function menuFront(){
    	// Get a reference to the controller object
	    $CI = get_instance();

	    // You may need to load the model if it hasn't been pre-loaded
	    $CI->load->model('Categorium_model');
		$CI->load->model('Familium_model');
		$CI->load->model('Marca_model');	    

	    // Call a function of the model
	    $data['nav_cats']   = $CI->Categorium_model->get_all_categoria_join();
	    $data['nav_family'] = $CI->Familium_model->get_all_familia();
	    $data['nav_marcas'] = $CI->Marca_model->get_all_marca();
	    return $data;
    }

    function build_img_url($images, $max_images = 1)
    {
        $quitar     = ['\\'];
        $remplazar  = '';
        $imagenes   = str_replace($quitar, $remplazar, $images);
        $imagenes   = json_decode($imagenes);
        
        if($imagenes != NULL){
            if (count($imagenes) > 0) {
                for ($i=0; $i < count($imagenes) ; $i++) {
                    if ($i < $max_images) {
                        return base_url($imagenes[$i]);
                    }
                }
            }
        }
        else
        {
            return site_url('resources/img/default.jpeg');
        }            
    }

    function build_img_url_new($images, $max_images = 1)
    {
        $quitar     = ['\\'];
        $remplazar  = '';
        $imagenes   = str_replace($quitar, $remplazar, $images);
        $imagenes   = json_decode($imagenes);
    
        $result = [];  // Nuevo array para almacenar las imágenes
    
        if ($imagenes != NULL) {
            if (count($imagenes) > 0) {
                for ($i = 0; $i < count($imagenes); $i++) {
                    if ($i < $max_images) {
                        $result[] = base_url($imagenes[$i]);  // Agregar la imagen al array
                    }
                }
            }
        } else {
            $result[] = site_url('resources/img/default.jpeg');  // Agregar imagen por defecto al array
        }
    
        return $result;  // Devolver el array de imágenes
    }
    



 ?>