<?php 

/**
 * Retornamos el array en formato Json al navegador
 * @array -> tipo array
 */
 
function returnJson($array){

	header('content-type: application/json');
	$json=json_encode($array, JSON_UNESCAPED_UNICODE);

	if(json_last_error()){

		// Vemos si da error la codificaciona json, el porqué
		switch(json_last_error()) {
	        case JSON_ERROR_NONE:
	            echo ' - Sin errores';
	        break;
	        case JSON_ERROR_DEPTH:
	            echo ' - Excedido tamaño máximo de la pila';
	        break;
	        case JSON_ERROR_STATE_MISMATCH:
	            echo ' - Desbordamiento de buffer o los modos no coinciden';
	        break;
	        case JSON_ERROR_CTRL_CHAR:
	            echo ' - Encontrado carácter de control no esperado';
	        break;
	        case JSON_ERROR_SYNTAX:
	            echo ' - Error de sintaxis, JSON mal formado';
	        break;
	        case JSON_ERROR_UTF8:
	            echo ' - Caracteres UTF-8 malformados, posiblemente codificados de forma incorrecta';
	        break;
	        default:
	            echo ' - Error desconocido';
	        break;
    	}

    	print_r($array);

	}else{
		echo $json;
	}

}

function array_find($needle, array $haystack)
{
    foreach ($haystack as $key => $value) {
        if (false !== stripos($value, $needle)) {
            return $key;
        }
    }
    return false;
}