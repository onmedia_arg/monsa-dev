<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ImportProducts {
    private $CI;
    private $client;
    private $Products_model;
    private $Sysoptions;
    private $Apilog;

    public function __construct() {
        $this->CI =& get_instance();
        
        // Cargar los modelos
        $this->CI->load->model('manager/Products_model', 'Products', TRUE);
        $this->CI->load->model('manager/Sysoptions_model', 'Sysoptions', TRUE);
        $this->CI->load->model('manager/Apilog_model', 'Apilog', TRUE);
        
        // Asignar los modelos
        $this->Products_model = $this->CI->Products;
        $this->Sysoptions = $this->CI->Sysoptions;
        $this->Apilog = $this->CI->Apilog;
        
        $this->CI->load->helper("logs");

        $this->client = new \GuzzleHttp\Client();
    }


    public function update_stock() {
        return $this->updateProductField('stock', 'Existencias', 'stock_quantity');
    }

    public function update_base_price() {
        return $this->updateProductField('base_price', 'PrecioBase', 'precio');
    }

    public function update_public_price() {
        return $this->updateProductField('public_price', 'PrecioMinorista', 'price_public');
    }

    private function updateProductField($process_name, $api_field, $db_field) {
        $skus = $this->Products_model->getAllSku();

        $cant_total = count($skus);
        
        // if (!$this->acquireLock($process_name)) {
        //     return $this->jsonResponse(409, "El proceso de actualización de $process_name se encuentra en ejecución");
        // }

        $process_config = $this->Sysoptions->getProcessConfig([
            "{$process_name}_update_batch",
            "{$process_name}_update_endpoint"
        ]);
        
        $batch_size = intval($process_config["{$process_name}_update_batch"]);
        $url_endpoint = $process_config["{$process_name}_update_endpoint"];

        $this->initializeProcess($process_name, $cant_total);

        try {
            $this->processBatch($process_name, $skus, $url_endpoint, $api_field, $db_field);
            // foreach (array_chunk($skus, $batch_size) as $batch) {
            // }
            
            $this->finalizeProcess($process_name);
            return $this->jsonResponse(200, "El proceso de actualización de $process_name finalizó satisfactoriamente");
        } catch (\Throwable $th) {
            $this->handleError($process_name, $th, $url_endpoint);
            return $this->jsonResponse(500, "El proceso de actualización de $process_name no se pudo ejecutar correctamente");
        } finally {
            $this->releaseLock($process_name);
        }
    }

    private function acquireLock($process_name) {
        // Implementar un sistema de bloqueo, por ejemplo, usando Redis o la base de datos
        // Retorna true si se adquiere el bloqueo, false si no
    }

    private function releaseLock($process_name) {
        // Liberar el bloqueo
    }

    private function initializeProcess($process_name, $cant_total) {
        $date_start = date('Y-m-d H:i:s');
        $this->Sysoptions->updateProcessState("{$process_name}_update_status", 'true');
        $this->Sysoptions->updateProcessState("{$process_name}_update_total", $cant_total);
        $this->Sysoptions->updateProcessState("{$process_name}_update_start", $date_start);
        $this->Sysoptions->updateProcessState("{$process_name}_update_processed", 0);
    }

    private function processBatch($process_name, $batch, $url_endpoint, $api_field, $db_field) {
        
        $data = array_map(function($sku) {
            return ["Clave" => str_pad($sku['sku'], 6, '0', STR_PAD_LEFT)];
        }, $batch);
    
        $response = $this->client->request('GET', $url_endpoint, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($data)
        ]);
    
        $list = json_decode($response->getBody()->getContents());
        
        $cant_processed = 0;
        
        foreach ($list as $item) {
            if ($item->Nombre !== 'Codigo Inexistente') {
                $sku = ltrim($item->Clave, "0");
                $value = [$db_field => $item->$api_field];
                $this->Products_model->updateProductSku($sku, $value);
                $cant_processed++;
                $this->custom_log("{$process_name}: SKU: {$sku}, {$db_field}: {$item->$api_field}");

                // log_message('info', "Total procesados: $cant_processed");

                $this->Sysoptions->incrementProcessState("{$process_name}_update_processed", $cant_processed);
            }

        }

        $this->logApiCall($url_endpoint, json_encode($data), $response);
    }

    private function finalizeProcess($process_name) {
        $date_finish = date('Y-m-d H:i:s');
        $this->Sysoptions->updateProcessState("{$process_name}_update_finish", $date_finish);
        $this->Sysoptions->updateProcessState("{$process_name}_update_status", 'false');
    }

    private function handleError($process_name, $th, $url_endpoint) {
        $this->logApiCall($url_endpoint, '', $th->getMessage(), $th->getCode());
        $this->Sysoptions->updateProcessState("{$process_name}_update_status", 'false');
    }

    private function logApiCall($url, $payload, $response, $status = null) {
        $this->Apilog->insertApiLog([
            'url' => $url,
            'payload' => $payload,
            'response' => is_string($response) ? $response : json_encode($response),
            'status' => $status ?? (is_object($response) ? $response->getStatusCode() : null)
        ]);
    }

    private function jsonResponse($code, $message) {
        return json_encode(['code' => $code, 'message' => $message]);
    }

    private function custom_log($message) {
        $log_file = APPPATH . 'logs/custom_' . date('Y-m-d') . '.log';
        $timestamp = date('Y-m-d H:i:s');
        file_put_contents($log_file, "[$timestamp] $message\n", FILE_APPEND);
    }    
}