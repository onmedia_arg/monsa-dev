<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'start';
// $route['default_controller'] = 'manager/Dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['alta-cliente'] = 'Start/showRegisterForm';

$route['api/get_products'] = 'api/get_products';

