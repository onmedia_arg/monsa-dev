$(document).on("click", ".attr-value", function () {
    if (searchInArray($(this).html(), "value") == "-1") {
      data.push({
        // name:  $(this).data("idattr"),
        name: $(this).data("slug"),
        value: $(this).html(),
      });
      $(this).addClass("attr-active");
    } else {
      data.splice(searchInArray($(this).html(), "value"), 1);
  
      $(this).removeClass("attr-active");
    }
  
    if (data.length == 1) {
      get_all_children(data);
    } else {
      get_children_filter(data);
    }
  });

function get_children_filter(data) {
    $.ajax({
      url: window.base + "producto/get_children",
      type: "POST",
      data: { data: data },
      success: function (data) {
        result = JSON.parse(data);
        output = "";
        output =
          "<tr><th>SKU</th><th>Atributo</th><th>Precio</th><th>Cantidad</th><th>Agregar</th></tr>";
  
        for (var i = 0; i < result.length; i++) {
          output +=
            "<tr> \
                                                                  <td>" +
            result[i]["sku"] +
            "</td>";
          output += "<td>";
  
          for (var j = 0; j < result[i]["atributos"].length; j++) {
            output +=
              result[i]["atributos"][j]["nombre"] +
              ": " +
              result[i]["atributos"][j]["valores"] +
              "<br>";
          }
  
          output += "</td>";
  
          output +=
            "<td>" +
            result[i]["show_price"] +
            '</td> \
                                                              <td><input type="number" name="qty" maxlength="12" value="1" title="Quantity:" class="input-text qty" id=' +
            result[i]["idProducto"] +
            '></td> \
                                                              <td><button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart" \
                                                                          data-productname = "' +
            result[i]["prod_nombre"] +
            '" \
                                                                          data-price       = "' +
            result[i]["precio"] +
            '" \
                                                                          data-sku       = "' +
            result[i]["sku"] +
            '" \
                                                                          data-productid   = "' +
            result[i]["idProducto"] +
            '"> Agregar</button></td></tr>';
        }
        $(".child-products").empty();
        $(".child-products").append(output);
      },
      error: function () {
        output = "";
        output = "<h3>No se encontraron productos</h3>";
        $(".child-products").empty();
        $(".child-products").append(output);
      },
    });
  }
  
  function get_all_children(data) {
    $.ajax({
      url: window.base + "producto/get_all_children",
      type: "POST",
      data: { data: data },
      success: function (data) {
        result = JSON.parse(data);
        output = "";
        output =
          "<tr><th>SKU</th><th>Atributo</th><th>Precio</th><th>Cantidad</th><th>Agregar</th></tr>";
  
        for (var i = 0; i < result.length; i++) {
          output +=
            "<tr> \
                                                                  <td>" +
            result[i]["sku"] +
            "</td>";
          output += "<td>";
          attrString = "";
          separador = "";
          for (var j = 0; j < result[i]["atributos"].length; j++) {
            output +=
              result[i]["atributos"][j]["nombre"] +
              ": " +
              result[i]["atributos"][j]["valores"] +
              "<br>";
            separador = " - ";
            if (j + 1 == result[i]["atributos"].length) {
              separador = "";
            }
            attrString += result[i]["atributos"][j]["valores"] + separador;
          }
  
          output += "</td>";
  
          output +=
            "<td>" +
            result[i]["show_price"] +
            '</td> \
                                                              <td><input type="number" name="qty" maxlength="12" value="1" title="Quantity:" class="input-text qty" id=' +
            result[i]["idProducto"] +
            '></td> \
                                                              <td><button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart" \
                                                                          data-productname = "' +
            result[i]["nombre"] +
            '" \
                                                                          data-price       = "' +
            result[i]["precio"] +
            '" \
                                                                          data-sku       = "' +
            result[i]["sku"] +
            '" \
                                                                          data-attributes        = "' +
            attrString +
            '" \
                                                                          data-productid   = "' +
            result[i]["idProducto"] +
            '"> Agregar</button></td></tr>';
        }
        $(".child-products").empty();
        $(".child-products").append(output);
      },
      error: function () {
        output = "";
        output = "<h3>No se encontraron productos</h3>";
        $(".child-products").empty();
        $(".child-products").append(output);
      },
    });
  }
  
  function get_children(data) {
    $.ajax({
      url: window.base + "producto/get_children",
      type: "POST",
      data: { data: data },
      success: function (data) {
        result = JSON.parse(data);
        output = "";
        output =
          "<tr><th>SKU</th><th>Atributo</th><th>Precio</th><th>Cantidad</th><th>Agregar</th></tr>";
  
        for (var i = 0; i < result.length; i++) {
          output +=
            "<tr> \
                                                                  <td>" +
            result[i]["sku"] +
            "</td>";
  
          // for (var j=5; j< result[i].length ; j++ ){
          //    // output += '<td> Medida: ' + result[i][j] + '<br>Rodado: ' + result[i]["rodado"] + '<br>Valvula: ' + result[i]["valvula"] +  '</td>';
          //    output += '<td> Attr1: ' + result[i][j] + '<br>'
          // }
          //  output += '</td>';
          var j = 0;
  
          output += "<td>";
          for (attr in result[i]) {
            j++;
            if (j > 5) {
              output += attr + ": " + result[i][attr] + "<br>";
            }
          }
          output += "</td>";
  
          output +=
            "<td>" +
            result[i]["show_price"] +
            '</td> \
                                                              <td><input type="number" name="qty" maxlength="12" value="1" title="Quantity:" class="input-text qty" id=' +
            result[i]["idProducto"] +
            '></td> \
                                                              <td><button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart" \
                                                                          data-productname = "' +
            result[i]["prod_nombre"] +
            '" \
                                                                          data-price       = "' +
            result[i]["precio"] +
            '" \
                                                                          data-sku       = "' +
            result[i]["sku"] +
            '" \
                                                                          data-productid   = "' +
            result[i]["idProducto"] +
            '"> Agregar</button></td></tr>';
        }
        $(".child-products").empty();
        $(".child-products").append(output);
      },
      error: function () {
        output = "";
        output = "<h3>No se encontraron productos</h3>";
        $(".child-products").empty();
        $(".child-products").append(output);
      },
    });
  }