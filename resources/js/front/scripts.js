var cart_list = "";
var popup_url = "";

const months = {
  0: "ENE",
  1: "FEB",
  2: "MAR",
  3: "ABR",
  4: "MAY",
  5: "JUN",
  6: "JUL",
  7: "AGO",
  8: "SEP",
  9: "OCT",
  10: "NOV",
  11: "DIC",
};

// var shop_layout = readCookie("shop_layout");
// if (shop_layout == null) {
// 		// do cookie doesn't exist stuff;
// 		document.cookie = "shop_layout=default";
// }

// iziToast - settings
iziToast.settings({
  timeout: 3000, // default timeout
  resetOnHover: true,
  // icon: '', // icon class
  transitionIn: "fadeInUp",
  transitionOut: "fadeOut",
  zindex: 999999999999999999,
});

var data = [];
var idOrderHdrDraft = "";
data.push({
  name: "idProducto",
  value: $('input[name="idProducto"]').val(),
});

$(document).ready(function () {
  set_popup();

  var parts = document.location.pathname.split("/");

  var lastSegment = parts.pop() || parts.pop();

  appendWhatsappButton();

  var window_width = $(window).width(),
    window_height = window.innerHeight,
    header_height = $(".default-header").height(),
    header_height_static = $(".site-header.static").outerHeight(),
    fitscreen = window_height - header_height;

  $(".fullscreen").css("height", window_height);
  $(".fitscreen").css("height", fitscreen);

  //------- Active Nice Select --------//

  $(".navbar-nav li.dropdown").hover(
    function () {
      $(this).find(".dropdown-menu").stop(true, true).delay(200).fadeIn(500);
    },
    function () {
      $(this).find(".dropdown-menu").stop(true, true).delay(200).fadeOut(500);
    }
  );

  /*=================================
		Funciones para el HOME
		==================================*/

  /*==========================
		javaScript for sticky header
		============================*/
  $(".sticky-header").sticky();

  /*=================================
		Javascript for banner area carousel
		==================================*/
  $(".active-banner-slider").owlCarousel({
    items: 1,
    autoplay: true,
    autoplayTimeout: 5000,
    loop: true,
    dots: false,
    lazyLoad: true,
    responsive: {
      0: {
        items: 1,
      },
    },
  });

  $("#marcas-carousel").owlCarousel({
    autoplay: true,
    autoplayTimeout: 1000,
    items: 6,
    loop: true,
    margin: 25,
    // nav: false,
    // dots:true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 3,
      },
      1000: {
        items: 6,
      },
    },
  });
  $("#marcas-carousel-ltr").owlCarousel({
    rtl: true,
    autoplay: true,
    autoplayTimeout: 1000,
    items: 6,
    loop: true,
    margin: 25,
    // nav: false,
    // dots:true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 3,
      },
      1000: {
        items: 6,
      },
    },
  });

  /*=================================
		Levantar archivo descargable de la BD
		==================================*/

  getHomeFiles();

  /*=================================
		Javascript for product area carousel
		==================================*/
  $(".active-product-area").owlCarousel({
    items: 1,
    autoplay: true,
    autoplayTimeout: 5000,
    loop: true,
    dots: false,
  });

  $(".product_carousel").owlCarousel();

  /*=================================
		Javascript for single product area carousel
		==================================*/
  $(".s_Product_carousel").owlCarousel({
    items: 1,
    autoplay: false,
    autoplayTimeout: 5000,
    loop: true,
    nav: false,
    dots: true,
  });

  /*=================================
		Javascript for exclusive area carousel
		==================================*/
  $(".active-exclusive-product-slider").owlCarousel({
    items: 1,
    autoplay: false,
    autoplayTimeout: 5000,
    loop: true,
    nav: true,
    navText: [
      "<img src='img/product/prev.png'>",
      "<img src='img/product/next.png'>",
    ],
    dots: false,
  });

  $(".quick-view-carousel-details").owlCarousel({
    loop: true,
    dots: true,
    items: 1,
  });

  /*=================================
		Fn para Productos del HOME
		==================================*/
  //	render_productos_home();

  render_destacados();

  render_notas();

  /*======================================
		Fn para armar el contenido de Side Cart
		=======================================*/
  // $('#show-side-cart').click(function(){
  // 	render_side_cart();
  // });
  render_side_cart();

  /*======================================
		Fn para Mostrar pagina Customer
		=======================================*/

  if (lastSegment == "customer") {
    render_orders_list();

    var table = $("#orders_list").DataTable();

    var pedido = GetURLParameter("pedido");
    if (pedido != undefined && pedido.length > 0) {
      render_order_details(pedido);
      render_order_items(pedido);
    }
  }

  $("#orders_list tbody").on("click", "tr", function () {
    $(this).toggleClass("tr-active");

    var data = table.row(this).data();

    // Se buscan los Items en la BD
    idOrderHdr = data[0]; //idOrderHdr
    render_order_items(idOrderHdr);

    //Se completan los detalles del pedido
    $("#detail-created span").html(data[1]);
    $("#detail-nota span").html(data[5]);
    $("#detail-total span").html(data[3]);
    $("#detail-estado span").html(data[2]);

    var orderStatus = data[4];

    if (orderStatus == 1) {
      // $('#btn-load-draft').attr('data-order_id', idOrderHdr);
      $("#btn-repeat-order").css("display", "none");
      // $('#btn-load-draft').css('display', 'block')
    } else if (orderStatus == 2 || orderStatus == 4) {
      //Si el pedido es Nuevo(enviado) o Finalizado(entregado)
      $("#btn-repeat-order").attr("data-order_id", idOrderHdr);
      // $('#btn-load-draft').css('display', 'none')
      $("#btn-repeat-order").css("display", "block");
    }
  });

  $("#btn-repeat-order").on("click", function () {
    var url = window.base + "Cart/fill_cart_from_order";

    var id_order = $(this).data("order_id");

    $.ajax({
      async: false,
      url: url,
      method: "POST",
      data: { id: id_order },
      success: function (r) {
        result = JSON.parse(r);

        // if ($('#detail-estado span').html() == 1){
        // 	idOrderHdrDraft = id_order
        // }

        iziToast.success({
          title: "OK",
          message: result.message,
        });

        redirect_carrito();
      },
    });
  });

  // $('#btn-load-draft').on('click', function(){

  // 	var url = window.base + 'Cart/fill_cart_from_draft';

  // 	var id_order = $(this).data('order_id')

  // 	$.ajax({
  // 		async  : false,
  // 		url    : url,
  // 		method : 'POST',
  // 		data: {id : id_order},
  // 		success : function(r){
  // 				result = JSON.parse(r);

  // 				iziToast.success({
  // 					title: 'OK',
  // 					message: result.message,
  // 				})

  // 				redirect_carrito()
  // 		}
  // 	})
  // })

  // $("#attr-producto").submit(function (event) {
  //   event.preventDefault();
  //   console.log($(this).serializeArray());

  //   data = $(this).serializeArray();

  //   get_children(data);
  // });

  // $("#clear-filter").on("click", function () {
  //   $("#attr-producto").trigger("reset");
  // });

  // $("#show-all").on("click", function () {
  //   $("#attr-producto").trigger("reset");
  //   data = "";
  //   get_children(data);
  // });

  $(document).on("keypress", function (e) {
    var hasFocus = $("#search").is(":focus");

    if (hasFocus) {
      var search = $("#search").val();

      if (e.which == 13 && search.length > 0) {
        searchForm();
      }
    }
  });
});

async function set_popup() {
  const post = `${window.base}manager/popup/get_popup`;
  const r = await fetch(post, { method: "POST" });
  const popup = await r.json();

  if (popup.active == true) {
    $("#popup").attr("src", popup.image);
    $("#popup").fancybox({ width: 600, height: 600 });
    $("#popup").trigger("click");

    $(".fancybox-content img").on("click", function () {
      window.open(
        popup.url,
        "_blank" // <- This is what makes it open in a new window.
      );
    });
  }
}

function redirect_carrito() {
  window.location.replace(window.base + "frontController/carrito");
}

var parts_url = document.location.pathname.split("/");
// if (window.location.href.indexOf("detalle") > -1) {
//   get_all_children(data);
// }

// $(document).on("click", ".attr-value", function () {
//   if (searchInArray($(this).html(), "value") == "-1") {
//     data.push({
//       // name:  $(this).data("idattr"),
//       name: $(this).data("slug"),
//       value: $(this).html(),
//     });
//     $(this).addClass("attr-active");
//   } else {
//     data.splice(searchInArray($(this).html(), "value"), 1);

//     $(this).removeClass("attr-active");
//   }

//   if (data.length == 1) {
//     get_all_children(data);
//   } else {
//     get_children_filter(data);
//   }
// });

function searchInArray(searchFor, property) {
  var retVal = -1;
  var self = data;
  for (var index = 0; index < self.length; index++) {
    var item = self[index];
    if (item.hasOwnProperty(property)) {
      if (item[property].toLowerCase() === searchFor.toLowerCase()) {
        retVal = index;
        return retVal;
      }
    }
  }
  return retVal;
}

// function build_data() {}

// function get_children_filter(data) {
//   $.ajax({
//     url: window.base + "producto/get_children",
//     type: "POST",
//     data: { data: data },
//     success: function (data) {
//       result = JSON.parse(data);
//       output = "";
//       output =
//         "<tr><th>SKU</th><th>Atributo</th><th>Precio</th><th>Cantidad</th><th>Agregar</th></tr>";

//       for (var i = 0; i < result.length; i++) {
//         output +=
//           "<tr> \
// 																<td>" +
//           result[i]["sku"] +
//           "</td>";
//         output += "<td>";

//         for (var j = 0; j < result[i]["atributos"].length; j++) {
//           output +=
//             result[i]["atributos"][j]["nombre"] +
//             ": " +
//             result[i]["atributos"][j]["valores"] +
//             "<br>";
//         }

//         output += "</td>";

//         output +=
//           "<td>" +
//           result[i]["show_price"] +
//           '</td> \
// 															<td><input type="number" name="qty" maxlength="12" value="1" title="Quantity:" class="input-text qty" id=' +
//           result[i]["idProducto"] +
//           '></td> \
// 															<td><button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart" \
// 																		data-productname = "' +
//           result[i]["prod_nombre"] +
//           '" \
// 																		data-price       = "' +
//           result[i]["precio"] +
//           '" \
// 																		data-sku       = "' +
//           result[i]["sku"] +
//           '" \
// 																		data-productid   = "' +
//           result[i]["idProducto"] +
//           '"> Agregar</button></td></tr>';
//       }
//       $(".child-products").empty();
//       $(".child-products").append(output);
//     },
//     error: function () {
//       output = "";
//       output = "<h3>No se encontraron productos</h3>";
//       $(".child-products").empty();
//       $(".child-products").append(output);
//     },
//   });
// }

// function get_all_children(data) {
//   $.ajax({
//     url: window.base + "producto/get_all_children",
//     type: "POST",
//     data: { data: data },
//     success: function (data) {
//       result = JSON.parse(data);
//       output = "";
//       output =
//         "<tr><th>SKU</th><th>Atributo</th><th>Precio</th><th>Cantidad</th><th>Agregar</th></tr>";

//       for (var i = 0; i < result.length; i++) {
//         output +=
//           "<tr> \
// 																<td>" +
//           result[i]["sku"] +
//           "</td>";
//         output += "<td>";
//         attrString = "";
//         separador = "";
//         for (var j = 0; j < result[i]["atributos"].length; j++) {
//           output +=
//             result[i]["atributos"][j]["nombre"] +
//             ": " +
//             result[i]["atributos"][j]["valores"] +
//             "<br>";
//           separador = " - ";
//           if (j + 1 == result[i]["atributos"].length) {
//             separador = "";
//           }
//           attrString += result[i]["atributos"][j]["valores"] + separador;
//         }

//         output += "</td>";

//         output +=
//           "<td>" +
//           result[i]["show_price"] +
//           '</td> \
// 															<td><input type="number" name="qty" maxlength="12" value="1" title="Quantity:" class="input-text qty" id=' +
//           result[i]["idProducto"] +
//           '></td> \
// 															<td><button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart" \
// 																		data-productname = "' +
//           result[i]["nombre"] +
//           '" \
// 																		data-price       = "' +
//           result[i]["precio"] +
//           '" \
// 																		data-sku       = "' +
//           result[i]["sku"] +
//           '" \
// 																		data-attributes        = "' +
//           attrString +
//           '" \
// 																		data-productid   = "' +
//           result[i]["idProducto"] +
//           '"> Agregar</button></td></tr>';
//       }
//       $(".child-products").empty();
//       $(".child-products").append(output);
//     },
//     error: function () {
//       output = "";
//       output = "<h3>No se encontraron productos</h3>";
//       $(".child-products").empty();
//       $(".child-products").append(output);
//     },
//   });
// }

// function get_children(data) {
//   $.ajax({
//     url: window.base + "producto/get_children",
//     type: "POST",
//     data: { data: data },
//     success: function (data) {
//       result = JSON.parse(data);
//       output = "";
//       output =
//         "<tr><th>SKU</th><th>Atributo</th><th>Precio</th><th>Cantidad</th><th>Agregar</th></tr>";

//       for (var i = 0; i < result.length; i++) {
//         output +=
//           "<tr> \
// 																<td>" +
//           result[i]["sku"] +
//           "</td>";

//         // for (var j=5; j< result[i].length ; j++ ){
//         //    // output += '<td> Medida: ' + result[i][j] + '<br>Rodado: ' + result[i]["rodado"] + '<br>Valvula: ' + result[i]["valvula"] +  '</td>';
//         //    output += '<td> Attr1: ' + result[i][j] + '<br>'
//         // }
//         //  output += '</td>';
//         var j = 0;

//         output += "<td>";
//         for (attr in result[i]) {
//           j++;
//           if (j > 5) {
//             output += attr + ": " + result[i][attr] + "<br>";
//           }
//         }
//         output += "</td>";

//         output +=
//           "<td>" +
//           result[i]["show_price"] +
//           '</td> \
// 															<td><input type="number" name="qty" maxlength="12" value="1" title="Quantity:" class="input-text qty" id=' +
//           result[i]["idProducto"] +
//           '></td> \
// 															<td><button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart" \
// 																		data-productname = "' +
//           result[i]["prod_nombre"] +
//           '" \
// 																		data-price       = "' +
//           result[i]["precio"] +
//           '" \
// 																		data-sku       = "' +
//           result[i]["sku"] +
//           '" \
// 																		data-productid   = "' +
//           result[i]["idProducto"] +
//           '"> Agregar</button></td></tr>';
//       }
//       $(".child-products").empty();
//       $(".child-products").append(output);
//     },
//     error: function () {
//       output = "";
//       output = "<h3>No se encontraron productos</h3>";
//       $(".child-products").empty();
//       $(".child-products").append(output);
//     },
//   });
// }

function GetURLParameter(sParam) {
  var sPageURL = window.location.search.substring(1);
  var sURLVariables = sPageURL.split("&");

  for (var i = 0; i < sURLVariables.length; i++) {
    var sParameterName = sURLVariables[i].split("=");
    if (sParameterName[0] == sParam) {
      return sParameterName[1];
    }
  }
}
// Funcion para renderizar la vista en modo GRILLA
function render_home_default(output, data) {
  //Renderiza el tipo Producto SIMPLE
  if (data["idTipo"] == 0) {
    output +=
      '<div id="product-' + data["idProducto"] + '" class="col-lg-3 col-md-6">';
    output += '<div class="single-product">';
    output +=
      '<a href="' +
      window.base +
      "frontController/detalle/" +
      data["slug"] +
      '">';
    output +=
      '<div class="img-prod" style="background:url(' +
      data["imagen"] +
      ')"></div>';
    output += "</a>";
    output += '<div class="product-details">';
    output +=
      '<h6><a href="' +
      window.base +
      "frontController/detalle/" +
      data["slug"] +
      '">' +
      data["nombre"] +
      "</a></h6>";
    output += '<div class="price">';
    output += '<p class="p-price">$ ' + data["precio"] + "</p>";
    output += "</div>";
    output += '<div class="add-bag align-items-center">';
    output +=
      '<input type="number" name="quantity" class="form-control quantity" value="1" id="' +
      data["idProducto"] +
      '"/>';
    output +=
      '<button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart"';
    output += '  data-productname = "' + data["nombre"];
    output += '" data-price       = "' + data["precio"];
    output += '" data-productid   = "' + data["idProducto"];
    output += '" data-sku   = "' + data["sku"];
    output += '">AGREGAR</button>';
    output += "</div>";
    output += "</div>";
    output += "</div>";
    output += "</div>";

    //Renderiza el tipo Producto Variable
  } else {
    output +=
      '<div id="product-' + data["idProducto"] + '" class="col-lg-3 col-md-6">';
    output += '<div class="single-product">';
    output +=
      '<a href="' +
      window.base +
      "frontController/detalle/" +
      data["slug"] +
      '">';
    output +=
      '<div class="img-prod" style="background:url(' +
      data["imagen"] +
      ')"></div>';
    output += "</a>";
    output += '<div class="product-details">';
    output +=
      '<h6><a href="' +
      window.base +
      "frontController/detalle/" +
      data["slug"] +
      '">' +
      data["nombre"] +
      "</a></h6>";
    output +=
      '<div class="add-bag d-flex align-items-center no-quantity-visible">';
    output +=
      '<input type="number" name="quantity" class="form-control quantity" value="1" id="' +
      data["idProducto"] +
      '"/>';
    output +=
      '<button type="button" id="add_cart" name="add_cart" class="cust-btn primary-btn add_cart"';
    output += '  data-productname = "' + data["nombre"];
    output += '" data-price       = "' + data["precio"];
    output += '" data-productid   = "' + data["idProducto"];
    output += '" data-sku   = "' + data["sku"];
    output += '">AGREGAR</button>';
    output += "</div>";
    output += "</div>";
    output += "</div>";
    output += "</div>";
  }
  return output;
}

function unblock_home() {
  $("#wrapper").unblock();
}

function render_prod_dest(output, data) {
  output += '<div class="col-lg-3 col-md-4 col-sm-6 mb-20">';
  output +=
    '<div class="single-related-product d-flex featured-box" id="' +
    data["idProducto"] +
    '">';
  output +=
    '<a href="' +
    window.base +
    "frontController/detalle/" +
    data["slug"] +
    '">';
  output += '<img class="img-fluid" src=' + data["imagen"] + ' alt="">';
  output += "</a>";
  output += '<div class="desc">';
  output +=
    '<a href="' +
    window.base +
    "frontController/detalle/" +
    data["slug"] +
    '" class="title">' +
    data["modelo"];
  output += "</a>";
  output += '<div class="price">';
  output += "<h6>$" + data["precio"] + "</h6>";
  output += "</div>";
  output += "</div>";
  output += "</div>";
  output += "</div>";

  return output;
}

// DEPRECATED
// TODO: Eliminar luego de pasar por algunas iteraciones.
// En su lugar llamar al método "build_src" dentro del controller respectivo (Funcion del BaseController heredada al resto de controllers)
function build_img_src(data) {
  var url = window.base + "shop/build_src";

  $.ajax({
    async: false,
    url: url,
    method: "POST",
    data: { images: data["imagen"] },
    success: function (r) {
      result = JSON.parse(r);
      data["imagen"] = result.img_src;
    },
  });
}

function render_orders_list() {
  $("#orders_list").DataTable({
    bPaginate: false,
    bLengthChange: false,
    bFilter: false,
    pageLength: 50,
    info: false,
    ajax: {
      url: window.base + "customer/listOrders",
      type: "POST",
    },
    order: [[0, "desc"]],
    columnDefs: [{ className: "text-center", targets: "_all" }],
  });
}

function render_order_items(idOrderHdr) {
  $("#order_items").DataTable({
    searching: false,
    bPaginate: false,
    bLengthChange: false,
    info: false,
    destroy: true,
    ajax: {
      url: window.base + "Customer/listItems",
      type: "POST",
      data: { idOrderHdr: idOrderHdr },
    },
  });

  $("#nro-pedido").html(" Nº " + idOrderHdr);

  // $('#btn-repeat-order').attr('data-order_id', idOrderHdr);

  $(".pedido-detail").css("display", "block");
}

function render_order_details(idOrderHdr) {
  $.ajax({
    url: window.base + "Customer/getOrder",
    method: "POST",
    data: { idOrderHdr: idOrderHdr },
    success: function (data) {
      data = JSON.parse(data);
      //Se completan los detalles del pedido
      $("#detail-created span").html(data[0]);
      $("#detail-nota    span").html(data[1]);
      $("#detail-total   span").html(data[2]);
      $("#detail-estado  span").html(data[3]);
    },
  });
}

// function render_productos_home(){

// 	var url = window.base + 'home/getProductosHome';

// 	$.ajax({
// 		url     : url,
// 		method  : 'POST',
// 		success : function(data){
// 				data = JSON.parse(data);
// 				var output = "";
// 				for( var i = 0; i < data.length ; i++ ) {
// 					 output = render_home_default(output, data[i]);
// 				}
// 				$('#productos-home').html(output);
// 		}
// 	})
// }

function render_destacados() {
  var url = window.base + "frontController/getProdDestacados";

  $.ajax({
    url: url,
    method: "POST",
    success: function (data) {
      data = JSON.parse(data);
      var output = "";
      for (var i = 0; i < data.length; i++) {
        output = render_prod_dest(output, data[i]);
      }
      $("#prod-destacados").html(output);
    },
  });
}

async function render_notas() {
  // var url = 'https://monsa-srl.com.ar/wp-json/wp/v2/posts?_embed';
  // const r = await fetch(url,{method:'GET'} )
  // const response = await r.json()
  // 	var notas = ""
  // 	response.slice(0,3).forEach( (nota) => {
  // 		var img = "/resources/img/default.jpeg"
  // 		if(nota.featured_image_url){
  // 			img = nota.featured_image_url
  // 		}
  // 		var date = new Date(nota.date)
  // 		var dateFormated = date.getDate() + ' ' +  months[date.getMonth()]
  // 		notas += `<div class="col-4 home-notas-item">
  // 					<div class="nota-img"><span class="nota-date">${dateFormated}</span><div class="nota-image" style="background-image:url('${img}')"></div> </div>
  // 					<div class="nota-title">${nota.title.rendered}</div>
  // 					<div class="nota-extract">${nota.excerpt.rendered.slice(0,160)}... <a href="${nota.link}" target="_blank">Leer más</a></div>
  // 				</div>`
  // 	})
  // 	console.log(notas)
  // 	$('.home-notas-wrapper').empty().append(notas)
}

$(document).on("click", "[remove-filter]", function () {
  var name = $(this).attr("data-name"),
    val = $(this).attr("data-id");

  if (name == "search") {
    $(".search-label").remove();
    filter_data(1);
  } else {
    $(
      '.common_selector[name="' + name + '"][value="' + val + '"]:checked'
    ).click();
    $(this).parent().fadeOut();

    console.log(
      $('.common_selector[name="' + name + '"][value="' + val + '"]')
    );
  }
});

function appendWhatsappButton() {
  $.ajax({
    url: window.base + "frontController/get_whatsapp_config",
    method: "POST",
    success: function (data) {
      var response = $.parseJSON(data);

      if (response.code == 200) {
        response.data.forEach((row) => {
          switch (row.opt_name) {
            case "wsp_number":
              wsp_number = row.opt_value;
              break;
            case "wsp_message":
              wsp_message = row.opt_value;
              break;
          }
        });

        if (wsp_number.length > 0) {
          var boton = `<a href="https://api.whatsapp.com/send?phone=${wsp_number}&text=${wsp_message}" class="wsp_float" target="_blank">
										<i class="fab fa-whatsapp wsp_my-float"></i></a>`;

          $("body").append(boton);
        }
      }
    },
  });
}
