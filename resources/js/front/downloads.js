function getHomeFiles(){

    var post = base + 'manager/Downloads/get_home_file'

    fetch(post, {
        method: 'POST'
    })
        .then(data=>data.json())
        .then(data=>{
            if(data.status===200){

                // const xls = data.files.find( ({ type }) => type === 'XLS' )
                // $('#link-xls').attr('href', xls.publicfullpath)
                // var xls_date = moment(xls.updated_at).format('DD/MM/YYYY')
                // $('#link-xls h6').html(`Actualizado: ${xls_date}`)

                const pdf = data.files.find( ({ type }) => type === 'PDF' )
                $('#link-pdf').attr('href', pdf.publicfullpath)
                var pdf_date = moment(pdf.updated_at).format('DD/MM/YYYY')
                $('#link-pdf h6').html(`Actualizado: ${pdf_date}`)

                const csv = data.files.find( ({ type }) => type === 'CSV' )
                $('#link-csv').attr('href', csv.publicfullpath)                
                var csv_date = moment(csv.updated_at).format('DD/MM/YYYY')
                $('#link-csv h6').html(`Actualizado: ${csv_date}`)

                if(data.msj.opt_value.length > 0){
                    $('.msj').empty().html(data.msj.opt_value)
                }else{
                    $('.container-msj').addClass('d-none')
                }
            }else{
                /*Armamos el mensaje toast*/
                dangerToast(data.title, data.message) 

            }
        })

}