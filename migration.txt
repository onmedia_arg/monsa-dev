21/11
ALTER TABLE `monsawebapp`.`producto` 
ADD COLUMN `alto` INT(11) NULL AFTER `created`,
ADD COLUMN `ancho` INT(11) NULL AFTER `alto`,
ADD COLUMN `largo` INT(11) NULL AFTER `ancho`;

09/11

ALTER TABLE `monsawebapp`.`bulk_update_logs` 
ADD COLUMN `success` TEXT NULL AFTER `updated`;



07/10/2021

ALTER TABLE `monsawebapp`.`producto` 
ADD COLUMN `stock_type` VARCHAR(45) NULL AFTER `stock`,
ADD COLUMN `stock_fijo` VARCHAR(45) NULL AFTER `stock_type`,
ADD COLUMN `stock_quantity` INT(11) NULL AFTER `stock_fijo`,
ADD COLUMN `stock_limit1` INT(11) NULL AFTER `stock_quant`,
ADD COLUMN `stock_limit2` INT(11) NULL AFTER `stock_limit1`;

CREATE TABLE `popup` (
  `id_popup` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `date_from` datetime DEFAULT NULL,
  `date_to` datetime DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id_popup`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


ALTER TABLE `monsawebapp`.`producto` 
CHANGE COLUMN `destacado` `show` VARCHAR(50) NOT NULL DEFAULT 0 ;



08/06/2021
INSERT INTO `monsawebapp`.`system_options` (`opt_name`, `opt_value`) VALUES ('shop_msj_title', 'Novedades:');
INSERT INTO `monsawebapp`.`system_options` (`opt_name`, `opt_value`) VALUES ('shop_msj_detail', 'Los articulos PIRELLI se envian solo al centro y sur del pais.');


27/04/2021

UPDATE `monsawebapp`.`familia` SET `import_file` = 'uploads/templateUpload/templateBaterias.xlsx' WHERE (`idFamilia` = '1');
UPDATE `monsawebapp`.`familia` SET `import_file` = 'uploads/templateUpload/templateCascos.xlsx' WHERE (`idFamilia` = '2');
UPDATE `monsawebapp`.`familia` SET `import_file` = 'uploads/templateUpload/templateCamaras.xlsx' WHERE (`idFamilia` = '3');
UPDATE `monsawebapp`.`familia` SET `import_file` = 'uploads/templateUpload/templateCadenas.xlsx' WHERE (`idFamilia` = '4');
UPDATE `monsawebapp`.`familia` SET `import_file` = 'uploads/templateUpload/templateKitTransmision.xlsx' WHERE (`idFamilia` = '5');
UPDATE `monsawebapp`.`familia` SET `import_file` = 'uploads/templateUpload/templateGuantes.xlsx' WHERE (`idFamilia` = '12');

*-------------------------------------------------


18/04/2021
ALTER TABLE `monsawebapp`.`familia` 
ADD COLUMN `import_file` VARCHAR(300) NULL AFTER `orden`;

*-------------------------------------------------


ALTER TABLE `monsawebapp`.`producto_atributo` 
DROP PRIMARY KEY,
ADD PRIMARY KEY (`idProdAtrib`, `idProducto`, `idAtributo`);
;
